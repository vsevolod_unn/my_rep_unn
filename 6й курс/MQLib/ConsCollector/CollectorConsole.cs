﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MQLib;

namespace ConsCollector
{
    class CollectorConsole
    {
        static void Main(string[] args)
        {
            CCollector collector = new CCollector(true, "5052", "192.168.9.49");
            collector.report = false;

            string data = "";
            while (data!="END")
            {
                data = collector.ReceiveFrame();
            }

            Console.ReadKey();
        }
    }
}
