﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MQLib;

namespace ConsoleDistributor
{
    class DistributorConsole
    {
        static void Main(string[] args)
        {
            //CDistributor dis1, dis2;
            var dis1 = new CDistributorToWorker(true, "5050", "192.168.9.62");
            //dis1.ConfigureToWorkers();
            //dis2 = new CDistributor("5051");
            var dis2 = new CDistributorToCollector(true, "5051", "192.168.9.53");
            //var dis2 = new CDistributorToCollector();
            //dis2.ConfigureToCollector();
            
            //Console.ReadKey();

            Thread t1 = new Thread(new ParameterizedThreadStart(ToWorkers));
            t1.Start(dis1);

            Thread t2 = new Thread(new ParameterizedThreadStart(ToCollector));
            //t2.Start(dis2);
        }

        public static void ToWorkers(object obj)
        {
            CDistributorToWorker dis1 = (CDistributorToWorker)obj;
            string data;
            for (int i = 0; i < 40; i++)
            {
                data = $"Picture #{i} for worker";
                dis1.SendFrame(data);
                Thread.Sleep(30);
            }
            dis1.SendFrame("END");

            //dis1.ShutDownPullers(8);
        }

        public static void ToCollector(object obj)
        {
            CDistributorToCollector dis2 = (CDistributorToCollector)obj;
            string data;
            for (int i = 0; i < 40; i++)
            {
                data = $"Picture #{i} for collector";
                dis2.SendFrame(data);
                Thread.Sleep(30);
            }
            dis2.SendFrame("END");

            //dis2.ShutDownPullers();
        }
    }
}
