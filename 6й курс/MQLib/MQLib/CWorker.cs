﻿using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MQLib
{
    public class CWorker
    {
        string pullAddr, pushAddr;
        string pullPort, pushPort;

        string id1, id2;
        string cnn1, cnn2;

        public bool report;

        RequestSocket collectFromDistributor;
        RequestSocket pushToCollector;

        public CWorker(bool _report = true, string _pullPort = "5050", string _pushPort = "5051", string _pullAddr = "localhost", string _pushAddr = "localhost")
        {
            pullAddr = _pullAddr;
            pushAddr = _pushAddr;

            pullPort = _pullPort;
            pushPort = _pushPort;

            collectFromDistributor = new RequestSocket();
            pushToCollector = new RequestSocket();

            id1 = ZHelpers.SetID(collectFromDistributor, Encoding.Unicode);
            id2 = ZHelpers.SetID(pushToCollector, Encoding.Unicode);

            cnn1 = "tcp://" + pullAddr + ":" + pullPort;
            cnn2 = "tcp://" + pushAddr + ":" + pushPort;

            collectFromDistributor.Connect(cnn1);
            pushToCollector.Connect(cnn2);
            Console.WriteLine("[W] ID {0} connect to {1} for receiving", id1, cnn1);
            Console.WriteLine("[W] ID {0} connect to {1} for pushing", id2, cnn2);

            report = _report;
        }

        public string ReceiveFrame()
        {
            // Tell the router we're ready for work
            collectFromDistributor.SendFrame("Ready");

            // Get workload from router, until finished
            string workload = collectFromDistributor.ReceiveFrameString();
            if (report)
                Console.WriteLine("[W] ID [{0}] Workload received: {1}", id1, workload);

            if (workload == "END")
            {
                if (report)
                    Console.WriteLine("[W] " + id1 + " has been shutted down");
                collectFromDistributor.Disconnect(cnn1);
                return workload;
            }
            else
            {
                return workload;
            }
            return "";
        }

        public byte[] ReceiveByteFrame()
        {
            // Tell the router we're ready for work
            collectFromDistributor.SendFrame("Ready");

            // Get workload from router, until finished
            byte[] workload = collectFromDistributor.ReceiveFrameBytes();
            if (report)
                Console.WriteLine("[W] ID [{0}] Workload received: {1}", id1, workload);

            if (Encoding.ASCII.GetString(workload) == "END" ||
                Encoding.UTF8.GetString(workload) == "END" ||
                Encoding.Unicode.GetString(workload) == "END")
            {
                if (report)
                    Console.WriteLine("[W] " + id1 + " has been shutted down");
                collectFromDistributor.Disconnect(cnn1);
                return workload;
            }
            else
            {
                return workload;
            }
        }

        public void SendFrame(string data = "Hello")
        {
            if (data == null || data == "")
            {
                data = "Hello from worker " + id1;
            }
            string answer = "";
            int time0 = DateTime.Now.Millisecond % (int)(3.6e6), time1 = time0;
            while (answer != "OK")
            {
                pushToCollector.SendFrame(data);
                answer = pushToCollector.ReceiveFrameString();
                time1 = DateTime.Now.Millisecond % (int)(3.6e6);

                if ((time1 - time0) > 1000) break;
            }
        }

        public void SendFrame(byte[] data )
        {
            if (data == null)
                data = new byte[] { 72, 101, 108, 108, 111 };
            string answer = "";
            int time0 = DateTime.Now.Millisecond % (int)(3.6e6), time1 = time0;
            while (answer != "OK")
            {
                answer = pushToCollector.ReceiveFrameString();
                time1 = DateTime.Now.Millisecond % (int)(3.6e6);

                if ((time1 - time0) > 1000) break;
            }
        }


        /// <summary>
        /// Старые функции, не использовать
        /// </summary>

        private void StartReceiving()
        {
            new Thread(new ThreadStart(ReceiveOld)).Start();
        }

        private void ReceiveOld()
        {
            int received_pockets = 0;
            bool end = false;

            while (!end)
            {
                // Tell the router we're ready for work
                collectFromDistributor.SendFrame("Ready");

                // Get workload from router, until finished
                string workload = collectFromDistributor.ReceiveFrameString();
                if (report)
                    Console.WriteLine("[W] ID [{0}] Workload received: {1}", id1, workload);

                if (workload == "END")
                {
                    if (report)
                        Console.WriteLine("[W] " + id1 + " has been shutted down");
                    end = true;
                }
                else
                {
                    received_pockets++;

                    Thread.Sleep(new Random(DateTime.Now.Millisecond).Next(100, 300)); //  Simulate 'work'
                    workload += " (processed)";

                    SendFrame(workload);
                }
            }

            if (report)
                Console.WriteLine("ID ({0}) processed: {1} tasks", Encoding.Unicode.GetString(collectFromDistributor.Options.Identity), received_pockets);
        }

    }

    public static class ZHelpers
    {
        public static string SetID(NetMQSocket client, Encoding unicode)
        {
            var str = Guid.NewGuid().ToString(); // client.GetHashCode().ToString();
            client.Options.Identity = unicode.GetBytes(str);
            return str;
        }
    }
}
