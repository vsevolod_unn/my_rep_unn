﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MQLib;

namespace ConsWorker
{
    class WorkerConsole
    {
        static void Main(string[] args)
        {
            CWorker worker = new CWorker(true, "5050", "5052", "192.168.9.49", "192.168.9.53");
            //CWorker worker = new CWorker();

            string data = "";
            while (data != "END")
            {
                data = worker.ReceiveFrame();
                Thread.Sleep(new Random(DateTime.Now.Millisecond).Next(100, 300)); //  Simulate 'work'
                if (data != "END")
                    data += " (processed)";
                worker.SendFrame(data);
            }   

            Console.ReadKey();
        }
    }
}
