﻿using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQLib
{
    public class CDistributor
    {
        public RouterSocket senderToWorkers;
        public RequestSocket senderToCollector;

        public string cnn, id;

        public bool report;

        public CDistributor()
        {

        }
    }

    public class CDistributorToWorker : CDistributor
    {
        public CDistributorToWorker(bool _report = true, string _pushPort1 = "5050", string _pushAddr1 = "localhost")
        {
            senderToWorkers = new RouterSocket();
            cnn = "tcp://" + _pushAddr1 + ":" + _pushPort1;
            senderToWorkers.Bind(cnn);
            Console.WriteLine("[D] Connected to {0}", cnn);

            report = _report;
        }

        public void SendFrame(string data = "Hello")
        {
            // LRU worker is next waiting in queue
            string address = senderToWorkers.ReceiveFrameString();
            //Console.WriteLine("[B] Message received: {0}", address);
            string empty = senderToWorkers.ReceiveFrameString();
            //Console.WriteLine("[B] Message received: {0}", empty);
            string ready = senderToWorkers.ReceiveFrameString();
            if (report)
                Console.WriteLine("[D] Message received: {0}", ready);

            senderToWorkers.SendMoreFrame(address);
            if (report)
                Console.WriteLine("[D] Message sent: {0}", address);
            senderToWorkers.SendMoreFrame("");
            senderToWorkers.SendFrame(data);
            if (report)
                Console.WriteLine("[D] Message sent: {0}", data);
        }

        public void SendFrame(byte[] data)
        {
            if (data == null)
                data = new byte[] { 72, 101, 108, 108, 111 };
            // LRU worker is next waiting in queue
            string address = senderToWorkers.ReceiveFrameString();
            //Console.WriteLine("[B] Message received: {0}", address);
            string empty = senderToWorkers.ReceiveFrameString();
            //Console.WriteLine("[B] Message received: {0}", empty);
            string ready = senderToWorkers.ReceiveFrameString();
            if (report)
                Console.WriteLine("[D] Message received: {0}", ready);

            senderToWorkers.SendMoreFrame(address);
            if (report)
                Console.WriteLine("[D] Message sent: {0}", address);
            senderToWorkers.SendMoreFrame("");
            senderToWorkers.SendFrame(data);
            if (report)
                Console.WriteLine("[D] Message sent: {0}", data);
        }

        public void ShutDownPullers(int _pullerCount = 1)
        {
            for (int i = 0; i < _pullerCount; i++)
            {
                string address = senderToWorkers.ReceiveFrameString();
                string empty = senderToWorkers.ReceiveFrameString();
                string ready = senderToWorkers.ReceiveFrameString();

                senderToWorkers.SendMoreFrame(address);
                senderToWorkers.SendMoreFrame("");
                senderToWorkers.SendFrame("END");
            }

            senderToWorkers.Unbind(cnn);
        }
    }

    public class CDistributorToCollector : CDistributor
    {
        public CDistributorToCollector(bool _report = true, string _pushPort1 = "5051", string _pushAddr1 = "localhost")
        {
            senderToCollector = new RequestSocket();
            id = ZHelpers.SetID(senderToCollector, Encoding.Unicode);
            cnn = "tcp://" + _pushAddr1 + ":" + _pushPort1;
            senderToCollector.Connect(cnn);
            Console.WriteLine("[D] Connected to {0}", cnn);

            report = _report;
        }

        public void SendFrame(string data = "Hello")
        {
            if (data == null || data == "")
            {
                data = "Hello from distributor " + id;
            }
            string answer = "";
            int time0 = DateTime.Now.Millisecond % (int)(3.6e6), time1 = time0;
            while (answer != "OK")
            {
                senderToCollector.SendFrame(data);
                if (report)
                    Console.WriteLine("[D] Sending to collector " + data);
                answer = senderToCollector.ReceiveFrameString();
                time1 = DateTime.Now.Millisecond % (int)(3.6e6);

                if ((time1 - time0) > 1000) break;
            }
        }

        public void ShutDownPullers(int _pullerCount = 1)
        {
            for (int i = 0; i < _pullerCount; i++)
            {
                string address = senderToWorkers.ReceiveFrameString();
                string empty = senderToWorkers.ReceiveFrameString();
                string ready = senderToWorkers.ReceiveFrameString();

                senderToWorkers.SendMoreFrame(address);
                senderToWorkers.SendMoreFrame("");
                senderToWorkers.SendFrame("END");
            }

            senderToWorkers.Unbind(cnn);
        }
    }
}
