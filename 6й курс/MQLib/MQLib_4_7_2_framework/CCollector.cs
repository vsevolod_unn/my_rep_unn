﻿using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MQLib
{
    public class CCollector
    {
        string pullAddr1, pullAddr2;
        string pullPort1, pullPort2;

        string cnn1, cnn2;
        string id1, id2;

        public bool report;
        public int received_from_distributor, received_from_workers;

        RouterSocket collectFromWorkers;
        RouterSocket collectData;

        //public List<string> dataFromDis, dataFromWorkers;

        public CCollector(bool _report = true, string _pullPort1 = "5051", string _pullPort2 = "5052", string _pullAddr1 = "localhost", string _pullAddr2 = "localhost")
        {
            pullAddr1 = _pullAddr1;
            pullAddr2 = _pullAddr2;

            pullPort1 = _pullPort1;
            pullPort2 = _pullPort2;

            collectData = new RouterSocket();
            collectFromWorkers = new RouterSocket();

            id1 = ZHelpers.SetID(collectData, Encoding.Unicode);
            cnn1 = "tcp://" + pullAddr1 + ":" + pullPort1;
            collectData.Bind(cnn1);
            Console.WriteLine("[C] ID {0} connect to {1} - collecting from distributor", id1, cnn1);

            id2 = ZHelpers.SetID(collectFromWorkers, Encoding.Unicode);
            cnn2 = "tcp://" + pullAddr2 + ":" + pullPort2;
            collectFromWorkers.Bind(cnn2);
            Console.WriteLine("[C] ID {0} connect to {1} - collecting from workes", id2, cnn2);

            report = _report;

            //dataFromDis = new List<string>();
            //dataFromWorkers = new List<string>();

            received_from_distributor = 0;
            received_from_workers = 0;

            /*
            pullAddr1 = _pullAddr1;
            pullAddr2 = _pullAddr2;

            pullPort1 = _pullPort1;
            pullPort2 = _pullPort2;

            collectFromDistributor = new RequestSocket();
            collectFromWorkers = new RouterSocket();

            id1 = ZHelpers.SetID(collectFromDistributor, Encoding.Unicode);
            cnn1 = $"tcp://{pullAddr1}:{pullPort1}";
            collectFromDistributor.Connect(cnn1);
            Console.WriteLine("[C] ID {0} connect to {1} - collecting from distributor", id1, cnn1);

            id2 = ZHelpers.SetID(collectFromWorkers, Encoding.Unicode);
            cnn2 = $"tcp://{pullAddr2}:{pullPort2}";
            collectFromWorkers.Bind(cnn2);
            Console.WriteLine("[C] ID {0} connect to {1} - collecting from workes", id2, cnn2);

            report = _report;
            bufferSize = _bufferSize;

            dataFromDis = new List<string>();
            dataFromWorkers = new List<string>();

            received_from_distributor = 0;
            received_from_workers = 0;
            */
        }

        public string ReceiveFrame()
        {
            bool end = false;

            while (!end)
            {
                string address = collectData.ReceiveFrameString();
                string empty = collectData.ReceiveFrameString();
                string workload = collectData.ReceiveFrameString();

                collectData.SendMoreFrame(address);
                if (report)
                    Console.WriteLine("[D] Message sent: {0}", address);
                collectData.SendMoreFrame("");
                collectData.SendFrame("OK");
                if (report)
                    Console.WriteLine("[D] Message sent: {0}", "OK");

                Console.WriteLine("[C] ID [{0}] Workload received: {1}", id1, workload);

                if (workload == "END")
                {
                    end = true;
                }
                else
                {
                    received_from_workers++;
                    return workload;
                    /*
                    if (dataFromWorkers.Count == bufferSize)
                    {
                        dataFromWorkers.RemoveAt(0);
                    }
                    dataFromWorkers.Add(workload);
                    */
                }
            }
            return "";
        }



        /// <summary>
        /// Старые функции, не использовать
        /// </summary>

        /*
    private void StartReceiving()
    {
        new Thread(new ThreadStart(ReceiveFromDistributor)).Start();
        new Thread(new ThreadStart(ReceiveFromWorkers)).Start();
    }

    private void ReceiveFromDistributor()
    {
        bool end = false;

        while (!end)
        {
            // Tell the router we're ready for work
            collectFromDistributor.SendFrame("Ready");

            // Get workload from router, until finished
            string workload = collectFromDistributor.ReceiveFrameString();
            //if (report)
            Console.WriteLine("[C] ID [{0}] Workload received: {1}", id1, workload);

            if (workload == "END")
            {
                end = true;
            }
            else
            {
                received_from_distributor++;
                Thread.Sleep(new Random(DateTime.Now.Millisecond).Next(25, 45)); //  Simulate 'work'
            }
        }

        if (report)
            Console.WriteLine("ID ({0}) processed: {1} tasks", Encoding.Unicode.GetString(collectFromDistributor.Options.Identity), received_from_distributor);
    }

    private void ReceiveFromWorkers()
    {
        bool end = false;

        while (!end)
        {
            string address = collectFromWorkers.ReceiveFrameString();
            string empty = collectFromWorkers.ReceiveFrameString();
            string workload = collectFromWorkers.ReceiveFrameString();

            collectFromWorkers.SendMoreFrame(address);
            if (report)
                Console.WriteLine("[D] Message sent: {0}", address);
            collectFromWorkers.SendMoreFrame("");
            if (report)
                Console.WriteLine("[B] Message sent: {0}", "");
            collectFromWorkers.SendFrame("OK");
            if (report)
                Console.WriteLine("[D] Message sent: {0}", "OK");

            Console.WriteLine("[C] ID [{0}] Workload received: {1}", id1, workload);

            if (workload == "END")
            {
                end = true;
            }
            else
            {
                received_from_workers++;
            }
        }

        if (report)
            Console.WriteLine("[C] ID ({0}) processed: {1} tasks", Encoding.Unicode.GetString(collectFromWorkers.Options.Identity), received_from_workers);
    }
    */
    }
}
