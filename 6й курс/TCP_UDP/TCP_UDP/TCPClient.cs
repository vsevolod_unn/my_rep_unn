﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace TCP_UDP
{
    class TCPClient
    {
        static void Main(string[] args)
        {
            string ip = "", str_port = "", address = "";
            int port = 0;
            char resp = 'n';

            Console.WriteLine("КЛИЕНТ");

            while (true)
            {
                if (resp == 'n')
                {
                    /// Выбираем сервер
                    Console.WriteLine("Введите ip сервера, к которому желаете подключиться в формате xxx.xxx.xxx.xxx");
                    ip = Console.ReadLine();
                    if (ip == "")
                    {
                        ip = Dns.GetHostAddresses(Dns.GetHostName())[1].ToString();
                        Console.WriteLine("Подключение будет осуществляться к серверу, расположенному на этом pc.\nЕго ip: " + ip);
                    }

                    /// Выбираем порт
                    Console.WriteLine("Введите порт (нажмите return для значения по умолчанию: 8080)");
                    str_port = Console.ReadLine();
                    if (str_port == "")
                        port = 8080;
                    else
                        port = Convert.ToInt32(str_port.ToString());
                    Console.WriteLine("Выбранный порт: " + port.ToString());
                }

                address = ip + ":" + port.ToString();

                Console.WriteLine("Ожидайте подключения...");
                Console.WriteLine();

                /// Сеанс связи
                try
                {
                    /// Создаемся как клиент
                    NetworkStream stream;
                    char key;
                    do
                    {
                        /// Подключаемся к выбранному серверу
                        TcpClient client = new TcpClient();
                        client.Connect(ip, port);
                        Console.WriteLine("Подключение к серверу " + address + " установлено.");

                        /// Переменная ответственная за чтение/запись между клиентом и сервером
                        stream = client.GetStream();

                        /// Создаем переменные для передачи данных 
                        string question = "5 + 2";
                        byte[] out_data = Encoding.UTF8.GetBytes(question);
                        /// Передаем серверу данные
                        stream.Write(out_data, 0, out_data.Length);

                        /// Создаем переменные для чтения 
                        byte[] in_data = new byte[256];
                        StringBuilder server_response = new StringBuilder();
                        /// Получаем ответ от сервера
                        do
                        {
                            int bytes = stream.Read(in_data, 0, in_data.Length);
                            server_response.Append(Encoding.UTF8.GetString(in_data, 0, bytes));
                        }
                        while (stream.DataAvailable); /// пока данные есть в потоке

                        /// Выводим ответ от сервера
                        Console.WriteLine(server_response.ToString());
                        client.Close();

                        Console.WriteLine("Введите 'c' для повторного сеанса связи");
                        key = Console.ReadKey().KeyChar;
                        Console.WriteLine();
                    } while (key == 'c');

                    /// Закрываем потоки
                    stream.Close();
                    //client.Close();
                }
                catch (SocketException e)
                {
                    Console.WriteLine("Подключение к серверу " + address + " не установлено.");
                    Console.WriteLine("Ошибка: " + e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Нет возможности подключиться к указанному ({0}) серверу...", address);
                    Console.WriteLine(e.Message);
                }

                /// Повторяем операции
                Console.WriteLine("Сеанс связи был завершен по инициативе клиента.");
                Console.WriteLine("Введите: \n'r' для повторного сеанса с сохранением параметров подключения, " +
                                           "\n'n' для повторного сеанса с изменением параметров подключения, " +
                                           "\nлюбую другую кнопку для завершения работы");
                /// Продалжать работу или нет?
                resp = Console.ReadKey().KeyChar;
                if (resp == 'r' || resp == 'n')
                {
                    Console.WriteLine("\n");
                }
                else
                {
                    Console.WriteLine();
                    break;
                }
            }
        }
    }
}
