﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            string ip, str_port;
            int port;

            /// Инициализируем адрес сервера
            Console.WriteLine("СЕРВЕР");
            ip = Dns.GetHostAddresses(Dns.GetHostName())[1].ToString();
            Console.WriteLine("IP данного pc: " + ip);

            /// Инициализируем порт сервера
            Console.WriteLine("Введите порт (нажмите return для значения по умолчанию: 8080)");
            str_port = Console.ReadLine();
            if (str_port == "")
                port = 8080;
            else
                port = Convert.ToInt32(str_port.ToString());

            /// Сеанс связи
            TcpListener server = null;
            try
            {
                /// Инициализируем сервер
                IPAddress localAddr = IPAddress.Parse(ip);
                server = new TcpListener(localAddr, port);
                /// Запускаем сервер
                server.Start();
                /// Ожидаем клиентов до тех пор, пока не надоест
                while (true)
                {
                    Console.WriteLine("Ожидание подключений клиентов...");

                    /// Получаем входящее подключение
                    TcpClient client = server.AcceptTcpClient();
                    string clientIP = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();
                    Console.WriteLine("Подключен клиент " + clientIP + ". Выполнение запроса...");

                    /// Получаем сетевой поток для чтения и записи
                    NetworkStream stream = client.GetStream();

                    /// Создаем переменные для чтения запроса от клиента
                    byte[] in_data = new byte[256];
                    StringBuilder question = new StringBuilder();
                    /// Получаем запрос от клиента
                    do
                    {
                        int bytes = stream.Read(in_data, 0, in_data.Length);
                        question.Append(Encoding.UTF8.GetString(in_data, 0, bytes));
                    }
                    while (stream.DataAvailable); /// пока данные есть в потоке
                                                  /// Распаковываем "пакет"
                    int a = (int)(char)(question[0]) - 48;
                    int b = (int)(char)(question[4]) - 48;

                    /// Сообщение для отправки клиенту
                    string
                        server_response = "Привет пользователю " + clientIP + " от сервера " + ip + ":" + port.ToString() +
                        "\nОтвет на ваш вопрос: " + a + " + " + b + " = " + (a + b);
                    /// Преобразуем сообщение в массив байтов
                    byte[] data = Encoding.UTF8.GetBytes(server_response);

                    /// Отправка сообщения
                    stream.Write(data, 0, data.Length);
                    Console.WriteLine("Отправлено сообщение: {0}", server_response);

                    /// Закрываем поток
                    stream.Close();
                    /// Закрываем подключение
                    client.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (server != null)
                    server.Stop();
            }

            Console.ReadKey();
        }
    }
}
