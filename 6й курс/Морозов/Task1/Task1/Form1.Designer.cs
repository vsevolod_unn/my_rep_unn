﻿namespace Task1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SignalPicture = new System.Windows.Forms.PictureBox();
            this.Rxx_graph = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AM_rb = new System.Windows.Forms.RadioButton();
            this.FT_rb = new System.Windows.Forms.RadioButton();
            this.PM2_rb = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.TextBox88 = new System.Windows.Forms.TextBox();
            this.devAmp_param = new System.Windows.Forms.TextBox();
            this.TexBox = new System.Windows.Forms.TextBox();
            this.Amp_param = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.TotalTime_param = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.ModFreq_param = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.MainFreq_param = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.DFreq_param = new System.Windows.Forms.TextBox();
            this.OneExpBT = new System.Windows.Forms.Button();
            this.ExpsSeries_button = new System.Windows.Forms.Button();
            this.ExpCount_param = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SNRmin_param = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RandomData_check = new System.Windows.Forms.CheckBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.TextBox44 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.TextData_param = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.SNRmax_param = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.ExpTau_param = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.Tau_param = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.StepsSNR_param = new System.Windows.Forms.TextBox();
            this.Statistics_graph = new System.Windows.Forms.PictureBox();
            this.ProgressGraph = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.SignalPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rxx_graph)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Statistics_graph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressGraph)).BeginInit();
            this.SuspendLayout();
            // 
            // SignalPicture
            // 
            this.SignalPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SignalPicture.Location = new System.Drawing.Point(12, 12);
            this.SignalPicture.Name = "SignalPicture";
            this.SignalPicture.Size = new System.Drawing.Size(1180, 284);
            this.SignalPicture.TabIndex = 0;
            this.SignalPicture.TabStop = false;
            this.SignalPicture.Click += new System.EventHandler(this.SignalPicture_Click);
            // 
            // Rxx_graph
            // 
            this.Rxx_graph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Rxx_graph.Location = new System.Drawing.Point(12, 306);
            this.Rxx_graph.Name = "Rxx_graph";
            this.Rxx_graph.Size = new System.Drawing.Size(586, 284);
            this.Rxx_graph.TabIndex = 1;
            this.Rxx_graph.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AM_rb);
            this.groupBox1.Controls.Add(this.FT_rb);
            this.groupBox1.Controls.Add(this.PM2_rb);
            this.groupBox1.Location = new System.Drawing.Point(1199, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 97);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Виды модуляции:";
            // 
            // AM_rb
            // 
            this.AM_rb.AutoSize = true;
            this.AM_rb.Location = new System.Drawing.Point(6, 74);
            this.AM_rb.Name = "AM_rb";
            this.AM_rb.Size = new System.Drawing.Size(99, 17);
            this.AM_rb.TabIndex = 2;
            this.AM_rb.Text = "модуляция АМ";
            this.AM_rb.UseVisualStyleBackColor = true;
            this.AM_rb.CheckedChanged += new System.EventHandler(this.AM_rb_CheckedChanged);
            // 
            // FT_rb
            // 
            this.FT_rb.AutoSize = true;
            this.FT_rb.Location = new System.Drawing.Point(6, 51);
            this.FT_rb.Name = "FT_rb";
            this.FT_rb.Size = new System.Drawing.Size(98, 17);
            this.FT_rb.TabIndex = 1;
            this.FT_rb.Text = "модуляция ЧТ";
            this.FT_rb.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.FT_rb.UseVisualStyleBackColor = true;
            this.FT_rb.CheckedChanged += new System.EventHandler(this.FT_rb_CheckedChanged);
            // 
            // PM2_rb
            // 
            this.PM2_rb.AutoSize = true;
            this.PM2_rb.Location = new System.Drawing.Point(6, 28);
            this.PM2_rb.Name = "PM2_rb";
            this.PM2_rb.Size = new System.Drawing.Size(112, 17);
            this.PM2_rb.TabIndex = 0;
            this.PM2_rb.Text = "модуляция ФМ-2";
            this.PM2_rb.UseVisualStyleBackColor = true;
            this.PM2_rb.CheckedChanged += new System.EventHandler(this.PM2_rb_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Controls.Add(this.textBox10);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.TextBox88);
            this.groupBox2.Controls.Add(this.devAmp_param);
            this.groupBox2.Controls.Add(this.TexBox);
            this.groupBox2.Controls.Add(this.Amp_param);
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Controls.Add(this.TotalTime_param);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.ModFreq_param);
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.MainFreq_param);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.DFreq_param);
            this.groupBox2.Location = new System.Drawing.Point(1199, 115);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(175, 181);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры модуляции:";
            // 
            // textBox12
            // 
            this.textBox12.Enabled = false;
            this.textBox12.Location = new System.Drawing.Point(146, 148);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(23, 20);
            this.textBox12.TabIndex = 33;
            this.textBox12.Text = "Гц";
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(146, 122);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(23, 20);
            this.textBox10.TabIndex = 32;
            this.textBox10.Text = "Гц";
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(146, 96);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(23, 20);
            this.textBox8.TabIndex = 31;
            this.textBox8.Text = "Гц";
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(146, 70);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(23, 20);
            this.textBox6.TabIndex = 30;
            this.textBox6.Text = "с";
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(146, 45);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(23, 20);
            this.textBox4.TabIndex = 29;
            this.textBox4.Text = "В";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(146, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(23, 20);
            this.textBox2.TabIndex = 28;
            this.textBox2.Text = "В";
            // 
            // TextBox88
            // 
            this.TextBox88.Enabled = false;
            this.TextBox88.Location = new System.Drawing.Point(6, 45);
            this.TextBox88.Name = "TextBox88";
            this.TextBox88.ReadOnly = true;
            this.TextBox88.Size = new System.Drawing.Size(64, 20);
            this.TextBox88.TabIndex = 26;
            this.TextBox88.Text = "Амп. дев.:";
            // 
            // devAmp_param
            // 
            this.devAmp_param.Location = new System.Drawing.Point(76, 45);
            this.devAmp_param.Name = "devAmp_param";
            this.devAmp_param.Size = new System.Drawing.Size(64, 20);
            this.devAmp_param.TabIndex = 27;
            this.devAmp_param.Text = "0,9";
            // 
            // TexBox
            // 
            this.TexBox.Enabled = false;
            this.TexBox.Location = new System.Drawing.Point(6, 19);
            this.TexBox.Name = "TexBox";
            this.TexBox.ReadOnly = true;
            this.TexBox.Size = new System.Drawing.Size(64, 20);
            this.TexBox.TabIndex = 24;
            this.TexBox.Text = "Амплитуда:";
            // 
            // Amp_param
            // 
            this.Amp_param.Location = new System.Drawing.Point(76, 19);
            this.Amp_param.Name = "Amp_param";
            this.Amp_param.Size = new System.Drawing.Size(64, 20);
            this.Amp_param.TabIndex = 25;
            this.Amp_param.Text = "1,0";
            // 
            // textBox15
            // 
            this.textBox15.Enabled = false;
            this.textBox15.Location = new System.Drawing.Point(6, 70);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(64, 20);
            this.textBox15.TabIndex = 22;
            this.textBox15.Text = "T сигнала:";
            // 
            // TotalTime_param
            // 
            this.TotalTime_param.Location = new System.Drawing.Point(76, 70);
            this.TotalTime_param.Name = "TotalTime_param";
            this.TotalTime_param.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TotalTime_param.Size = new System.Drawing.Size(64, 20);
            this.TotalTime_param.TabIndex = 23;
            this.TotalTime_param.Text = "0,128";
            // 
            // textBox13
            // 
            this.textBox13.Enabled = false;
            this.textBox13.Location = new System.Drawing.Point(6, 148);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(64, 20);
            this.textBox13.TabIndex = 20;
            this.textBox13.Text = "F мод.:";
            // 
            // ModFreq_param
            // 
            this.ModFreq_param.Location = new System.Drawing.Point(76, 148);
            this.ModFreq_param.Name = "ModFreq_param";
            this.ModFreq_param.Size = new System.Drawing.Size(64, 20);
            this.ModFreq_param.TabIndex = 21;
            this.ModFreq_param.Text = "1000";
            // 
            // textBox11
            // 
            this.textBox11.Enabled = false;
            this.textBox11.Location = new System.Drawing.Point(6, 122);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(64, 20);
            this.textBox11.TabIndex = 18;
            this.textBox11.Text = "F нес.:";
            // 
            // MainFreq_param
            // 
            this.MainFreq_param.Location = new System.Drawing.Point(76, 122);
            this.MainFreq_param.Name = "MainFreq_param";
            this.MainFreq_param.Size = new System.Drawing.Size(64, 20);
            this.MainFreq_param.TabIndex = 19;
            this.MainFreq_param.Text = "6200";
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(6, 96);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(64, 20);
            this.textBox9.TabIndex = 16;
            this.textBox9.Text = "F дискр.:";
            // 
            // DFreq_param
            // 
            this.DFreq_param.Location = new System.Drawing.Point(76, 96);
            this.DFreq_param.Name = "DFreq_param";
            this.DFreq_param.Size = new System.Drawing.Size(64, 20);
            this.DFreq_param.TabIndex = 17;
            this.DFreq_param.Text = "21500";
            // 
            // OneExpBT
            // 
            this.OneExpBT.Location = new System.Drawing.Point(1199, 537);
            this.OneExpBT.Name = "OneExpBT";
            this.OneExpBT.Size = new System.Drawing.Size(175, 24);
            this.OneExpBT.TabIndex = 5;
            this.OneExpBT.Text = "Новый эксперимент";
            this.OneExpBT.UseVisualStyleBackColor = true;
            this.OneExpBT.Click += new System.EventHandler(this.OneExpButton);
            // 
            // ExpsSeries_button
            // 
            this.ExpsSeries_button.Location = new System.Drawing.Point(1199, 566);
            this.ExpsSeries_button.Name = "ExpsSeries_button";
            this.ExpsSeries_button.Size = new System.Drawing.Size(175, 24);
            this.ExpsSeries_button.TabIndex = 7;
            this.ExpsSeries_button.Text = "Серия экспериментов";
            this.ExpsSeries_button.UseVisualStyleBackColor = true;
            this.ExpsSeries_button.Click += new System.EventHandler(this.ExpsSeries_button_Click);
            // 
            // ExpCount_param
            // 
            this.ExpCount_param.Location = new System.Drawing.Point(76, 123);
            this.ExpCount_param.Name = "ExpCount_param";
            this.ExpCount_param.Size = new System.Drawing.Size(64, 20);
            this.ExpCount_param.TabIndex = 9;
            this.ExpCount_param.Text = "250";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(6, 123);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(64, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "N:";
            // 
            // SNRmin_param
            // 
            this.SNRmin_param.Location = new System.Drawing.Point(76, 175);
            this.SNRmin_param.Name = "SNRmin_param";
            this.SNRmin_param.Size = new System.Drawing.Size(64, 20);
            this.SNRmin_param.TabIndex = 11;
            this.SNRmin_param.Text = "-25";
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(6, 175);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(64, 20);
            this.textBox3.TabIndex = 10;
            this.textBox3.Text = "SNR from:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RandomData_check);
            this.groupBox3.Controls.Add(this.textBox18);
            this.groupBox3.Controls.Add(this.textBox14);
            this.groupBox3.Controls.Add(this.textBox16);
            this.groupBox3.Controls.Add(this.TextBox44);
            this.groupBox3.Controls.Add(this.textBox21);
            this.groupBox3.Controls.Add(this.TextData_param);
            this.groupBox3.Controls.Add(this.textBox22);
            this.groupBox3.Controls.Add(this.SNRmax_param);
            this.groupBox3.Controls.Add(this.textBox19);
            this.groupBox3.Controls.Add(this.ExpTau_param);
            this.groupBox3.Controls.Add(this.textBox17);
            this.groupBox3.Controls.Add(this.Tau_param);
            this.groupBox3.Controls.Add(this.textBox7);
            this.groupBox3.Controls.Add(this.StepsSNR_param);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.ExpCount_param);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.SNRmin_param);
            this.groupBox3.Location = new System.Drawing.Point(1199, 302);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(175, 229);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры эксперимента:";
            // 
            // RandomData_check
            // 
            this.RandomData_check.AutoSize = true;
            this.RandomData_check.Checked = true;
            this.RandomData_check.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RandomData_check.Location = new System.Drawing.Point(6, 45);
            this.RandomData_check.Name = "RandomData_check";
            this.RandomData_check.Size = new System.Drawing.Size(122, 17);
            this.RandomData_check.TabIndex = 41;
            this.RandomData_check.Text = "Случайные даныне";
            this.RandomData_check.UseVisualStyleBackColor = true;
            this.RandomData_check.CheckedChanged += new System.EventHandler(this.RandomData_check_CheckedChanged);
            // 
            // textBox18
            // 
            this.textBox18.Enabled = false;
            this.textBox18.Location = new System.Drawing.Point(146, 98);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(23, 20);
            this.textBox18.TabIndex = 40;
            this.textBox18.Text = "с";
            // 
            // textBox14
            // 
            this.textBox14.Enabled = false;
            this.textBox14.Location = new System.Drawing.Point(146, 201);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(23, 20);
            this.textBox14.TabIndex = 39;
            this.textBox14.Text = "дБ";
            // 
            // textBox16
            // 
            this.textBox16.Enabled = false;
            this.textBox16.Location = new System.Drawing.Point(146, 175);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(23, 20);
            this.textBox16.TabIndex = 38;
            this.textBox16.Text = "дБ";
            // 
            // TextBox44
            // 
            this.TextBox44.Enabled = false;
            this.TextBox44.Location = new System.Drawing.Point(6, 201);
            this.TextBox44.Name = "TextBox44";
            this.TextBox44.ReadOnly = true;
            this.TextBox44.Size = new System.Drawing.Size(64, 20);
            this.TextBox44.TabIndex = 22;
            this.TextBox44.Text = "SNR to:";
            // 
            // textBox21
            // 
            this.textBox21.Enabled = false;
            this.textBox21.Location = new System.Drawing.Point(6, 19);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(64, 20);
            this.textBox21.TabIndex = 20;
            this.textBox21.Text = "Данные:";
            // 
            // TextData_param
            // 
            this.TextData_param.Location = new System.Drawing.Point(76, 19);
            this.TextData_param.Name = "TextData_param";
            this.TextData_param.Size = new System.Drawing.Size(64, 20);
            this.TextData_param.TabIndex = 21;
            this.TextData_param.Text = "ИТФИ";
            // 
            // textBox22
            // 
            this.textBox22.Enabled = false;
            this.textBox22.Location = new System.Drawing.Point(146, 72);
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(23, 20);
            this.textBox22.TabIndex = 35;
            this.textBox22.Text = "с";
            // 
            // SNRmax_param
            // 
            this.SNRmax_param.Location = new System.Drawing.Point(76, 201);
            this.SNRmax_param.Name = "SNRmax_param";
            this.SNRmax_param.Size = new System.Drawing.Size(64, 20);
            this.SNRmax_param.TabIndex = 23;
            this.SNRmax_param.Text = "5";
            // 
            // textBox19
            // 
            this.textBox19.Enabled = false;
            this.textBox19.Location = new System.Drawing.Point(6, 98);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(64, 20);
            this.textBox19.TabIndex = 18;
            this.textBox19.Text = "τ0 оценка:";
            // 
            // ExpTau_param
            // 
            this.ExpTau_param.Location = new System.Drawing.Point(76, 98);
            this.ExpTau_param.Name = "ExpTau_param";
            this.ExpTau_param.Size = new System.Drawing.Size(64, 20);
            this.ExpTau_param.TabIndex = 19;
            // 
            // textBox17
            // 
            this.textBox17.Enabled = false;
            this.textBox17.Location = new System.Drawing.Point(6, 72);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(64, 20);
            this.textBox17.TabIndex = 16;
            this.textBox17.Text = "τ0:";
            // 
            // Tau_param
            // 
            this.Tau_param.Location = new System.Drawing.Point(76, 72);
            this.Tau_param.Name = "Tau_param";
            this.Tau_param.Size = new System.Drawing.Size(64, 20);
            this.Tau_param.TabIndex = 17;
            this.Tau_param.Text = "0,128";
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(6, 149);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(64, 20);
            this.textBox7.TabIndex = 14;
            this.textBox7.Text = "M:";
            // 
            // StepsSNR_param
            // 
            this.StepsSNR_param.Location = new System.Drawing.Point(76, 149);
            this.StepsSNR_param.Name = "StepsSNR_param";
            this.StepsSNR_param.Size = new System.Drawing.Size(64, 20);
            this.StepsSNR_param.TabIndex = 15;
            this.StepsSNR_param.Text = "60";
            // 
            // Statistics_graph
            // 
            this.Statistics_graph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Statistics_graph.Location = new System.Drawing.Point(606, 306);
            this.Statistics_graph.Name = "Statistics_graph";
            this.Statistics_graph.Size = new System.Drawing.Size(586, 284);
            this.Statistics_graph.TabIndex = 8;
            this.Statistics_graph.TabStop = false;
            // 
            // ProgressGraph
            // 
            this.ProgressGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProgressGraph.Location = new System.Drawing.Point(12, 596);
            this.ProgressGraph.Name = "ProgressGraph";
            this.ProgressGraph.Size = new System.Drawing.Size(1362, 13);
            this.ProgressGraph.TabIndex = 9;
            this.ProgressGraph.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 610);
            this.Controls.Add(this.ProgressGraph);
            this.Controls.Add(this.Statistics_graph);
            this.Controls.Add(this.ExpsSeries_button);
            this.Controls.Add(this.OneExpBT);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Rxx_graph);
            this.Controls.Add(this.SignalPicture);
            this.Name = "Form1";
            this.Text = "Исследование типов модуляции";
            ((System.ComponentModel.ISupportInitialize)(this.SignalPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rxx_graph)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Statistics_graph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressGraph)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox SignalPicture;
        private System.Windows.Forms.PictureBox Rxx_graph;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton AM_rb;
        private System.Windows.Forms.RadioButton FT_rb;
        private System.Windows.Forms.RadioButton PM2_rb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button OneExpBT;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox TotalTime_param;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox ModFreq_param;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox MainFreq_param;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox DFreq_param;
        private System.Windows.Forms.Button ExpsSeries_button;
        private System.Windows.Forms.TextBox ExpCount_param;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox SNRmin_param;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox TextData_param;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox ExpTau_param;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox Tau_param;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox StepsSNR_param;
        private System.Windows.Forms.TextBox TextBox88;
        private System.Windows.Forms.TextBox devAmp_param;
        private System.Windows.Forms.TextBox TexBox;
        private System.Windows.Forms.TextBox Amp_param;
        private System.Windows.Forms.PictureBox Statistics_graph;
        private System.Windows.Forms.TextBox TextBox44;
        private System.Windows.Forms.TextBox SNRmax_param;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.CheckBox RandomData_check;
        private System.Windows.Forms.PictureBox ProgressGraph;
    }
}

