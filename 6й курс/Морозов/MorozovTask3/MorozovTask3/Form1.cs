﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MorozovTask2;

namespace MorozovTask3
{
    public partial class Form1 : Form
    {
        Random rnd;
        Thread SNR_exp_thread, dopler_exp_thread;

        DrawerCollection d;
        DRWDrawer drawerSignal, drawerRxx, drawerStat, drawerProgress;
        Drawer3D drawer3D;

        double A, devA, DF, modF, devF;
        Modulation pure_md, dopler_md;

        int mod_type;
        bool[] maximize = new bool[3] { false, false, false };

        int accuracy = 5;

        int CountsPerBit;

        int[] bits, changed_bits;

        double[][] x, wx;

        double[][] s, ws;
        double[][] ns, nws;

        double[][] rxx;
        double[][] SNR_Exp_results, dopler_Exp_results;

        Complex[][] cs, wcs;
        Complex[][] crxx;

        double SNR, minSNR, maxSNR;
        double[][] SNR_values;

        double doplerF, min_doplerF = 0, max_doplerF = 1000;
        double[][] doplerF_values;

        double[][][] Z;


        int m_type = 0,
            N,  // Число усреднений
            M,  // Число значений SNR
            L;  // Число значений добавки Доплера

        public Form1()
        {
            InitializeComponent();

            StartInit();

            UpdateValues();
        }


        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public void UpdateValues()
        {
            A = ToDouble(Amp_param);
            devA = ToDouble(devAmp_param);
            DF = ToDouble(DFreq_param);
            modF = ToDouble(modFreq_param);
            devF = ToDouble(devFreq_param);
            doplerF = ToDouble(doplerF_param);
            CountsPerBit = ToInt(CountsPerBit_param);
            pure_md = new Modulation(DF, modF, devF, CountsPerBit, A, devA);
            dopler_md = new Modulation(DF, modF + doplerF, devF, CountsPerBit, A, devA);

            dTau_param.Text = (CountsPerBit / DF).ToString($"F{accuracy}");

            bits = Bits.FromString(Data_param.Text);
            changed_bits = ExpandBits(bits);

            N = ToInt(N_param);

            M = ToInt(M_param);
            SNR = ToDouble(SNR_param);
            minSNR = ToDouble(minSNR_param);
            maxSNR = ToDouble(maxSNR_param);

            L = M;

            SNR_values = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                SNR_values[i] = new double[M + 1];
            }
            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < M + 1; i++)
                {
                    SNR_values[j][i] = minSNR + i * (maxSNR - minSNR) / M;
                }
            }

            doplerF_values = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                doplerF_values[i] = new double[L + 1];
            }
            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < L + 1; i++)
                {
                    doplerF_values[j][i] = min_doplerF + i * (max_doplerF - min_doplerF) / L;
                }
            }

            x = new double[3][];
            s = new double[3][];
            ns = new double[3][];
            cs = new Complex[3][];
            Z = new double[3][][];

            wx = new double[3][];
            ws = new double[3][];
            nws = new double[3][];
            wcs = new Complex[3][];

            rxx = new double[3][];

            SNR_Exp_results = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                SNR_Exp_results[i] = new double[M + 1];
            }
            dopler_Exp_results = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                dopler_Exp_results[i] = new double[L + 1];
            }

            for (int i = 0; i < 3; i++)
            {
                GetSignal(i, s, ns, x, bits, SNR, pure_md);
            }
        }
        public void StartInit()
        {
            rnd = new Random();

            d = new DrawerCollection();

            drawerSignal = d.DRWCreator(Signal_graph, true, true, "Сигнал");
            drawerSignal.AxisConfig(new int[] { 17, 9, 9, 5 });
            drawerSignal.DrawEmptyScene();

            drawerRxx = d.DRWCreator(Rxx_graph, true, true, "Rxx");
            drawerRxx.DrawEmptyScene();

            drawerStat = d.DRWCreator(Stat_graph, true, true, "Статистика");
            drawerStat.AxisConfig(new int[] { 11, 8, 11, 8 });
            drawerStat.DrawEmptyScene();

            drawer3D = d.Drawer3DCreator(Stat_graph, false, false);

            drawerProgress = d.DRWCreator(Progress_graph);

            mod_type = 0;

            Bitrate_param.Text = (ToDouble(DFreq_param) / ToInt(CountsPerBit_param)).ToString();
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        private void Exp_action_Click(object sender, EventArgs e)
        {
            if (SNR_exp_rb.Checked)
            {
                SNR_ExpInit();
            }
            if (dopler_exp_rb.Checked)
            {
                dopler_ExpInit();
            }
        }

        private void SNR_ExpInit()
        {
            UpdateValues();
            drawerStat.Resize(new double[] { minSNR, maxSNR, -0.2, 1.2 });

            GetSignal(mod_type, ws, nws, wx, changed_bits, SNR, pure_md);

            SNR_Exp(mod_type, SNR, ToDouble(Tau_param));

            DrawWS(mod_type);
            DrawRxx(mod_type);
        }
        private void dopler_ExpInit(bool _draw = true)
        {
            UpdateValues();

            textBox19.Text = "SKO:";
            textBox24.Text = "max/SKO:";

            GetSignal(mod_type, ws, nws, wx, changed_bits, SNR, dopler_md);

            dopler_Exp(mod_type, 10, ToDouble(Tau_param), modF + doplerF);

            if (_draw)
            {
                DrawWS(mod_type);
                DrawRxx(mod_type);
            }
        }

        private bool SNR_Exp(int _mode, double _SNR, double _tau, bool _threaded = false)
        {
            ns[_mode] = Arrays.AddNoise(s[_mode], _SNR);
            nws[_mode] = Arrays.AddNoise(ws[_mode], _SNR);

            for (int i = 0; i < wx.Length; i++)
            {
                rxx[_mode] = Arrays.Rxx(nws[_mode], ns[_mode]);
            }
            int max_idx = Arrays.GetMaxIdx(rxx[_mode]);
            double exp_tau = (double)(max_idx) * 1.0 / DF;
            if (!_threaded)
            {
                ExpTau_param.Text = exp_tau.ToString($"F{accuracy}");
            }
            if (Math.Abs(_tau - exp_tau) <= CountsPerBit / DF)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private double dopler_Exp(int _mode, double _SNR, double _tau, double _modF, bool _threaded = false)
        {
            ns[_mode] = Arrays.AddNoise(s[_mode], _SNR);
            dopler_md = new Modulation(DF, _modF, devF, CountsPerBit, A, devA);
            GetSignal(_mode, ws, nws, wx, changed_bits, 10, dopler_md);
            //nws[_mode] = Arrays.AddNoise(ws[_mode], _SNR);

            for (int i = 0; i < wx.Length; i++)
            {
                rxx[_mode] = Arrays.Rxx(nws[_mode], ns[_mode]);
            }
            double sko = Arrays.GetSKO(rxx[_mode], Arrays.GetAvg(rxx[_mode]));
            double max = Arrays.GetMax(rxx[_mode]);
            if (!_threaded)
            {
                ExpTau_param.Text = sko.ToString($"F{accuracy}");
                dTau_param.Text = (max / sko).ToString($"F{accuracy}");
            }
            return max / sko;
        }


        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        private void manyExps_action_Click(object sender, EventArgs e)
        {
            if (dopler_exp_thread != null)
                if (dopler_exp_thread.IsAlive)
                    dopler_exp_thread.Abort();
            if (SNR_exp_thread != null)
                if (SNR_exp_thread.IsAlive)
                    SNR_exp_thread.Abort();
            if (SNR_exp_rb.Checked)
            {
                UpdateValues();

                for (int i = 0; i < 3; i++)
                {
                    GetSignal(i, s, ns, x, bits, SNR, pure_md);
                    GetSignal(i, ws, nws, wx, changed_bits, SNR, dopler_md);
                }

                SNR_exp_thread = new Thread(new ThreadStart(SNR_ExpThread));
                SNR_exp_thread.Start();
            }
            if (dopler_exp_rb.Checked)
            {
                UpdateValues();
                for (int i = 0; i < 3; i++)
                {
                    GetSignal(i, s, ns, x, bits, 10, pure_md);
                    GetSignal(i, ws, nws, wx, changed_bits, 10, dopler_md);
                }
                dopler_exp_thread = new Thread(new ThreadStart(dopler_ExpThread));
                dopler_exp_thread.Start();
            }
        }
        private void SNR_ExpThread()
        {
            double tau = ToDouble(Tau_param);
            double progress = 0;
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M + 1; j++)
                {
                    Parallel.For(0, 3, k =>
                    {
                        if (i == 0)
                        {
                            SNR_Exp_results[k][j] += (SNR_Exp(k, SNR_values[k][j], tau, true) == true ? 1.0 : 0.0);
                        }
                        else
                        {
                            double res = (SNR_Exp(k, SNR_values[k][j], tau, true) == true ? 1.0 : 0.0);
                            SNR_Exp_results[k][j] = (SNR_Exp_results[k][j] * (i) + res) / (i + 1);
                        }
                    });
                    progress += 1.0 / N / (M + 1);
                    drawerProgress.Progress(progress, null);
                    drawerStat.DrawGraphMulti(SNR_values, SNR_Exp_results, false);
                    //Thread.Sleep(10);
                }
            }
        }
        private void dopler_ExpThread()
        {
            double tau = ToDouble(Tau_param);
            double progress = 0;
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < L + 1; j++)
                {
                    Parallel.For(0, 3, k =>
                    {
                        if (i == 0)
                        {
                            dopler_Exp_results[k][j] += (dopler_Exp(k, 10, tau, modF + doplerF_values[k][j], true));
                        }
                        else
                        {
                            double res = (dopler_Exp(k, 10, tau, modF + doplerF_values[k][j], true));
                            dopler_Exp_results[k][j] = (dopler_Exp_results[k][j] * (i) + res) / (i + 1);
                        }
                    });
                    progress += 1.0 / N / (L + 1);
                    drawerProgress.Progress(progress, null);
                    DrawDoplerStat();
                    //Thread.Sleep(10);
                }
            }
        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        private void Draw3D_param_Click(object sender, EventArgs e)
        {
            UpdateValues();
            double tau = ToDouble(Tau_param);
            List<double> maxes = new List<double>();

            dopler_md = new Modulation(DF, modF + doplerF, devF, CountsPerBit, A, devA);
            for (int m = 0; m < 3; m++)
                GetSignal(m, ws, nws, wx, changed_bits, 10, dopler_md);

            for (int m = 0; m < 3; m++)
            {
                Z[m] = new double[M + 1][];
                for (int j = 0; j < M + 1; j++)
                {
                    dopler_md = new Modulation(DF, modF + doplerF_values[m][j], devF, CountsPerBit, A, devA);
                    GetSignal(m, s, ns, x, bits, 10, dopler_md);
                    rxx[m] = Arrays.Rxx(nws[m], ns[m]);

                    Z[m][j] = new double[rxx[0].Length];
                    for (int i = 0; i < rxx[m].Length; i++)
                    {
                        if (rxx[m][i] > 0)
                            Z[m][j][i] = rxx[m][i];
                        else
                            Z[m][j][i] = 0;
                    }
                    Z[m][j] = Arrays.Zip(Z[m][j], 8);
                    maxes.Add(Arrays.GetMax(Z[m][j]));
                }
            }
            double AbsoluteMax = Arrays.GetMax(maxes.ToArray<double>());
            double[][] X = new double[3][];
            double[][] Y = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                X[i] = Arrays.Normalize(doplerF_values[i], -4, 4);
                Y[i] = Arrays.Normalize(wx[i], -4, 4);
                Y[i] = Arrays.Zip(Y[i], 10);
                Y[i] = Arrays.Normalize(Y[i], -4, 4);
                for (int j = 0; j < M + 1; j++)
                {
                    Z[i][j] = Arrays.Normalize(Z[i][j], 0, maxes[j] / AbsoluteMax);
                    for (int k = 0; k < Z[i][j].Length; k++)
                    {
                        Z[i][j][k] = Math.Pow(Z[i][j][k], 1.5);
                    }

                }
            }

            double wxMax = Arrays.GetMax(wx[0]);
            for (int i = 0; i < 3; i++)
            {
                //for (int j = 0; j < M + 1; j++)

                //X[i][j] = (-1000 / 2 + doplerF_values[i][j]) / 250;

                //for (int j = 0; j < wx[0].Length; j++)

                //wx[i][j] = (wx[i][j] - wxMax / 2) * 100;

            }


            Form3D f3d = new Form3D(X, Y, Z);
            f3d.Show();
        }


        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public void GetSignal(int idx, double[][] signal, double[][] noised_signal, double[][] time_arr, int[] data_bits, double _SNR, Modulation _md)
        {
            switch (idx)
            {
                case 0:
                    signal[idx] = _md.PM2(data_bits);
                    time_arr[idx] = _md.GetTime();
                    noised_signal[idx] = Arrays.AddNoise(signal[idx], _SNR);
                    break;
                case 1:
                    signal[idx] = _md.FT(data_bits);
                    time_arr[idx] = _md.GetTime();
                    noised_signal[idx] = Arrays.AddNoise(signal[idx], _SNR);
                    break;
                case 2:
                    signal[idx] = _md.AM(data_bits);
                    time_arr[idx] = _md.GetTime();
                    noised_signal[idx] = Arrays.AddNoise(signal[idx], _SNR);
                    break;
            }
        }
        private int[] ExpandBits(int[] data_bits)
        {
            int[] to_return;
            List<int> additional_arr = new List<int>();
            int add;
            add = rnd.Next(-2, 2) * 2;
            add = 0;
            double tau = (bits.Length + add) * CountsPerBit / DF;
            Tau_param.Text = tau.ToString($"F{accuracy}");
            for (int i = 0; i < data_bits.Length + add; i++)
            {
                additional_arr.Add(rnd.Next(0, 2));
            }
            to_return = Arrays.Merge(additional_arr.ToArray<int>(), data_bits);
            additional_arr.Clear();
            add = rnd.Next(-2, 2) * 2;
            add = 0;
            for (int i = 0; i < data_bits.Length + add; i++)
            {
                additional_arr.Add(rnd.Next(0, 2));
            }
            to_return = Arrays.Merge(to_return, additional_arr.ToArray<int>());
            return to_return;
        }


        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        private void PM2_rb_CheckedChanged(object sender, EventArgs e)
        {
            if (PM2_rb.Checked)
            {
                mod_type = 0;
            }
        }
        private void FT_rb_CheckedChanged(object sender, EventArgs e)
        {
            if (FT_rb.Checked)
            {
                mod_type = 1;
            }
        }
        private void AM_rb_CheckedChanged(object sender, EventArgs e)
        {
            if (AM_rb.Checked)
            {
                mod_type = 2;
            }
        }
        private void CountPerBit_param_TextChanged(object sender, EventArgs e)
        {
            if (CountsPerBit_param.Text != "")
                if (ToInt(CountsPerBit_param) > 0)
                    Bitrate_param.Text = (ToDouble(DFreq_param) / ToInt(CountsPerBit_param)).ToString();
        }
        private void DFreq_param_TextChanged(object sender, EventArgs e)
        {
            if (DFreq_param.Text != "")
                if (ToInt(DFreq_param) > 0)
                    Bitrate_param.Text = (ToDouble(DFreq_param) / ToInt(CountsPerBit_param)).ToString();
        }


        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        private void Signal_graph_Click(object sender, EventArgs e)
        {

        }
        public void DrawS(int idx)
        {
            drawerSignal.DrawGraph(x[idx], ns[idx], true);
        }
        public void DrawWS(int idx)
        {
            drawerSignal.DrawGraph(wx[idx], nws[idx], true);
        }
        public void DrawRxx(int idx)
        {
            drawerRxx.DrawGraph(wx[idx], rxx[idx], true);
        }
        public void DrawSNRStat()
        {
            drawerStat.DrawGraphMulti(SNR_values, SNR_Exp_results, false);
        }
        public void DrawDoplerStat()
        {
            drawerStat.DrawGraphMulti(doplerF_values, dopler_Exp_results, true);
        }


        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public double ToDouble(TextBox TB)
        {
            return Convert.ToDouble(TB.Text);
        }
        public int ToInt(TextBox TB)
        {
            return Convert.ToInt32(TB.Text);
        }
        private void SaveGraphs_action_Click(object sender, EventArgs e)
        {
            int directory_count = Directory.GetDirectories("..\\..\\..\\Графики").Length;
            Directory.CreateDirectory($"..\\..\\..\\Графики\\Эксперимент {directory_count}");
            string path = $"..\\..\\..\\Графики\\Эксперимент {directory_count}\\";
            Image img;

            img = Signal_graph.Image;
            img.Save($"..\\..\\..\\Графики\\Эксперимент {directory_count}\\График сигнала.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

            img = Rxx_graph.Image;
            img.Save($"..\\..\\..\\Графики\\Эксперимент {directory_count}\\График I&Q.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

            img = Stat_graph.Image;
            img.Save($"..\\..\\..\\Графики\\Эксперимент {directory_count}\\Статистика.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

            Rectangle bounds = this.Bounds;
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                }
                bitmap.Save(path + "Окно.jpeg", ImageFormat.Jpeg);
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (SNR_exp_thread != null)
                if (SNR_exp_thread.IsAlive)
                    SNR_exp_thread.Abort();
        }
    }
}
