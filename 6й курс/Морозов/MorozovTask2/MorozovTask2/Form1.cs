﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Numerics;

namespace MorozovTask2
{
    public partial class Form1 : Form
    {
        DrawerCollection d;
        DRWDrawer drawerIQ, drawerIQ2, drawerStat, drawerProgress;
        Modulation md;
        Random rnd;
        Thread exp_thread;

        bool[] maximize = new bool[3] { false, false, false };

        int[] bits, changed_bits;
        int[][] gold;
        double[][] gold_qpsk, h;
        double[][] decs;
        Complex[] z;

        double[] x, I, Q, fi, s;
        double[] nI, nQ, nfi, ns;
        double[] success, SNR_values;

        int CountsPerBit;
        int mode = 0,
            N,  // Число усреднений
            M;  // Число значений SNR

        double Beta;
        double SNR, minSNR, maxSNR;


        public Form1()
        {
            InitializeComponent();

            UpdateValues();

            StartInit();
        }

        public void StartInit()
        {
            d = new DrawerCollection();

            drawerIQ = d.DRWCreator(IQ_graph, true, true, "I component");
            drawerIQ.DrawEmptyScene();

            drawerIQ2 = d.DRWCreator(IQ_graph2, true, true, "I&Q");
            drawerIQ2.AxisConfig(new int[] { 9, 9, 5, 5 });
            drawerIQ2.DrawEmptyScene();

            drawerStat = d.DRWCreator(StatGraph, true, true);
            drawerStat.GraphName = "Корреляции сигнала с кодами голда";
            drawerStat.AxisConfig(new int[] { 5, 9, 5, 5 });
            drawerStat.DrawEmptyScene();

            drawerProgress = d.DRWCreator(ProgressGraph);
            drawerProgress.Progress(0, null);

            Bitrate_param.Text = ((int)(ToDouble(DFreq_param) / ToInt(CountsPerBit_param))).ToString();

            rnd = new Random();

            /// Инициализируем все последовательности Голда
            //int[] m1 = Sequences.MSequence(new int[] { 1, 0, 1, 0, 1 });
            //int[] m2 = Sequences.MSequence(new int[] { 1, 1, 1, 1, 1 });
            //int[] m1 = new int[] { 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0 };
            //int[] m2 = new int[] { 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0 };

            gold = new int[4][];
            gold_qpsk = new double[4][];


            /*
            int[] m1 = Sequences.MSeq(new int[] { 0, 1, 0, 0, 0});
            int[] m2 = Sequences.MSeq(new int[] { 0, 1, 1, 1, 0});
            //19, 14, 12, 17 - для кодов Голда длины 31
            gold[0] = Sequences.GoldSequence(m1, m2, 19);
            gold[1] = Sequences.GoldSequence(m1, m2, 14);
            gold[2] = Sequences.GoldSequence(m1, m2, 12);
            gold[3] = Sequences.GoldSequence(m1, m2, 17);
            */

            int[] m1 = Sequences.MSeq(new int[] { 0, 1, 0, 0, 0, 0 });
            int[] m2 = Sequences.MSeq(new int[] { 0, 1, 1, 1, 0, 0 });
            // 5, 8, 17, 33 - для кодов Голда длины 63
            gold[0] = Sequences.GoldSequence(m1, m2, 5, true);
            gold[1] = Sequences.GoldSequence(m1, m2, 8, true);
            gold[2] = Sequences.GoldSequence(m1, m2, 17, true);
            gold[3] = Sequences.GoldSequence(m1, m2, 33, true);

            /*
            Track_action0.Maximum = gold[0].Length;
            Track_action1.Maximum = gold[0].Length;
            Track_action2.Maximum = gold[0].Length;
            Track_action3.Maximum = gold[0].Length;
            */

            h = new double[4][];
            for (int i = 0; i < 4; i++)
            {
                /// h[i] = GetH(gold_qpsk[i]);
            }

            decs = new double[4][];
            for (int i = 0; i < 4; i++)
            {
                /// decs[i] = new double[...];
            }
        }

        public void UpdateValues()
        {
            Beta = ToDouble(Beta_param);

            CountsPerBit = ToInt(CountsPerBit_param);
            SNR = ToDouble(SNR_param);

            minSNR = ToDouble(SNRmin_param);
            maxSNR = ToDouble(SNRmax_param);
            M = ToInt(CountOfSNRSteps_param);
            N = ToInt(CountOfExps_param);
        }

        private void Modulation_action_Click(object sender, EventArgs e)
        {
            UpdateValues();

            bits = Bits.FromString(Bits_param.Text);
            changed_bits = Sequences.GoldTransform(bits, gold);

            GetSignals();
            NoiseSignals(SNR);

            DrawIQ();
            DrawCurrent();
            Experiment();
        }

        private void Debug_param_CheckedChanged(object sender, EventArgs e)
        {
            Modulation_action_Click(sender, e);
        }

        private void Experiment_action_Click(object sender, EventArgs e)
        {
            if (exp_thread != null)
            {
                if (exp_thread.IsAlive)
                {
                    exp_thread.Abort();
                }
            }

            UpdateValues();

            bits = Bits.FromString(Bits_param.Text);
            changed_bits = Sequences.GoldTransform(bits, gold);

            GetSignals();

            success = new double[M + 1];
            SNR_values = new double[M + 1];
            for (int i = 0; i < SNR_values.Length; i++)
            {
                SNR_values[i] = minSNR + (maxSNR - minSNR) * (double)i / M;
            }
            DrawStat();

            exp_thread = new Thread(new ThreadStart(ExpThread));
            exp_thread.Start();
        }

        public void ExpThread()
        {
            int right_bits = 0;
            int total_bits = bits.Length * N;
            double local_success;
            double progress = 0;

            /// Эксперимент
            for (int i = 0; i < N; i++)
            {
                for (int snr = 0; snr < M + 1; snr++)
                {
                    NoiseSignals(SNR_values[snr]);
                    right_bits = Experiment(true);
                    local_success = (double)right_bits / total_bits;
                    success[snr] += local_success;
                    progress += 1.0 / N / (M + 1);
                }
                drawerProgress.Progress(progress, null);
                drawerStat.DrawGraph(SNR_values, success, false);
            }
        }

        private int Experiment(bool is_threaded = false)
        {
            for (int i = 0; i < decs.Length; i++)
            {
                decs[i] = Arrays.Rxx(nfi, gold_qpsk[i]);
            }

            if (!is_threaded)
                DrawCorrelation();

            int[] max_idx = new int[bits.Length / 2];
            for (int i = 0; i < max_idx.Length; i++)
            {
                double[][] cut_of_rxx = new double[4][];
                double max = double.MinValue;
                int idx = -1;
                for (int k = 0; k < 4; k++)
                {
                    cut_of_rxx[k] = Arrays.Cut(decs[k], i * gold[0].Length * CountsPerBit, (i + 1) * gold[0].Length * CountsPerBit);
                    double potentional_max = Arrays.GetMax(cut_of_rxx[k]);
                    if (potentional_max > max)
                    {
                        max = potentional_max;
                        idx = k;
                    }
                }
                max_idx[i] = idx;
            }

            int right_bits = 0;
            int[] decoded_bits = Sequences.GoldDetransform(max_idx);
            for (int i = 0; i < bits.Length; i++)
            {
                right_bits += bits[i] == decoded_bits[i] ? 1 : 0;
            }
            double success = (double)(right_bits) / bits.Length;
            if (!is_threaded)
                success_param.Text = (success * 100).ToString("F2");

            string decoded_bits_str = Bits.ToString(decoded_bits, 16);
            if (!is_threaded)
                DecodedBits_param.Text = decoded_bits_str;

            return right_bits;
        }

        private void NoiseSignals(double SNR)
        {
            nI = Arrays.AddNoise(I, SNR);
            nQ = Arrays.AddNoise(Q, SNR);
            nfi = Arrays.AddNoise(fi, SNR);
            ns = Arrays.AddNoise(s, SNR);
        }

        private void GetSignals()
        {
            md = new Modulation(ToDouble(DFreq_param), ToDouble(MainFreq_param), 6000, CountsPerBit);
            var outputs = md.PM4(changed_bits);
            I = outputs[0];
            Q = outputs[1];
            fi = outputs[2];
            s = outputs[3];
            x = md.GetTime(0);
            /// Модуляция каждой последовательности Голда ФМ-4 модуляцией
            for (int i = 0; i < 4; i++)
            {
                gold_qpsk[i] = md.PM4(gold[i])[2];
            }
        }


        private void IQ_graph_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                SwitchImage();
            }
            if (e.Button == MouseButtons.Left)
            {
                Maximize(0);
            }
        }

        private void IQ_graph2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Maximize(1);
            }
        }

        private void StatGraph_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Maximize(2);
            }
        }

        private void SaveGraphs_action_Click(object sender, EventArgs e)
        {
            int old_mode = mode;
            mode = -1;
            int directory_count = Directory.GetDirectories("..\\..\\..\\Графики").Length;
            Directory.CreateDirectory($"..\\..\\..\\Графики\\Эксперимент {directory_count}");
            string path = $"..\\..\\..\\Графики\\Эксперимент {directory_count}\\";
            Image img;
            for (int i = 0; i < 4; i++)
            {
                SwitchImage();
                img = IQ_graph.Image;
                string[] graph = new string[] { "I", "Q", "fi", "S" };
                img.Save($"..\\..\\..\\Графики\\Эксперимент {directory_count}\\График {graph[i]}.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            img = IQ_graph2.Image;
            img.Save($"..\\..\\..\\Графики\\Эксперимент {directory_count}\\График I&Q.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

            img = StatGraph.Image;
            img.Save($"..\\..\\..\\Графики\\Эксперимент {directory_count}\\Статистика.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

            Rectangle bounds = this.Bounds;
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                }
                bitmap.Save(path + "Окно.jpeg", ImageFormat.Jpeg);
            }
            mode = old_mode;
            DrawCurrent();
        }


        private void Debug_param_CheckedChanged_1(object sender, EventArgs e)
        {
            DrawCorrelation();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (exp_thread != null)
            {
                if (exp_thread.IsAlive)
                {
                    exp_thread.Abort();
                }
            }
        }

        /*
        private void Track_action_Scroll(object sender, EventArgs e)
        {
            int[] m1 = Sequences.MSeq(new int[] { 0, 1, 0, 0, 0, 0 });
            int[] m2 = Sequences.MSeq(new int[] { 0, 1, 1, 1, 0, 0 });

            gold[0] = Sequences.GoldSequence(m1, m2, Track_action0.Value);
            gold[1] = Sequences.GoldSequence(m1, m2, Track_action1.Value);
            gold[2] = Sequences.GoldSequence(m1, m2, Track_action2.Value);
            gold[3] = Sequences.GoldSequence(m1, m2, Track_action3.Value);

            Track_values.Text = $"{Track_action0.Value}, {Track_action1.Value}, {Track_action2.Value}, {Track_action3.Value}";

            drawerStat.GraphName = "Корреляции чистых кодов голда";
            for (int i = 0; i < decs.Length; i++)
            {
                decs[i] = Arrays.Rxx(gold[i], gold[i]);
            }
            double[][] spx = new double[4][];
            for (int j = 0; j < 4; j++)
            {
                spx[j] = new double[decs[0].Length];
                for (int i = 0; i < decs[0].Length; i++)
                {
                    spx[j][i] = i;
                }
            }
            drawerStat.DrawGraphMulti(spx, decs, true);
        }
        */

        public void SwitchImage()
        {
            mode = (mode + 1) % 4;
            DrawCurrent();
        }

        public void Maximize(int idx)
        {
            switch (idx)
            {
                case 0:
                    if (!maximize[idx])
                    {
                        IQ_graph.Width = 996;
                        IQ_graph.Height = 586;
                        IQ_graph2.Visible = false;
                        StatGraph.Visible = false;
                        maximize[idx] = true;
                        DrawCurrent();
                    }
                    else
                    {
                        IQ_graph.Width = 700;
                        IQ_graph.Height = 290;
                        IQ_graph2.Visible = true;
                        StatGraph.Visible = true;
                        maximize[idx] = false;
                        DrawCurrent();
                    }
                    break;
                case 1:
                    /// Location 715; 12
                    /// Default size 263; 263
                    if (!maximize[idx])
                    {
                        IQ_graph2.Location = new Point(12, 12);
                        IQ_graph2.Width = 996;
                        IQ_graph2.Height = 586;
                        IQ_graph.Visible = false;
                        StatGraph.Visible = false;
                        maximize[idx] = true;
                        DrawIQ();
                    }
                    else
                    {
                        IQ_graph2.Width = 290;
                        IQ_graph2.Height = 290;
                        IQ_graph2.Location = new Point(718, 12);
                        IQ_graph.Visible = true;
                        StatGraph.Visible = true;
                        maximize[idx] = false;
                        DrawIQ();
                    }
                    break;
                case 2:
                    /// Location 12; 281
                    /// Default size 966; 263
                    if (!maximize[idx])
                    {
                        StatGraph.Location = new Point(12, 12);
                        StatGraph.Width = 996;
                        StatGraph.Height = 586;
                        IQ_graph.Visible = false;
                        IQ_graph2.Visible = false;
                        maximize[idx] = true;
                        DrawCorrelation();
                    }
                    else
                    {
                        StatGraph.Width = 996;
                        StatGraph.Height = 290;
                        StatGraph.Location = new Point(12, 308);
                        IQ_graph.Visible = true;
                        IQ_graph2.Visible = true;
                        maximize[idx] = false;
                        DrawCorrelation();
                    }
                    break;
            }
        }


        public void DrawCurrent()
        {
            switch (mode)
            {
                case 0:
                    DrawI();
                    break;
                case 1:
                    DrawQ();
                    break;
                case 2:
                    DrawFi();
                    break;
                case 3:
                    DrawS();
                    break;
            }
        }

        public void DrawI()
        {
            drawerIQ = d.DRWCreator(IQ_graph, true, true, "I component");
            drawerIQ.GraphName = "I component";
            if (x == null || I == null)
                drawerIQ.DrawEmptyScene();
            else
            {
                drawerIQ.Resize(new double[] { x[0], x.Last<double>(), -1, 1 });
                drawerIQ.DrawGraph(x, I, true, GraphType.Lines, 0.005);
            }
        }

        public void DrawQ()
        {
            drawerIQ = d.DRWCreator(IQ_graph, true, true, "I component");
            drawerIQ.GraphName = "Q component";
            if (x == null || Q == null)
                drawerIQ.DrawEmptyScene();
            else
            {
                drawerIQ.Resize(new double[] { x[0], x.Last<double>(), -1, 1 });
                drawerIQ.DrawGraph(x, Q, true, GraphType.Lines, 0.005);
            }
        }

        public void DrawFi()
        {
            drawerIQ = d.DRWCreator(IQ_graph, true, true, "I component");
            drawerIQ.GraphName = "Atan2(Q(t), I(t))";
            if (x == null || fi == null)
                drawerIQ.DrawEmptyScene();
            else
            {
                drawerIQ.Resize(new double[] { x[0], x.Last<double>(), -1, 1 });
                drawerIQ.DrawGraph(x, fi, true, GraphType.Lines, 0.005);
            }
        }

        public void DrawS()
        {
            drawerIQ = d.DRWCreator(IQ_graph, true, true, "I component");
            drawerIQ.GraphName = "Комплексная огибающая";
            if (x == null || s == null)
                drawerIQ.DrawEmptyScene();
            else
            {
                drawerIQ.Resize(new double[] { x[0], x.Last<double>(), -1, 1 });
                drawerIQ.DrawGraph(x, s, true, GraphType.Lines, 0.005);
            }
        }

        public void DrawIQ()
        {
            drawerIQ2 = d.DRWCreator(IQ_graph2, true, true, "I&Q");
            drawerIQ2.AxisConfig(new int[] { 9, 9, 5, 5 });
            if (I == null || Q == null)
                drawerIQ2.DrawEmptyScene();
            else
            {
                drawerIQ2.DrawGraph(Q, I, true);
            }
        }

        public void DrawCorrelation()
        {
            drawerStat = d.DRWCreator(StatGraph, true, true);
            drawerStat.GraphName = "Корреляции сигнала с кодами голда";

            int AxisY_count = 5;
            if (bits != null)
                AxisY_count = bits.Length / 2 + 1;
            drawerStat.AxisConfig(new int[] { AxisY_count, 9, AxisY_count, 5 });

            if (bits != null)
            {
                for (int i = 0; i < decs.Length; i++)
                    drawerStat.Resize(x, decs[i]);

                drawerStat.DrawGraph(x, decs[0], false);
                for (int i = 1; i < decs.Length; i++)
                    drawerStat.AddGraph(x, decs[i], GraphType.Lines, i);

                if (Debug_param.Checked)
                {
                    double[] spx = new double[changed_bits.Length];
                    for (int i = 0; i < spx.Length; i++)
                    {
                        spx[i] = i;
                    }

                    for (int i = 0; i < decs.Length; i++)
                    {
                        decs[i] = Arrays.Rxx(changed_bits, gold[i]);
                    }

                    for (int i = 0; i < decs.Length; i++)
                    {
                        drawerStat.Resize(spx, decs[i]);
                    }

                    drawerStat.DrawGraph(spx, decs[0], false);

                    for (int i = 1; i < decs.Length; i++)
                    {
                        drawerStat.AddGraph(spx, decs[i], GraphType.Lines, i);
                    }
                }
            }
            else
            {
                drawerStat.DrawEmptyScene();
            }
        }

        public void DrawStat()
        {
            drawerStat = d.DRWCreator(StatGraph, true, true);
            drawerStat.GraphName = "Результаты правильного декодирования";

            drawerStat.AxisConfig(new int[] { 25, 8, 6, 8 });
            drawerStat.SetRanges(new double[] { Arrays.GetMin(SNR_values), Arrays.GetMax(SNR_values), -0.2, 1.2 });

            if (success != null)
            {
                drawerStat.DrawGraph(SNR_values, success, false);
            }
        }

        private void CountsPerBit_param_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CountsPerBit = Convert.ToInt32(CountsPerBit_param.Text);
                Bitrate_param.Text = ((int)(Convert.ToDouble(DFreq_param.Text) / CountsPerBit)).ToString();
            }
            catch (FormatException)
            {
                ;
            }
        }

        private void DFreq_param_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CountsPerBit = Convert.ToInt32(CountsPerBit_param.Text);
                Bitrate_param.Text = ((int)(Convert.ToDouble(DFreq_param.Text) / CountsPerBit)).ToString();
            }
            catch (FormatException)
            {
                ;
            }
        }

        public double ToDouble(TextBox TB)
        {
            return Convert.ToDouble(TB.Text);
        }

        public int ToInt(TextBox TB)
        {
            return Convert.ToInt32(TB.Text);
        }
    }
}
