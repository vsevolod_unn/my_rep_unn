﻿namespace MorozovTask2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.IQ_graph = new System.Windows.Forms.PictureBox();
            this.ProgressGraph = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.Beta_param = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.CountsPerBit_param = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.Bitrate_param = new System.Windows.Forms.TextBox();
            this.SNR_param = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.TexBox = new System.Windows.Forms.TextBox();
            this.Amp_param = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.MainFreq_param = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.DFreq_param = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.RandomData_check = new System.Windows.Forms.CheckBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.TextBox44 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.Bits_param = new System.Windows.Forms.TextBox();
            this.SNRmax_param = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.CountOfSNRSteps_param = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.CountOfExps_param = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.SNRmin_param = new System.Windows.Forms.TextBox();
            this.Experiment_action = new System.Windows.Forms.Button();
            this.StatGraph = new System.Windows.Forms.PictureBox();
            this.Modulation_action = new System.Windows.Forms.Button();
            this.IQ_graph2 = new System.Windows.Forms.PictureBox();
            this.SaveGraphs_action = new System.Windows.Forms.Button();
            this.Debug_param = new System.Windows.Forms.CheckBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.DecodedBits_param = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.success_param = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.IQ_graph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressGraph)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IQ_graph2)).BeginInit();
            this.SuspendLayout();
            // 
            // IQ_graph
            // 
            this.IQ_graph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IQ_graph.Location = new System.Drawing.Point(12, 12);
            this.IQ_graph.Name = "IQ_graph";
            this.IQ_graph.Size = new System.Drawing.Size(700, 290);
            this.IQ_graph.TabIndex = 0;
            this.IQ_graph.TabStop = false;
            this.IQ_graph.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IQ_graph_MouseDown);
            // 
            // ProgressGraph
            // 
            this.ProgressGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ProgressGraph.Location = new System.Drawing.Point(12, 604);
            this.ProgressGraph.Name = "ProgressGraph";
            this.ProgressGraph.Size = new System.Drawing.Size(1190, 14);
            this.ProgressGraph.TabIndex = 1;
            this.ProgressGraph.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.Beta_param);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox23);
            this.groupBox2.Controls.Add(this.CountsPerBit_param);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Controls.Add(this.textBox20);
            this.groupBox2.Controls.Add(this.Bitrate_param);
            this.groupBox2.Controls.Add(this.SNR_param);
            this.groupBox2.Controls.Add(this.textBox10);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.TexBox);
            this.groupBox2.Controls.Add(this.Amp_param);
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.MainFreq_param);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.DFreq_param);
            this.groupBox2.Location = new System.Drawing.Point(1014, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 203);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры ФМ-4 модуляции:";
            // 
            // textBox12
            // 
            this.textBox12.Enabled = false;
            this.textBox12.Location = new System.Drawing.Point(146, 97);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(32, 20);
            this.textBox12.TabIndex = 53;
            this.textBox12.Text = "ед.";
            this.textBox12.Visible = false;
            // 
            // textBox13
            // 
            this.textBox13.Enabled = false;
            this.textBox13.Location = new System.Drawing.Point(6, 97);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(64, 20);
            this.textBox13.TabIndex = 51;
            this.textBox13.Text = "β:";
            this.textBox13.Visible = false;
            // 
            // Beta_param
            // 
            this.Beta_param.Location = new System.Drawing.Point(76, 97);
            this.Beta_param.Name = "Beta_param";
            this.Beta_param.Size = new System.Drawing.Size(64, 20);
            this.Beta_param.TabIndex = 52;
            this.Beta_param.Text = "0,9";
            this.Beta_param.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(146, 175);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(32, 20);
            this.textBox4.TabIndex = 50;
            this.textBox4.Text = "ед.";
            // 
            // textBox23
            // 
            this.textBox23.Enabled = false;
            this.textBox23.Location = new System.Drawing.Point(6, 175);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(64, 20);
            this.textBox23.TabIndex = 48;
            this.textBox23.Text = "Отч/бит";
            // 
            // CountsPerBit_param
            // 
            this.CountsPerBit_param.Location = new System.Drawing.Point(76, 175);
            this.CountsPerBit_param.Name = "CountsPerBit_param";
            this.CountsPerBit_param.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CountsPerBit_param.Size = new System.Drawing.Size(64, 20);
            this.CountsPerBit_param.TabIndex = 49;
            this.CountsPerBit_param.Text = "1";
            this.CountsPerBit_param.TextChanged += new System.EventHandler(this.CountsPerBit_param_TextChanged);
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(146, 149);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(32, 20);
            this.textBox6.TabIndex = 47;
            this.textBox6.Text = "Бит";
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(146, 123);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(32, 20);
            this.textBox5.TabIndex = 44;
            this.textBox5.Text = "дБ";
            // 
            // textBox15
            // 
            this.textBox15.Enabled = false;
            this.textBox15.Location = new System.Drawing.Point(6, 149);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(64, 20);
            this.textBox15.TabIndex = 45;
            this.textBox15.Text = "Бит/с";
            // 
            // textBox20
            // 
            this.textBox20.Enabled = false;
            this.textBox20.Location = new System.Drawing.Point(6, 123);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(64, 20);
            this.textBox20.TabIndex = 42;
            this.textBox20.Text = "SNR:";
            // 
            // Bitrate_param
            // 
            this.Bitrate_param.Location = new System.Drawing.Point(76, 149);
            this.Bitrate_param.Name = "Bitrate_param";
            this.Bitrate_param.ReadOnly = true;
            this.Bitrate_param.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Bitrate_param.Size = new System.Drawing.Size(64, 20);
            this.Bitrate_param.TabIndex = 46;
            // 
            // SNR_param
            // 
            this.SNR_param.Location = new System.Drawing.Point(76, 123);
            this.SNR_param.Name = "SNR_param";
            this.SNR_param.Size = new System.Drawing.Size(64, 20);
            this.SNR_param.TabIndex = 43;
            this.SNR_param.Text = "100";
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(146, 71);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(32, 20);
            this.textBox10.TabIndex = 32;
            this.textBox10.Text = "Гц";
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(146, 45);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(32, 20);
            this.textBox8.TabIndex = 31;
            this.textBox8.Text = "Гц";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(146, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(32, 20);
            this.textBox2.TabIndex = 28;
            this.textBox2.Text = "В";
            // 
            // TexBox
            // 
            this.TexBox.Enabled = false;
            this.TexBox.Location = new System.Drawing.Point(6, 19);
            this.TexBox.Name = "TexBox";
            this.TexBox.ReadOnly = true;
            this.TexBox.Size = new System.Drawing.Size(64, 20);
            this.TexBox.TabIndex = 24;
            this.TexBox.Text = "Амплитуда:";
            // 
            // Amp_param
            // 
            this.Amp_param.Location = new System.Drawing.Point(76, 19);
            this.Amp_param.Name = "Amp_param";
            this.Amp_param.Size = new System.Drawing.Size(64, 20);
            this.Amp_param.TabIndex = 25;
            this.Amp_param.Text = "1,0";
            // 
            // textBox11
            // 
            this.textBox11.Enabled = false;
            this.textBox11.Location = new System.Drawing.Point(6, 71);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(64, 20);
            this.textBox11.TabIndex = 18;
            this.textBox11.Text = "F нес.:";
            // 
            // MainFreq_param
            // 
            this.MainFreq_param.Location = new System.Drawing.Point(76, 71);
            this.MainFreq_param.Name = "MainFreq_param";
            this.MainFreq_param.Size = new System.Drawing.Size(64, 20);
            this.MainFreq_param.TabIndex = 19;
            this.MainFreq_param.Text = "6200";
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(6, 45);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(64, 20);
            this.textBox9.TabIndex = 16;
            this.textBox9.Text = "F дискр.:";
            // 
            // DFreq_param
            // 
            this.DFreq_param.Location = new System.Drawing.Point(76, 45);
            this.DFreq_param.Name = "DFreq_param";
            this.DFreq_param.Size = new System.Drawing.Size(64, 20);
            this.DFreq_param.TabIndex = 17;
            this.DFreq_param.Text = "32800";
            this.DFreq_param.TextChanged += new System.EventHandler(this.DFreq_param_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox22);
            this.groupBox3.Controls.Add(this.textBox24);
            this.groupBox3.Controls.Add(this.success_param);
            this.groupBox3.Controls.Add(this.textBox18);
            this.groupBox3.Controls.Add(this.textBox19);
            this.groupBox3.Controls.Add(this.DecodedBits_param);
            this.groupBox3.Controls.Add(this.textBox17);
            this.groupBox3.Controls.Add(this.RandomData_check);
            this.groupBox3.Controls.Add(this.textBox14);
            this.groupBox3.Controls.Add(this.textBox16);
            this.groupBox3.Controls.Add(this.TextBox44);
            this.groupBox3.Controls.Add(this.textBox21);
            this.groupBox3.Controls.Add(this.Bits_param);
            this.groupBox3.Controls.Add(this.SNRmax_param);
            this.groupBox3.Controls.Add(this.textBox7);
            this.groupBox3.Controls.Add(this.CountOfSNRSteps_param);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.CountOfExps_param);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.SNRmin_param);
            this.groupBox3.Location = new System.Drawing.Point(1014, 308);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(188, 230);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры эксперимента:";
            // 
            // textBox17
            // 
            this.textBox17.Enabled = false;
            this.textBox17.Location = new System.Drawing.Point(146, 19);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(32, 20);
            this.textBox17.TabIndex = 42;
            this.textBox17.Text = "биты";
            // 
            // RandomData_check
            // 
            this.RandomData_check.AutoSize = true;
            this.RandomData_check.Location = new System.Drawing.Point(6, 103);
            this.RandomData_check.Name = "RandomData_check";
            this.RandomData_check.Size = new System.Drawing.Size(122, 17);
            this.RandomData_check.TabIndex = 41;
            this.RandomData_check.Text = "Случайные данные";
            this.RandomData_check.UseVisualStyleBackColor = true;
            // 
            // textBox14
            // 
            this.textBox14.Enabled = false;
            this.textBox14.Location = new System.Drawing.Point(146, 204);
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(32, 20);
            this.textBox14.TabIndex = 39;
            this.textBox14.Text = "дБ";
            // 
            // textBox16
            // 
            this.textBox16.Enabled = false;
            this.textBox16.Location = new System.Drawing.Point(146, 178);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(32, 20);
            this.textBox16.TabIndex = 38;
            this.textBox16.Text = "дБ";
            // 
            // TextBox44
            // 
            this.TextBox44.Enabled = false;
            this.TextBox44.Location = new System.Drawing.Point(6, 204);
            this.TextBox44.Name = "TextBox44";
            this.TextBox44.ReadOnly = true;
            this.TextBox44.Size = new System.Drawing.Size(64, 20);
            this.TextBox44.TabIndex = 22;
            this.TextBox44.Text = "SNR to:";
            // 
            // textBox21
            // 
            this.textBox21.Enabled = false;
            this.textBox21.Location = new System.Drawing.Point(6, 19);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(64, 20);
            this.textBox21.TabIndex = 20;
            this.textBox21.Text = "Данные:";
            // 
            // Bits_param
            // 
            this.Bits_param.Location = new System.Drawing.Point(76, 19);
            this.Bits_param.Name = "Bits_param";
            this.Bits_param.Size = new System.Drawing.Size(64, 20);
            this.Bits_param.TabIndex = 21;
            this.Bits_param.Text = "0x27";
            // 
            // SNRmax_param
            // 
            this.SNRmax_param.Location = new System.Drawing.Point(76, 204);
            this.SNRmax_param.Name = "SNRmax_param";
            this.SNRmax_param.Size = new System.Drawing.Size(64, 20);
            this.SNRmax_param.TabIndex = 23;
            this.SNRmax_param.Text = "10";
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(6, 152);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(64, 20);
            this.textBox7.TabIndex = 14;
            this.textBox7.Text = "M:";
            // 
            // CountOfSNRSteps_param
            // 
            this.CountOfSNRSteps_param.Location = new System.Drawing.Point(76, 152);
            this.CountOfSNRSteps_param.Name = "CountOfSNRSteps_param";
            this.CountOfSNRSteps_param.Size = new System.Drawing.Size(64, 20);
            this.CountOfSNRSteps_param.TabIndex = 15;
            this.CountOfSNRSteps_param.Text = "40";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(6, 126);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(64, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "N:";
            // 
            // CountOfExps_param
            // 
            this.CountOfExps_param.Location = new System.Drawing.Point(76, 126);
            this.CountOfExps_param.Name = "CountOfExps_param";
            this.CountOfExps_param.Size = new System.Drawing.Size(64, 20);
            this.CountOfExps_param.TabIndex = 9;
            this.CountOfExps_param.Text = "250";
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(6, 178);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(64, 20);
            this.textBox3.TabIndex = 10;
            this.textBox3.Text = "SNR from:";
            // 
            // SNRmin_param
            // 
            this.SNRmin_param.Location = new System.Drawing.Point(76, 178);
            this.SNRmin_param.Name = "SNRmin_param";
            this.SNRmin_param.Size = new System.Drawing.Size(64, 20);
            this.SNRmin_param.TabIndex = 11;
            this.SNRmin_param.Text = "-10";
            // 
            // Experiment_action
            // 
            this.Experiment_action.Location = new System.Drawing.Point(1014, 544);
            this.Experiment_action.Name = "Experiment_action";
            this.Experiment_action.Size = new System.Drawing.Size(188, 24);
            this.Experiment_action.TabIndex = 7;
            this.Experiment_action.Text = "Эксперимент";
            this.Experiment_action.UseVisualStyleBackColor = true;
            this.Experiment_action.Click += new System.EventHandler(this.Experiment_action_Click);
            // 
            // StatGraph
            // 
            this.StatGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StatGraph.Location = new System.Drawing.Point(12, 308);
            this.StatGraph.Name = "StatGraph";
            this.StatGraph.Size = new System.Drawing.Size(996, 290);
            this.StatGraph.TabIndex = 8;
            this.StatGraph.TabStop = false;
            this.StatGraph.MouseDown += new System.Windows.Forms.MouseEventHandler(this.StatGraph_MouseDown);
            // 
            // Modulation_action
            // 
            this.Modulation_action.Location = new System.Drawing.Point(1014, 248);
            this.Modulation_action.Name = "Modulation_action";
            this.Modulation_action.Size = new System.Drawing.Size(188, 24);
            this.Modulation_action.TabIndex = 9;
            this.Modulation_action.Text = "Модуляция";
            this.Modulation_action.UseVisualStyleBackColor = true;
            this.Modulation_action.Click += new System.EventHandler(this.Modulation_action_Click);
            // 
            // IQ_graph2
            // 
            this.IQ_graph2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IQ_graph2.Location = new System.Drawing.Point(718, 12);
            this.IQ_graph2.Name = "IQ_graph2";
            this.IQ_graph2.Size = new System.Drawing.Size(290, 290);
            this.IQ_graph2.TabIndex = 10;
            this.IQ_graph2.TabStop = false;
            this.IQ_graph2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IQ_graph2_MouseDown);
            // 
            // SaveGraphs_action
            // 
            this.SaveGraphs_action.Location = new System.Drawing.Point(1014, 574);
            this.SaveGraphs_action.Name = "SaveGraphs_action";
            this.SaveGraphs_action.Size = new System.Drawing.Size(188, 24);
            this.SaveGraphs_action.TabIndex = 11;
            this.SaveGraphs_action.Text = "Сохранить все графики";
            this.SaveGraphs_action.UseVisualStyleBackColor = true;
            this.SaveGraphs_action.Click += new System.EventHandler(this.SaveGraphs_action_Click);
            // 
            // Debug_param
            // 
            this.Debug_param.AutoSize = true;
            this.Debug_param.Location = new System.Drawing.Point(1014, 225);
            this.Debug_param.Name = "Debug_param";
            this.Debug_param.Size = new System.Drawing.Size(58, 17);
            this.Debug_param.TabIndex = 44;
            this.Debug_param.Text = "Debug";
            this.Debug_param.UseVisualStyleBackColor = true;
            this.Debug_param.CheckedChanged += new System.EventHandler(this.Debug_param_CheckedChanged_1);
            // 
            // textBox18
            // 
            this.textBox18.Enabled = false;
            this.textBox18.Location = new System.Drawing.Point(146, 45);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(32, 20);
            this.textBox18.TabIndex = 45;
            this.textBox18.Text = "биты";
            // 
            // textBox19
            // 
            this.textBox19.Enabled = false;
            this.textBox19.Location = new System.Drawing.Point(6, 45);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(64, 20);
            this.textBox19.TabIndex = 43;
            this.textBox19.Text = "Итог:";
            // 
            // DecodedBits_param
            // 
            this.DecodedBits_param.Location = new System.Drawing.Point(76, 45);
            this.DecodedBits_param.Name = "DecodedBits_param";
            this.DecodedBits_param.ReadOnly = true;
            this.DecodedBits_param.Size = new System.Drawing.Size(64, 20);
            this.DecodedBits_param.TabIndex = 44;
            // 
            // textBox22
            // 
            this.textBox22.Enabled = false;
            this.textBox22.Location = new System.Drawing.Point(146, 71);
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(32, 20);
            this.textBox22.TabIndex = 48;
            this.textBox22.Text = "%";
            // 
            // textBox24
            // 
            this.textBox24.Enabled = false;
            this.textBox24.Location = new System.Drawing.Point(6, 71);
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(64, 20);
            this.textBox24.TabIndex = 46;
            this.textBox24.Text = "Успех:";
            // 
            // success_param
            // 
            this.success_param.Location = new System.Drawing.Point(76, 71);
            this.success_param.Name = "success_param";
            this.success_param.ReadOnly = true;
            this.success_param.Size = new System.Drawing.Size(64, 20);
            this.success_param.TabIndex = 47;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1212, 624);
            this.Controls.Add(this.Debug_param);
            this.Controls.Add(this.SaveGraphs_action);
            this.Controls.Add(this.IQ_graph2);
            this.Controls.Add(this.Modulation_action);
            this.Controls.Add(this.StatGraph);
            this.Controls.Add(this.Experiment_action);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ProgressGraph);
            this.Controls.Add(this.IQ_graph);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.IQ_graph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressGraph)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IQ_graph2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox IQ_graph;
        private System.Windows.Forms.PictureBox ProgressGraph;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox SNR_param;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox TexBox;
        private System.Windows.Forms.TextBox Amp_param;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox MainFreq_param;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox DFreq_param;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox RandomData_check;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox TextBox44;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox Bits_param;
        private System.Windows.Forms.TextBox SNRmax_param;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox CountOfSNRSteps_param;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox CountOfExps_param;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox SNRmin_param;
        private System.Windows.Forms.Button Experiment_action;
        private System.Windows.Forms.PictureBox StatGraph;
        private System.Windows.Forms.Button Modulation_action;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox Bitrate_param;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox CountsPerBit_param;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox Beta_param;
        private System.Windows.Forms.PictureBox IQ_graph2;
        private System.Windows.Forms.Button SaveGraphs_action;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.CheckBox Debug_param;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox DecodedBits_param;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox success_param;
    }
}

