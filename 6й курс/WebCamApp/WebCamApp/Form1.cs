﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;

namespace WebCamApp
{
    public partial class Form1 : Form
    {
        bool started = false;
        bool first_start = true;
        Image img;

        public Form1()
        {
            InitializeComponent();
        }

        private void StartBT_Click(object sender, EventArgs e)
        {
            if (first_start)
            {
                first_start = false;
                MainTimer.Start();
                StartBT.Text = "Стоп";

                VideoCapture capture = new VideoCapture();

                using (var nextFrame = capture.QueryFrame())
                {
                    if (nextFrame != null)
                    {
                        var img = nextFrame.ToImage<Bgr, Byte>();
                        var smth = img[1][2, 3];
                    }
                }

                return;
            }
            if (started)
            {
                StartBT.Text = "Стоп";
                started = false;
                return;
            }
            else
            {
                StartBT.Text = "Продолжить";
                started = true;
                return;
            }
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {

        }
    }
}
