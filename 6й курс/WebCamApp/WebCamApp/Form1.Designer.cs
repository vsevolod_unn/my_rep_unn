﻿namespace WebCamApp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MainScene = new System.Windows.Forms.PictureBox();
            this.StartBT = new System.Windows.Forms.Button();
            this.PauseBT = new System.Windows.Forms.Button();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.MainScene)).BeginInit();
            this.SuspendLayout();
            // 
            // MainScene
            // 
            this.MainScene.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainScene.Location = new System.Drawing.Point(12, 12);
            this.MainScene.Name = "MainScene";
            this.MainScene.Size = new System.Drawing.Size(800, 450);
            this.MainScene.TabIndex = 0;
            this.MainScene.TabStop = false;
            // 
            // StartBT
            // 
            this.StartBT.Location = new System.Drawing.Point(715, 475);
            this.StartBT.Name = "StartBT";
            this.StartBT.Size = new System.Drawing.Size(97, 25);
            this.StartBT.TabIndex = 1;
            this.StartBT.Text = "Старт";
            this.StartBT.UseVisualStyleBackColor = true;
            this.StartBT.Click += new System.EventHandler(this.StartBT_Click);
            // 
            // PauseBT
            // 
            this.PauseBT.Enabled = false;
            this.PauseBT.Location = new System.Drawing.Point(612, 475);
            this.PauseBT.Name = "PauseBT";
            this.PauseBT.Size = new System.Drawing.Size(97, 25);
            this.PauseBT.TabIndex = 2;
            this.PauseBT.Text = "Стоп";
            this.PauseBT.UseVisualStyleBackColor = true;
            // 
            // MainTimer
            // 
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 512);
            this.Controls.Add(this.PauseBT);
            this.Controls.Add(this.StartBT);
            this.Controls.Add(this.MainScene);
            this.Name = "Form1";
            this.Text = "WebCam";
            ((System.ComponentModel.ISupportInitialize)(this.MainScene)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox MainScene;
        private System.Windows.Forms.Button StartBT;
        private System.Windows.Forms.Button PauseBT;
        private System.Windows.Forms.Timer MainTimer;
    }
}

