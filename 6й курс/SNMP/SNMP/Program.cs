﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using SnmpSharpNet;

namespace SNMP
{
    class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            Console.WriteLine("Введите ключ");
            while (true)
            {
                string key = Console.ReadLine();
                new Thread(new ParameterizedThreadStart(Dialog)).Start((object)key);

                /*
                pdu.VbList.Add(new Oid(key), new OctetString("Some other value"));
                //pdu.VbList.Add(new Oid("1.3.6.1.2.1.1.1.0"), new OctetString("Some other value"));
                // Set a value to integer
                //pdu.VbList.Add(new Oid("1.3.6.1.2.1.67.1.1.1.1.5.0"), new Integer32(500));
                // Set a value to unsigned integer
                //pdu.VbList.Add(new Oid("1.3.6.1.2.1.67.1.1.1.1.6.0"), new UInteger32(101));
                // Set Agent security parameters
                AgentParameters aparam = new AgentParameters(SnmpVersion.Ver2, new OctetString("rpublic"));
                // Response packet
                SnmpV2Packet response;
                try
                {
                    // Send request and wait for response
                    response = target.Request(pdu, aparam) as SnmpV2Packet;
                }
                catch (Exception ex)
                {
                    // If exception happens, it will be returned here
                    Console.WriteLine(String.Format("Request failed with exception: {0}", ex.Message));
                    target.Close();
                    return;
                }
                // Make sure we received a response
                if (response == null)
                {
                    Console.WriteLine("Error in sending SNMP request.");
                }
                else
                {
                    // Check if we received an SNMP error from the agent
                    if (response.Pdu.ErrorStatus != 0)
                    {
                        Console.WriteLine(String.Format("SNMP agent returned ErrorStatus {0} on index {1}",
                          response.Pdu.ErrorStatus, response.Pdu.ErrorIndex));
                    }
                    else
                    {
                        // Everything is ok. Agent will return the new value for the OID we changed
                        Console.WriteLine(String.Format("Agent response {0}: {1}",
                          response.Pdu[0].Oid.ToString(), response.Pdu[0].Value.ToString()));
                    }
                }
                */

            }
        }

        public static void Dialog(object _key)
        {
            UdpTarget target = new UdpTarget((IPAddress)new IpAddress("192.168.1.1"));
            // Create a SET PDU
            Pdu pdu = new Pdu(PduType.Get);

            string key = (string)_key;

            while (true)
            {
                string s;
                s = pdu.VbCount.ToString();
                Console.WriteLine(s);

                pdu.VbList.Add(new Oid(key), new Integer32(500));

                s = pdu.VbCount.ToString();
                Console.WriteLine(s);

                AgentParameters aparam = new AgentParameters(SnmpVersion.Ver2, new OctetString("rpublic"));
                // Response packet
                SnmpV2Packet response;
                try
                {
                    // Send request and wait for response
                    response = target.Request(pdu, aparam) as SnmpV2Packet;
                }
                catch (Exception ex)
                {
                    // If exception happens, it will be returned here
                    Console.WriteLine(String.Format("Request failed with exception: {0}", ex.Message));
                    target.Close();
                    return;
                }
                // Make sure we received a response
                if (response == null)
                {
                    Console.WriteLine("Error in sending SNMP request.");
                    break;
                }
                else
                {
                    // Check if we received an SNMP error from the agent
                    if (response.Pdu.ErrorStatus != 0)
                    {
                        Console.WriteLine(String.Format("SNMP agent returned ErrorStatus {0} on index {1}",
                          response.Pdu.ErrorStatus, response.Pdu.ErrorIndex));
                        break;
                    }
                    else
                    {
                        //for (int i = 0; i < response.Pdu.ToArray<object>().Length; i++)
                        // Everything is ok. Agent will return the new value for the OID we changed
                        Console.WriteLine(String.Format("Agent response {0}: {1}",
                          response.Pdu[0].Oid.ToString(), response.Pdu[0].Value.ToString()));
                        break;
                    }
                }
            }
        }
    }
}



/*static void Main(string[] args)
{
    // SNMP community name
    OctetString community = new OctetString("public");
    // Define agent parameters class
    AgentParameters param = new AgentParameters(community);
    // Set SNMP version to 1 (or 2)
    param.Version = SnmpVersion.Ver1;
    // Construct the agent address object
    // IpAddress class is easy to use here because
    //  it will try to resolve constructor parameter if it doesn't
    //  parse to an IP address
    IpAddress agent = new IpAddress("192.168.1.9");
    // Construct target
    UdpTarget target = new UdpTarget((IPAddress)agent, 161, 2000, 1);
    // Pdu class used for all requests
    Pdu pdu = new Pdu(PduType.Get);
    pdu.VbList.Add("19.45.26.458.1"); //sysDescr
    /*pdu.VbList.Add("1.3.6.1.2.1.1.1.0"); //sysDescr
    pdu.VbList.Add("1.3.6.1.2.1.1.2.0"); //sysObjectID
    pdu.VbList.Add("1.3.6.1.2.1.1.3.0"); //sysUpTime
    pdu.VbList.Add("1.3.6.1.2.1.1.4.0"); //sysContact
    pdu.VbList.Add("1.3.6.1.2.1.1.5.0"); //sysName
    // Make SNMP request
    SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);
    // If result is null then agent didn't reply or we couldn't parse the reply.
    if (result != null)
    {
        // ErrorStatus other then 0 is an error returned by
        // the Agent - see SnmpConstants for error definitions
        if (result.Pdu.ErrorStatus != 0)
        {
            // agent reported an error with the request
            Console.WriteLine("Error in SNMP reply. Error {0} index {1}",
                result.Pdu.ErrorStatus,
                result.Pdu.ErrorIndex);
        }
        else
        {
            // Reply variables are returned in the same order as they were added
            //  to the VbList
            Console.WriteLine("sysDescr({0}) ({1}): {2}",
                result.Pdu.VbList[0].Oid.ToString(),
                SnmpConstants.GetTypeName(result.Pdu.VbList[0].Value.Type),
                result.Pdu.VbList[0].Value.ToString());
            Console.WriteLine("sysObjectID({0}) ({1}): {2}",
                result.Pdu.VbList[1].Oid.ToString(),
                SnmpConstants.GetTypeName(result.Pdu.VbList[1].Value.Type),
                result.Pdu.VbList[1].Value.ToString());
            Console.WriteLine("sysUpTime({0}) ({1}): {2}",
                result.Pdu.VbList[2].Oid.ToString(),
                SnmpConstants.GetTypeName(result.Pdu.VbList[2].Value.Type),
                result.Pdu.VbList[2].Value.ToString());
            Console.WriteLine("sysContact({0}) ({1}): {2}",
                result.Pdu.VbList[3].Oid.ToString(),
                SnmpConstants.GetTypeName(result.Pdu.VbList[3].Value.Type),
                result.Pdu.VbList[3].Value.ToString());
            Console.WriteLine("sysName({0}) ({1}): {2}",
                result.Pdu.VbList[4].Oid.ToString(),
                SnmpConstants.GetTypeName(result.Pdu.VbList[4].Value.Type),
                result.Pdu.VbList[4].Value.ToString());
        }
    }
    else
    {
        Console.WriteLine("No response received from SNMP agent.");
    }
    target.Close();
    Console.ReadKey();
}*/


