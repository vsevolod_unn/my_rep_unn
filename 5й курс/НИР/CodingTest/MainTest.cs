using Microsoft.VisualStudio.TestTools.UnitTesting;
using Coding;

namespace CodingTest
{
    [TestClass]
    public class CoderTest
    {
        /// 0 - ��� �������
        /// 1 - ������� ������ �����
        /// 2 - ����������� ������ ������ ����
        /// 3 - �� ����� ����� �����������
        [TestMethod]
        public void TestignCodeError0()
        {
            string text = "test_string";
            double percentage = 0.1;
            Coder test_coder = new Coder(text, percentage);
            test_coder.ham74 = new Ham74(text, percentage);
            int res = test_coder.code();
            Assert.AreEqual(res, 0);
            Assert.AreEqual(text, test_coder.ham74.text);
        }
        [TestMethod]
        public void TestignCodeError1()
        {
            string text = "";
            double percentage = 0.1;
            Coder test_coder = new Coder(text, percentage);
            test_coder.ham74 = new Ham74(text, percentage);
            int res = test_coder.code();
            Assert.AreEqual(res, 1);
        }
        [TestMethod]
        public void TestignCodeError2()
        {
            string text = "test_string";
            double percentage = -0.1;
            Coder test_coder = new Coder(text, percentage);
            test_coder.ham74 = new Ham74(text, percentage);
            int res = test_coder.code();
            Assert.AreEqual(res, 2);
        }
        [TestMethod]
        public void TestignCodeError3()
        {
            string text = "test_string";
            double percentage = 0.1;
            Coder test_coder = new Coder(text, percentage);
            int res = test_coder.code();
            Assert.AreEqual(res, 3);
        }

        /// 0 - ��� �������
        /// 1 - �� ����� ����� �����������
        /// 2 - ������������ ����� �����
        [TestMethod]
        public void TestignNoiseError0()
        {
            string text = "test_string";
            double percentage = 0.1;
            Coder test_coder = new Coder(text, percentage);
            test_coder.ham74 = new Ham74(text, percentage);
            test_coder.code();
            int res = test_coder.noise();
            Assert.AreEqual(res, 0);
        }
        [TestMethod]
        public void TestignNoiseError1()
        {
            string text = "test_string";
            double percentage = 0.1;
            Coder test_coder = new Coder(text, percentage);
            test_coder.code();
            int res = test_coder.noise();
            Assert.AreEqual(res, 1);
        }
        [TestMethod]
        public void TestignNoiseError2()
        {
            int res;
            string text = "test_string";
            double percentage = 0;
            Coder test_coder = new Coder(text, percentage);
            test_coder.ham74 = new Ham74(text, percentage);
            res = test_coder.code();
            test_coder.ham74.coded_text += "1";
            res = test_coder.noise();
            Assert.AreEqual(res, 2);
        }

        /// 0 - ��� �������
        /// 1 - �� ����� ����� �����������
        /// 2 - ������������� �� �������
        [TestMethod]
        public void TestignDecodeError0()
        {
            string text = "test_string", decoded_text;
            double percentage = 0;
            Coder test_coder = new Coder(text, percentage);
            test_coder.ham74 = new Ham74(text, percentage);
            test_coder.code();
            test_coder.noise();
            int res = test_coder.decode();
            decoded_text = test_coder.ham74.decoded_text;
            Assert.AreEqual(res, 0);
            Assert.AreEqual(text, decoded_text);
        }
        [TestMethod]
        public void TestignDecodeError1()
        {
            string text = "test_string";
            double percentage = 0.1;
            Coder test_coder = new Coder(text, percentage);
            test_coder.code();
            test_coder.noise();
            int res = test_coder.decode();
            Assert.AreEqual(res, 1);
        }
        [TestMethod]
        public void TestignDecodeError2()
        {
            string text = "test_string";
            double percentage = 0;
            Coder test_coder = new Coder(text, percentage);
            test_coder.ham74 = new Ham74(text, percentage);
            test_coder.code();
            test_coder.noise();
            test_coder.ham74.text = "tets_string";
            int res = test_coder.decode();
            Assert.AreEqual(res, 2);
        }

        /// 0 - ��� �������
        /// 1 - ������� ������ ������
        /// 2 - ������ �� � ��� �������
        [TestMethod]
        public void Testign_cdtoi_Error0()
        {
            Coder test_coder = new Coder();
            int[] test_arr = new int[] { 1, 0, 1, 0 };
            int res, int_res;
            res = test_coder.cdtoi(test_arr, out int_res);
            Assert.AreEqual(res, 0);
            Assert.AreEqual(int_res, 10);
        }
        [TestMethod]
        public void Testign_cdtoi_Error1()
        {
            Coder test_coder = new Coder();
            int[] test_arr = new int[] { };
            int res, int_res;
            res = test_coder.cdtoi(test_arr, out int_res);
            Assert.AreEqual(res, 1);
            Assert.AreEqual(int_res, 0);
        }
        [TestMethod]
        public void Testign_cdtoi_Error2()
        {
            Coder test_coder = new Coder();
            int[] test_arr = new int[] { -1, 0, 1, 0 };
            int res, int_res;
            res = test_coder.cdtoi(test_arr, out int_res);
            Assert.AreEqual(res, 2);
            Assert.AreEqual(int_res, 0);
        }

        /// 0 - ��� �������
        /// 1 - ������ ������
        /// 2 - ������ ������� 
        /// 3 - �������������� �������� ������� � �������
        /// 4 - ������������ ������ ������� ��� �������
        [TestMethod]
        public void Testign_vect_mult_matr_Error0()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 1, 1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[] { 1, 0 };
            matr[1] = new int[] { 0, 1 };
            res = test_coder.vect_mult_matr(vect, matr, out returned);
            Assert.AreEqual(res, 0);
            for (int i = 0; i < 2; i++)
            {
                Assert.AreEqual(returned[i], 1);
            }
        }
        [TestMethod]
        public void Testign_vect_mult_matr_Error1()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[0];
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[2];
            matr[1] = new int[2];
            res = test_coder.vect_mult_matr(vect, matr, out returned);
            Assert.AreEqual(res, 1);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }
        [TestMethod]
        public void Testign_vect_mult_matr_Error2()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 1, 1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[0];
            matr[1] = new int[0];
            res = test_coder.vect_mult_matr(vect, matr, out returned);
            Assert.AreEqual(res, 2);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }
        [TestMethod]
        public void Testign_vect_mult_matr_Error3()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 1, 0, 1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[2];
            matr[1] = new int[2];
            res = test_coder.vect_mult_matr(vect, matr, out returned);
            Assert.AreEqual(res, 3);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }
        [TestMethod]
        public void Testign_vect_mult_matr_Error4()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 11, -1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[2];
            matr[1] = new int[2];
            res = test_coder.vect_mult_matr(vect, matr, out returned);
            Assert.AreEqual(res, 4);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }

        /// 0 - ��� �������
        /// 1 - ������ ������
        /// 2 - ������ ������� 
        /// 3 - �������������� �������� ������� � �������
        /// 4 - ������������ ������ ������� ��� �������
        [TestMethod]
        public void Testign_matr_mult_vect_Error0()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 1, 1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[] { 1, 0 };
            matr[1] = new int[] { 0, 1 };
            res = test_coder.matr_mult_vect(matr, vect, out returned);
            Assert.AreEqual(res, 0);
            for (int i = 0; i < 2; i++)
            {
                Assert.AreEqual(returned[i], vect[i]);
            }
        }
        [TestMethod]
        public void Testign_matr_mult_vect_Error1()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[0];
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[2];
            matr[1] = new int[2];
            res = test_coder.matr_mult_vect(matr, vect, out returned);
            Assert.AreEqual(res, 1);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }
        [TestMethod]
        public void Testign_matr_mult_vect_Error2()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 1, 1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[0];
            matr[1] = new int[0];
            res = test_coder.matr_mult_vect(matr, vect, out returned);
            Assert.AreEqual(res, 2);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }
        [TestMethod]
        public void Testign_matr_mult_vect_Error3()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 1, 0, 1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[2];
            matr[1] = new int[2];
            res = test_coder.matr_mult_vect(matr, vect, out returned);
            Assert.AreEqual(res, 3);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }
        [TestMethod]
        public void Testign_matr_mult_vect_Error4()
        {
            Coder test_coder = new Coder();
            int res;
            int[] vect = new int[] { 11, -1 };
            int[] returned;
            int[][] matr;
            matr = new int[2][];
            matr[0] = new int[2];
            matr[1] = new int[2];
            res = test_coder.matr_mult_vect(matr, vect, out returned);
            Assert.AreEqual(res, 4);
            for (int i = 0; i < 1; i++)
            {
                Assert.AreEqual(returned[i], 0);
            }
        }
    }
}
