﻿namespace Coding
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MainTextTB = new System.Windows.Forms.RichTextBox();
            this.DecodedTextTB = new System.Windows.Forms.RichTextBox();
            this.NoisePercentageTB = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BlocksCountTB = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.BitsCountTB = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.TextLengthTB = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.DoAllBT = new System.Windows.Forms.Button();
            this.SuccessfulityTB = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ErrorPocketTB = new System.Windows.Forms.TextBox();
            this.ErrorPocketCB = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SNR_TB = new System.Windows.Forms.TextBox();
            this.CodecParam4_desc = new System.Windows.Forms.TextBox();
            this.CodecParam3_desc = new System.Windows.Forms.TextBox();
            this.CodecParam4_TB = new System.Windows.Forms.TextBox();
            this.CodecParam3_TB = new System.Windows.Forms.TextBox();
            this.CodecParam2_desc = new System.Windows.Forms.TextBox();
            this.CodecParam2_TB = new System.Windows.Forms.TextBox();
            this.ham74RB = new System.Windows.Forms.RadioButton();
            this.golayRB = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Laby_RB = new System.Windows.Forms.RadioButton();
            this.TurboRS_RB = new System.Windows.Forms.RadioButton();
            this.TurboHamRB = new System.Windows.Forms.RadioButton();
            this.ReedSolomonRB = new System.Windows.Forms.RadioButton();
            this.FileBT = new System.Windows.Forms.Button();
            this.ErrorsCountTB = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.CountingTimer = new System.Windows.Forms.Timer(this.components);
            this.CountingProgress = new System.Windows.Forms.ProgressBar();
            this.Random_str_BT = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.SymbolCountTB = new System.Windows.Forms.TextBox();
            this.ModulationBT = new System.Windows.Forms.Button();
            this.SendCodedBits_CB = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTextTB
            // 
            this.MainTextTB.Location = new System.Drawing.Point(4, 20);
            this.MainTextTB.Margin = new System.Windows.Forms.Padding(2);
            this.MainTextTB.Name = "MainTextTB";
            this.MainTextTB.Size = new System.Drawing.Size(262, 227);
            this.MainTextTB.TabIndex = 0;
            this.MainTextTB.Text = "ИТФИ";
            this.MainTextTB.TextChanged += new System.EventHandler(this.MainTextTB_TextChanged);
            // 
            // DecodedTextTB
            // 
            this.DecodedTextTB.Location = new System.Drawing.Point(4, 20);
            this.DecodedTextTB.Margin = new System.Windows.Forms.Padding(2);
            this.DecodedTextTB.Name = "DecodedTextTB";
            this.DecodedTextTB.Size = new System.Drawing.Size(262, 264);
            this.DecodedTextTB.TabIndex = 4;
            this.DecodedTextTB.Text = "";
            // 
            // NoisePercentageTB
            // 
            this.NoisePercentageTB.Location = new System.Drawing.Point(106, 22);
            this.NoisePercentageTB.Margin = new System.Windows.Forms.Padding(2);
            this.NoisePercentageTB.Name = "NoisePercentageTB";
            this.NoisePercentageTB.ReadOnly = true;
            this.NoisePercentageTB.Size = new System.Drawing.Size(86, 20);
            this.NoisePercentageTB.TabIndex = 7;
            this.NoisePercentageTB.Text = "0,05";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(8, 24);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(94, 13);
            this.textBox2.TabIndex = 8;
            this.textBox2.Text = "p ошибки на бит:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BlocksCountTB);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.BitsCountTB);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.TextLengthTB);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Location = new System.Drawing.Point(279, 162);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(196, 93);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры текста";
            // 
            // BlocksCountTB
            // 
            this.BlocksCountTB.Location = new System.Drawing.Point(106, 68);
            this.BlocksCountTB.Margin = new System.Windows.Forms.Padding(2);
            this.BlocksCountTB.Name = "BlocksCountTB";
            this.BlocksCountTB.ReadOnly = true;
            this.BlocksCountTB.Size = new System.Drawing.Size(86, 20);
            this.BlocksCountTB.TabIndex = 14;
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(8, 71);
            this.textBox8.Margin = new System.Windows.Forms.Padding(2);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(79, 13);
            this.textBox8.TabIndex = 15;
            this.textBox8.Text = "Число блоков:";
            // 
            // BitsCountTB
            // 
            this.BitsCountTB.Location = new System.Drawing.Point(106, 46);
            this.BitsCountTB.Margin = new System.Windows.Forms.Padding(2);
            this.BitsCountTB.Name = "BitsCountTB";
            this.BitsCountTB.ReadOnly = true;
            this.BitsCountTB.Size = new System.Drawing.Size(86, 20);
            this.BitsCountTB.TabIndex = 12;
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(8, 48);
            this.textBox6.Margin = new System.Windows.Forms.Padding(2);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(79, 13);
            this.textBox6.TabIndex = 13;
            this.textBox6.Text = "Число битов:";
            // 
            // TextLengthTB
            // 
            this.TextLengthTB.Location = new System.Drawing.Point(106, 23);
            this.TextLengthTB.Margin = new System.Windows.Forms.Padding(2);
            this.TextLengthTB.Name = "TextLengthTB";
            this.TextLengthTB.ReadOnly = true;
            this.TextLengthTB.Size = new System.Drawing.Size(86, 20);
            this.TextLengthTB.TabIndex = 10;
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(8, 25);
            this.textBox4.Margin = new System.Windows.Forms.Padding(2);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(79, 13);
            this.textBox4.TabIndex = 11;
            this.textBox4.Text = "Длина текста:";
            // 
            // DoAllBT
            // 
            this.DoAllBT.Location = new System.Drawing.Point(384, 525);
            this.DoAllBT.Margin = new System.Windows.Forms.Padding(2);
            this.DoAllBT.Name = "DoAllBT";
            this.DoAllBT.Size = new System.Drawing.Size(90, 22);
            this.DoAllBT.TabIndex = 10;
            this.DoAllBT.Text = "Выполнить";
            this.DoAllBT.UseVisualStyleBackColor = true;
            this.DoAllBT.Click += new System.EventHandler(this.DoAllBT_Click);
            // 
            // SuccessfulityTB
            // 
            this.SuccessfulityTB.Location = new System.Drawing.Point(106, 19);
            this.SuccessfulityTB.Margin = new System.Windows.Forms.Padding(2);
            this.SuccessfulityTB.Name = "SuccessfulityTB";
            this.SuccessfulityTB.ReadOnly = true;
            this.SuccessfulityTB.Size = new System.Drawing.Size(86, 20);
            this.SuccessfulityTB.TabIndex = 16;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(8, 21);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(79, 13);
            this.textBox3.TabIndex = 17;
            this.textBox3.Text = "Успешность, %:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.MainTextTB);
            this.groupBox2.Location = new System.Drawing.Point(4, 3);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(270, 252);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Исходный текст";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DecodedTextTB);
            this.groupBox3.Location = new System.Drawing.Point(4, 259);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(270, 288);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Декодированный текст";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ErrorPocketTB);
            this.groupBox4.Controls.Add(this.ErrorPocketCB);
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Controls.Add(this.SNR_TB);
            this.groupBox4.Controls.Add(this.CodecParam4_desc);
            this.groupBox4.Controls.Add(this.CodecParam3_desc);
            this.groupBox4.Controls.Add(this.CodecParam4_TB);
            this.groupBox4.Controls.Add(this.CodecParam3_TB);
            this.groupBox4.Controls.Add(this.CodecParam2_desc);
            this.groupBox4.Controls.Add(this.CodecParam2_TB);
            this.groupBox4.Controls.Add(this.textBox2);
            this.groupBox4.Controls.Add(this.NoisePercentageTB);
            this.groupBox4.Location = new System.Drawing.Point(279, 259);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(196, 163);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Параметры кодирования";
            // 
            // ErrorPocketTB
            // 
            this.ErrorPocketTB.Location = new System.Drawing.Point(106, 68);
            this.ErrorPocketTB.Margin = new System.Windows.Forms.Padding(2);
            this.ErrorPocketTB.Name = "ErrorPocketTB";
            this.ErrorPocketTB.ReadOnly = true;
            this.ErrorPocketTB.Size = new System.Drawing.Size(86, 20);
            this.ErrorPocketTB.TabIndex = 30;
            this.ErrorPocketTB.Text = "4";
            // 
            // ErrorPocketCB
            // 
            this.ErrorPocketCB.AutoSize = true;
            this.ErrorPocketCB.Location = new System.Drawing.Point(8, 69);
            this.ErrorPocketCB.Name = "ErrorPocketCB";
            this.ErrorPocketCB.Size = new System.Drawing.Size(98, 17);
            this.ErrorPocketCB.TabIndex = 29;
            this.ErrorPocketCB.Text = "Пакет ошибок";
            this.ErrorPocketCB.UseVisualStyleBackColor = true;
            this.ErrorPocketCB.CheckedChanged += new System.EventHandler(this.ErrorPocketCB_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(8, 46);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(79, 13);
            this.textBox1.TabIndex = 28;
            this.textBox1.Text = "SNR, дБ";
            // 
            // SNR_TB
            // 
            this.SNR_TB.Location = new System.Drawing.Point(106, 44);
            this.SNR_TB.Margin = new System.Windows.Forms.Padding(2);
            this.SNR_TB.Name = "SNR_TB";
            this.SNR_TB.Size = new System.Drawing.Size(86, 20);
            this.SNR_TB.TabIndex = 27;
            this.SNR_TB.Text = "2";
            this.SNR_TB.TextChanged += new System.EventHandler(this.SNR_TB_TextChanged);
            // 
            // CodecParam4_desc
            // 
            this.CodecParam4_desc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodecParam4_desc.Location = new System.Drawing.Point(8, 142);
            this.CodecParam4_desc.Margin = new System.Windows.Forms.Padding(2);
            this.CodecParam4_desc.Name = "CodecParam4_desc";
            this.CodecParam4_desc.ReadOnly = true;
            this.CodecParam4_desc.Size = new System.Drawing.Size(94, 13);
            this.CodecParam4_desc.TabIndex = 26;
            this.CodecParam4_desc.Text = "Pow:";
            this.CodecParam4_desc.Visible = false;
            // 
            // CodecParam3_desc
            // 
            this.CodecParam3_desc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodecParam3_desc.Location = new System.Drawing.Point(8, 119);
            this.CodecParam3_desc.Margin = new System.Windows.Forms.Padding(2);
            this.CodecParam3_desc.Name = "CodecParam3_desc";
            this.CodecParam3_desc.ReadOnly = true;
            this.CodecParam3_desc.Size = new System.Drawing.Size(79, 13);
            this.CodecParam3_desc.TabIndex = 12;
            this.CodecParam3_desc.Text = "K:";
            this.CodecParam3_desc.Visible = false;
            // 
            // CodecParam4_TB
            // 
            this.CodecParam4_TB.Location = new System.Drawing.Point(106, 139);
            this.CodecParam4_TB.Margin = new System.Windows.Forms.Padding(2);
            this.CodecParam4_TB.Name = "CodecParam4_TB";
            this.CodecParam4_TB.Size = new System.Drawing.Size(86, 20);
            this.CodecParam4_TB.TabIndex = 25;
            this.CodecParam4_TB.Text = "4";
            this.CodecParam4_TB.Visible = false;
            // 
            // CodecParam3_TB
            // 
            this.CodecParam3_TB.Location = new System.Drawing.Point(106, 116);
            this.CodecParam3_TB.Margin = new System.Windows.Forms.Padding(2);
            this.CodecParam3_TB.Name = "CodecParam3_TB";
            this.CodecParam3_TB.Size = new System.Drawing.Size(86, 20);
            this.CodecParam3_TB.TabIndex = 11;
            this.CodecParam3_TB.Text = "9";
            this.CodecParam3_TB.Visible = false;
            // 
            // CodecParam2_desc
            // 
            this.CodecParam2_desc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodecParam2_desc.Location = new System.Drawing.Point(8, 96);
            this.CodecParam2_desc.Margin = new System.Windows.Forms.Padding(2);
            this.CodecParam2_desc.Name = "CodecParam2_desc";
            this.CodecParam2_desc.ReadOnly = true;
            this.CodecParam2_desc.Size = new System.Drawing.Size(94, 13);
            this.CodecParam2_desc.TabIndex = 10;
            this.CodecParam2_desc.Text = "N:";
            this.CodecParam2_desc.Visible = false;
            // 
            // CodecParam2_TB
            // 
            this.CodecParam2_TB.Location = new System.Drawing.Point(106, 94);
            this.CodecParam2_TB.Margin = new System.Windows.Forms.Padding(2);
            this.CodecParam2_TB.Name = "CodecParam2_TB";
            this.CodecParam2_TB.Size = new System.Drawing.Size(86, 20);
            this.CodecParam2_TB.TabIndex = 9;
            this.CodecParam2_TB.Text = "15";
            this.CodecParam2_TB.Visible = false;
            this.CodecParam2_TB.TextChanged += new System.EventHandler(this.CodecParam2_TB_TextChanged);
            // 
            // ham74RB
            // 
            this.ham74RB.AutoSize = true;
            this.ham74RB.Location = new System.Drawing.Point(6, 21);
            this.ham74RB.Margin = new System.Windows.Forms.Padding(2);
            this.ham74RB.Name = "ham74RB";
            this.ham74RB.Size = new System.Drawing.Size(132, 17);
            this.ham74RB.TabIndex = 19;
            this.ham74RB.Text = "Код Хэмминга (7,4,3)";
            this.ham74RB.UseVisualStyleBackColor = true;
            this.ham74RB.CheckedChanged += new System.EventHandler(this.ham74RB_CheckedChanged);
            // 
            // golayRB
            // 
            this.golayRB.AutoSize = true;
            this.golayRB.Location = new System.Drawing.Point(6, 43);
            this.golayRB.Margin = new System.Windows.Forms.Padding(2);
            this.golayRB.Name = "golayRB";
            this.golayRB.Size = new System.Drawing.Size(122, 17);
            this.golayRB.TabIndex = 20;
            this.golayRB.Text = "Код Голея (24,12,8)";
            this.golayRB.UseVisualStyleBackColor = true;
            this.golayRB.CheckedChanged += new System.EventHandler(this.golayRB_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Laby_RB);
            this.groupBox5.Controls.Add(this.TurboRS_RB);
            this.groupBox5.Controls.Add(this.TurboHamRB);
            this.groupBox5.Controls.Add(this.ReedSolomonRB);
            this.groupBox5.Controls.Add(this.golayRB);
            this.groupBox5.Controls.Add(this.ham74RB);
            this.groupBox5.Location = new System.Drawing.Point(279, 3);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(196, 155);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Выбор кодирования";
            // 
            // Laby_RB
            // 
            this.Laby_RB.AutoSize = true;
            this.Laby_RB.Location = new System.Drawing.Point(6, 86);
            this.Laby_RB.Margin = new System.Windows.Forms.Padding(2);
            this.Laby_RB.Name = "Laby_RB";
            this.Laby_RB.Size = new System.Drawing.Size(73, 17);
            this.Laby_RB.TabIndex = 24;
            this.Laby_RB.Text = "Код Лаби";
            this.Laby_RB.UseVisualStyleBackColor = true;
            this.Laby_RB.CheckedChanged += new System.EventHandler(this.Laby_RB_CheckedChanged);
            // 
            // TurboRS_RB
            // 
            this.TurboRS_RB.AutoSize = true;
            this.TurboRS_RB.Location = new System.Drawing.Point(6, 128);
            this.TurboRS_RB.Margin = new System.Windows.Forms.Padding(2);
            this.TurboRS_RB.Name = "TurboRS_RB";
            this.TurboRS_RB.Size = new System.Drawing.Size(90, 17);
            this.TurboRS_RB.TabIndex = 23;
            this.TurboRS_RB.Text = "Турбокод РС";
            this.TurboRS_RB.UseVisualStyleBackColor = true;
            this.TurboRS_RB.CheckedChanged += new System.EventHandler(this.TurboRS_RB_CheckedChanged);
            // 
            // TurboHamRB
            // 
            this.TurboHamRB.AutoSize = true;
            this.TurboHamRB.Location = new System.Drawing.Point(6, 107);
            this.TurboHamRB.Margin = new System.Windows.Forms.Padding(2);
            this.TurboHamRB.Name = "TurboHamRB";
            this.TurboHamRB.Size = new System.Drawing.Size(122, 17);
            this.TurboHamRB.TabIndex = 22;
            this.TurboHamRB.Text = "Турбокод Хэмминг";
            this.TurboHamRB.UseVisualStyleBackColor = true;
            this.TurboHamRB.CheckedChanged += new System.EventHandler(this.TurboHamRB_CheckedChanged);
            // 
            // ReedSolomonRB
            // 
            this.ReedSolomonRB.AutoSize = true;
            this.ReedSolomonRB.Checked = true;
            this.ReedSolomonRB.Location = new System.Drawing.Point(6, 65);
            this.ReedSolomonRB.Margin = new System.Windows.Forms.Padding(2);
            this.ReedSolomonRB.Name = "ReedSolomonRB";
            this.ReedSolomonRB.Size = new System.Drawing.Size(126, 17);
            this.ReedSolomonRB.TabIndex = 21;
            this.ReedSolomonRB.TabStop = true;
            this.ReedSolomonRB.Text = "Код Рида-Соломона";
            this.ReedSolomonRB.UseVisualStyleBackColor = true;
            this.ReedSolomonRB.CheckedChanged += new System.EventHandler(this.ReedSolomonRB_CheckedChanged);
            // 
            // FileBT
            // 
            this.FileBT.Location = new System.Drawing.Point(278, 525);
            this.FileBT.Margin = new System.Windows.Forms.Padding(2);
            this.FileBT.Name = "FileBT";
            this.FileBT.Size = new System.Drawing.Size(90, 22);
            this.FileBT.TabIndex = 23;
            this.FileBT.Text = "Файл";
            this.FileBT.UseVisualStyleBackColor = true;
            this.FileBT.Click += new System.EventHandler(this.FileBT_Click);
            // 
            // ErrorsCountTB
            // 
            this.ErrorsCountTB.Location = new System.Drawing.Point(106, 41);
            this.ErrorsCountTB.Margin = new System.Windows.Forms.Padding(2);
            this.ErrorsCountTB.Name = "ErrorsCountTB";
            this.ErrorsCountTB.ReadOnly = true;
            this.ErrorsCountTB.Size = new System.Drawing.Size(86, 20);
            this.ErrorsCountTB.TabIndex = 18;
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(8, 44);
            this.textBox5.Margin = new System.Windows.Forms.Padding(2);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(79, 13);
            this.textBox5.TabIndex = 19;
            this.textBox5.Text = "Число ошибок:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ErrorsCountTB);
            this.groupBox6.Controls.Add(this.textBox3);
            this.groupBox6.Controls.Add(this.textBox5);
            this.groupBox6.Controls.Add(this.SuccessfulityTB);
            this.groupBox6.Location = new System.Drawing.Point(278, 426);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(196, 66);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Результаты декодирования";
            // 
            // CountingTimer
            // 
            this.CountingTimer.Interval = 1;
            this.CountingTimer.Tick += new System.EventHandler(this.CountingTimer_Tick);
            // 
            // CountingProgress
            // 
            this.CountingProgress.Location = new System.Drawing.Point(4, 557);
            this.CountingProgress.Margin = new System.Windows.Forms.Padding(2);
            this.CountingProgress.Name = "CountingProgress";
            this.CountingProgress.Size = new System.Drawing.Size(270, 14);
            this.CountingProgress.TabIndex = 25;
            // 
            // Random_str_BT
            // 
            this.Random_str_BT.Location = new System.Drawing.Point(278, 499);
            this.Random_str_BT.Margin = new System.Windows.Forms.Padding(2);
            this.Random_str_BT.Name = "Random_str_BT";
            this.Random_str_BT.Size = new System.Drawing.Size(90, 22);
            this.Random_str_BT.TabIndex = 26;
            this.Random_str_BT.Text = "Строка";
            this.Random_str_BT.UseVisualStyleBackColor = true;
            this.Random_str_BT.Click += new System.EventHandler(this.Random_str_BT_Click);
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(372, 503);
            this.textBox7.Margin = new System.Windows.Forms.Padding(2);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(60, 13);
            this.textBox7.TabIndex = 30;
            this.textBox7.Text = "Символов:";
            // 
            // SymbolCountTB
            // 
            this.SymbolCountTB.Location = new System.Drawing.Point(436, 501);
            this.SymbolCountTB.Margin = new System.Windows.Forms.Padding(2);
            this.SymbolCountTB.Name = "SymbolCountTB";
            this.SymbolCountTB.Size = new System.Drawing.Size(34, 20);
            this.SymbolCountTB.TabIndex = 29;
            this.SymbolCountTB.Text = "10";
            // 
            // ModulationBT
            // 
            this.ModulationBT.Enabled = false;
            this.ModulationBT.Location = new System.Drawing.Point(384, 551);
            this.ModulationBT.Margin = new System.Windows.Forms.Padding(2);
            this.ModulationBT.Name = "ModulationBT";
            this.ModulationBT.Size = new System.Drawing.Size(90, 22);
            this.ModulationBT.TabIndex = 31;
            this.ModulationBT.Text = "Модуляция";
            this.ModulationBT.UseVisualStyleBackColor = true;
            this.ModulationBT.Click += new System.EventHandler(this.ModulationBT_Click);
            // 
            // SendCodedBits_CB
            // 
            this.SendCodedBits_CB.AutoSize = true;
            this.SendCodedBits_CB.Location = new System.Drawing.Point(279, 555);
            this.SendCodedBits_CB.Name = "SendCodedBits_CB";
            this.SendCodedBits_CB.Size = new System.Drawing.Size(101, 17);
            this.SendCodedBits_CB.TabIndex = 32;
            this.SendCodedBits_CB.Text = "Кодированные";
            this.SendCodedBits_CB.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 582);
            this.Controls.Add(this.SendCodedBits_CB);
            this.Controls.Add(this.ModulationBT);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.Random_str_BT);
            this.Controls.Add(this.SymbolCountTB);
            this.Controls.Add(this.CountingProgress);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.FileBT);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.DoAllBT);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Исследование помехоустойчивых кодов";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox MainTextTB;
        private System.Windows.Forms.RichTextBox DecodedTextTB;
        private System.Windows.Forms.TextBox NoisePercentageTB;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox BlocksCountTB;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox BitsCountTB;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox TextLengthTB;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button DoAllBT;
        private System.Windows.Forms.TextBox SuccessfulityTB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton ham74RB;
        private System.Windows.Forms.RadioButton golayRB;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button FileBT;
        private System.Windows.Forms.TextBox ErrorsCountTB;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox CodecParam4_desc;
        private System.Windows.Forms.TextBox CodecParam3_desc;
        private System.Windows.Forms.TextBox CodecParam4_TB;
        private System.Windows.Forms.TextBox CodecParam3_TB;
        private System.Windows.Forms.TextBox CodecParam2_desc;
        private System.Windows.Forms.TextBox CodecParam2_TB;
        private System.Windows.Forms.RadioButton ReedSolomonRB;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Timer CountingTimer;
        private System.Windows.Forms.ProgressBar CountingProgress;
        private System.Windows.Forms.RadioButton TurboHamRB;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox SNR_TB;
        private System.Windows.Forms.Button Random_str_BT;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox SymbolCountTB;
        private System.Windows.Forms.TextBox ErrorPocketTB;
        private System.Windows.Forms.CheckBox ErrorPocketCB;
        private System.Windows.Forms.RadioButton TurboRS_RB;
        private System.Windows.Forms.Button ModulationBT;
        private System.Windows.Forms.CheckBox SendCodedBits_CB;
        private System.Windows.Forms.RadioButton Laby_RB;
    }
}

