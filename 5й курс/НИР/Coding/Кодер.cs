﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Coding
{
    public class Coder
    {
        public Coder() { }
        public Coder(string txt, double percent, int _pocketLength = 1)
        {
            text = txt;
            noise_percentage = percent;
            PocketLength = _pocketLength;
            noise_percentage /= (double)PocketLength;
        }

        public string text, coded_text, decoded_text, noised_text;
        public double noise_percentage;
        public int PocketLength;
        public int[] info_bits, coded_bits, error_bits;

        public Ham74 ham74;
        public Golay golay;
        public ReedSolomon4Bit reed_solomon4b;
        public ReedSolomon8bit reed_solomon8b;
        public TurboHamming turbo_ham74;
        public TurboRS turbo_rs;
        public Laby laby;

        /// 0 - все успешно
        /// 1 - передан пустой текст
        /// 2 - вероятность ошибки меньше нуля
        /// 3 - не задан метод кодирования
        public int code()
        {
            if (text == null || text == "")
            {
                return 1;
            }
            if (noise_percentage < 0)
            {
                return 2;
            }
            if (ham74 != null)
            {
                text = ham74.text;
                ham74.code();
                coded_text = ham74.coded_text;
                return 0;
            }
            if (golay != null)
            {
                text = golay.text;
                golay.code();
                coded_text = golay.coded_text;
                return 0;
            }
            if (reed_solomon4b != null)
            {
                text = reed_solomon4b.text;
                reed_solomon4b.code();
                coded_text = reed_solomon4b.coded_text;
                return 0;
            }
            if (turbo_ham74 != null)
            {
                text = turbo_ham74.text;
                turbo_ham74.code();
                coded_text = turbo_ham74.coded_text;
                return 0;
            }
            if (turbo_rs != null)
            {
                text = turbo_rs.text;
                turbo_rs.code();
                coded_text = turbo_rs.coded_text;
                return 0;
            }
            if (laby != null)
            {
                text = laby.text;
                laby.code();
                return 0;
            }
            return 3;
        }

        /// 0 - все успешно
        /// 1 - не задан метод кодирования
        /// 2 - неправильная порча битов
        public int noise()
        {
            if (ham74 != null)
            {
                ham74.noise();
                if (noise_percentage == 0 && ham74.noised_text != ham74.coded_text)
                {
                    return 2;
                }
                noised_text = ham74.noised_text;
                return 0;
            }
            if (golay != null)
            {
                golay.noise();
                if (noise_percentage == 0 && golay.noised_text != golay.coded_text)
                {
                    return 2;
                }
                noised_text = golay.noised_text;
                return 0;
            }
            if (reed_solomon4b != null)
            {
                reed_solomon4b.noise();
                noised_text = reed_solomon4b.noised_text;
                return 0;
            }
            if (turbo_rs != null)
            {
                turbo_rs.noise();
                noised_text = turbo_rs.noised_text;
                return 0;
            }
            if (laby != null)
            {
                laby.noise(noise_percentage);
                return 0;
            }

            return 1;
        }

        /// 0 - все успешно
        /// 1 - не задан метод кодирования
        /// 2 - декодирование не удалось
        public int decode()
        {
            if (ham74 != null)
            {
                ham74.decode();
                if (ham74.noise_percentage == 0 && ham74.decoded_text != ham74.text)
                {
                    return 2;
                }
                decoded_text = ham74.decoded_text;
                return 0;
            }
            if (golay != null)
            {
                golay.decode();
                if (golay.noise_percentage == 0 && golay.decoded_text != golay.text)
                {
                    return 2;
                }
                decoded_text = golay.decoded_text;
                return 0;
            }
            if (reed_solomon4b != null)
            {
                reed_solomon4b.decode();
                decoded_text = reed_solomon4b.decoded_text;
                return 0;
            }
            if (turbo_rs != null)
            {
                turbo_rs.decode();
                decoded_text = turbo_rs.decoded_text;
                return 0;
            }
            if (laby != null)
            {
                laby.decode();
                decoded_text = laby.decoded_text;
                return 0;
            }
            return 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int[] GetCodedBits()
        {
            List<int> to_return = new List<int>();

            if (ham74 != null)
            {
                for (int i = 0; i < ham74.coded_blocks.Length; i++)
                {
                    to_return.AddRange(ham74.coded_blocks[i]);
                }
            }
            if (golay != null)
            {
                for (int i = 0; i < golay.coded_blocks.Length; i++)
                {
                    to_return.AddRange(golay.coded_blocks[i]);
                }
            }
            if (reed_solomon4b != null)
            {
                for (int i = 0; i < reed_solomon4b.symbols.Count; i++)
                {
                    for (int j = 0; j < reed_solomon4b.N; j++)
                    {
                        to_return.AddRange(reed_solomon4b.symbols[i].poly[j].cvalue);
                    }
                }
            }
            if (turbo_ham74 != null)
            {
                coded_text = turbo_ham74.coded_text;
            }
            if (turbo_rs != null)
            {
                coded_text = turbo_rs.coded_text;
            }

            return to_return.ToArray<int>();
        }


        public string CoderName
        {
            get
            {
                if (ham74 != null)
                    return "Hamming";
                if (golay != null)
                    return "Golay";
                if (reed_solomon4b != null)
                    return "RS";
                return "";
            }
        }


        /// 0 - все успешно
        /// 1 - пустой вектор
        /// 2 - пустая матрица 
        /// 3 - нессответствие размеров матрицы и вектора
        /// 4 - неправильный формат вектора или матрицы
        public int vect_mult_matr(int[] vect, int[][] matr, out int[] to_return)
        {
            if (vect.Length < 1)
            {
                to_return = new int[] { 0 };
                return 1;
            }
            if (matr.Length > 0)
                if (matr[0].Length > 0)
                { }
                else
                {
                    to_return = new int[] { 0 };
                    return 2;
                }
            if (vect.Length != matr[0].Length)
            {
                to_return = new int[] { 0 };
                return 3;
            }
            to_return = new int[matr.Length];

            for (int i = 0; i < matr.Length; i++)
            {
                for (int j = 0; j < matr[0].Length; j++)
                {
                    if (vect[j] < 0 || vect[j] > 1 || matr[i][j] < 0 || matr[i][j] > 1)
                    {
                        to_return = new int[] { 0 };
                        return 4;
                    }
                    to_return[i] += vect[j] * matr[i][j];
                }
                to_return[i] = to_return[i] % 2;
            }
            return 0;
        }

        /// 0 - все успешно
        /// 1 - пустой вектор
        /// 2 - пустая матрица 
        /// 3 - нессответствие размеров матрицы и вектора
        /// 4 - неправильный формат вектора или матрицы
        public int matr_mult_vect(int[][] matr, int[] vect, out int[] to_return)
        {
            if (vect.Length < 1)
            {
                to_return = new int[] { 0 };
                return 1;
            }
            if (matr.Length < 1)
            {
                to_return = new int[] { 0 };
                return 2;
            }
            else
            {
                if (matr[0].Length < 1)
                {
                    to_return = new int[] { 0 };
                    return 2;
                }
            }
            if (vect.Length != matr.Length)
            {
                to_return = new int[] { 0 };
                return 3;
            }
            to_return = new int[matr[0].Length];

            for (int j = 0; j < matr[0].Length; j++)
            {
                for (int i = 0; i < matr.Length; i++)
                {
                    if (vect[i] < 0 || vect[i] > 1 || matr[i][j] < 0 || matr[i][j] > 1)
                    {
                        to_return = new int[] { 0 };
                        return 4;
                    }
                    to_return[j] += vect[i] * matr[i][j];
                }
                to_return[j] = to_return[j] % 2;
            }
            return 0;
        }

        /// 0 - все успешно
        /// 1 - передан пустой массив
        /// 2 - массив не в том формате
        public int cdtoi(int[] code, out int res)
        {
            res = 0;
            if (code.Length < 1)
            {
                return 1;
            }
            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] < 0 || code[i] > 1)
                {
                    return 2;
                }
                res += (int)(code[i] * Math.Pow(2, code.Length - i - 1));
            }
            return 0;
        }
        public int itocd(int value, out int[] code)
        {
            if (value == 0) code = new int[] { 0 };
            else
            {
                code = new int[(int)Math.Log(value, 2) + 1];
                for (int i = code.Length - 1; i > -1; i--)
                {
                    if (value > 0)
                        if (value - Math.Pow(2, i) >= 0)
                        {
                            code[i] = 1;
                            value -= (int)Math.Pow(2, i);
                        }
                        else
                            code[i] = 0;
                }
            }
            return 0;
        }

        /// ---------------------------------------------------------------------------------------------------------------
        /// Неиспользуемые методы
        /// ---------------------------------------------------------------------------------------------------------------

        public int btoi(bool a)
        {
            return a ? 1 : 0;
        }
        public bool itob(int a)
        {
            return a == 1 ? true : false;
        }
        public bool ctob(char a)
        {
            return a == '1' ? true : false;
        }
        public char itoc(int a)
        {
            return a == 1 ? '1' : '0';
        }
        public char btoc(bool a)
        {
            return a ? '1' : '0';
        }

        char[] error_chars = new char[]{
              '\u0000',    '\u0001',    '\u0002',    '\u0003',    '\u0004',    '\u0005',
              '\u0006',    '\u0007',    '\u0008',    '\u0009',    '\u000A',    '\u000B',
              '\u000C',    '\u000D',    '\u000E',    '\u000F',    '\u0010',    '\u0011',
              '\u0012',    '\u0013',    '\u0014',    '\u0015',    '\u0016',    '\u0017',
              '\u0018',    '\u0019',    '\u001A',    '\u001B',    '\u001C',    '\u001D',
              '\u001E',    '\u001F',    '\u007F',    '\u0080',    '\u0081',    '\u0082',
              '\u0083',    '\u0084',    '\u0085',    '\u0086',    '\u0087',    '\u0088',
              '\u0089',    '\u008A',    '\u008B',    '\u008C',    '\u008D',    '\u008E',
              '\u008F',    '\u0090',    '\u0091',    '\u0092',    '\u0093',    '\u0094',
              '\u0095',    '\u0096',    '\u0097',    '\u0098',    '\u0099',    '\u009A',
              '\u009B',    '\u009C',    '\u009D',    '\u009E',    '\u009F'};
        string[] error_strings;

        public int ham_dist(int[] vect1, int[] vect2)
        {
            int to_return = 0;
            for (int i = 0; i < vect1.Length; i++)
            {
                if (vect1[i] == vect2[i]) to_return++;
            }
            return to_return;
        }
        public int[] shift(int[] vect, int shift)
        {
            int[] to_return = new int[vect.Length];
            if (shift > 0)
            {
                for (int i = 0; i < shift; i++)
                {
                    to_return[i] = vect[vect.Length - shift + i];
                }
                for (int i = shift; i < vect.Length; i++)
                {
                    to_return[i] = vect[i - shift];
                }
            }
            if (shift < 0)
            {
                for (int i = 0; i < vect.Length + shift; i++)
                {
                    to_return[i] = vect[i - shift];
                }
                for (int i = vect.Length + shift; i < vect.Length; i++)
                {
                    to_return[i] = vect[i - vect.Length - shift];
                }
            }
            return to_return;
        }
        public int ctoi(char a)
        {
            return a == '1' ? 1 : 0;
        }
        public Vector union(Vector v1, Vector v2)
        {
            Vector res = new Vector(v1.Length + v2.Length);
            for (int i = 0; i < v1.Length; i++)
                res.code[i] = v1.code[i];
            for (int i = 0; i < v2.Length; i++)
                res.code[v1.Length + i] = v2.code[i];
            return res;
        }
    }

    public class Ham74 : Coder
    {
        int size_t, syndrome;
        int[][] blocks;
        public int[][] coded_blocks;
        int[][] matrix, syndrome_matrix;
        int bites_per_symbol = 16;

        public Ham74() { }

        public Ham74(string txt, double percent, int _pocket_length = 1)
        {
            text = txt;
            size_t = text.Length;
            PocketLength = _pocket_length;
            noise_percentage = percent / (double)PocketLength;

            rand = new Random();
        }
        new public void code()
        {
            size_block = 4;
            size_coded_block = 7;

            matrix = new int[size_coded_block][];
            syndrome_matrix = new int[size_coded_block - size_block][];
            syndrome_matrix[0] = new int[] { 1, 1, 1, 0, 1, 0, 0 };
            syndrome_matrix[1] = new int[] { 0, 1, 1, 1, 0, 1, 0 };
            syndrome_matrix[2] = new int[] { 1, 1, 0, 1, 0, 0, 1 };

            for (int i = 0; i < size_coded_block; i++)
            {
                matrix[i] = new int[size_block];
                for (int j = 0; j < 4; j++)
                {
                    if (i == j) matrix[i][j] = 1;
                    else matrix[i][j] = 0;
                }
            }
            matrix[4] = new int[] { 1, 1, 1, 0 };
            matrix[5] = new int[] { 0, 1, 1, 1 };
            matrix[6] = new int[] { 1, 1, 0, 1 };

            List<int> bit_data = new List<int>();
            for (int i = 0; i < text.Length; i++)
            {
                var bits = UTF16.ToBits(text[i]);
                bit_data.AddRange(bits);
            }

            blocks = new int[bites_per_symbol * size_t / size_block][];
            coded_blocks = new int[bites_per_symbol * size_t / size_block][];

            info_bits = bit_data.ToArray();
            coded_bits = new int[blocks.Length * size_coded_block];
            error_bits = new int[coded_bits.Length];

            for (int i = 0; i < bites_per_symbol * size_t / size_block; i++)
            {
                blocks[i] = new int[size_block];
                coded_blocks[i] = new int[size_coded_block];
                for (int j = 0; j < size_block; j++)
                {
                    blocks[i][j] = bit_data[j + i * size_block];
                }
                vect_mult_matr(blocks[i], matrix, out coded_blocks[i]);
                for (int j = 0; j < size_coded_block; j++)
                {
                    coded_bits[i * size_coded_block + j] = coded_blocks[i][j];
                }
            }

            coded_text = "";
            List<int> vct32 = new List<int>();
            int idx = 0;
            for (int i = 0; i < coded_blocks.Length; i++)
            {
                for (int j = 0; j < size_coded_block; j++)
                {
                    vct32.Add(coded_blocks[i][j]);
                    idx++;
                    if (idx == bites_per_symbol)
                    {
                        int res;
                        cdtoi(vct32.ToArray(), out res);
                        if (res == 0) res = 1;
                        coded_text += (char)res;
                        vct32.Clear();
                        idx = 0;
                    }
                }
            }
        }
        new public void noise()
        {
            for (int i = 0; i < coded_bits.Length; i++)
            {
                if (rand.NextDouble() < noise_percentage)
                {
                    if (PocketLength == 1)
                    {
                        error_bits[i] = 1;
                    }
                    else
                    {
                        for (int k = 0; k < PocketLength; k++)
                        {
                            error_bits[i] = 1;
                            i++;
                        }
                    }
                }
                else
                {
                    error_bits[i] = 0;
                }
            }

            for (int i = 0; i < coded_blocks.Length; i++)
            {
                for (int k = 0; k < size_coded_block; k++)
                {
                    coded_blocks[i][k] = (coded_bits[i * size_coded_block + k] + error_bits[i * size_coded_block + k]) % 2;
                }
            }

            {
                /*
                noised_text = "";
                List<int> vct32 = new List<int>();
                int idx = 0;
                for (int i = 0; i < coded_blocks.Length; i++)
                {
                    for (int j = 0; j < size_coded_block; j++)
                    {
                        if (PocketLength == 1)
                        {
                            if (rand.NextDouble() < noise_percentage)
                            {
                                coded_blocks[i][j] = coded_blocks[i][j] == 1 ? 0 : 1;
                            }
                            vct32.Add(coded_blocks[i][j]);
                            idx++;
                            if (idx == 32)
                            {
                                int res;
                                cdtoi(vct32.ToArray(), out res);
                                if (res == 0) res = 1;
                                noised_text += (char)res;
                                vct32.Clear();
                                idx = 0;
                            }
                        }
                        else
                        {
                            if (rand.NextDouble() < noise_percentage)
                            {
                                coded_blocks[i][j] = coded_blocks[i][j] == 1 ? 0 : 1;
                                for (int k = 1; k < PocketLength; k++)
                                {
                                    try
                                    {
                                        coded_blocks[i][j + k] = coded_blocks[i][j + k] == 1 ? 0 : 1;
                                    }
                                    catch (IndexOutOfRangeException)
                                    {
                                        coded_blocks[i][j - (PocketLength - k)] = coded_blocks[i][j - (PocketLength - k)] == 1 ? 0 : 1;
                                    }
                                }
                                idx += 4;
                            }
                            else
                            {
                                idx++;
                            }
                            vct32.Add(coded_blocks[i][j]);
                            if (idx >= 32)
                            {
                                int res;
                                cdtoi(vct32.ToArray(), out res);
                                if (res == 0) res = 1;
                                noised_text += (char)res;
                                vct32.Clear();
                                idx = 0;
                            }
                        }
                    }            
                }
                */
            }
        }
        new public void decode()
        {
            decoded_text = "";
            List<int> bit_data = new List<int>();
            List<int> symbol_bites = new List<int>();
            List<int> vct32 = new List<int>();
            for (int i = 0; i < coded_blocks.Length; i++)
            {
                int[] vct;
                vect_mult_matr(coded_blocks[i], syndrome_matrix, out vct);
                cdtoi(vct, out syndrome);
                switch (syndrome)
                {
                    case 1:
                        coded_blocks[i][6] = coded_blocks[i][6] == 1 ? 0 : 1;
                        break;
                    case 2:
                        coded_blocks[i][5] = coded_blocks[i][5] == 1 ? 0 : 1;
                        break;
                    case 3:
                        coded_blocks[i][3] = coded_blocks[i][3] == 1 ? 0 : 1;
                        break;
                    case 4:
                        coded_blocks[i][4] = coded_blocks[i][4] == 1 ? 0 : 1;
                        break;
                    case 5:
                        coded_blocks[i][0] = coded_blocks[i][0] == 1 ? 0 : 1;
                        break;
                    case 6:
                        coded_blocks[i][2] = coded_blocks[i][2] == 1 ? 0 : 1;
                        break;
                    case 7:
                        coded_blocks[i][1] = coded_blocks[i][1] == 1 ? 0 : 1;
                        break;
                    default:
                        break;
                }

                for (int j = 0; j < size_block; j++)
                {
                    blocks[i][j] = coded_blocks[i][j];
                }
                bit_data.AddRange(blocks[i]);
            }

            for (int i = 0; i < bit_data.Count; i++)
            {
                symbol_bites.Add(bit_data[i]);
                if (i % bites_per_symbol == bites_per_symbol - 1)
                {
                    decoded_text += Bits.ToUTF16(symbol_bites.ToArray());
                    symbol_bites.Clear();
                }
            }

            /*
            if (i % (32 / size_block) == 7)
            {
                int res;
                cdtoi(vct32.ToArray(), out res);
                decoded_text += (char)res;
                vct32.Clear();
            }
            */
        }


        int size_block;
        int size_coded_block;
        Random rand;
    }

    public class Golay : Coder
    {
        int size_t;
        int[][] blocks;
        public int[][] coded_blocks;
        int[][] Gsys, Hsys, B, I;

        Vector initial = new Vector(24);
        Vector g = new Vector(new int[] { 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1 });
        Vector h;
        Vector s;

        public Golay() { }
        public Golay(string txt, double percent, int _pocket_length = 1)
        {
            PocketLength = _pocket_length;
            text = txt;
            size_t = text.Length;
            /*for (int i = 0; i < 3 - size_t % 3; i++)
            {
                text += " ";
            }*/
            size_t = text.Length;
            noise_percentage = percent / (double)PocketLength;

            rand = new Random();

            initial.code[initial.Length - 1] = initial.code[0] = 1;
            h = new Vector(initial / g);

            i_size = 12;
            r_size = 24;
            k_size = r_size - i_size;
        }

        new public void code()
        {
            matrix_generate();

            List<int> bit_data = new List<int>();
            bit_data.AddRange(Coding.Text.ToBits(text));
            /*
            for (int i = 0; i < size_t; i++)
            {
                string tmp = (new BitVector32(text[i])).ToString();
                for (int j = 0; j < tmp.Length; j++)
                {
                    if (tmp[j] == '0' || tmp[j] == '1')
                        bit_data.Add(ctoi(tmp[j]));
                }
            }
            */

            if (bit_data.Count % 12 != 0 || (int)(bit_data.Count / 12) * 12 != bit_data.Count)
            {
                for (int i = 0; i < 16 - bit_data.Count % 12; i++)
                {
                    bit_data.Add(0);
                }
            }

            blocks = new int[bit_data.Count / i_size][];
            coded_blocks = new int[bit_data.Count / i_size][];
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i] = new int[i_size];
                coded_blocks[i] = new int[r_size];
                for (int j = 0; j < i_size; j++)
                {
                    blocks[i][j] = bit_data[j + i * i_size];
                }
                matr_mult_vect(Gsys, blocks[i], out coded_blocks[i]);
            }

            coded_text = "";
            List<int> vct32 = new List<int>();
            int idx = 0;
            for (int i = 0; i < coded_blocks.Length; i++)
            {
                for (int j = 0; j < r_size; j++)
                {
                    vct32.Add(coded_blocks[i][j]);
                    idx++;
                    if (idx == 32)
                    {
                        int res;
                        cdtoi(vct32.ToArray(), out res);
                        if (res == 0) res = 1;
                        coded_text += (char)res;
                        vct32.Clear();
                        idx = 0;
                    }
                }
            }
        }
        new public void noise()
        {
            noised_text = "";
            List<int> vct32 = new List<int>();
            int idx = 0;
            for (int i = 0; i < coded_blocks.Length; i++)
            {
                for (int j = 0; j < r_size; j++)
                {
                    if (PocketLength == 1)
                    {
                        if (rand.NextDouble() < noise_percentage)
                        {
                            coded_blocks[i][j] = coded_blocks[i][j] == 1 ? 0 : 1;
                        }
                        /*vct32.Add(coded_blocks[i][j]);
                        idx++;
                        if (idx == 32)
                        {
                            int res;
                            cdtoi(vct32.ToArray(), out res);
                            if (res == 0) res = 1;
                            noised_text += (char)res;
                            vct32.Clear();
                            idx = 0;
                        }*/
                    }
                    else
                    {
                        if (rand.NextDouble() < noise_percentage)
                        {
                            coded_blocks[i][j] = coded_blocks[i][j] == 1 ? 0 : 1;
                            for (int k = 1; k < PocketLength; k++)
                            {
                                try
                                {
                                    coded_blocks[i][j + k] = coded_blocks[i][j + k] == 1 ? 0 : 1;
                                }
                                catch (IndexOutOfRangeException)
                                {
                                    coded_blocks[i][j - (PocketLength - k)] = coded_blocks[i][j - (PocketLength - k)] == 1 ? 0 : 1;
                                }
                            }
                            idx += 4;
                        }
                        else
                        {
                            idx++;
                        }
                        /*vct32.Add(coded_blocks[i][j]);
                        if (idx >= 32)
                        {
                            int res;
                            cdtoi(vct32.ToArray(), out res);
                            if (res == 0) res = 1;
                            noised_text += (char)res;
                            vct32.Clear();
                            idx = 0;
                        }*/
                    }
                }
            }
        }
        new public void decode()
        {
            Vector e = new Vector(r_size);
            Vector c = new Vector(r_size);
            Vector r;
            int res;
            for (int i = 0; i < coded_blocks.Length; i++)
            {
                r = new Vector(coded_blocks[i]);
                s = new Vector(get_syndrome(r, Hsys));
                res = s.ham_w();

                // point 2
                if (res <= 3)
                {
                    e = new Vector(union(new Vector(k_size), s));
                    goto step8;
                }
                // point 3
                for (int j = 0; j < i_size; j++)
                {
                    res = new Vector(s + new Vector(B[j])).ham_w();
                    if (res <= 2)
                    {
                        e = new Vector(union(new Vector(I[j]), s + new Vector(B[j])));
                        goto step8;
                    }
                }
                // point 4, 5
                int[] sb;
                matr_mult_vect(B, s.code, out sb);
                Vector sB = new Vector(sb);
                res = sB.ham_w();
                if (res <= 3)
                {
                    e = new Vector(union(sB, new Vector(new int[i_size])));
                    goto step8;
                }
                // point 6
                for (int j = 0; j < i_size; j++)
                {
                    res = new Vector(sB + new Vector(B[j])).ham_w();
                    if (res <= 2)
                    {
                        e = new Vector(union(sB + new Vector(B[j]), new Vector(I[j])));
                        goto step8;
                    }
                }

                step8:
                c = new Vector(r + e);
                coded_blocks[i] = c.code;
            }

            // Создаем текст
            decoded_text = "";
            List<int> bits = new List<int>();
            List<int> bits_for_symbol = new List<int>();
            for (int i = 0; i < coded_blocks.Length; i++)
            {
                bits.AddRange(coded_blocks[i]);
            }
            for (int i = 0; i < (int)(bits.Count / 16); i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    bits_for_symbol.Add(bits[i * 16 + 15 - j]);
                }
                decoded_text += Coding.Bits.ToUTF16(bits_for_symbol.ToArray<int>());
                bits_for_symbol.Clear();
            }
            /*
            int idx = 0;
            List<int> vct32 = new List<int>();
            for (int i = 0; i < coded_blocks.Length; i++)
            {
                for (int j = 0; j < i_size; j++)
                {
                    blocks[i][j] = coded_blocks[i][j];
                    vct32.Add(blocks[i][j]);
                    idx++;
                    if (idx == 32)
                    {
                        idx = 0;
                        cdtoi(vct32.ToArray(), out res);
                        decoded_text += (char)res;
                        vct32.Clear();
                    }
                }
            }*/
        }

        public Vector get_syndrome(Vector r, int[][] matr)
        {
            Vector to_return = new Vector(i_size);
            for (int i = 0; i < i_size; i++)
            {
                for (int j = 0; j < r_size; j++)
                {
                    to_return.code[i] += r.code[j] * matr[i][j];
                }
                to_return.code[i] = to_return.code[i] % 2;
            }
            return to_return;
        }
        public void matrix_generate()
        {
            int[] str = new int[] { 1, 0, 0, 1, 1, 1, 1, 1, 0, 0 };
            Hsys = new int[i_size][];
            Gsys = new int[i_size][];
            B = new int[i_size][];
            I = new int[i_size][];
            // Initialize of matrixes
            for (int i = 0; i < i_size; i++)
            {
                Hsys[i] = new int[r_size];
                Gsys[i] = new int[r_size];
                B[i] = new int[r_size - i_size];
                I[i] = new int[r_size - i_size];
                for (int j = 0; j < r_size - i_size; j++)
                {
                    if (i == j) Gsys[i][j] = 1;
                    else Gsys[i][j] = 0;
                }
            }
            // Generating of Gsys
            for (int i = 0; i < i_size - 2; i++)
            {
                for (int j = k_size; j < r_size - 2; j++)
                {
                    Gsys[i][j] = str[j - (k_size)];
                }
                str = shift(str, 1);
            }
            for (int i = i_size - 2; i < i_size; i++)
            {
                for (int j = k_size; j < r_size; j++)
                {
                    Gsys[i][j] = (i + j) % 2;
                }
            }
            for (int i = 0; i < i_size; i++)
            {
                for (int j = r_size - 2; j < r_size; j++)
                {
                    Gsys[i][j] = (i + j) % 2;
                }
            }
            Gsys[10][22] = Gsys[11][23] = 1;

            // Creating other matrixes
            for (int i = 0; i < Hsys.Length; i++)
            {
                for (int j = 0; j < Hsys[0].Length; j++)
                {
                    if (j < i_size)
                    {
                        Hsys[i][j] = B[i][j] = Gsys[i][j + i_size];
                    }
                    else
                    {
                        Hsys[i][j] = I[i][j - i_size] = Gsys[i][j - i_size];
                    }
                }
            }
        }
        public void show_matrix()
        {
            decoded_text = "";
            for (int i = 0; i < i_size; i++)
            {
                string stroka = "";
                for (int j = 0; j < r_size; j++)
                {
                    stroka += Gsys[i][j].ToString();
                }
                decoded_text += stroka;
                decoded_text += '\n';
                stroka = "";
            }
            decoded_text += '\n';
            for (int i = 0; i < i_size; i++)
            {
                string stroka = "";
                for (int j = 0; j < r_size; j++)
                {
                    stroka += Hsys[i][j].ToString();
                }
                decoded_text += stroka;
                decoded_text += '\n';
                stroka = "";
            }
        }

        int i_size;
        int r_size;
        int k_size;
        Random rand;
    }

    public class ReedSolomon4Bit : Coder
    {
        public double noise_percentage;
        public int N, K;
        int p, pow, basis;
        int size_t;
        int bits_per_symbol;
        int added_bits;
        public Poly POLY;/*= new Poly(16, 15,
              (ulong)Math.Pow(16, 6) * 1 +
              (ulong)Math.Pow(16, 5) * 7 +
              (ulong)Math.Pow(16, 4) * 9 +
              (ulong)Math.Pow(16, 3) * 3 +
              (ulong)Math.Pow(16, 2) * 12 +
              (ulong)Math.Pow(16, 1) * 10 +
              (ulong)Math.Pow(16, 0) * 12);*/
        public Poly g;
        public List<Poly> symbols = new List<Poly>();
        List<Poly> original_s = new List<Poly>();

        int[] blocks;

        public ReedSolomon4Bit() { }
        public ReedSolomon4Bit(string txt, double percent, int _N, int _K, int _pow, int _pocket_length = 1)
        {
            text = txt;
            size_t = txt.Length;
            PocketLength = _pocket_length;
            noise_percentage = percent / (double)PocketLength;
            N = _N;
            K = _K;
            p = 2;
            pow = _pow;
            basis = (int)Math.Pow(p, pow);
            bits_per_symbol = 16;

            Poly tmp;
            g = new Poly(16, N, new GF[] { new GF(2, 4, 1) });
            for (int i = 1; i < N - K + 1; i++)
            {
                tmp = new Poly(16, N, new GF[] { new GF(2, 4, 1), new GF(2, 4, MAP.get_gf[i]) });
                g *= tmp;
            }
            POLY = new Poly(16, N, g.poly);
            POLY.Update();
        }

        new public void code()
        {
            int zeros = 0;
            List<int> zero_one_data = new List<int>();
            for (int i = 0; i < size_t; i++)
            {
                var buf = (ushort)text[i];
                IntToCode((int)(buf), out int[] tmp);
                //var bts = Bits.Inv(UTF16.ToBits(text[i]));
                for (int j = 0; j < tmp.Length; j++)
                {
                    if (j == 0 && tmp.Length < bits_per_symbol)
                        for (int k = 0; k < bits_per_symbol - tmp.Length; k++)
                            zero_one_data.Add(0);

                    zero_one_data.Add(tmp[j]);
                }
            }
            if (zero_one_data.Count % (K * 4) != 0)
            {
                added_bits = K * 4 - zero_one_data.Count % (K * 4);
                for (int j = 0; j < added_bits; j++)
                    zero_one_data.Add(0);
                size_t = zero_one_data.Count / bits_per_symbol;
            }

            blocks = new int[zero_one_data.Count / pow];
            int iteration = 0;
            for (int i = 0; i < size_t * bits_per_symbol / pow; i++)
            {
                int tmp = 0;
                for (int j = 0; j < pow; j++, iteration++)
                {
                    tmp += (int)(zero_one_data[iteration] * Math.Pow(2, pow - j - 1));
                    if (zero_one_data[iteration] == 0) zeros++;
                }
                blocks[i] = tmp;
            }
            zero_one_data.Clear();

            int symbols_count = blocks.Length / K;
            if (blocks.Length - symbols_count * K != 0) symbols_count++;

            for (int i = 0; i < symbols_count; i++)
            {
                Poly s = new Poly(16, N);
                for (int j = 0; j < N; j++)
                {
                    if (j < K)
                    {
                        try
                        {
                            s.poly[j] = new GF(2, 4, blocks[j + i * K]);
                        }
                        catch (IndexOutOfRangeException)
                        {
                            s.poly[j] = new GF(2, 4);
                        }
                    }
                    else
                        s.poly[j] = new GF(2, 4);
                }
                s.Update();
                symbols.Add(s);
            }

            for (int i = 0; i < symbols.Count; i++)
            {
                var remainder = symbols[i] / POLY;
                symbols[i] += remainder;
                symbols[i].Update();
            }

            for (int i = 0; i < symbols.Count; i++)
            {
                original_s.Add(new Poly(16, N, symbols[i].value));
                var res = symbols[i] / POLY;
                res += new Poly(16, N);
                var res_value = res.value;
            }
        }

        new public void noise()
        {
            Random rand = new Random();
            if (noise_percentage == 0)
            {

            }
            else
                for (int i = 0; i < symbols.Count; i++)
                {
                    for (int j = 0; j < symbols[i].pow; j++)
                    {
                        int[] noise_code = new int[4];
                        if (PocketLength > 1)
                        {
                            var random_value = rand.NextDouble();
                            for (int k = 0; k < noise_code.Length; k++)
                            {
                                noise_code[k] = 0;
                            }
                            if (random_value < noise_percentage)
                            {

                                for (int k = 0; k < PocketLength; k++)
                                {
                                    try
                                    {
                                        noise_code[k] = 1;
                                    }
                                    catch (IndexOutOfRangeException)
                                    {
                                    }
                                }
                            }
                        }
                        else
                        {

                            for (int k = 0; k < 4; k++)
                            {
                                var random_value = rand.NextDouble();
                                if (random_value < noise_percentage)
                                    noise_code[k] = 1;
                                else
                                    noise_code[k] = 0;
                            }
                        }
                        cdtoi(noise_code, out int res);
                        symbols[i].poly[j] += new GF(2, 4, res);
                    }
                    symbols[i].Update();
                }
        }

        new public void decode()
        {
            for (int idx = 0; idx < symbols.Count; idx++)
            {
                bool otladka = false;
                if (otladka)
                {
                    idx = symbols.Count;
                    Poly test = new Poly(16, N);

                    test.poly[0] = new GF(2, 4, 9);
                    test.poly[1] = new GF(2, 4, 1);
                    test.poly[2] = new GF(2, 4, 1);
                    test.poly[3] = new GF(2, 4, 1);
                    test.poly[4] = new GF(2, 4, 9);
                    test.poly[5] = new GF(2, 4, 0);
                    test.poly[6] = new GF(2, 4, 10);
                    test.poly[7] = new GF(2, 4, 5);
                    test.poly[8] = new GF(2, 4, 7);
                    test.Update();
                    test = test + test / POLY;
                    test.poly[1] += new GF(2, 4, 2);
                    test.poly[3] += new GF(2, 4, 3);
                    test.poly[6] += new GF(2, 4, 7);
                    test.Update();

                    symbols.Add(test);
                }

                var e = symbols[idx] / POLY;

                // Если ошибок нет
                if (e == new Poly(16, N))
                {
                    get_text(symbols);
                }
                // Если ошибки есть
                else
                {
                    GF[] S = new GF[N - K];
                    GF[] V;
                    GF[] L;
                    GF[][] M;
                    // Получаем значения синдрома
                    Poly syndrome = new Poly(16, N);
                    for (int k = 0; k < N - K; k++)
                    {
                        GF Sk = new GF(2, 4);

                        for (int j = 0; j < N - K; j++)
                        {
                            GF value = new GF(2, 4, 1);
                            if (j > 0)
                                for (int p = 0; p < j; p++)
                                {
                                    value = value * new GF(2, 4, MAP.get_gf[k + 1]);
                                }
                            Sk += e.poly[e.pow - 1 - j] * value;
                        }
                        syndrome.poly[e.pow - 1 - k] = S[k] = Sk;
                    }
                    syndrome.Update();

                    // Решаем уравнение, находим L(x)
                    if (otladka)
                    {
                        GF[][] E = new GF[4][];
                        E[0] = new GF[4];
                        E[1] = new GF[4];
                        E[2] = new GF[4];
                        E[3] = new GF[4];
                        for (int i = 0; i < 4; i++)
                            for (int j = 0; j < 4; j++)
                            {
                                if (i == j) E[i][j] = new GF(2, 4, 1);
                                else E[i][j] = new GF(2, 4);
                            }
                        E[0][0] = new GF(2, 4, 2);
                        E[1][1] = new GF(2, 4, 3);
                    }
                    // Перестроить М с t = t-1, если решений нет - сделать!
                    int t = (N - K) / 2;
                    bool result;
                    LS ls;
                    // Решаем систему линейных уравнений
                    do
                    {
                        V = new GF[t];
                        L = new GF[t];
                        M = new GF[t][];
                        for (int k = 0; k < t; k++)
                        {
                            M[k] = new GF[t];
                        }

                        // Инициализируем вектор свободных членов
                        for (int i = 0; i < t; i++)
                        {
                            V[i] = S[t + i];
                        }
                        // Инициализируем матрицу коэффициентов
                        for (int i = 0; i < t; i++)
                        {
                            for (int j = 0; j < t; j++)
                            {
                                M[i][j] = S[t + i - j - 1];
                            }
                        }
                        ls = new LS(M, V);
                        var invM = Maths.GaussJordan(M);
                        result = ls.Solve();

                        GF[][] mtr = new GF[3][];
                        mtr[0] = new GF[] { new GF(2, 4), new GF(2, 4), new GF(2, 4) };
                        mtr[1] = new GF[] { new GF(2, 4), new GF(2, 4), new GF(2, 4) };
                        mtr[2] = new GF[] { new GF(2, 4), new GF(2, 4), new GF(2, 4) };

                        /*for (int i = 0; i < 3; i++)
                        {
                            for (int j = 0; j < 3; j++)
                            {
                                for (int k = 0; k < 3; k++)
                                {
                                    mtr[i][j] += M[i][k] * ls.invM[k][j];
                                }
                            }
                        }*/

                        t--;
                    } while (!result);
                    L = ls.vect_x;

                    /*
                    if (otladka)
                    {
                        L = new GF[3];
                        L[0] = new GF(2, 4, 6);
                        L[1] = new GF(2, 4, 5);
                        L[2] = new GF(2, 4, 4);
                    }
                    */

                    // Находим полином W(x)
                    Poly Sp = new Poly(16, N);
                    Poly Lp = new Poly(16, N);
                    for (int i = 0; i < S.Length; i++)
                    {
                        Sp.push(S[i]);
                    }
                    for (int i = 0; i < L.Length; i++)
                    {
                        Lp.push(L[i]);
                    }
                    Lp *= new Poly(Lp.p, Lp.pow, (ulong)Lp.p);
                    Lp.poly[Lp.pow - 1] = new GF(2, 4, 1);
                    Lp.Update();
                    Poly Wp = syndrome * Lp;
                    for (int i = 0; i < Wp.pow; i++)
                    {
                        if (i < Wp.pow - (N - K))
                            Wp.poly[i] = new GF(2, 4);
                    }
                    Wp.Update();

                    // Находим производную L(x)
                    Poly Lp1 = new Poly(16, N);
                    for (int i = 1; i < Lp.pow; i++)
                    {
                        if (i % 2 != 0)
                            Lp1.poly[i] = new GF(2, 4);
                        else
                            Lp1.poly[i] = Lp.poly[i - 1];
                    }
                    Lp1.Update();

                    // Получение корней L(x)
                    List<GF> roots = new List<GF>();
                    List<int> roots_p = new List<int>();
                    for (int i = 0; i < 16; i++)
                    {
                        GF root = new GF(2, 4, i);
                        if (Lp.paste(root).value == 0)
                        {
                            roots.Add(root);
                            root.find_inverse();
                            roots_p.Add(MAP.get_p[root.inverse_value.value]);
                        }
                    }

                    // Получаем значения ошибок Yi
                    List<GF> Y = new List<GF>();
                    for (int i = 0; i < roots.Count; i++)
                    {
                        Y.Add(Wp.paste(roots[i]) / Lp1.paste(roots[i]));
                    }

                    // Формируем многочлен ошибок e
                    e = new Poly(16, N);
                    for (int i = 0; i < roots_p.Count; i++)
                    {
                        e.poly[e.pow - 1 - roots_p[i]] = Y[i];
                    }
                    e.Update();
                    // Исправляем кодовое слово
                    symbols[idx] += e;
                    var res = symbols[idx] / POLY;

                    // Переходим к следующему слову
                    // goto start
                }
            }
            // Выводим результат декодирования 
            get_text(symbols);
        }

        public void get_text(List<Poly> s)
        {
            int CHAR = 0;
            List<int> decoded_bits = new List<int>();
            for (int idx = 0; idx < s.Count; idx++)
            {
                for (int i = 0; i < K; i++)
                {
                    for (int j = 0; j < s[idx].poly[i].cvalue.Length; j++)
                    {
                        decoded_bits.Add(s[idx].poly[i].cvalue[j]);
                    }
                }
            }

            List<int> block = new List<int>();
            for (int i = 0; i < (decoded_bits.Count) / bits_per_symbol; i++)
            {
                for (int j = 0; j < bits_per_symbol; j++)
                {
                    block.Add(decoded_bits[i * bits_per_symbol + j]);
                }
                cdtoi(block.ToArray<int>(), out CHAR);
                char letter = (char)CHAR;
                decoded_text += letter;
                CHAR = 0;
                block.Clear();
            }

            /*for (int i = 0; i < K; i++)
            {
                //CHAR += (int)Math.Pow(16, K / 2 - 1 - i % (K / 2)) * symbol.poly[i].value;
                if (i == K / 2 - 1 || i == K - 1)
                {
                    char letter = (char)CHAR;
                    decoded_text += letter;
                    CHAR = 0;
                }
            }*/


            /*
            for (int j = 0; j < symbol.poly.Length - K; j++)
            {
                for (int i = 0; i < bits_per_symbol; i++)
                {
                    CHAR += (int)Math.Pow(16, (bits_per_symbol / 4 - 1 - j % (bits_per_symbol / 4))) * symbol.poly[j].value;
                    if (j != 0 && j % (bits_per_symbol / 4) == (bits_per_symbol / 4 - 1))
                    {
                        char letter = (char)CHAR;
                        decoded_text += letter;
                        CHAR = 0;
                    }
                }
            }
            */
        }

        public GF[] solve(GF[][] m, GF[] v)
        {
            int d1 = m.GetLength(0);
            int d2 = m[0].GetLength(0);

            LS ls = new LS(m, v);

            LS ls1, ls2;
            List<int> rows, cols;
            rows = new List<int>();
            cols = new List<int>();
            var result = ls.Solve();
            if (!result)
            {
                while (!result)
                {
                    for (int idx = 1; idx < d1; idx++)
                    {
                        if (idx == 1)
                        {
                            ls1 = ls;
                            ls2 = ls;
                            for (int i = 0; i < ls1.M[0].Length; i++)
                            {
                                for (int j = 0; j < ls1.M[0].Length; j++)
                                {
                                    ls2 = new LS(ls1.Get_matr(ls1.M, new int[] { i }, new int[] { j }), ls1.Get_vect(ls1.vect_b, new int[] { i }));
                                    this.Cut_matr(ls1, ls2);
                                    result = ls2.Solve();
                                    if (result)
                                    {
                                        ls = ls2;
                                        break;
                                    }
                                }
                            }
                        }
                        if (idx == 2)
                        {
                            ls1 = ls;
                            ls2 = ls;
                            for (int i = 0; i < ls1.M[0].Length; i++)
                            {
                                for (int j = 0; j < ls1.M[0].Length; j++)
                                {
                                    for (int ii = 0; ii < ls1.M[0].Length; ii++)
                                    {
                                        for (int jj = 0; jj < ls1.M[0].Length; jj++)
                                        {
                                            if (i != ii && j != jj)
                                            {
                                                ls2 = new LS(ls1.Get_matr(ls1.M, new int[] { i, ii }, new int[] { j, jj }), ls1.Get_vect(ls1.vect_b, new int[] { ii }));
                                                this.Cut_matr(ls1, ls2);
                                                result = ls2.Solve();
                                                if (result)
                                                {
                                                    ls = ls2;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return ls.vect_x;
        }
        public LS Cut_matr(LS ls1, LS ls2)
        {
            bool result;
            for (int i = 0; i < ls1.M[0].Length; i++)
            {
                for (int j = 0; j < ls1.M[0].Length; j++)
                {
                    ls2 = new LS(ls1.Get_matr(ls1.M, new int[] { i }, new int[] { j }), ls1.Get_vect(ls1.vect_b, new int[] { i }));
                    result = ls2.Solve();
                    if (result)
                    {
                        return ls2;
                    }
                }
            }
            return ls1;
        }
        public GF[][] iM(GF[][] m)
        {
            GF[][] result = m;

            return result;
        }
        public GF[] MV(GF[][] m, GF[] v)
        {
            GF[] res = new GF[v.Length];
            for (int i = 0; i < m.GetLength(0); i++)
            {
                res[i] = new GF(2, 4);
                for (int j = 0; j < m[i].Length; j++)
                {
                    res[i] += m[i][j] * v[j];
                }
            }
            return res;
        }

        /// <summary>
        /// Для отладки
        /// </summary>
        public void f()
        {
            GF res;

            res = new GF(2, 4, 13) * new GF(2, 4, 15);
            res = new GF(2, 4, 7) / new GF(2, 4, 15);

            GF x13 = new GF(2, 4, 13);
            GF x15 = new GF(2, 4, 15);

            x13.find_inverse();
            x15.find_inverse();

            res = x13 * x15;
            res = x13 * x13.inverse_value;
            res = x15 * x15.inverse_value;

            Poly pl1 = new Poly(16, 4, 16 * 16 * 16 * 7 + 16 * 16 * 15 + 16 * 15 + 8);
            Poly pl2 = new Poly(16, 16, (ulong)Math.Pow(16, 11) * 13 + (ulong)Math.Pow(16, 9) * 5);
            Poly pl3 = new Poly(16, 16,
                (ulong)Math.Pow(16, 6) * 1 +
                (ulong)Math.Pow(16, 5) * 7 +
                (ulong)Math.Pow(16, 4) * 9 +
                (ulong)Math.Pow(16, 3) * 3 +
                (ulong)Math.Pow(16, 2) * 12 +
                (ulong)Math.Pow(16, 1) * 10 +
                (ulong)Math.Pow(16, 0) * 12);

            Poly pl4 = new Poly(16, 4,
                16 * 16 * 16 * 7 +
                16 * 16 * 15 +
                16 * 6 +
                3);
            Poly pl5 = new Poly(16, 4,
                16 * 16 * 4 +
                16 * 5 +
                1);
            Poly.div(pl4, pl5, out Poly pl_rs, out Poly pl_rm);

            Poly pl6 = new Poly(16, 4,
                16 * 5 +
                4);
            Poly pl7 = new Poly(16, 4,
                16 * 14 +
                8);
            Poly pl8 = new Poly(16, 4,
                16 * 16 * 7 +
                16 * 5 +
                4);

            Poly p_res;

            p_res = pl6 * pl7;
            p_res = pl6 * pl8;
            p_res = pl7 * pl8;
        }

        public int IntToCode(int value, out int[] code)
        {
            if (value == 0) code = new int[] { 0 };
            else
            {
                code = new int[(int)Math.Log(value, 2) + 1];
                for (int i = 0; i < code.Length; i++)
                {
                    if (value > 0)
                        if (value - Math.Pow(2, code.Length - 1 - i) >= 0)
                        {
                            code[i] = 1;
                            value -= (int)Math.Pow(2, code.Length - 1 - i);
                        }
                        else
                            code[i] = 0;
                }
            }
            return 0;
        }

    }

    public class ReedSolomon8bit : Coder
    {

    }

    public class TurboHamming : Coder
    {
        int[][] gm, sm;
        int[][] matr;
        int[] cx;
        int[] cy;
        int N, K;
        int bites_per_symbol = 16;

        /// Переменные, относящиеся непосредственно к турбокоду
        int[][][] turbo_blocks;

        public TurboHamming() { }
        public TurboHamming(string _text, double _percent, int _N = 7, int _K = 4, int _iterations = 2, int _pocket_length = 1)
        {
            text = _text;
            PocketLength = _pocket_length;
            noise_percentage = _percent / (double)_pocket_length;
            N = _N;
            K = _K;

            sm = new int[N - K][];
            sm[0] = new int[] { 1, 1, 1, 0, 1, 0, 0 };
            sm[1] = new int[] { 0, 1, 1, 1, 0, 1, 0 };
            sm[2] = new int[] { 1, 1, 0, 1, 0, 0, 1 };

            gm = new int[N][];
            for (int i = 0; i < N; i++)
            {
                gm[i] = new int[K];
                for (int j = 0; j < K; j++)
                {
                    if (i == j)
                        gm[i][j] = 1;
                    else
                        gm[i][j] = 0;
                }
            }
            gm[4] = new int[] { 1, 1, 1, 0 };
            gm[5] = new int[] { 0, 1, 1, 1 };
            gm[6] = new int[] { 1, 1, 0, 1 };
        }

        new public void code()
        {
            int[][] irows;
            irows = new int[(int)(text.Length * bites_per_symbol / K)][];
            List<int> bites = new List<int>();
            for (int i = 0; i < text.Length; i++)
            {
                var bits = Int.ToBits((ushort)text[i]);
                for (int j = 0; j < bits.Length; j++)
                {
                    bites.Add(bits[j]);
                }
            }
            int idx = 0;
            for (int i = 0; i < irows.Length; i++)
            {
                irows[i] = new int[K];
                for (int j = 0; j < K; j++)
                {
                    try
                    {
                        irows[i][j] = bites[idx];
                        idx++;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        irows[i][j] = 0;
                        idx++;
                    }
                }
            }

            int[][] cdrows = new int[irows.Length][];
            for (int i = 0; i < irows.Length; i++)
            {
                cdrows[i] = new int[N];
                cdrows[i] = MatrVect(gm, irows[i]);
            }

            /// Формируем вектор матриц проверочных слов
            /// Инициализация
            turbo_blocks = new int[irows.Length / K][][];
            for (int i = 0; i < irows.Length / K; i++)
            {
                turbo_blocks[i] = new int[N][];
                for (int j = 0; j < N; j++)
                {
                    turbo_blocks[i][j] = new int[N];
                }
            }

            /// Начинаем заполнение матрицы проверочных слов
            for (int i = 0; i < irows.Length / K; i++)
            {
                /// Заполняем i-ю матрицу
                /// Заполнение первых K кодовых строк (проверочная часть уже вычислена)
                for (int j = 0; j < 4; j++)
                {
                    turbo_blocks[i][j] = cdrows[4 * i + j];
                }
                /// Создаем матрицу N*K информационных колонок
                int[][] icol = new int[N][];
                for (int j = 0; j < N; j++)
                {
                    icol[j] = new int[K];
                }
                for (int j = 0; j < N; j++)
                {
                    for (int k = 0; k < K; k++)
                    {
                        icol[j][k] = cdrows[i * 4 + k][j];
                    }
                }
                /// Создаем матрицу N*K вертикальных проверочных слов
                int[][] cdcol = new int[N][];
                for (int j = 0; j < N; j++)
                {
                    cdcol[j] = MatrVect(gm, icol[j]);
                }
                /// Дополняем последние 3 строки матрицы кодовых слов проверочной частью вертикальных кодовых слов
                for (int j = 0; j < N; j++)
                {
                    for (int k = K; k < N; k++)
                    {
                        turbo_blocks[i][k][j] = cdcol[j][k];
                    }
                }
                /// Формирование i-й матрицы проверочных слов завершено, переходим к следующей
            }
        }

        new public void noise()
        {
            /// Формируем массив ошибочных бит
            List<int> noise_bits = new List<int>();
            Random rand = new Random();
            for (int i = 0; i < turbo_blocks[0].Length * N * N; i++)
            {
                if (rand.NextDouble() < noise_percentage)
                {
                    for (int j = 0; j < PocketLength; j++)
                    {
                        i++;
                        noise_bits.Add(1);
                    }
                }
                else
                {
                    noise_bits.Add(0);
                }
            }

            /// Складываем массив ошибочных бит с битами, содержащимися в матрице кодовых слов
            for (int i = 0; i < turbo_blocks[0].Length; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        turbo_blocks[i][j][k] = (turbo_blocks[i][j][k] + noise_bits[i * N * N + j * N + k]) % 2;
                    }
                }
            }
        }

        new public void decode()
        {
            /// Надо реализовать MAP
        }


        public int[] IntToCode(int val, int _length = 0)
        {
            int[] code;
            int l = _length;
            if (l != 0)
            {
                code = new int[l];
            }
            else
            {
                l = (int)Math.Log(val, 2) + 1;
                code = new int[l];
            }
            int value = val;
            for (int i = code.Length - 1; i > -1; i--)
            {
                if (value > 0)
                    if (value - Math.Pow(2, i) >= 0)
                    {
                        code[i] = 1;
                        value -= (int)Math.Pow(2, i);
                    }
                    else
                        code[i] = 0;
            }
            return code;
        }
        public int CodeToInt(int[] code)
        {
            int val = 0;
            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] != 0)
                    val += code[i] * (int)Math.Pow(2, i);
            }
            return val;
        }
        public int[] MatrVect(int[][] matr, int[] vect)
        {
            int[] to_return;
            if (vect.Length < 1)
            {
                return new int[] { 0 };
            }
            if (matr.Length > 0)
                if (!(matr[0].Length > 0))
                {
                    return new int[] { 0 };
                }
            if (vect.Length != matr[0].Length)
            {
                return new int[] { 0 };
            }

            to_return = new int[matr.Length];
            for (int i = 0; i < matr.Length; i++)
            {
                for (int j = 0; j < matr[0].Length; j++)
                {
                    if (vect[j] < 0 || vect[j] > 1 || matr[i][j] < 0 || matr[i][j] > 1)
                    {
                        return new int[] { 0 };
                    }
                    to_return[i] += vect[j] * matr[i][j];
                }
                to_return[i] = to_return[i] % 2;
            }
            return to_return;
        }
    }

    public class TurboRS : Coder
    {
        int N;
        int K;
        int p = 2, pow = 4;
        int bits_per_symbol = 16;
        int added_bits;
        double noise_percentage;

        List<Poly> cx = new List<Poly>();
        List<Poly> cy = new List<Poly>();

        GF[,,] mats;

        public Poly POLY;
        public Poly g;
        List<Poly> symbols = new List<Poly>();
        List<Poly> original_s = new List<Poly>();

        int[] blocks;

        public void Init(string txt, double _noise_percentage, int _N, int _K, int _pocket_length = 1)
        {
            text = txt;
            noise_percentage = _noise_percentage / (double)_pocket_length;
            N = _N;
            K = _K;
            PocketLength = _pocket_length;

            Poly tmp;
            g = new Poly(16, N, new GF[] { new GF(2, 4, 1) });
            for (int i = 1; i < N - K + 1; i++)
            {
                tmp = new Poly(16, N, new GF[] { new GF(2, 4, 1), new GF(2, 4, MAP.get_gf[i]) });
                g *= tmp;
            }
            POLY = new Poly(16, N, g.poly);
            POLY.Update();
        }

        public void code()
        {
            List<List<int>> bits = new List<List<int>>();
            for (int i = 0; i < text.Length; i++)
            {
                var raw_bits = Bits.Inv(UTF16.ToBits(text[i]));
                for (int j = 0; j < 4; j++)
                {
                    List<int> block = new List<int>();
                    for (int k = 0; k < 4; k++)
                    {
                        block.Add(raw_bits[k + 4 * j]);
                    }
                    bits.Add((block.ToArray<int>()).ToList<int>());
                    block.Clear();
                }
            }

            if (bits.Count % (K * K) != 0)
            {
                int added_blocks = (K * K - bits.Count % (K * K));
                added_bits = 4 * added_blocks;
                for (int i = 0; i < added_blocks; i++)
                {
                    List<int> block = new List<int>();
                    block.AddRange(new int[] { 0, 0, 0, 0 });
                    bits.Add(block);
                }
            }
            int count_of_mats = bits.Count / K / K;
            mats = new GF[count_of_mats, N, N];

            /// Создали полиномы
            for (int i = 0; i < bits.Count;)
            {
                cx.Add(new Poly(16, N, 0));
                for (int j = 0; j < K; j++)
                {
                    cx[cx.Count - 1].poly[j] = new GF(2, 4, Bits.ToInt(Bits.Inv(bits[i].ToArray<int>())));
                    i++;
                }
                cx[cx.Count - 1].Update();
                cx[cx.Count - 1] += cx[cx.Count - 1] / POLY;
                cx[cx.Count - 1].Update();
            }
            /// Высчитаем контрольные суммы
            for (int i = 0; i < cx.Count; i++)
            {
            }
            /// Заполнили все строки
            for (int i = 0; i < count_of_mats; i++)
            {
                for (int j = 0; j < K; j++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        mats[i, j, k] = cx[i * K + j].poly[k];
                    }
                }
            }

            /// Заролним все столбцы
            /// Формируем cy и вписываем их в mats
            for (int i = 0; i < count_of_mats; i++)
            {
                for (int k = 0; k < N; k++)
                {
                    cy.Add(new Poly(16, N));
                    for (int j = 0; j < K; j++)
                    {
                        cy[cy.Count - 1].poly[j] = mats[i, j, k];
                    }
                    cy[cy.Count - 1].Update();
                    cy[cy.Count - 1] += cy[cy.Count - 1] / POLY;
                    cy[i * N + k].Update();
                    for (int j = K; j < N; j++)
                    {
                        mats[i, j, k] = cy[i * N + k].poly[j];
                    }
                }
            }
        }

        public void noise()
        {

        }

        public void decode()
        {

        }
    }

    public class Laby
    {
        public string text;
        int
            L = 16, // Число бит на символ
            K,  // Число исходных символов           
            N,  // Число кодовых символов
            M,  // Максимальная степень кодовых символов
            P;  // Число пакетов
        List<LabyCS> d;
        double
            c,  // Параметр распределения 
            o,  // Вероятность успешного декодирования
            S,
            k,
            noise_perc;
        Random rand;
        Random rand_essintial;
        int seed;
        int[][] data;

        Laby() { }

        public Laby(string txt, int _L, int _M, double C, double O, double _k)
        {
            text = txt;
            L = _L;
            K = text.Length;
            if (K * 16 % L == 0)
                P = K * 16 / L;
            else
            {
                P = K * 16 / L + 1;
            }
            k = _k;
            N = (int)(k * P);
            M = _M;
            c = C;
            o = O;
            S = c * Math.Log(P / o) * Math.Sqrt(P);
            //rand = new Random(128578508);
            rand = new Random();
            seed = rand.Next();
            rand = new Random(seed);
        }

        private int[][] PocketsGenerator()
        {
            List<int> bits = new List<int>();

            for (int i = 0; i < text.Length; i++)
                bits.AddRange(Bits.Inv(Coding.UTF16.ToBits(text[i])));

            if (!(bits.Count % L == 0))
                for (int i = 0; i < L - bits.Count % L; i++)
                    bits.Add(0);

            int[][] to_return = new int[P][];
            for (int i = 0; i < to_return.Length; i++)
            {
                to_return[i] = new int[L];
                for (int j = 0; j < L; j++)
                {
                    if (i * L + j < bits.Count)
                        to_return[i][j] = bits[i * L + j];
                    else
                        to_return[i][j] = 0;
                }
            }
            bits.Clear();
            return to_return;
        }

        public void code()
        {
            int[][] data_pockets = PocketsGenerator();
            d = new List<LabyCS>();
            List<int> indexes = new List<int>();

            for (int i = 0; i < N; i++)
            {
                List<int> idx = new List<int>();
                int[] value = new int[L];
                int power;
                bool add = true;
                int _d;
                _d = (int)f(M + 1);
                _d = rand.Next(1, M + 1);
                if (_d == 0) _d = 1;

                /*
                power = u(_d);
                for (int j = 0, k; j < power; j++)
                {
                    k = rand.Next(0, P);

                    idx.Add(k);

                    value = LabyCS.XOR(value, data_pockets[k]);
                }
                d.Add(new LabyCS(power, value, idx, d.Count));
                */

                
                power = (int)(1 + Math.Pow(rand.NextDouble(), 3) * M);
                for (int j = 0, k; j < power; j++)
                {
                    k = rand.Next(0, P);
                    if (power == 1)
                    {
                        if (!indexes.Exists(index => index == k) && d.Count < data_pockets.Length)
                        {
                            indexes.Add(k);
                        }
                        else
                        {
                            i--;
                            add = false;
                            break;
                        }
                    }
                    idx.Add(k);
                    value = LabyCS.XOR(value, data_pockets[k]);
                }
                if (add)
                    d.Add(new LabyCS(power, value, idx, d.Count));
                    
            }
        }

        public void noise(double noise_percentage)
        {
            noise_perc = noise_percentage;
            for (int i = 0; i < d.Count; i++)
            {
                if (rand.NextDouble() < noise_percentage)
                {
                    d.RemoveAt(i);
                    i--;
                }
            }
        }

        public void decode()
        {
            List<int> powers = new List<int>();
            List<int> Idxs = new List<int>();
            for (int i = 0; i < d.Count; i++)
            {
                powers.Add(d[i].power);
                for (int j = 0; j < d[i].idx.Count; j++)
                {
                    if (!Idxs.Exists(index => index == d[i].idx[j]))
                        Idxs.Add(d[i].idx[j]);
                }
            }
            Idxs.Sort();
            /// Нашли пакеты с одним соседом
            int idx = 0;
            data = new int[P][];
            for (int i = 0; i < P; i++, idx++)
                data[i] = new int[L];
            List<int> indexes = new List<int>();
            idx = 0;
            for (int i = 0; i < d.Count; i++)
            {
                if (d[i].power == 1)
                {
                    data[d[i].idx[0]] = d[i].value;
                    indexes.Add(d[i].idx[0]);
                    d.RemoveAt(i);
                    i--;
                }
            }

            int max_p = 1;
            for (int i = 0; i < d.Count; i++)
            {
                if (d[i].power > max_p) max_p = d[i].power;
            }
            powers.Sort();
            indexes.Sort();

            int all_powers_before = 0, all_powers_after = 0;
            do
            {
                /// Считаем сколько у нас степеней было изначально
                all_powers_before = 0;
                all_powers_after = 0;
                for (int i = 0; i < d.Count; i++)
                    all_powers_before += d[i].power;

                /// Избавляемся от степеней 2, если есть с чем сXORить
                for (int i = 0; i < d.Count; i++)
                {
                    if (FindNewDataPocket(d, i, indexes, data))
                    {
                        i = -1;
                    }
                }

                /// Применяем найденные индексы ко всем степеням выше 2                 
                for (int i = 0; i < d.Count; i++)
                {
                    if (FindIndex(d, i, indexes, data))
                    {
                        i--;
                    }
                }

                /// Ищем пакеты, отличающиеся только одним индексом
                for (int i = 0; i < d.Count; i++)
                {
                    for (int j = 0; j < d.Count; j++)
                    {
                        if (Math.Abs(d[i].power - d[j].power) == 1)
                        {
                            int index = LabyCS.Equal(d[i].idx.ToArray<int>(), d[j].idx.ToArray<int>());
                            if (index != -1)
                            {
                                indexes.Add(index);
                                data[index] = LabyCS.XOR(d[i].value, d[j].value);
                                if (d[i].power > d[j].power)
                                {
                                    d.RemoveAt(i);
                                    i = -1;
                                    break;
                                }
                                else
                                {
                                    d.RemoveAt(j);
                                    i = -1;
                                    break;
                                }
                            }
                        }
                    }
                }

                /// Пересчитываем сколько у нас степеней теперь
                for (int i = 0; i < d.Count; i++)
                    all_powers_after += d[i].power;
            } while (all_powers_after - all_powers_before != 0 && d.Count != 0);
            indexes.Sort();

            //CreateText();

            int errors = 0;
            string s = "";
            for (int i = 0; i < data.Length; i++)
            {
                if (indexes.Exists(index => index == i))
                {
                    s += Bits.ToUTF16(Bits.Inv(data[i]));
                }
                else
                {
                    for (int j = 0; j < L / 16; j++)
                    {
                        s += "_";
                        errors++;
                    }
                }
            }
            var success = 1 - (double)errors / (data.Length * L / 16);

            StreamWriter sw = new StreamWriter("laby results.txt", append: true);
            sw.WriteLine($"noise = {noise_perc}, M = {M}, k = {k}, success = {success}");
            sw.Close();
        }

        public string CreateText()
        {
            List<int> bits = new List<int>();
            for (int i = 0; i < data.Length; i++)
            {
                bits.AddRange(data[i]);
            }
            List<List<int>> string_bits = new List<List<int>>();
            for (int i = 0; i < bits.Count; i += 16)
            {
                List<int> buffer = new List<int>();
                for (int j = 0; j < 16; j++)
                {
                    buffer.Add(bits[j + i]);
                }
                string_bits.Add(buffer);
                if (string_bits.Count == K) break;
            }
            string to_return = "";
            for (int i = 0; i < string_bits.Count; i++)
            {
                to_return += Bits.ToUTF16(Bits.Inv(string_bits[i].ToArray<int>()));
            }
            return to_return;
        }

        public bool FindNewDataPocket(List<LabyCS> d, int i, List<int> indexes, int[][] data)
        {
            if (d[i].power == 2 && indexes.Exists(index => index == d[i].idx[0]))
            {
                data[d[i].idx[1]] = LabyCS.XOR(d[i].value, data[d[i].idx[0]]);
                if (!indexes.Exists(index => index == d[i].idx[1]))
                {
                    indexes.Add(d[i].idx[1]);
                    indexes.Sort();
                }
                d.RemoveAt(i);
                return true;
            }
            if (d[i].power == 2 && indexes.Exists(index => index == d[i].idx[1]))
            {
                data[d[i].idx[0]] = LabyCS.XOR(d[i].value, data[d[i].idx[1]]);
                if (!indexes.Exists(index => index == d[i].idx[0]))
                {
                    indexes.Add(d[i].idx[0]);
                    indexes.Sort();
                }
                d.RemoveAt(i);
                return true;
            }
            return false;
        }

        public bool FindIndex(List<LabyCS> d, int i, List<int> indexes, int[][] data)
        {
            int indexes_founded = 0;
            for (int j = 0; j < d[i].idx.Count; j++)
            {
                if (indexes.Exists(index => index == d[i].idx[j]))
                {
                    d[i].power--;
                    d[i].value = LabyCS.XOR(d[i].value, data[d[i].idx[j]]);
                    d[i].idx.RemoveAt(j);
                    j--;
                    indexes_founded++;
                }
            }
            /// Добавляем новый индекс
            if (d[i].power == 1)
            {
                data[d[i].idx[0]] = d[i].value;
                indexes.Add(d[i].idx[0]);
                d.RemoveAt(i);
                return true;
            }
            /// Устраняем полностью повторный пакет
            if (d[i].power == 0)
            {
                d.RemoveAt(i);
                return true;
            }
            return false;
        }

        public string decoded_text
        {

            get
            {
                return CreateText();
                string to_return = "";
                for (int i = 0; i < data.Length; i++)
                {
                    to_return += Bits.ToUTF16(Bits.Inv(data[i]));
                }
                return to_return;
            }
        }

        public double f(int _M)
        {
            double to_return = 0;
            for (int i = 0; i < 1; i++)
            {
                to_return += rand.NextDouble() / 1;
            }
            var value = _M * to_return;
            return value;
        }

        public double Tau(int _d)
        {
            double value;
            if (_d <= (int)(P / S - 1)) value = (S / P / _d);
            else if (_d == (int)(P / S)) value = (S / P * Math.Log(S / o, 10));
            else value = 0;
            return value;
        }

        public double s()
        {
            var value = 0;
            return value;
        }

        public double p(int _d)
        {
            if (_d == 1) return 1.0 / (double)P;
            else return 1.0 / (double)_d / (double)(_d - 1);
        }

        public int u(int _d)
        {
            double spd = 0;
            double pd = p(_d);
            double tau = Tau(_d);
            for (int i = 1; i <= _d; i++)
                spd += p(i);
            double to_return = (pd + tau) / (spd + tau);
            if ((int)to_return == 0) return 1;
            return (int)to_return;
        }
    }
}
