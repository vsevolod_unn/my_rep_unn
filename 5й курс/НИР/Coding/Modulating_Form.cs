﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Xml.Serialization;
using System.Xml;
using System.Globalization;

namespace Coding
{
    public partial class Modulating_Form : Form
    {
        Modulation md;
        DRWDrawer mainDrawer, rxxDrawer, statisticsDrawer;
        Thread exp_thread;
        double[][] Signals;
        double[][] NoisedSignals;
        double[][] X;
        double[][] Y;
        int[] bits, bits_from_coder;
        static int N, M;
        static double[] p;
        static double DFreq, MainFreq, ModFreq, Amp, devAmp, tau0, minSNR, maxSNR;
        static int CountsPerBit;
        static bool Initialized = false, coding = false;
        static int MainGraphVision = 1;
        string coder;
        string coded;

        public Modulating_Form()
        {
            InitializeComponent();

            OneExpBT.Enabled = true;

            minSNR = Convert.ToDouble(SNRmin_param.Text);
            maxSNR = Convert.ToDouble(SNRmax_param.Text);

            DrawerCollection d = new DrawerCollection();
            mainDrawer = d.DRWCreator(SignalPicture, true, true, "График сигнала без шума");
            rxxDrawer = new DrawerCollection().DRWCreator(Rxx_graph, true, true, "График АКФ, SNR = " + SNRmin_param.Text + "дБ");
            statisticsDrawer = d.DRWCreator(Statistics_graph, true, true, "Результаты");

            statisticsDrawer.SetRanges(new double[] { minSNR, maxSNR, -0.1, 1.1 });
            statisticsDrawer.AxisConfig(new int[] { 21, 13 });

            Signals = new double[3][];
            NoisedSignals = new double[3][];

            Initialized = true;

            AM_rb.Checked = true;
        }

        public Modulating_Form(int[] _bits, string _type_of_code, bool _coded)
        {
            InitializeComponent();

            OneExpBT.Enabled = true;

            minSNR = Convert.ToDouble(SNRmin_param.Text);
            maxSNR = Convert.ToDouble(SNRmax_param.Text);

            DrawerCollection d = new DrawerCollection();
            mainDrawer = d.DRWCreator(SignalPicture, true, true, "График сигнала без шума");
            rxxDrawer = new DrawerCollection().DRWCreator(Rxx_graph, true, true, "График АКФ, SNR = " + SNRmin_param.Text + "дБ");
            statisticsDrawer = d.DRWCreator(Statistics_graph, true, true, "Результаты");

            statisticsDrawer.SetRanges(new double[] { minSNR, maxSNR, -0.1, 1.1 });
            statisticsDrawer.AxisConfig(new int[] { 21, 13 });

            Signals = new double[3][];
            NoisedSignals = new double[3][];

            bits_from_coder = _bits;
            coding = true;
            coder = "";
            coded = "";
            if (_coded)
            {
                coded = "";
                coder = _type_of_code;
            }

            Initialized = true;

            AM_rb.Checked = true;

            Tau_param.Text = (_bits.Length * Convert.ToInt32(TotalTime_param.Text) / DFreq).ToString("F4");
        }

        private void UpdateValues()
        {
            TotalProgress.Minimum = 0;
            TotalProgress.Maximum = Convert.ToInt32(ExpCount_param.Text) * Convert.ToInt32(StepsSNR_param.Text) * 3;
            TotalProgress.Value = 0;

            DFreq = Convert.ToDouble(DFreq_param.Text);
            MainFreq = Convert.ToDouble(MainFreq_param.Text);
            ModFreq = Convert.ToDouble(ModFreq_param.Text);
            CountsPerBit = Convert.ToInt32(TotalTime_param.Text);
            Amp = Convert.ToDouble(Amp_param.Text);
            devAmp = Convert.ToDouble(devAmp_param.Text);
            Tau_param.Text = (bits_from_coder.Length * Convert.ToInt32(TotalTime_param.Text) / DFreq).ToString("F4");
            tau0 = Convert.ToDouble(Tau_param.Text);
            N = Convert.ToInt32(ExpCount_param.Text);
            M = Convert.ToInt32(StepsSNR_param.Text);

            minSNR = Convert.ToDouble(SNRmin_param.Text);
            maxSNR = Convert.ToDouble(SNRmax_param.Text);

            //CountsPerBit = (int)(CountsPerBit * DFreq / (TextData_param.Text.Length * 16));

            md = new Modulation(
                    DFreq,
                    MainFreq,
                    ModFreq,
                    CountsPerBit,
                    Amp,
                    devAmp);

            for (int i = 0; i < 3; i++)
            {
                Signals[i] = GetSignal(Coding.Text.ToBits(TextData_param.Text), i);
            }

            X = new double[3][];
            Y = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                X[i] = new double[Convert.ToInt32(ExpCount_param.Text)];
                Y[i] = new double[Convert.ToInt32(ExpCount_param.Text)];
            }
        }

        private void OneExpButton(object sender, EventArgs e)
        {
            UpdateValues();

            md = new Modulation(
                DFreq,
                MainFreq,
                ModFreq,
                CountsPerBit,
                Amp,
                devAmp);

            bits = new int[0];
            if (coding)
            {
                bits = bits_from_coder;
            }
            else
            {
                if (RandomData_check.Checked)
                {
                    Random rand = new Random();
                    bits = new int[64];
                    for (int i = 0; i < 64; i++)
                    {
                        bits[i] = rand.Next(0, 2);
                    }
                }
                else
                {
                    bits = Coding.Text.ToBits(TextData_param.Text);
                }
            }

            double[] y = new double[0];
            double[] x;
            int mode = 0;
            if (PM2_rb.Checked)
            {
                y = md.PM2(bits);
                mode = 0;
            }
            if (FT_rb.Checked)
            {
                y = md.FT(bits);
                mode = 1;
            }
            if (AM_rb.Checked)
            {
                y = md.AM(bits);
                mode = 2;
            }

            x = md.GetTime(0);

            mainDrawer.DrawGraph(x, y, true, GraphType.Lines);

            if (PM2_rb.Checked) mode = 0;
            if (FT_rb.Checked) mode = 1;
            if (AM_rb.Checked) mode = 2;
            double[] current_signal = GetNoisedSignal(mode, Convert.ToDouble(SNRmin_param.Text));
            NoisedSignals[mode] = NoiseArr(Signals[mode], Convert.ToDouble(SNRmin_param.Text));
            double[] rxx = Rxx(current_signal, mode);
            double[] x_for_rxx = new double[rxx.Length];

            int max = 0;
            x_for_rxx[0] = 0;
            for (int i = 1; i < rxx.Length; i++)
            {
                if (rxx[i] > rxx[max]) max = i;
                x_for_rxx[i] = (i / (double)x.Length * tau0);
            }

            rxxDrawer.GraphName = "График АКФ, SNR = " + SNRmin_param.Text + "дБ";
            rxxDrawer.DrawGraph(x_for_rxx, rxx);

            var tau = max / (double)(x.Length) * tau0; ;//max / ((2 * tau0 + CountsPerBit) * DFreq) * (2 * tau0 + CountsPerBit);
            ExpTau_param.Text = tau.ToString("F4");
        }

        private void PM2_rb_CheckedChanged(object sender, EventArgs e)
        {
            if (Initialized)
                OneExpButton(sender, e);
        }
        private void FT_rb_CheckedChanged(object sender, EventArgs e)
        {
            if (Initialized)
                OneExpButton(sender, e);
        }
        private void AM_rb_CheckedChanged(object sender, EventArgs e)
        {
            if (Initialized)
                OneExpButton(sender, e);
        }
        private void RandomData_check_CheckedChanged(object sender, EventArgs e)
        {
            if (Initialized)
                OneExpButton(sender, e);
        }
        private void SignalPicture_Click(object sender, EventArgs e)
        {
            string GraphName = "";
            double[] x = md.GetTime(0), y = new double[0];

            double SNR = Convert.ToDouble(SNRmin_param.Text);

            switch (MainGraphVision)
            {
                case 1:
                    if (PM2_rb.Checked)
                    {
                        y = NoisedSignals[0];
                    }
                    if (FT_rb.Checked)
                    {
                        y = NoisedSignals[1];
                    }
                    if (AM_rb.Checked)
                    {
                        y = NoisedSignals[2];
                    }
                    GraphName = "График сигнала с шумом. SNR = " + SNRmin_param.Text + "дБ";
                    break;
                case 0:
                    if (PM2_rb.Checked)
                    {
                        y = Signals[0];
                    }
                    if (FT_rb.Checked)
                    {
                        y = Signals[1];
                    }
                    if (AM_rb.Checked)
                    {
                        y = Signals[2];
                    }
                    GraphName = "График сигнала без шума";
                    break;
                case 2:
                    x = new double[x.Length * 3];
                    for (int i = 0; i < x.Length; i++)
                    {
                        x[i] = i / (double)x.Length * CountsPerBit * 3;
                    }
                    if (PM2_rb.Checked)
                    {
                        y = GetNoisedSignal(0, SNR);
                    }
                    if (FT_rb.Checked)
                    {
                        y = GetNoisedSignal(1, SNR);
                    }
                    if (AM_rb.Checked)
                    {
                        y = GetNoisedSignal(2, SNR);
                    }
                    GraphName = "Расширенный сигнал с шумом. SNR = " + SNRmin_param.Text + "дБ";
                    break;
            }

            mainDrawer.GraphName = GraphName;
            mainDrawer.DrawGraph(x, y);
            MainGraphVision = (MainGraphVision + 1) % 3;
        }

        public void ExpsSeries_button_Click(object sender, EventArgs e)
        {
            if (exp_thread != null)
                if (exp_thread.IsAlive)
                    exp_thread.Abort();
            UpdateValues();
            statisticsDrawer.SetRanges(new double[] { minSNR, maxSNR, -0.1, 1.1 });
            statisticsDrawer.AxisConfig(new int[] { 15, 13 });
            exp_thread = new Thread(new ThreadStart(Experiment));

            X = new double[3][];
            Y = new double[3][];
            p = new double[3];

            int stepsSNR = Convert.ToInt32(StepsSNR_param.Text);

            for (int i = 0; i < 3; i++)
            {
                Signals[i] = GetSignal(bits, i);
                X[i] = new double[Convert.ToInt16(StepsSNR_param.Text) + 1];
                Y[i] = new double[Convert.ToInt16(StepsSNR_param.Text) + 1];
                for (int j = 0; j < X[0].Length; j++)
                {
                    double current_SNR = minSNR + j / (double)stepsSNR * (maxSNR - minSNR);

                    X[i][j] = current_SNR;
                    Y[i][j] = 0;
                }
            }

            exp_thread.Start();
        }

        /// <summary>
        /// Основной эксперимент, все методы вызываются тут. Это же и начало потока
        /// </summary>
        public void Experiment()
        {
            double progress = 0;
            for (int expStep = 0; expStep < N; expStep++)
            {
                for (int SNR_idx = 0; SNR_idx < M + 1; SNR_idx++)
                {
                    double current_SNR = X[0][SNR_idx];
                    Parallel.For(0, 3, mode_idx =>
                    {
                        Signals[mode_idx] = GetSignal(bits, mode_idx);
                        OneStepOfExperiment(mode_idx, SNR_idx, current_SNR);

                    });
                    progress += 1.0 / ((double)N * (M + 1));
                    statisticsDrawer.GraphName = "Результаты, прогресс = " + (progress * 100).ToString("F2") + "%";
                    statisticsDrawer.DrawGraphMulti(X, Y, false);
                }
            }

            WriteData();

            /*StreamWriter sw = new StreamWriter("Results.txt", true);
            sw.Write("Данные: ");
            for (int i = 0; i < bits.Length; i++)
            {
                sw.Write(bits[i]);
                if (i % 4 == 3)
                    sw.Write("");
            }
            sw.WriteLine();
            for (int mode = 0; mode < 3; mode++)
            {
                string type = "";
                switch (mode)
                {
                    case 0:
                        type = "фазовая";
                        break;
                    case 1:
                        type = "частотный телеграф";
                        break;
                    case 2:
                        type = "амплитудная";
                        break;
                }
                sw.WriteLine("Модуляция - " + type);
                for (int i = 0; i < X[0].Length; i++)
                {
                    sw.WriteLine("SNR = {0}дБ, p = {1}", X[mode][i], Y[mode][i]);
                }
            }
            sw.WriteLine("Конец эксперимента\n");
            sw.Close();*/
        }

        /// <summary>
        /// Запись данных в excel
        /// </summary>
        public void WriteData()
        {
            string file_name = DateTime.Today.AddHours(DateTime.Now.Hour % 24).AddMinutes(DateTime.Now.Minute % 60).ToString();
            string date = DateTime.Now.ToString("MM-dd-HH-mm");
            string path = "D:\\Учеба\\my_rep_unn\\5й курс\\НИР\\Семестр 3\\Результаты xml\\";

            StreamWriter sw = new StreamWriter(path + coder + " " + coded + date + ".txt");

            XmlDocument xml = new XmlDocument();
            XmlNode root = xml.CreateElement("Types_of_modulation");
            xml.AppendChild(root);

            for (int mode = 0; mode < 3; mode++)
            {
                string m_type = "";
                switch (mode)
                {
                    case 0:
                        m_type = "PM";
                        break;
                    case 1:
                        m_type = "FT";
                        break;
                    case 2:
                        m_type = "AM";
                        break;
                }

                XmlNode type = xml.CreateElement(m_type);
                sw.WriteLine(m_type);
                sw.WriteLine("SNR values:");
                string specifier = "F2";
                CultureInfo culture = CultureInfo.CreateSpecificCulture("eu-ES");

                for (int i = 0; i < X[0].Length; i++)
                {
                    XmlNode snr = xml.CreateElement("SNR");
                    snr.InnerText = X[mode][i].ToString("F0", CultureInfo.InvariantCulture);
                    type.AppendChild(snr);

                    XmlNode p = xml.CreateElement("p");
                    p.InnerText = Y[mode][i].ToString(specifier, CultureInfo.InvariantCulture);
                    type.AppendChild(p);

                    sw.WriteLine(X[mode][i].ToString(specifier, CultureInfo.InvariantCulture));
                }
                root.AppendChild(type);

                sw.WriteLine("P values:");
                for (int i = 0; i < X[0].Length; i++)
                {
                    sw.WriteLine(Y[mode][i].ToString(specifier, CultureInfo.InvariantCulture));
                }
                sw.WriteLine();

            }

            sw.Close();

            xml.Save(path + coder + " " + coded + date + ".xml");
        }

        /// <summary>
        /// Один шаг эксперимента 
        /// </summary>
        /// <param name="mode"> Тип модуляции </param>
        /// <param name="SNR_idx"> Индекс SNR (для отрисовки) </param>
        /// <param name="SNR"> Значение SNR</param>
        private double OneStepOfExperiment(int mode, int SNR_idx, double SNR)
        {
            double[] current_signal = GetNoisedSignal(mode, SNR);
            double[] rxx = Rxx(current_signal, mode);

            int max = 0;
            for (int i = 1; i < rxx.Length; i++)
            {
                if (rxx[i] > rxx[max]) max = i;
            }

            X[mode][SNR_idx] = SNR;
            if (Math.Abs(Signals[mode].Length - max) <= CountsPerBit)
                Y[mode][SNR_idx] += 1.0 / (double)N;
            var to_return = max / ((2 * tau0 + CountsPerBit) * DFreq) * (2 * tau0 + CountsPerBit);
            return to_return;
        }

        /// <summary>
        /// Помещение чистого сигнала в промежуток и наложение шума
        /// </summary>
        /// <param name="mode"> Вид модуляции </param>
        /// <returns> Искаженный сигнал с выбранной модуляцией </returns>
        private double[] GetNoisedSignal(int mode, double SNR)
        {
            Random rand = new Random();
            double[] left = new double[Signals[mode].Length];
            double[] right = new double[left.Length];

            //tau0 = left.Length;

            /// Формируем массивы случайных битов
            int[] left_bits = new int[left.Length / CountsPerBit];
            int[] right_bits = new int[left_bits.Length];
            for (int i = 0; i < left_bits.Length; i++)
            {
                left_bits[i] = rand.Next(0, 2);
                right_bits[i] = rand.Next(0, 2);
            }

            /// Получаем для них сигналы с модуляцией
            md = new Modulation(
                        DFreq,
                        MainFreq,
                        ModFreq,
                        CountsPerBit,
                        Amp,
                        devAmp);
            switch (mode)
            {
                case 0:
                    left = md.PM2(left_bits);
                    right = md.PM2(right_bits);
                    break;
                case 1:

                    left = md.FT(left_bits);
                    right = md.FT(right_bits);
                    break;
                case 2:
                    left = md.AM(left_bits);
                    right = md.AM(right_bits);
                    break;
            }

            /// Склеиваем их с переданным
            double[] to_return = MergeArrays(MergeArrays(left, Signals[mode]), right);
            /// Накладываем шум
            to_return = NoiseArr(to_return, SNR);
            NoisedSignals[mode] = NoiseArr(Signals[mode], SNR);
            return to_return;
        }

        /// <summary>
        /// Наложение шума с заданной энергией на передаваемый массив
        /// </summary>
        /// <param name="array"> Массив, который будет зашумлен </param>
        /// <param name="SNR"> Значение SNR </param>
        /// <returns> Зашумленный массив </returns>
        private double[] NoiseArr(double[] array, double SNR)
        {
            Random rand = new Random();
            /// Начнем накладывать шум
            double[] noise = new double[array.Length];
            double[] to_return = new double[array.Length];
            for (int k = 0; k < array.Length; k++)
            {
                noise[k] = (rand.NextDouble() - 0.5) / 12;
            }
            for (int i = 1; i < 12; i++)
            {
                for (int k = 0; k < array.Length; k++)
                {
                    noise[k] += (rand.NextDouble() - 0.5) / 12;
                }
            }
            /// Отнормировали шум
            double s_energy = GetEnergy(array);
            double n_energy = GetEnergy(noise);
            double multiplier = Math.Sqrt(s_energy / n_energy * Math.Pow(10, -SNR / 10));
            for (int k = 0; k < array.Length; k++)
            {
                to_return[k] = array[k] + noise[k] * multiplier;
            }
            return to_return;
        }

        /// <summary>
        /// Получение чистого сигнала, в котором закодирован определенный текст
        /// </summary>
        /// <param name="str"> Массив бит </param>
        /// <param name="mode"> Вид модуляции </param>
        /// <returns> Чистый сигнал с выбранной модуляцией </returns>
        private double[] GetSignal(int[] bits, int mode)
        {
            md = new Modulation(
                  DFreq,
                  MainFreq,
                  ModFreq,
                  CountsPerBit,
                  Amp,
                  devAmp);
            double[] signal = new double[] { 0 };
            switch (mode)
            {
                case 0:
                    signal = md.PM2(bits);
                    break;
                case 1:
                    signal = md.FT(bits);
                    break;
                case 2:
                    signal = md.AM(bits);
                    break;
            }
            return signal;
        }

        /// <summary>
        /// Функция расчета АКФ
        /// </summary>
        /// <param name="arr"> Массив, для которого будет вычеслена АКФ </param>
        /// <param name="mode"> Костылёк </param>
        /// <returns> Массив со значениями АКФ </returns>
        private double[] Rxx(double[] arr, int mode)
        {
            double temp;
            List<double> rxx = new List<double>();
            for (int i = 0; i < arr.Length; i++)
            {
                temp = 0;
                for (int k = 0; k < Signals[0].Length; k++)
                {
                    if (i + k < arr.Length)
                        temp += arr[i + k] * NoisedSignals[mode][k];
                    else
                        temp += arr[i + k - arr.Length] * NoisedSignals[mode][k];
                }
                rxx.Add(temp);
            }
            return rxx.ToArray<double>();
        }

        /// <summary>
        /// Функция расчета АКФ
        /// </summary>
        /// <param name="arr"> Массив, для которого будет вычеслена АКФ </param>
        /// <param name="mode"> Костылёк </param>
        /// <returns> Массив со значениями АКФ </returns>
        private double[] Rxx(double[] arr)
        {
            double temp = 0;
            List<double> rxx = new List<double>();
            for (int i = 0; i < arr.Length; i++)
            {
                temp = 0;
                for (int j = i, k = 0; k < arr.Length; k++)
                {
                    if (i + j < arr.Length)
                        temp += arr[i + j] * arr[i];
                    else
                        temp += arr[i + j - arr.Length + 1] * arr[i];
                }
                rxx.Add(temp);
            }
            return rxx.ToArray<double>();
        }


        /// <summary>
        /// Получение энергии массива
        /// </summary>
        /// <param name="array"> Массив </param>
        /// <returns> Энергия массива array </returns>
        private static double GetEnergy(double[] array)
        {
            double energy = 0;
            for (int i = 0; i < array.Length; i++)
            {
                energy += array[i] * array[i];
            }
            return energy;
        }

        /// <summary>
        /// Слияние массивов
        /// </summary>
        /// <param name="arr1"> Левый массив </param>
        /// <param name="arr2"> Правый массив </param>
        /// <returns> {arr1, arr2} </returns>
        private static double[] MergeArrays(double[] arr1, double[] arr2)
        {
            List<double> to_return = new List<double>();
            to_return.AddRange(arr1);
            to_return.AddRange(arr2);
            return to_return.ToArray<double>();
        }
    }

    enum ModulationType : ushort
    {
        PM2 = 0,
        FT = 1,
        AM = 2
    }
}
