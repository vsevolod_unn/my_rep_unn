﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Coding;

public class LinearSystem
{
    private GF[,] initial_a_matrix;
    private GF[,] a_matrix;  // матрица A
    public GF[] x_vector;   // вектор неизвестных x
    private GF[] initial_b_vector;
    private GF[] b_vector;   // вектор b
    private GF[] u_vector;   // вектор невязки U
    private int size;            // размерность задачи

    public LinearSystem(GF[,] a_matrix, GF[] b_vector)
        : this(a_matrix, b_vector, new GF(2, 4))
    {
    }
    public LinearSystem(GF[,] a_matrix, GF[] b_vector, GF eps)
    {

        int b_length = b_vector.Length;
        int a_length = a_matrix.Length;

        this.initial_a_matrix = a_matrix;  // запоминаем исходную матрицу
        this.a_matrix = (GF[,])a_matrix.Clone(); // с её копией будем производить вычисления
        this.initial_b_vector = b_vector;  // запоминаем исходный вектор
        this.b_vector = (GF[])b_vector.Clone();  // с его копией будем производить вычисления
        this.x_vector = new GF[b_length];
        this.u_vector = new GF[b_length];
        this.size = b_length;

        GaussSolve();
    }

    public GF[] XVector
    {
        get
        {
            return x_vector;
        }
    }

    public GF[] UVector
    {
        get
        {
            return u_vector;
        }
    }

    // инициализация массива индексов столбцов
    private int[] InitIndex()
    {
        int[] index = new int[size];
        for (int i = 0; i < index.Length; ++i)
            index[i] = i;
        return index;
    }

    // поиск главного элемента в матрице
    private GF FindR(int row, int[] index)
    {
        int max_index = row;
        GF max = a_matrix[row, index[max_index]];
        GF max_abs = max;
        //if(row < size - 1)
        for (int cur_index = row + 1; cur_index < size; ++cur_index)
        {
            GF cur = a_matrix[row, index[cur_index]];
            GF cur_abs = cur;
            if (cur_abs.value > max_abs.value)
            {
                max_index = cur_index;
                max = cur;
                max_abs = cur_abs;
            }
        }

        // меняем местами индексы столбцов
        int temp = index[row];
        index[row] = index[max_index];
        index[max_index] = temp;

        return max;
    }

    // Нахождение решения СЛУ методом Гаусса
    private void GaussSolve()
    {
        int[] index = InitIndex();
        GaussForwardStroke(index);
        GaussBackwardStroke(index);
        GaussDiscrepancy();
    }

    // Прямой ход метода Гаусса
    private void GaussForwardStroke(int[] index)
    {
        // перемещаемся по каждой строке сверху вниз
        for (int i = 0; i < size; ++i)
        {
            // 1) выбор главного элемента
            GF r = FindR(i, index);

            // 2) преобразование текущей строки матрицы A
            for (int j = 0; j < size; ++j)
                a_matrix[i, j] /= r;

            // 3) преобразование i-го элемента вектора b
            b_vector[i] /= r;

            // 4) Вычитание текущей строки из всех нижерасположенных строк
            for (int k = i + 1; k < size; ++k)
            {
                GF p = a_matrix[k, index[i]];
                for (int j = i; j < size; ++j)
                    a_matrix[k, index[j]] -= a_matrix[i, index[j]] * p;
                b_vector[k] -= b_vector[i] * p;
                a_matrix[k, index[i]] = new GF(2, 4);
            }
        }
    }

    // Обратный ход метода Гаусса
    private void GaussBackwardStroke(int[] index)
    {
        // перемещаемся по каждой строке снизу вверх
        for (int i = size - 1; i >= 0; --i)
        {
            // 1) задаётся начальное значение элемента x
            GF x_i = b_vector[i];

            // 2) корректировка этого значения
            for (int j = i + 1; j < size; ++j)
                x_i -= x_vector[index[j]] * a_matrix[i, index[j]];
            x_vector[index[i]] = x_i;
        }
    }

    // Вычисление невязки решения
    // U = b - x * A
    // x - решение уравнения, полученное методом Гаусса
    private void GaussDiscrepancy()
    {
        for (int i = 0; i < size; ++i)
        {
            GF actual_b_i = new GF(2, 4);   // результат перемножения i-строки 
                                            // исходной матрицы на вектор x
            for (int j = 0; j < size; ++j)
                actual_b_i += initial_a_matrix[i, j] * x_vector[j];
            // i-й элемент вектора невязки
            u_vector[i] = initial_b_vector[i] - actual_b_i;
        }
    }
}

public class LS
{
    public GF[][] M;
    public GF[][] invM;
    public GF[] vect_x;
    public GF[] vect_b;
    public GF det;

    public LS(GF[][] _M, GF[] _vect_b)
    {
        M = _M;
        invM = new GF[M.GetLength(0)][];
        for (int i = 0; i < M[0].Length; i++)
        {
            invM[i] = new GF[M[0].Length];
        }
        vect_b = _vect_b;
        vect_x = new GF[vect_b.Length];
    }

    public bool Solve()
    {
        det = Det(M);
        int d1 = M.GetLength(0), d2 = M[0].Length;
        if (d1 == 1)
        {
            vect_x[0] = vect_b[0] / det;
            invM[0][0] = M[0][0] / det;
            return true;
        }
        if (det.value == 0)
        {
            return false;
        }
        if (det.value != 0)
        {
            invM = Maths.GaussJordan(M);
            /*for (int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d1; j++)
                {
                    int m = d1 - 1;
                    GF[][] temp_matr = new GF[m][];
                    for (int k = 0; k < m; k++)
                        temp_matr[k] = new GF[m];
                    Get_matr(M, d1, temp_matr, i, j);
                    invM[i][j] = new GF(2, 4, (int)Math.Pow(-1, i + j + 2)) * Det(temp_matr) / det;
                }
            }*/
        }
        for (int i = 0; i < d1; i++)
        {
            vect_x[i] = new GF(2, 4);
            for (int j = 0; j < d1; j++)
            {
                vect_x[i] += invM[i][j] * vect_b[j];
            }
        }
        return true;
    }

    public GF Det(GF[][] matr)
    {
        if (matr.GetLength(0) == 1)
            return matr[0][0];
        GF result = new GF(2, 4);   //временная переменная для хранения определителя
        int k = 1;      //степень
        int d1 = matr.GetLength(0), d2 = matr[0].Length;
        if (d1 == 1)
            result = matr[0][0];
        else if (d1 == 2)
            result = matr[0][0] * matr[1][1] - matr[1][0] * matr[0][1];
        else
        {
            for (int i = 0; i < d1; i++)
            {
                int m = d1 - 1;
                GF[][] temp_matr = new GF[m][];
                for (int j = 0; j < m; j++)
                    temp_matr[j] = new GF[m];
                Get_matr(matr, d1, temp_matr, 0, i);
                result = result + new GF(2, 4, k * matr[0][i].value) * Det(temp_matr);
                k = -k;
            }
        }
        return result;
    }

    public GF[][] Get_matr(GF[][] matr, int n, GF[][] temp_matr, int indRow, int indCol)
    {
        int ki = 0;
        for (int i = 0; i < n; i++)
        {
            if (i != indRow)
            {
                for (int j = 0, kj = 0; j < n; j++)
                {
                    if (j != indCol)
                    {
                        temp_matr[ki][kj] = matr[i][j];
                        kj++;
                    }
                }
                ki++;
            }
        }
        return temp_matr;
    }

    public GF[][] Get_matr(GF[][] matr, int[] indRow, int[] indCol)
    {
        int d1 = M.GetLength(0), d2 = M[0].Length;
        int ki = 0;
        bool y_n1, y_n2;
        GF[][] temp_matr = new GF[d1 - indRow.Length][];
        for (int i = 0; i < d1; i++)
        {
            y_n1 = true;
            for (int idx1 = 0; idx1 < indRow.Length; idx1++)
                if (i == indRow[idx1]) y_n1 = false;
            if (y_n1)
            {
                temp_matr[ki] = new GF[d2 - indRow.Length];
                for (int j = 0, kj = 0; j < d2; j++)
                {
                    y_n2 = true;
                    for (int idx2 = 0; idx2 < indRow.Length; idx2++)
                        if (j == indCol[idx2]) y_n2 = false;
                    if (y_n2)
                    {
                        temp_matr[ki][kj] = matr[i][j];
                        kj++;
                    }
                }
                ki++;
            }
        }
        return temp_matr;
    }

    public GF[] Get_vect(GF[] vect, int[] idx)
    {
        bool y_n = true;
        int d1 = vect.Length;
        GF[] new_vect = new GF[d1 - idx.Length];
        for (int i = 0, ki = 0; i < d1; i++)
        {
            y_n = true;
            for (int bool_idx = 0; bool_idx < idx.Length; bool_idx++)
                if (i == idx[bool_idx]) y_n = false;
            if (y_n)
            {
                new_vect[ki] = vect[i];
                ki++;
            }
        }
        return new_vect;
    }
}

class Maths
{
    // Метод вычисления обратной матрицы Гаусса-Жордана
    public static GF[][] GaussJordan(GF[][] matr)
    {
        int n = matr.GetLength(0); //Размерность начальной матрицы

        GF[][] M = new GF[n][];
        for (int i = 0; i<n; i++)
        {
            M[i] = new GF[n];
            for (int j = 0; j<n; j++)
            {
                M[i][j] = new GF(2, 4, matr[i][j].value);
            }
        }

        GF[][] invM = new GF[n][]; //Единичная матрица (искомая обратная матрица)
        for (int i = 0; i < n; i++)
        {
            invM[i] = new GF[n];
        }
        for (int i = 0; i<n; i++)
        { 
            for (int j = 0; j<n; j++)
            {
                invM[i][j] = new GF(2, 4);
            }
            invM[i][i] = new GF(2, 4, 1);
        }

        GF[][] ExpandedM = new GF[n][]; //Общая матрица, получаемая скреплением Начальной матрицы и единичной
        for (int i = 0; i <n; i++)
            ExpandedM[i] = new GF[2*n];

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
            {
                ExpandedM[i][j] = M[i][j];
                ExpandedM[i][j + n] = invM[i][j];
            }

        //Прямой ход (Зануление нижнего левого угла)
        for (int k = 0; k < n; k++) //k-номер строки
        {
            for (int i = 0; i < 2 * n; i++) //i-номер столбца
                ExpandedM[k][i] = ExpandedM[k][i] / M[k][k]; //Деление k-строки на первый член !=0 для преобразования его в единицу
            for (int i = k + 1; i < n; i++) //i-номер следующей строки после k
            {
                GF K = ExpandedM[i][k] / ExpandedM[k][k]; //Коэффициент
                for (int j = 0; j < 2 * n; j++) //j-номер столбца следующей строки после k
                    ExpandedM[i][j] = ExpandedM[i][j] - ExpandedM[k][j] * K; //Зануление элементов матрицы ниже первого члена, преобразованного в единицу
            }
            for (int i = 0; i < n; i++) //Обновление, внесение изменений в начальную матрицу
                for (int j = 0; j < n; j++)
                    M[i][j] = ExpandedM[i][j];
        }

        //Обратный ход (Зануление верхнего правого угла)
        for (int k = n - 1; k > -1; k--) //k-номер строки
        {
            for (int i = 2 * n - 1; i > -1; i--) //i-номер столбца
                ExpandedM[k][i] = ExpandedM[k][i] / M[k][k];
            for (int i = k - 1; i > -1; i--) //i-номер следующей строки после k
            {
                GF K = ExpandedM[i][k] / ExpandedM[k][k];
                for (int j = 2 * n - 1; j > -1; j--) //j-номер столбца следующей строки после k
                    ExpandedM[i][j] = ExpandedM[i][j] - ExpandedM[k][j] * K;
            }
        }

        //Отделяем от общей матрицы
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                invM[i][j] = ExpandedM[i][j + n];

        return invM;
    }





}
