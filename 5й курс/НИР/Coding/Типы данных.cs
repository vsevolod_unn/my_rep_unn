﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coding
{
    public class Vector
    {
        public int Length;
        public int[] code;

        public Vector() { }
        public Vector(int N)
        {
            Length = N;
            code = new int[N];
        }
        public Vector(Vector a)
        {
            this.Length = a.Length;
            this.code = new int[this.Length];
            for (int i = 0; i < a.Length; i++)
                this.code[i] = a.code[i];
        }
        public Vector(int[] a)
        {
            this.Length = a.Length;
            this.code = new int[this.Length];
            for (int i = 0; i < a.Length; i++)
                this.code[i] = a[i];
        }

        public static Vector operator +(Vector a, Vector b)
        {
            int s1, s2;
            Vector res;
            if (a.Length > b.Length)
            {
                s1 = a.Length;
                s2 = b.Length;
            }
            else
            {
                s1 = b.Length;
                s2 = a.Length;
            }
            res = new Vector(s1);
            for (int i = 0; i < s1 - s2; i++)
            {
                res.code[i] = a.code[i];
            }
            for (int i = s1 - s2; i < s1; i++)
            {
                res.code[i] = (a.code[i] + b.code[i - (s1 - s2)]) % 2;
            }
            return res;
        }
        public static Vector operator <<(Vector a, int b)
        {
            int s1 = a.Length + b;
            Vector res = new Vector(s1);
            for (int i = 0; i < a.Length; i++)
            {
                res.code[i] = a.code[i];
            }
            for (int i = a.Length; i < s1; i++)
            {
                res.code[i] = 0;
            }
            return res;
        }
        public static Vector operator /(Vector a, Vector b)
        {
            int s1, s2;
            Vector res = new Vector();
            if (a.Length > b.Length)
            {
                s1 = a.Length;
                s2 = b.Length;
            }
            else
            {
                s1 = b.Length;
                s2 = a.Length;
            }
            res = new Vector(s1 - s2 + 1);
            int deg = s1;
            List<int> tmp = new List<int>();
            Vector buf = new Vector(a);
            while (deg >= s2)
            {
                tmp.Add(deg - s2);
                var v = b << (deg - s2);
                var c = buf + v;
                buf = new Vector((buf + (b << (deg - s2))));
                buf.cut();
                deg = buf.Length;
            }
            for (int i = 0; i < tmp.Count; i++)
            {
                res.code[res.Length - tmp[i] - 1] = 1;
            }
            return res;
        }

        public void cut()
        {
            int last_idx = 0;
            for (int i = 0; i < this.Length; i++)
            {
                if (this.code[i] == 0) last_idx = i + 1;
                else break;
            }
            Vector buf = new Vector(this);
            this.Length -= last_idx;
            this.code = new int[this.Length];
            for (int i = 0; i < this.Length; i++)
            {
                this.code[i] = buf.code[i + last_idx];
            }
        }
        public int cdtoi()
        {
            int result = 0;

            for (int i = 0; i < code.Length; i++)
            {
                result += (int)(code[i] * Math.Pow(2, code.Length - i - 1));
            }

            return result;
        }
        public int ham_dist(Vector vect1)
        {
            int to_return = 0;
            for (int i = 0; i < vect1.Length; i++)
            {
                if (vect1.code[i] == this.code[i]) to_return++;
            }
            return to_return;
        }
        public int ham_w()
        {
            int w = 0;
            for (int i = 0; i < this.Length; i++)
            {
                if (this.code[i] == 1) w++;
            }
            return w;
        }
    }

    public class GF
    {
        public int max_pow;
        public int basis;
        public int value;
        public int pow;
        public int p;
        public int[] cvalue;
        public GF inverse_value;
        public static GF poly = new GF(2, 5, 19);
        public int ipoly;

        public GF()
        {
            basis = 1;
            value = 0;
        }
        public GF(int _p, int _pow, int _value = 0, SortedDictionary<int, int> m = null)
        {
            p = _p;
            pow = _pow;
            basis = (int)Math.Pow(p, pow);

            if (basis == 256)
                ipoly = 283;
            if (basis == 16)
                ipoly = 19;

            if (_value >= 0)
            {
                value = _value % (basis);
            }
            else
            {
                if (_value % (basis) == 0)
                    value = 0;
                else
                    value = _value % (basis) + (basis);
            }

            itocd(value, out int[] cd);
            cvalue = cd;
            //itocd(inverse_value, out cd);
            //cinverse_value = cd;
            expand();
            for (int i = 0; i < pow; i++)
            {
                if (cvalue[i] == 1) { max_pow = pow - i - 1; break; }
            }
        }

        public GF(int _p, int _pow, int[] _cvalue)
        {
            p = _p;
            pow = _pow;
            cvalue = _cvalue;
            expand();
            for (int i = 0; i < pow; i++)
            {
                if (cvalue[i] == 1) { max_pow = pow - i - 1; break; }
            }
        }

        public void find_inverse()
        {
            for (int i = 0; i < this.basis; i++)
            {
                if ((this * new GF(2, 4, i)).value == 1)
                    inverse_value = new GF(2, 4, i);
            }
        }

        public static GF operator +(GF a, GF b)
        {
            GF new_b, new_a;
            if (a.pow > b.pow)
            { new_b = new GF(a.p, a.pow, b.value); new_a = a; }
            else
            { new_b = b; new_a = new GF(a.p, b.pow, a.value); }

            int[] code = new int[new_a.pow];
            for (int i = 0; i < new_a.pow; i++)
            {
                code[i] = (new_a.cvalue[i] + new_b.cvalue[i]) % new_a.p;
            }
            int value;
            cdtoi(code, out value);
            return new GF(new_a.p, new_a.pow, value);
        }
        public static GF operator -(GF a, GF b)
        {
            return a + b;
        }
        public static GF operator *(GF a, GF b)
        {
            if (a.value == 0 || b.value == 0)
                return new GF(2, 4, 0);
            var power = (MAP.get_p[a.value] + MAP.get_p[b.value]) % (a.basis - 1);
            var to_return = new GF(2, 4, MAP.get_gf[power]);
            return to_return;
        }
        public static GF operator /(GF a, GF b)
        {
            if (a.value == 0 || b.value == 0)
                return new GF(2, 4, 0);
            else
            {
                int pow = MAP.get_p[a.value] - MAP.get_p[b.value];
                if (pow < 0) pow += a.basis - 1;
                return new GF(2, 4, MAP.get_gf[pow]);
            }
        }
        public static bool operator ==(GF a, GF b)
        {
            if (a.value == b.value)
                return true;
            else
                return false;
        }
        public static bool operator !=(GF a, GF b)
        {
            if (a.value == b.value)
                return false;
            else
                return true;
        }

        public static GF mult(GF a, GF b)
        {
            GF g1 = new GF(a.p, a.max_pow + b.max_pow + 1, a.value);
            GF g2 = new GF(a.p, a.max_pow + b.max_pow + 1, 0);
            for (int i = 0; i <= b.max_pow; i++)
            {
                if (b.cvalue[b.pow - i - 1] == 1)
                    g2 += new GF(a.p, g1.pow, g1.value * (int)Math.Pow(a.p, i));
            }
            if (g2.value >= a.basis)
            {
                div(g2, poly, out g2, out GF remainder);
                return new GF(a.p, a.pow, remainder.value);
            }
            else
            {
                return new GF(a.p, a.pow, g2.value);
            }
        }
        public static void div(GF a, GF b, out GF res, out GF remainder)
        {
            if (a.max_pow < b.max_pow)
            {
                res = new GF(a.p, a.pow);
                remainder = new GF(a.p, a.pow, a.value ^ b.value);
            }
            else
            {
                GF tmp = a;
                int result = 0;
                for (int i = 0; i <= a.max_pow - b.max_pow; i++)
                {
                    if (tmp.cvalue[tmp.pow - tmp.max_pow - i - 1] == 1)
                    {
                        var add_pow = tmp.max_pow - b.max_pow - i;
                        var add = (int)Math.Pow(tmp.p, add_pow);
                        result += add;
                        remainder = new GF(a.p, a.pow, b.value * add);
                        tmp = tmp + remainder;
                    }
                }
                remainder = tmp;
                res = new GF(a.p, a.pow, result);
            }
        }
        public void expand()
        {
            int[] new_cvalue = new int[pow];
            int i = 0;
            for (; i < pow - cvalue.Length; i++)
            {
                new_cvalue[i] = 0;
            }
            for (; i < pow; i++)
            {
                new_cvalue[i] = cvalue[i - (pow - cvalue.Length)];
            }
            cvalue = new_cvalue;

            /*
            int[] new_cinverse_value = new int[pow];
            i = 0;
            for (; i < pow - cinverse_value.Length; i++)
            {
                new_cinverse_value[i] = 0;
            }
            for (; i < pow; i++)
            {
                new_cinverse_value[i] = cinverse_value[i - (pow - cinverse_value.Length)];
            }
            cinverse_value = new_cvalue;
            */
        }
        public static int cdtoi(int[] code, out int res)
        {
            res = 0;
            if (code.Length < 1)
            {
                return 1;
            }
            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] < 0 || code[i] > 1)
                {
                    return 2;
                }
                res += (int)(code[i] * Math.Pow(2, code.Length - i - 1));
            }
            return 0;
        }
        public int itocd(int value, out int[] code)
        {
            if (value == 0) code = new int[] { 0 };
            else
            {
                code = new int[(int)Math.Log(value, 2) + 1];
                for (int i = 0; i < code.Length; i++)
                {
                    if (value > 0)
                        if (value - Math.Pow(2, code.Length - 1 - i) >= 0)
                        {
                            code[i] = 1;
                            value -= (int)Math.Pow(2, code.Length - 1 - i);
                        }
                        else
                            code[i] = 0;
                }
            }
            return 0;
        }
    }

    public class Poly : GF
    {
        new public ulong value;
        new public ulong[] cvalue;
        new public GF[] poly;
        public GF[] arr
        {
            get
            {
                return poly;
            }
        }
        public static Poly POLY = new Poly(16, 15,
               (ulong)Math.Pow(16, 6) * 1 +
               (ulong)Math.Pow(16, 5) * 7 +
               (ulong)Math.Pow(16, 4) * 9 +
               (ulong)Math.Pow(16, 3) * 3 +
               (ulong)Math.Pow(16, 2) * 12 +
               (ulong)Math.Pow(16, 1) * 10 +
               (ulong)Math.Pow(16, 0) * 12);

        public Poly() { }
        public Poly(int _p, int _pow, ulong _value = 0)
        {
            p = _p;
            pow = _pow;
            value = _value;
            cvalue = new ulong[pow];
            poly = new GF[pow];
            IntToCode(value, out cvalue);

            for (int i = 0; i < pow; i++)
            {
                poly[i] = new GF(2, 4, Convert.ToInt32(cvalue[i]));
                poly[i].find_inverse();
            }

            UpdateMaxPow();
        }
        public Poly(int _p, int _pow, GF[] _values)
        {
            p = _p;
            pow = _pow;
            cvalue = new ulong[pow];
            poly = new GF[pow];

            for (int i = 0; i < _values.Length; i++)
            {
                poly[pow - 1 - (_values.Length - 1 - i)] = new GF(_values[i].p, _values[i].pow, _values[i].value);
                poly[pow - 1 - (_values.Length - 1 - i)].find_inverse();
            }
            for (int i = _values.Length; i < _pow; i++)
            {
                poly[pow - 1 - i] = new GF(2, 4);
                poly[pow - 1 - i].inverse_value = new GF(2, 4);
            }

            UpdateMaxPow();
        }

        public static Poly operator +(Poly a, Poly b)
        {
            Poly to_return = new Poly(a.p, a.pow);
            for (int i = 0; i < a.pow; i++)
            {
                to_return.poly[i] = a.poly[i] + b.poly[i];
            }
            to_return.Update();
            return to_return;
        }
        public static Poly operator *(Poly a, Poly b)
        {
            if (a.max_pow + b.max_pow < a.pow)
            {
                Poly res = new Poly(16, a.pow);
                //if (a.max_pow > b.max_pow)
                for (int i = a.pow - 1 - a.max_pow; i < a.pow; i++)
                {
                    for (int j = b.pow - 1 - b.max_pow; j < b.pow; j++)
                    {
                        int idx = res.pow - 1 - (res.pow - 1 - i) - (res.pow - 1 - j);
                        res.poly[idx] += a.poly[i] * b.poly[j];
                    }
                }
                res.Update();
                return res;
            }


            Poly g1 = new Poly(a.p, a.max_pow + b.max_pow + 1, a.value);
            Poly g2 = new Poly(a.p, a.max_pow + b.max_pow + 1, 0);

            for (int i = 0; i <= b.max_pow; i++)
            {
                if (b.cvalue[b.pow - i - 1] != 0)
                    g2 += new Poly(a.p, g1.pow, (ulong)(g1.value * (ulong)Math.Pow(a.p, i) * b.cvalue[b.pow - i - 1]));
            }
            if (g2.value >= Convert.ToUInt64(a.basis))
            {
                div(g2, POLY, out g2, out Poly remainder);
                return new Poly(a.p, a.pow, remainder.value);
            }
            else
            {
                return new Poly(a.p, a.pow, g2.value);
            }
        }
        public static Poly operator /(Poly a, Poly b)
        {
            if (a.pow < b.pow)
            {
                a.Update();
                return a;
            }
            else
            {
                Poly divider = b;
                Poly tmp_res = a;
                ulong result = 0;
                for (int i = 0; i < a.max_pow - b.max_pow + 1; i++)
                {
                    divider = b;
                    if (tmp_res.poly[tmp_res.pow - 1 - tmp_res.max_pow].value != 0)
                    {
                        var add_pow = tmp_res.max_pow - divider.max_pow;
                        if (add_pow >= 0)
                        {
                            divider = new Poly(b.p, b.pow, b.value * (ulong)Math.Pow(a.p, add_pow));
                            var multier = divider.poly[divider.pow - divider.max_pow - 1].inverse_value * tmp_res.poly[tmp_res.pow - tmp_res.max_pow - 1];
                            for (int k = 0; k < divider.pow; k++)
                            {
                                divider.poly[k] *= multier;
                            }
                        }
                        divider.Update();

                        tmp_res = tmp_res + divider;

                        tmp_res.Update();

                        if (tmp_res.max_pow < b.max_pow)
                            return tmp_res;
                    }
                }
                return tmp_res;
            }
        }
        public static bool operator ==(Poly a, Poly b)
        {
            bool answer = true;
            for (int i = 0; i < a.pow && answer; i++)
            {
                answer &= a.poly[i] == b.poly[i];
            }
            return answer;
        }
        public static bool operator !=(Poly a, Poly b)
        {
            bool answer = true;
            for (int i = 0; i < a.pow && answer; i++)
            {
                answer &= a.poly[i] == b.poly[i];
            }
            return !answer;
        }
        public static Poly operator <<(Poly a, int shift)
        {
            Poly b = new Poly(a.p, a.pow);
            for (int i = 0; i < a.pow - shift; i++)
            {
                b.poly[i] = a.poly[i + shift];
            }
            b.Update();
            return b;
        }
        public static Poly operator >>(Poly a, int shift)
        {
            Poly b = new Poly(a.p, a.pow);
            for (int i = shift; i < a.pow; i++)
            {
                b.poly[i] = a.poly[i - shift];
            }
            b.Update();
            return b;
        }
        public void push(GF elem)
        {
            int idx = this.pow - 1;
            for (int i = 0; i < this.pow; i++)
            {
                if (this.poly[i].value != 0) { idx = i - 1; break; }
            }
            this.poly[idx] = elem;
            this.Update();
        }
        public GF paste(GF a)
        {
            GF res = new GF(2, 4);
            for (int i = 0; i < this.pow; i++)
            {
                if (this.poly[i].value == 0)
                    continue;
                else
                {
                    GF temp_res = new GF(2, 4, 1);
                    for (int p = 0; p < this.pow - i - 1; p++)
                    {
                        temp_res *= a;
                    }
                    temp_res *= this.poly[i];
                    res += new GF(2, 4, temp_res.value);
                }
            }
            return res;
        }

        public static void div(Poly a, Poly b, out Poly res, out Poly remainder)
        {
            if (a.pow < b.pow)
            {
                res = new Poly(a.p, a.pow, 0);
                remainder = a;
            }
            else
            {
                Poly divider = b;
                Poly tmp_res = a;
                ulong result = 0;
                for (int i = 0; i <= a.max_pow - b.max_pow; i++)
                {
                    divider = b;
                    if (tmp_res.poly[tmp_res.pow - 1 - tmp_res.max_pow].value != 0)
                    {
                        var add_pow = tmp_res.max_pow - divider.max_pow;
                        divider = new Poly(b.p, b.pow, b.value * (ulong)Math.Pow(a.p, add_pow));
                        var multier = divider.poly[divider.pow - divider.max_pow - 1].inverse_value * tmp_res.poly[tmp_res.pow - tmp_res.max_pow - 1];
                        for (int k = 0; k < divider.pow; k++)
                        {
                            divider.poly[k] *= multier;
                        }

                        tmp_res = tmp_res + divider;
                        tmp_res.UpdateValue();
                        tmp_res.UpdateMaxPow();
                    }
                }
                remainder = tmp_res;
                res = new Poly(a.p, a.pow, result);
            }
        }

        public int IntToCode(ulong _value, out ulong[] code)
        {
            var val = _value;
            if (val == 0) code = new ulong[this.pow];
            else
            {
                code = new ulong[this.pow];
                for (int i = 0; i < code.Length; i++)
                {
                    if (val > 0)
                        if (val - Math.Pow(this.p, code.Length - 1 - i) >= 0)
                        {
                            var power = (ulong)Math.Pow(this.p, code.Length - 1 - i);
                            code[i] = (ulong)(val / power);
                            var minus = code[i] * power;
                            val -= minus;
                        }
                        else
                            code[i] = 0;
                }
            }
            return 0;
        }
        public int CodeToInt(ulong[] code, out ulong res)
        {
            res = 0;
            if (code.Length < 1)
            {
                return 1;
            }
            for (int i = 0; i < code.Length; i++)
            {
                if (code[i] < 0 || code[i] > 1)
                {
                    return 2;
                }
                res += (ulong)(code[i] * Math.Pow(2, code.Length - i - 1));
            }
            return 0;
        }

        public void UpdateValue()
        {
            for (int i = 0; i < pow; i++)
            {
                cvalue[i] = (ulong)poly[i].value;
            }

            value = 0;
            for (int i = 0; i < pow; i++)
            {
                value += (ulong)Math.Pow(p, pow - i - 1) * Convert.ToUInt64(poly[i].value);
                poly[i].find_inverse();
            }
            //IntToCode(value, out cvalue);
        }
        public void UpdateMaxPow()
        {
            for (int i = 0; i < this.pow; i++)
            {
                if (poly[i].value != 0) { max_pow = pow - 1 - i; break; }
            }
        }
        public void Update()
        {
            UpdateValue();
            UpdateMaxPow();
        }
    }

    public class Poly8
    {
        public int p, pow, max_pow;
        public int[] cvalue;
        public GF[] poly;
        public static Poly8 POLY = new Poly8(16, 15);

        public Poly8() { }
        public Poly8(int _p, int _pow)
        {
            p = _p;
            pow = _pow;
            cvalue = new int[pow];
            poly = new GF[pow];

            for (int i = 0; i < pow; i++)
            {
                poly[i] = new GF(2, 8);
            }
            UpdateMaxPow();
        }
        public Poly8(int _p, int _pow, GF[] _values)
        {
            p = _p;
            pow = _pow;
            cvalue = new int[pow];
            poly = new GF[pow];

            for (int i = 0; i < _values.Length; i++)
            {
                poly[pow - 1 - (_values.Length - 1 - i)] = new GF(_values[i].p, _values[i].pow, _values[i].value);
                poly[i].find_inverse();
            }

            UpdateMaxPow();
        }


        public static Poly8 operator +(Poly8 a, Poly8 b)
        {
            Poly8 to_return = new Poly8(a.p, a.pow);
            for (int i = 0; i < a.pow; i++)
            {
                to_return.poly[i] = a.poly[i] + b.poly[i];
            }
            to_return.Update();
            return to_return;
        }
        public static Poly8 operator *(Poly8 a, Poly8 b)
        {
            if (a.max_pow + b.max_pow < a.pow)
            {
                Poly8 res = new Poly8(16, a.pow);
                for (int i = a.pow - 1 - a.max_pow; i < a.pow; i++)
                {
                    for (int j = b.pow - 1 - b.max_pow; j < b.pow; j++)
                    {
                        int idx = res.pow - 1 - (res.pow - 1 - i) - (res.pow - 1 - j);
                        res.poly[idx] += a.poly[i] * b.poly[j];
                    }
                }
                res.Update();
                return res;
            }


            Poly8 g1 = new Poly8(a.p, a.max_pow + b.max_pow + 1, a.poly);
            Poly8 g2 = new Poly8(a.p, a.max_pow + b.max_pow + 1);

            for (int i = 0; i <= b.max_pow; i++)
            {
                if (b.cvalue[b.pow - i - 1] != 0)
                    g2 = g2 + g1 << i;
            }
            if (g2.max_pow >= a.pow)
            {
                div(g2, POLY, out g2, out Poly8 remainder);
                return new Poly8(a.p, a.pow, remainder.poly);
            }
            else
            {
                return new Poly8(a.p, a.pow, g2.poly);
            }
        }
        public static Poly8 operator /(Poly8 a, Poly8 b)
        {
            if (a.pow < b.pow)
            {
                a.Update();
                return a;
            }
            else
            {
                Poly8 divider = b;
                Poly8 tmp_res = a;
                ulong result = 0;
                for (int i = 0; i < a.max_pow - b.max_pow + 1; i++)
                {
                    divider = b;
                    if (tmp_res.poly[tmp_res.pow - 1 - tmp_res.max_pow].value != 0)
                    {
                        var add_pow = tmp_res.max_pow - divider.max_pow;
                        if (add_pow >= 0)
                        {
                            divider = b << add_pow;
                            var multier = divider.poly[divider.pow - divider.max_pow - 1].inverse_value * tmp_res.poly[tmp_res.pow - tmp_res.max_pow - 1];
                            for (int k = 0; k < divider.pow; k++)
                            {
                                divider.poly[k] *= multier;
                            }
                        }
                        divider.Update();

                        tmp_res = tmp_res + divider;

                        tmp_res.Update();

                        if (tmp_res.max_pow < b.max_pow)
                            return tmp_res;
                    }
                }
                return tmp_res;
            }
        }
        public static bool operator ==(Poly8 a, Poly8 b)
        {
            bool answer = true;
            for (int i = 0; i < a.pow && answer; i++)
            {
                answer &= a.poly[i] == b.poly[i];
            }
            return answer;
        }
        public static bool operator !=(Poly8 a, Poly8 b)
        {
            bool answer = true;
            for (int i = 0; i < a.pow && answer; i++)
            {
                answer &= a.poly[i] == b.poly[i];
            }
            return !answer;
        }
        public static Poly8 operator <<(Poly8 a, int shift)
        {
            Poly8 b = new Poly8(a.p, a.pow);
            for (int i = 0; i < a.pow - shift; i++)
            {
                b.poly[i] = a.poly[i + shift];
            }
            b.Update();
            return b;
        }
        public static Poly8 operator >>(Poly8 a, int shift)
        {
            Poly8 b = new Poly8(a.p, a.pow);
            for (int i = shift; i < a.pow; i++)
            {
                b.poly[i] = a.poly[i - shift];
            }
            b.Update();
            return b;
        }
        public void push(GF elem)
        {
            int idx = this.pow - 1;
            for (int i = 0; i < this.pow; i++)
            {
                if (this.poly[i].value != 0) { idx = i - 1; break; }
            }
            this.poly[idx] = elem;
            this.Update();
        }
        public GF paste(GF a)
        {
            GF res = new GF(a.p, a.pow);
            for (int i = 0; i < this.pow; i++)
            {
                if (this.poly[i].value == 0)
                    continue;
                else
                {
                    GF temp_res = new GF(a.p, a.pow, 1);
                    for (int p = 0; p < this.pow - i - 1; p++)
                    {
                        temp_res *= a;
                    }
                    temp_res *= this.poly[i];
                    res += new GF(a.p, a.pow, temp_res.value);
                }
            }
            return res;
        }

        public static void div(Poly8 a, Poly8 b, out Poly8 res, out Poly8 remainder)
        {
            if (a.pow < b.pow)
            {
                res = new Poly8(a.p, a.pow);
                remainder = a;
            }
            else
            {
                Poly8 divider;
                Poly8 tmp_res = new Poly8(a.p, a.pow, a.poly);
                for (int i = 0; i <= a.max_pow - b.max_pow; i++)
                {
                    divider = b;
                    if (tmp_res.poly[tmp_res.pow - 1 - tmp_res.max_pow].value != 0)
                    {
                        var add_pow = tmp_res.max_pow - divider.max_pow;
                        divider = b << add_pow;
                        var multier = divider.poly[divider.pow - divider.max_pow - 1].inverse_value * tmp_res.poly[tmp_res.pow - tmp_res.max_pow - 1];
                        for (int k = 0; k < divider.pow; k++)
                        {
                            divider.poly[k] *= multier;
                        }

                        tmp_res = tmp_res + divider;
                        tmp_res.UpdateValue();
                        tmp_res.UpdateMaxPow();
                    }
                }
                remainder = tmp_res;
                res = new Poly8(a.p, a.pow);
            }
        }

        public void UpdateValue()
        {
            for (int i = 0; i < pow; i++)
            {
                cvalue[i] = poly[i].value;
            }
        }
        public void UpdateMaxPow()
        {
            for (int i = 0; i < this.pow; i++)
            {
                if (poly[i].value != 0) { max_pow = pow - 1 - i; break; }
            }
        }
        public void Update()
        {
            UpdateValue();
            UpdateMaxPow();
        }
    }


    public static class MAP
    {
        public static SortedDictionary<int, int> get_p = new SortedDictionary<int, int>();
        public static SortedDictionary<int, int> get_gf = new SortedDictionary<int, int>();

        static MAP()
        {
            get_p.Add(1, 0);
            get_gf.Add(0, 1);
            for (int i = 1; i < 16; i++)
            {
                GF two = new GF(2, 4, 1);
                for (int j = 0; j < i; j++)
                {
                    two = GF.mult(two, new GF(2, 4, 2));
                }
                try
                {
                    get_p.Add(two.value, i);
                }
                catch (ArgumentException)
                { }
                get_gf.Add(i, two.value);
            }
        }
    }

    public static class CPOLY
    {

        public static Poly poly16;
        public static Poly poly256;

        static CPOLY()
        {
            poly16 = new Poly(16, 15,
               (ulong)Math.Pow(16, 6) * 1 +
               (ulong)Math.Pow(16, 5) * 7 +
               (ulong)Math.Pow(16, 4) * 9 +
               (ulong)Math.Pow(16, 3) * 3 +
               (ulong)Math.Pow(16, 2) * 12 +
               (ulong)Math.Pow(16, 1) * 10 +
               (ulong)Math.Pow(16, 0) * 12);
        }
    }

    public class LabyCS
    {
        public int power;
        public List<int> idx;
        public int[] value;
        public char symbol;
        public int primal_index;

        public LabyCS(int _power, int[] _value, List<int> _idx, int primal_idx)
        {
            power = _power;
            idx = _idx;
            value = _value;
            symbol = Bits.ToUTF16(Bits.Inv(value));
            primal_index = primal_idx;
        }

        public static int[] XOR(int[] a, int[] b)
        {
            int[] to_return = new int[a.Length];
            for (int j = 0; j < a.Length; j++)
                to_return[j] = (a[j] + b[j]) % 2;
            return to_return;
        }

        public static LabyCS XOR(LabyCS a, LabyCS b)
        {
            List<int> ind1 = new List<int>(), ind2 = new List<int>();
            if (a.idx.Count > b.idx.Count)
            {
                for (int i = 0; i < b.idx.Count; i++)
                {
                    ind1.Add(a.idx[i]);
                    ind2.Add(b.idx[i]);
                }
                ind1.Add(a.idx[a.idx.Count - 1]); ;
            }
            else
            {
                for (int i = 0; i < a.idx.Count; i++)
                {
                    ind1.Add(b.idx[i]);
                    ind2.Add(a.idx[i]);
                }
                ind1.Add(b.idx[b.idx.Count - 1]);
            }

            for (int i = 0; i < ind1.Count; i++)
            {
                for (int j = 0; j < ind2.Count; j++)
                {
                    if (ind1[i] == ind2[j])
                    {
                        ind1.RemoveAt(i);
                        i--;
                        ind2.RemoveAt(j);

                        break;
                    }
                }
            }

            return new LabyCS(ind1.Count, LabyCS.XOR(a.value, b.value), ind1, -1);
        }

        public static int Equal(int[] a, int[] b)
        {
            if (Math.Abs(a.Length - b.Length) != 1) return -1;
            List<int> ind1 = new List<int>(), ind2 = new List<int>();
            if (a.Length > b.Length)
            {
                for (int i = 0; i < b.Length; i++)
                {
                    ind1.Add(a[i]);
                    ind2.Add(b[i]);
                }
                ind1.Add(a[a.Length - 1]);
            }
            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    ind1.Add(b[i]);
                    ind2.Add(a[i]);
                }
                ind1.Add(b[b.Length - 1]);
            }

            for (int i = 0; i < ind1.Count; i++)
            {
                for (int j = 0; j < ind2.Count; j++)
                {
                    if (ind1[i] == ind2[j])
                    {
                        ind1.RemoveAt(i);
                        i--;
                        ind2.RemoveAt(j);

                        break;
                    }
                }
            }
            if (ind2.Count == 0) return ind1[0];
            else return -1;
        }
    }

}
