﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coding
{
    /// <summary>
    /// Работа с символами в UTF-8
    /// </summary>
    public static class UTF8
    {
        /// <summary>
        ///                      Перевод символа в массив бит
        /// </summary>
        /// <param name="_char"> Символ в UTF-8 </param>
        /// <returns>            Массив битов в представлении int[8] </returns>
        public static int[] ToBits(char _char)
        {
            return Int.ToBits((ushort)_char, 8);
        }

        /// <summary>
        /// Перевод символа в UTF-8 в число
        /// </summary>
        /// <param name="_char"> Символ в UTF-8 </param>
        /// <returns> Число от 0 до 255 </returns>
        public static int ToInt(char _char)
        {
            return (ushort)_char;
        }
    }

    /// <summary>
    /// Работа с символами в UTF-16
    /// </summary>
    public static class UTF16
    {
        /// <summary>
        ///                      Перевод символа в массив бит
        /// </summary>
        /// <param name="_char"> Символ в UTF-16 </param>
        /// <returns>            Массив битов в представлении int[16] </returns>
        public static int[] ToBits(char _char)
        {
            return Int.ToBits((ushort)_char, 16);
        }

        /// <summary>
        /// Перевод символа в UTF-16 в число
        /// </summary>
        /// <param name="_char"> Символ в UTF-16 </param>
        /// <returns> Число от 0 до 65535 </returns>
        public static int ToInt(char _char)
        {
            return (ushort)_char;
        }
    }

    /// <summary>
    /// Работа с массивами бит в представлении int[]
    /// </summary>
    public static class Bits
    {
        /// <summary>
        ///                      Перевод массива битов в число
        /// </summary>
        /// <param name="_bits"> Массив бит в представлении int[N] </param>
        /// <param name="_size"> По умолчанию входной массив полностью копируется 
        ///                      При произвольном _size входной массив будет расширен или сокращен до int[_size] </param>
        /// <returns>            Число в диапазоне от 0 до 2^N-1 в случае _size = 1
        ///                      Число в диапазоне от 0 до 2^_size-1 в случае задания произвольного _size </returns>
        public static int ToInt(int[] _bits, int _size = -1)
        {
            int[] bits;
            if (_size != -1)
            {
                bits = new int[_size];
                for (int i = 0; i < _size; i++)
                {
                    try
                    {
                        bits[i] = _bits[i];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        bits[i] = 0;
                    }
                }
            }
            else
            {
                bits = _bits;
            }
            int value = 0;
            for (int i = 0; i < bits.Length; i++)
            {
                value += bits[i] * (int)Math.Pow(2, i);
            }
            return value;
        }

        /// <summary>
        ///                      Перевод массива бит в символ
        /// </summary>
        /// <param name="_bits"> Массив битов в представлении int[16] </param>
        /// <returns>            Символ в UTF-16 </returns>
        public static char ToUTF16(int[] _bits)
        {
            return (char)ToInt(_bits, 16);
        }

        /// <summary>
        ///                      Перевод массива бит в символ
        /// </summary>
        /// <param name="_bits"> Массив битов в представлении int[8] </param>
        /// <returns>            Символ в UTF-8 </returns>
        public static char ToUTF8(int[] _bits)
        {
            return (char)ToInt(_bits, 8);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_bits"></param>
        /// <returns></returns>
        public static int[] Inv(int[] _bits)
        {
            int[] inv_bits = new int[_bits.Length];
            for (int i = 0; i < _bits.Length; i++)
            {
                inv_bits[i] = _bits[_bits.Length - 1 - i];
            }
            return inv_bits;
        }
    }

    /// <summary>
    /// Работа с целыми числами
    /// </summary>
    public static class Int
    {
        /// <summary>
        ///                       Перевод числа в массив бит 
        /// </summary>
        /// <param name="_value"> Число от 0 до 2^N-1 </param>
        /// <param name="_value"> Задает определенный размер массива 
        ///                       Если задать его избыточным, лишние позиции будут заполнены нулями 
        ///                       Если задать его недостаточным, старшие степени будут утеряны </param>
        /// <returns>             Массив бит в представлении int[N]</returns>
        public static int[] ToBits(int _value, int _size = -1)
        {
            int value = _value;
            int[] bits;
            if (_size != -1)
            {
                bits = new int[_size];
            }
            else
            {
                bits = new int[(int)Math.Log(value, 2) + 1];
            }
            for (int i = (int)Math.Log(value, 2); i > -1; i--)
            {
                var minus = (int)Math.Pow(2, i);
                try
                {
                    if (value >= minus)
                    {
                        value -= minus;
                        bits[i] = 1;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    if (value >= minus)
                    {
                        value -= minus;
                    }
                }
            }
            return bits;
        }

        /// <summary>
        ///                       Перевод числа в символ UTF-16
        /// </summary>
        /// <param name="_value"> Число от 0 до 65535 </param>
        /// <returns>             Символ в UTF-16 </returns>
        public static char ToUTF16(int _value)
        {
            if (_value < 65536)
                return (char)_value;
            else
                return ' ';
        }

        /// <summary>
        ///                       Перевод числа в символ UTF-8
        /// </summary>
        /// <param name="_value"> Число от 0 до 256 </param>
        /// <returns>             Символ в UTF-8 </returns>
        public static char ToUTF8(int _value)
        {
            if (_value < 256)
                return (char)_value;
            else
                return ' ';
        }
    }


    public static class Text
    {
        public static int[] ToBits(string text, int UTF = 16)
        {
            List<int> bits = new List<int>();
            for (int i = 0; i < text.Length; i++)
            {
                if (UTF == 8)
                    bits.AddRange(Bits.Inv(UTF8.ToBits(text[i])));
                if (UTF == 16)
                    bits.AddRange(Bits.Inv(UTF16.ToBits(text[i])));
            }
            return bits.ToArray<int>();
        }
    }

}
