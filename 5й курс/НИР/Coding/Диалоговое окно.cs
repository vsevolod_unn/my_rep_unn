﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace Coding
{
    public partial class Form1 : Form
    {
        int errors, success;
        Coder coder;
        Modulating_Form mf;
        string text;
        double snr, noise_p;
        OpenFileDialog OD;
        int[] raw_bits, coded_bits;

        public Form1()
        {
            InitializeComponent();

            StreamWriter sw = new StreamWriter("laby results.txt");
            sw.WriteLine();
            sw.Close();

            TurboRS_RB.Visible = false;
            TurboHamRB.Visible = false;

            CodecParam2_desc.Visible = true;
            CodecParam3_desc.Visible = true;
            CodecParam4_desc.Visible = false;

            CodecParam2_TB.Visible = true;
            CodecParam3_TB.Visible = true;
            CodecParam4_TB.Visible = false;

            Laby_RB.Checked = true;

            Laby laby = new Laby(
                (char)(120) + "" +
                (char)(121) + "" +
                (char)(120) + "" +
                (char)(122) + "" +
                (char)(120) + "" +
                (char)(121) + "" +
                (char)(120) + "" +
                (char)(122) + "", 32, 5, 0.05, 0.2, 1.5);
            Random rnd = new Random(100);
            string str = "";
            for (int i = 0; i < 100; i++)
                str += (char)(rnd.Next(1025, 1024 * 64));
            //Laby laby = new Laby(str, 16, 7, 0.2, 0.2, 1.4);

            //laby.code();
            //laby.noise(0.20);
            //laby.decode();

            try
            {
                if (!Laby_RB.Checked)
                {
                    snr = Math.Pow(10, 0.1 * Convert.ToDouble(SNR_TB.Text));
                    noise_p = Q(Math.Sqrt(2 * snr));
                    if (noise_p < 1e-2)
                        NoisePercentageTB.Text = noise_p.ToString("E4");
                    else
                        NoisePercentageTB.Text = noise_p.ToString("f6");
                }
            }
            catch (System.FormatException)
            {
                ;
            }
        }

        private void FileBT_Click(object sender, EventArgs e)
        {
            OD = new OpenFileDialog();
            OD.Filter = "Текстовые файлы|*.txt";

            if (OD.ShowDialog() != DialogResult.OK) return;

            try
            {
                MainTextTB.Text = File.ReadAllText(OD.FileName);
            }
            catch
            {
                MessageBox.Show("Error");
                return;
            }
        }

        private void DoAllBT_Click(object sender, EventArgs e)
        {

            coder = new Coder();
            text = MainTextTB.Text;
            CountingProgress.Minimum = 0;
            CountingProgress.Maximum = text.Length;

            coder = new Coder(text, Convert.ToDouble(NoisePercentageTB.Text));
            if (ham74RB.Checked)
            {
                coder.text = text;
                if (ErrorPocketCB.Checked)
                {
                    coder.ham74 = new Ham74(text, Convert.ToDouble(NoisePercentageTB.Text),
                        Convert.ToInt32(ErrorPocketTB.Text));
                }
                else
                {
                    coder.ham74 = new Ham74(text, Convert.ToDouble(NoisePercentageTB.Text), 1);
                }
            }
            if (golayRB.Checked)
            {
                coder.text = text;
                if (ErrorPocketCB.Checked)
                {
                    coder.golay = new Golay(text, Convert.ToDouble(NoisePercentageTB.Text),
                        Convert.ToInt32(ErrorPocketTB.Text));
                }
                else
                {
                    coder.golay = new Golay(text, Convert.ToDouble(NoisePercentageTB.Text),
                        1);
                }
            }
            if (ReedSolomonRB.Checked)
            {
                coder.text = text;
                if (ErrorPocketCB.Checked)
                {
                    coder.reed_solomon4b = new ReedSolomon4Bit(text,
                    Convert.ToDouble(NoisePercentageTB.Text),
                    Convert.ToInt32(CodecParam2_TB.Text),
                    Convert.ToInt32(CodecParam3_TB.Text),
                    Convert.ToInt32(CodecParam4_TB.Text),
                    Convert.ToInt32(ErrorPocketTB.Text));
                }
                else
                {
                    coder.reed_solomon4b = new ReedSolomon4Bit(text,
                    Convert.ToDouble(NoisePercentageTB.Text),
                    Convert.ToInt32(CodecParam2_TB.Text),
                    Convert.ToInt32(CodecParam3_TB.Text),
                    Convert.ToInt32(CodecParam4_TB.Text));
                }
                //coder.reed_solomon4b.f();
            }
            if (TurboHamRB.Checked)
            {
                coder.text = text;
                if (ErrorPocketCB.Checked)
                {
                    coder.turbo_ham74 = new TurboHamming(text,
                        Convert.ToDouble(NoisePercentageTB.Text),
                        7,
                        4,
                        Convert.ToInt32(ErrorPocketTB.Text));
                }
                else
                {
                    coder.turbo_ham74 = new TurboHamming(text,
                        Convert.ToDouble(NoisePercentageTB.Text));
                }
            }
            if (TurboRS_RB.Checked)
            {
                coder.text = text;
                if (ErrorPocketCB.Checked)
                {
                    coder.turbo_rs = new TurboRS();
                    coder.turbo_rs.Init(text,
                    Convert.ToDouble(NoisePercentageTB.Text),
                    Convert.ToInt32(CodecParam2_TB.Text),
                    Convert.ToInt32(CodecParam3_TB.Text),
                    Convert.ToInt32(ErrorPocketTB.Text));
                }
                else
                {
                    coder.turbo_rs = new TurboRS();
                    coder.turbo_rs.Init(text,
                    Convert.ToDouble(NoisePercentageTB.Text),
                    Convert.ToInt32(CodecParam2_TB.Text),
                    Convert.ToInt32(CodecParam3_TB.Text));
                }
                //coder.reed_solomon4b.f();
            }
            if (Laby_RB.Checked)
            {
                coder.laby = new Laby(
                    text,
                    Convert.ToInt32(CodecParam2_TB.Text),
                    Convert.ToInt32(CodecParam3_TB.Text),
                    0.2, 0.2,
                    Convert.ToDouble(CodecParam4_TB.Text));
            }


            int status = coder.code();
            if (status == 0)
            {
                status = coder.noise();
                if (status == 0)
                {
                    status = coder.decode();
                    if (status == 0 )
                    {
                        DecodedTextTB.Text = "";
                        SuccessfulityTB.Text = "";
                        CountingProgress.Value = errors = success = 0;
                        CountingTimer.Start();
                    }
                }
            }
            else
            {
                if (status == 1)
                {
                    MessageBox.Show("Empty text.", "Error code is 1", MessageBoxButtons.OK);
                }
                if (status == 2)
                {
                    MessageBox.Show("Coder didn`t create.", "Error code is 2", MessageBoxButtons.OK);
                }
            }

            ModulationBT.Enabled = true;
        }

        private void CountingTimer_Tick(object sender, EventArgs e)
        {
            if (CountingProgress.Value < CountingProgress.Maximum)
            {
                var a = coder.decoded_text.Length;
                DecodedTextTB.Text += coder.decoded_text[CountingProgress.Value];
                if (text[CountingProgress.Value] == coder.decoded_text[CountingProgress.Value])
                    success++;
                else
                    errors++;
                CountingProgress.Value++;
                CountingProgress.Invalidate();
                ErrorsCountTB.Text = errors.ToString();
                if (CountingProgress.Value == text.Length)
                {
                    SuccessfulityTB.Text = (100 * success / (double)CountingProgress.Value).ToString("f2");
                    ErrorsCountTB.Text = errors.ToString();
                    CountingTimer.Stop();
                }
            }
        }

        private void MainTextTB_TextChanged(object sender, EventArgs e)
        {
            int text_size = MainTextTB.Text.Length;
            TextLengthTB.Text = text_size.ToString();
            BitsCountTB.Text = (text_size * 16).ToString();
            if (ham74RB.Checked)
            {
                BlocksCountTB.Text = (text_size * 2).ToString();
            }
            if (golayRB.Checked)
            {
                BlocksCountTB.Text = (text_size * 8 / 12).ToString();
            }
            if (ReedSolomonRB.Checked && MainTextTB.TextLength != 0)
            {
                var pow = (int)Math.Log(Convert.ToInt32(CodecParam2_TB.Text), 2);
                BlocksCountTB.Text = (text_size * 16 / 4).ToString();
            }
            if (Laby_RB.Checked && MainTextTB.Text.Length != 0)
            {
                string count = (text_size * 16 / Convert.ToInt32(CodecParam2_TB.Text)).ToString();
                BlocksCountTB.Text = count;
            }
            SuccessfulityTB.Text = "";
            ErrorsCountTB.Text = "";
        }
        private void ham74RB_CheckedChanged(object sender, EventArgs e)
        {
            if (ham74RB.Checked)
            {
                CodecParam2_desc.Visible = false;
                CodecParam3_desc.Visible = false;
                CodecParam4_desc.Visible = false;
                CodecParam2_TB.Visible = false;
                CodecParam3_TB.Visible = false;
                CodecParam4_TB.Visible = false;
            }
            MainTextTB_TextChanged(sender, e);
        }
        private void golayRB_CheckedChanged(object sender, EventArgs e)
        {
            if (golayRB.Checked)
            {
                CodecParam2_desc.Visible = false;
                CodecParam3_desc.Visible = false;
                CodecParam4_desc.Visible = false;
                CodecParam2_TB.Visible = false;
                CodecParam3_TB.Visible = false;
                CodecParam4_TB.Visible = false;
            }
            MainTextTB_TextChanged(sender, e);
        }
        private void ReedSolomonRB_CheckedChanged(object sender, EventArgs e)
        {
            if (ReedSolomonRB.Checked)
            {
                CodecParam2_desc.Visible = true;
                CodecParam3_desc.Visible = true;
                CodecParam4_desc.Visible = false;

                CodecParam2_TB.Visible = true;
                CodecParam3_TB.Visible = true;
                CodecParam4_TB.Visible = false;

                CodecParam2_desc.Text = "N:";
                CodecParam3_desc.Text = "K:";
                CodecParam4_desc.Text = "pow:";

                CodecParam2_TB.Text = "15";
                CodecParam3_TB.Text = "9";
                CodecParam4_TB.Text = "4";


                //CodecParam4_TB.Text = ((Convert.ToInt32(CodecParam2_TB.Text) - Convert.ToInt32(CodecParam3_TB.Text)) / 2).ToString();
            }
            else
            {

            }
            MainTextTB_TextChanged(sender, e);
        }
        private void TurboRS_RB_CheckedChanged(object sender, EventArgs e)
        {
            if (TurboRS_RB.Checked)
            {
                CodecParam2_desc.Visible = true;
                CodecParam3_desc.Visible = true;
                CodecParam4_desc.Visible = false;

                CodecParam2_TB.Visible = true;
                CodecParam3_TB.Visible = true;
                CodecParam4_TB.Visible = false;

                CodecParam2_desc.Text = "N:";

                CodecParam2_TB.Text = "15";

                CodecParam4_TB.Text = "4";


                //CodecParam4_TB.Text = ((Convert.ToInt32(CodecParam2_TB.Text) - Convert.ToInt32(CodecParam3_TB.Text)) / 2).ToString();
            }
            else
            {

            }
            MainTextTB_TextChanged(sender, e);
        }
        private void TurboHamRB_CheckedChanged(object sender, EventArgs e)
        {
            if (TurboHamRB.Checked)
            {
                CodecParam2_desc.Visible = true;
                CodecParam2_desc.Text = "Кол-во итераций";

                CodecParam2_TB.Visible = true;
                CodecParam2_TB.Text = "2";
            }
            else
            {
                CodecParam2_desc.Visible = false;
                CodecParam2_TB.Visible = false;
            }
        }
        private void Laby_RB_CheckedChanged(object sender, EventArgs e)
        {
            if (Laby_RB.Checked)
            {
                CodecParam2_desc.Text = "L";
                CodecParam2_TB.Text = "16";

                CodecParam3_desc.Text = "M";
                CodecParam3_TB.Text = "7";

                CodecParam4_desc.Text = "k";
                CodecParam4_TB.Text = "1,4";

                CodecParam2_desc.Visible = true;
                CodecParam3_desc.Visible = true;
                CodecParam4_desc.Visible = true;
                CodecParam2_TB.Visible = true;
                CodecParam3_TB.Visible = true;
                CodecParam4_TB.Visible = true;

                NoisePercentageTB.ReadOnly = false;
            }
            else
            {
                NoisePercentageTB.ReadOnly = true;
            }
        }

        private void SNR_TB_TextChanged(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToDouble(SNR_TB.Text) < 25 && !Laby_RB.Checked)
                {
                    snr = Math.Pow(10, 0.1 * Convert.ToDouble(SNR_TB.Text));
                    noise_p = Q(Math.Sqrt(2 * snr));
                    if (noise_p < 1e-2)
                        NoisePercentageTB.Text = noise_p.ToString("E4");
                    else
                        NoisePercentageTB.Text = noise_p.ToString("f6");
                }
            }
            catch (System.FormatException)
            {
                ;
            }
        }

        private void Random_str_BT_Click(object sender, EventArgs e)
        {
            MainTextTB.Text = "";
            var rand = new Random();
            string str = "";
            for (int i = 0; i < Convert.ToInt32(SymbolCountTB.Text); i++)
            {
                var s = (char)(rand.Next(0, ushort.MaxValue));
                str += s;
            }
            MainTextTB.Text = str;
        }

        private void ErrorPocketCB_CheckedChanged(object sender, EventArgs e)
        {
            if (ErrorPocketCB.Checked)
                ErrorPocketTB.ReadOnly = true;
            else
                ErrorPocketTB.ReadOnly = false;
        }

        private void CodecParam2_TB_TextChanged(object sender, EventArgs e)
        {
            if (MainTextTB.TextLength != 0 && ReedSolomonRB.Checked)
            {
                var pow = (int)Math.Log(Convert.ToInt32(CodecParam2_TB.Text) + 1, 2);
                BlocksCountTB.Text = (MainTextTB.TextLength * 8 / pow).ToString();
            }
            if (Laby_RB.Checked && MainTextTB.Text.Length != 0 && CodecParam2_TB.Text != "")
            {
                string count = (MainTextTB.Text.Length * 16 / Convert.ToInt32(CodecParam2_TB.Text)).ToString();
                BlocksCountTB.Text = count;

            }
        }

        public double Q(double _x)
        {
            double x = _x;
            double dx = 1e-7;
            double eps = 1e-7;
            double value = 0, addition = 0;
            do
            {
                addition = Math.Exp(-(x + dx) * (x + dx) / 2) * dx;
                x += dx;
                value += addition;
                if (addition / value < eps)
                    dx *= 10;
            }
            while (addition / value > eps || dx < 1e5);
            return value / Math.Sqrt(2 * Math.PI);
        }

        private void ModulationBT_Click(object sender, EventArgs e)
        {
            DoAllBT_Click(sender, e);
            string text = coder.text;

            /*
            if (SendCodedBits_CB.Checked)
                mf = new Modulating_Form(coder.GetCodedBits(), coder.CoderName, SendCodedBits_CB.Checked);
            else
                mf = new Modulating_Form(Coding.Text.ToBits(text), coder.CoderName, SendCodedBits_CB.Checked);
            mf.ShowDialog();
                */

            CountingProgress.Maximum = 4;
            CountingProgress.Minimum = 0;
            CountingProgress.Value = 0;
            /// Без кодера
            mf = new Modulating_Form(Coding.Text.ToBits("ННГУИТФИ"), coder.CoderName, false);
            mf.ExpsSeries_button_Click(sender, e);
            mf.ShowDialog();
            CountingProgress.Value = 1;
            /// Хэмминг
            coder = new Coder("ИТФИ1", Convert.ToDouble(NoisePercentageTB.Text));
            coder.ham74 = new Ham74(text, Convert.ToDouble(NoisePercentageTB.Text), 1);
            coder.code();
            mf = new Modulating_Form(coder.GetCodedBits(), coder.CoderName, true);
            mf.ExpsSeries_button_Click(sender, e);
            mf.ShowDialog();
            CountingProgress.Value = 2;
            /// Голей
            coder = new Coder("ИТФИ", Convert.ToDouble(NoisePercentageTB.Text));
            coder.golay = new Golay(text, Convert.ToDouble(NoisePercentageTB.Text), 1);
            coder.code();
            mf = new Modulating_Form(coder.GetCodedBits(), coder.CoderName, true);
            mf.ExpsSeries_button_Click(sender, e);
            mf.ShowDialog();
            CountingProgress.Value = 3;
            /// Рид-Соломон
            coder = new Coder("ИТФИ", Convert.ToDouble(NoisePercentageTB.Text));
            coder.reed_solomon4b = new ReedSolomon4Bit(text,
            Convert.ToDouble(NoisePercentageTB.Text),
            Convert.ToInt32(CodecParam2_TB.Text),
            Convert.ToInt32(CodecParam3_TB.Text),
            Convert.ToInt32(CodecParam4_TB.Text));
            coder.code();
            mf = new Modulating_Form(coder.GetCodedBits(), coder.CoderName, true);
            mf.ExpsSeries_button_Click(sender, e);
            mf.ShowDialog();
            CountingProgress.Value = 4;
        }
    }
}
