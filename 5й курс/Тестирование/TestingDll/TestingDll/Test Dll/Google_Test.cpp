#include "pch.h"
#include "Header_dll.h"

TEST(Results, Result_code_0)
{
	double x1, x2;
	int res = Square(1, 1, 0, x1, x2);
	EXPECT_EQ(0, res);
}

TEST(Results, Result_code_1)
{
	double x1, x2;
	int res = Square(0, 1, 1, x1, x2);
	EXPECT_EQ(1, res);
}

TEST(Results, Result_code_2)
{
	double x1, x2;
	int res = Square(1, 1, 2, x1, x2);
	EXPECT_EQ(2, res);
}

int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}