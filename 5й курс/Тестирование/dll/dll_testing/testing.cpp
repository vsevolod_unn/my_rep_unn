#include "pch.h"
#include "header.h"

TEST(Results, Result_code_0)
{
	double x1, x2;
	int res = Square(1, 1, 0, x1, x2);
	EXPECT_EQ(0, res);
	EXPECT_DOUBLE_EQ(0, x1);
	EXPECT_DOUBLE_EQ(-1, x2);
}

TEST(Results, Result_code_1)
{
	double x1, x2;
	int res = Square(0, 1, 1, x1, x2);
	EXPECT_EQ(1, res);
}

TEST(Results, Result_code_2)
{
	double x1, x2;
	int res = Square(1, 1, 2, x1, x2);
	EXPECT_EQ(2, res);
}

TEST(Results, Result_code_3)
{
	double x1, x2;
	int res = Square(1, 2, 1, x1, x2);
	EXPECT_EQ(3, res);
	EXPECT_DOUBLE_EQ(-1, x1);
	EXPECT_DOUBLE_EQ(-1, x2);
}

/*TEST(Results, Roots_situation_1)
{
	double x1, x2;
	int res = Square(1, 1, 0, x1, x2);
	EXPECT_DOUBLE_EQ(0, x1);
	EXPECT_DOUBLE_EQ(-1, x2);
}

TEST(Results, Roots_situation_2)
{
	double x1, x2;
	int res = Square(1, 2, 1, x1, x2);
	EXPECT_DOUBLE_EQ(-1, x1);
	EXPECT_DOUBLE_EQ(-1, x2);
}*/

int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}