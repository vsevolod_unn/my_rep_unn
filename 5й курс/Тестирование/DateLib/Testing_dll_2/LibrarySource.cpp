#define DATELIB_EXPORTS

#include "LibraryHeader.h"

using namespace std;

struct Month
{
	int days;
	string month;
};

int GetDatePlus3DaysStr(string date, string& date_str)
{
	if (date.length() != 5 || date[2] != '.') return 1;

	string day = "";
	day += date[0];
	day += date[1];
	int day_idx = stoi(day) - 1;
	if (day_idx < 0 || day_idx > 30) return 2;

	string month = "";
	month += date[3];
	month += date[4];
	int month_idx = stoi(month) - 1;
	if (month_idx < 0 || month_idx > 11) return 3;

	string days[31] = { "������", "������", "������", "���������", "�����", "������", "�������",
						"�������", "�������", "�������", "������������", "�����������", "������������", "�������������",
						"�����������", "������������", "�����������", "�������������", "�������������", "���������", "�������� ������",
						"�������� ������", "�������� ������", "�������� ���������", "�������� �����", "�������� ������", "�������� �������", "�������� �������",
						"�������� �������", "���������", "�������� ������" };
	Month months[12] = { {31, "������"},  {28, "�������"}, {31, "�����"},
						 {30, "������"},  {31, "���"},     {30, "����"},
						 {31, "����"},    {31, "�������"}, {30, "��������"},
						 {31, "�������"}, {30, "������"},  {31, "�������" } };

	if (day_idx > months[month_idx].days) return 4;

	if (day_idx + 3 >= months[month_idx].days)
	{
		day_idx = day_idx + 3 - months[month_idx].days;
		if (month_idx == 11)
			month_idx = 0;
		else
			month_idx++;
	}
	else
	{
		day_idx += 3;
	}

	date_str = days[day_idx] + " " + months[month_idx].month;

	return 0;
}
