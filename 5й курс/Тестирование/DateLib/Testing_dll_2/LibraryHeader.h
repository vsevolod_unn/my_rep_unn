#pragma once

#ifdef DATELIB_EXPORTS
#define DATELIB_API __declspec(dllexport)
#else
#define DATELIB_API __declspec(dllimport)
#endif

#include <iostream>
#include <string>
using namespace std;

/// <summary>
/// ���������� 3 ��� � ��������� ����
/// </summary>
/// <param name = "date"> ������� ���� � �������  ��.�� </param>
/// <param name = "date_str"> ����� ��� ������ ����� ���� � ������� "������ ������" </param>
/// <returns> 0 - �������� ���������� ��������� </returns>
/// <returns> 1 - �������� ������ ���� </returns>
/// <returns> 2 - ����������� ������ ���� </returns>
/// <returns> 3 - ����������� ������ ����� </returns>
/// <returns> 4 - � ���� ������ �� ������� ���� </returns>
extern "C" DATELIB_API int GetDatePlus3DaysStr(std::string date, std::string& date_stg);