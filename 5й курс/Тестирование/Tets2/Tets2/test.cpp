#include "pch.h"
#include "TimeLibHeader.h"
#include <fstream>

TEST(Testing, Test_res_code_0)
{
	setlocale(LC_ALL, "Russian");

	string res_str1;
	string res_str2;
	string res_str3;

	string tst_str1 = "01:01";
	string tst_str2 = "00:55";
	string tst_str3 = "23:55";

	int res1 = GetTimePlus5MinStr(tst_str1, res_str1);
	int res2 = GetTimePlus5MinStr(tst_str2, res_str2);
	int res3 = GetTimePlus5MinStr(tst_str3, res_str3);

	EXPECT_EQ("���� ��� ����� �����", res_str1);
	EXPECT_EQ(0, res1);

	EXPECT_EQ("���� ��� ���� �����", res_str2);
	EXPECT_EQ(0, res2);

	EXPECT_EQ("���� ����� ���� �����", res_str3);
	EXPECT_EQ(0, res3);

	EXPECT_TRUE(true);
}

TEST(Testing, Test_res_code_1)
{
	string res_str;
	string tst_str = "01;01";
	int res = GetTimePlus5MinStr(tst_str, res_str);

	EXPECT_EQ(1, res);
	EXPECT_TRUE(true);
}

TEST(Testing, Test_res_code_2)
{
	string res_str;
	string tst_str = "24:01";
	int res = GetTimePlus5MinStr(tst_str, res_str);

	EXPECT_EQ(2, res);
	EXPECT_TRUE(true);
}

TEST(Testing, Test_res_code_3)
{
	string res_str;
	string tst_str = "01:60";
	int res = GetTimePlus5MinStr(tst_str, res_str);

	EXPECT_EQ(3, res);
	EXPECT_TRUE(true);
}




