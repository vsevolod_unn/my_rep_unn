﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Converters;

namespace SDR_outputs
{
    public partial class TextForm : Form
    {
        public Form1 form1;
        string text;
        int value;
        Thread txt_thread;

        public int[] Iarr;
        public int[] Qarr;

        public List<int> I, Q;

        int bytes_per_symbol = 1;

        public TextForm()
        {
            InitializeComponent();
        }

        public TextForm(Form1 _form1, string txt)
        {
            InitializeComponent();
            form1 = _form1;
            form1.SwitchEnableDrawBT(false);
            text = txt;
            Progress.Maximum = text.Length;

            int smth1, smth2;
            smth1 = Bits.ToInt(new int[] { 0, 0, 1, 1, 1, 1, 1, 1 });
            smth2 = Bits.ToInt(new int[] { 1, 0, 1, 1, 1, 1, 1, 0 });
            List<int> lst = new List<int>();

            List<double> I = new List<double>();
            List<double> Q = new List<double>();

            txt_thread = new Thread(new ThreadStart(ThreadToDo));
            txt_thread.Start();
        }

        public void ThreadToDo()
        {
            value = 0;
            //Iarr = new int[text.Length / 2 + 1];
            //Qarr = new int[text.Length / 2 + 1];

            List<int> I = new List<int>();
            List<int> Q = new List<int>();

            bool flag = true;
            string full_text = "I outputs:";
            for (int i = 0; i < 16 - full_text.Length; i++)
            {
                full_text += " ";
            }
            full_text += "\t|\tQ outputs:      \t|\t\n";


            for (int i = 0; i < text.Length; i += 2 / bytes_per_symbol)
            {
                //Progress.Value = i;
                if (bytes_per_symbol == 2)
                {
                    value = UTF16.ToInt(text[i]);
                }
                else
                {
                    try
                    {
                        List<int> bits = new List<int>();
                        bits.AddRange(Bits.Inv(UTF8.ToBits(text[i])));
                        bits.AddRange(Bits.Inv(UTF8.ToBits(text[i + 1])));
                        value = Bits.ToInt(Bits.Inv(bits.ToArray()));
                        bits.Clear();
                        if (flag)
                        {
                            I.Add(value);
                            flag = !flag;
                        }
                        else
                        {
                            Q.Add(value);
                            flag = !flag;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        break;
                    }
                }

                /// Написание результатов в окно
                if ((value.ToString()).Length < 16)
                {
                    full_text += value.ToString();
                }
                for (int j = 0; j < 16 - value.ToString().Length; j++)
                {
                    full_text += " ";
                }
                if (i % (4 / bytes_per_symbol) ==0)
                {
                    full_text += "\t|\t";
                }
                else
                {
                    full_text += "\t|\t" + "index " + (i / 2).ToString() + "  from " + (int)(text.Length / 2 - 1) + "\n";
                }
                value = 0;
            }

            Iarr = new int[Q.Count];
            Qarr = new int[Q.Count];
            for (int i = 0; i<Iarr.Length; i++)
            {
                Iarr[i] = I[i];
                Qarr[i] = Q[i];
            }

            try
            {
                MainTextBox.Text = full_text;
            }
            catch (System.InvalidOperationException)
            {
                //MainTextBox.Text = full_text;
            }
            form1.SwitchEnableDrawBT(true);
        }

        private void TextForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            txt_thread.Abort();
        }
    }
}
