﻿namespace SDR_outputs
{
    partial class Созвездие
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPicture = new System.Windows.Forms.PictureBox();
            this.ShiftTR = new System.Windows.Forms.TrackBar();
            this.TrackInfoTB = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.MainPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftTR)).BeginInit();
            this.SuspendLayout();
            // 
            // MainPicture
            // 
            this.MainPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainPicture.Location = new System.Drawing.Point(12, 12);
            this.MainPicture.Name = "MainPicture";
            this.MainPicture.Size = new System.Drawing.Size(636, 612);
            this.MainPicture.TabIndex = 0;
            this.MainPicture.TabStop = false;
            this.MainPicture.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainPicture_MouseClick);
            // 
            // ShiftTR
            // 
            this.ShiftTR.Location = new System.Drawing.Point(116, 630);
            this.ShiftTR.Maximum = 15;
            this.ShiftTR.Name = "ShiftTR";
            this.ShiftTR.Size = new System.Drawing.Size(532, 56);
            this.ShiftTR.TabIndex = 1;
            this.ShiftTR.Scroll += new System.EventHandler(this.ShiftTR_Scroll);
            // 
            // TrackInfoTB
            // 
            this.TrackInfoTB.Enabled = false;
            this.TrackInfoTB.Location = new System.Drawing.Point(12, 630);
            this.TrackInfoTB.Name = "TrackInfoTB";
            this.TrackInfoTB.ReadOnly = true;
            this.TrackInfoTB.Size = new System.Drawing.Size(98, 22);
            this.TrackInfoTB.TabIndex = 2;
            this.TrackInfoTB.Text = "Cдвиг на: ";
            // 
            // Созвездие
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 674);
            this.Controls.Add(this.TrackInfoTB);
            this.Controls.Add(this.ShiftTR);
            this.Controls.Add(this.MainPicture);
            this.Name = "Созвездие";
            this.Text = "Созвездие";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Созвездие_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.MainPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftTR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox MainPicture;
        private System.Windows.Forms.TrackBar ShiftTR;
        private System.Windows.Forms.TextBox TrackInfoTB;
    }
}