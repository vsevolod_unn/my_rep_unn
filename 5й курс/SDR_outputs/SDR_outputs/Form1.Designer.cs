﻿namespace SDR_outputs
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPicture = new System.Windows.Forms.PictureBox();
            this.DrawBT = new System.Windows.Forms.Button();
            this.OpenBT = new System.Windows.Forms.Button();
            this.RangeX = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.MainPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RangeX)).BeginInit();
            this.SuspendLayout();
            // 
            // MainPicture
            // 
            this.MainPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainPicture.Location = new System.Drawing.Point(8, 8);
            this.MainPicture.Name = "MainPicture";
            this.MainPicture.Size = new System.Drawing.Size(1226, 443);
            this.MainPicture.TabIndex = 0;
            this.MainPicture.TabStop = false;
            // 
            // DrawBT
            // 
            this.DrawBT.Location = new System.Drawing.Point(1130, 464);
            this.DrawBT.Name = "DrawBT";
            this.DrawBT.Size = new System.Drawing.Size(106, 27);
            this.DrawBT.TabIndex = 2;
            this.DrawBT.Text = "Отрисовать";
            this.DrawBT.UseVisualStyleBackColor = true;
            this.DrawBT.Click += new System.EventHandler(this.DrawBT_Click);
            // 
            // OpenBT
            // 
            this.OpenBT.Location = new System.Drawing.Point(1018, 464);
            this.OpenBT.Name = "OpenBT";
            this.OpenBT.Size = new System.Drawing.Size(106, 27);
            this.OpenBT.TabIndex = 3;
            this.OpenBT.Text = "Открыть";
            this.OpenBT.UseVisualStyleBackColor = true;
            this.OpenBT.Click += new System.EventHandler(this.OpenBT_Click);
            // 
            // RangeX
            // 
            this.RangeX.Location = new System.Drawing.Point(8, 464);
            this.RangeX.Name = "RangeX";
            this.RangeX.Size = new System.Drawing.Size(1004, 56);
            this.RangeX.TabIndex = 4;
            this.RangeX.Scroll += new System.EventHandler(this.RangeX_Scroll);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 504);
            this.Controls.Add(this.RangeX);
            this.Controls.Add(this.OpenBT);
            this.Controls.Add(this.DrawBT);
            this.Controls.Add(this.MainPicture);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вывод записанного и отрисовка";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.MainPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RangeX)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox MainPicture;
        private System.Windows.Forms.Button DrawBT;
        private System.Windows.Forms.Button OpenBT;
        private System.Windows.Forms.TrackBar RangeX;
    }
}

