﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SDR_outputs
{
    public partial class Созвездие : Form
    {
        Form1 form1;
        DrawerCollection d;
        bool mode = true;
        double[] x, y;
        double[] x_raw, y_raw;
        public string text;
        public bool IsDelivered = false;
        int SPS = 72000;
        int WrFrq = 2448000;
        int CountsPerSample;

        public Созвездие()
        {
            InitializeComponent();
        }

        public Созвездие(Form1 _form1, double[] _x, double[] _y)
        {
            InitializeComponent();

            TrackInfoTB.Text = "Побитовый сдвиг на: " + ShiftTR.Value.ToString();

            form1 = _form1;

            this.x_raw = _x;
            this.y_raw = _y;
            d = new DrawerCollection();

            CountsPerSample = WrFrq / SPS;

            ShiftTR.Maximum = CountsPerSample - 1;

            TrackInfoTB.Text = "Cдвиг на: " + ShiftTR.Value.ToString();

            x = new double[x_raw.Length / CountsPerSample];
            y = new double[y_raw.Length / CountsPerSample];

            for (int i = ShiftTR.Value; i < x_raw.Length - CountsPerSample; i += CountsPerSample)
            {
                x[(i - ShiftTR.Value) / CountsPerSample] = x_raw[i];
                y[(i - ShiftTR.Value) / CountsPerSample] = y_raw[i];
            }

            Draw(mode);
        }

        private void MainPicture_MouseClick(object sender, MouseEventArgs e)
        {
            mode = !mode;
            Draw(mode);
        }

        private void ShiftTR_Scroll(object sender, EventArgs e)
        {
            TrackInfoTB.Text = "Cдвиг на: " + ShiftTR.Value.ToString();

            x = new double[x_raw.Length / CountsPerSample];
            y = new double[y_raw.Length / CountsPerSample];

            for (int i = ShiftTR.Value; i < x_raw.Length - CountsPerSample; i += CountsPerSample)
            {
                x[(i - ShiftTR.Value) / CountsPerSample] = x_raw[i];
                y[(i - ShiftTR.Value) / CountsPerSample] = y_raw[i];
            }

            /*
            int value;

            List<int> bits = new List<int>();
            List<int> temp_bits = new List<int>();

            for (int i = 0; i < text.Length; i++)
            {
                value = (short)text[i];
                var code = IntToCode(value);
                for (int j = 0; j < code.Length; j++)
                {
                    bits.Add(code[j]);
                }
            }

            int index = 0;
            x = new double[x.Length - 1];
            y = new double[y.Length - 1];

            for (int i = ShiftTR.Value; i < bits.Count && index < x.Length; i++)
            {
                temp_bits.Add(bits[i]);
                if ((i - ShiftTR.Value) % 32 == 15)
                {
                    x[index] = CodeToInt(temp_bits.ToArray());
                    temp_bits.Clear();
                }
                if ((i - ShiftTR.Value) % 32 == 31)
                {
                    y[index] = CodeToInt(temp_bits.ToArray());
                    index++;
                    temp_bits.Clear();
                }
            }
            */

            Draw(mode);

            //form1.Y[0] = x;
            //form1.Y[1] = y;
            //form1.ReDraw();
        }

        public void Draw(bool dot = false)
        {
            var drawer = d.DRWCreator(MainPicture, true, true, "Созвездие");
            drawer.ColorBackGround = Color.White;
            drawer.ColorAxisPen = Brushes.Gray;
            drawer.AxisConfig(new int[] { 19, 19, 5, 5, 0, 0 });
            drawer.SetRanges(new double[] { -40000, 40000, -40000, 40000 });

            if (dot)
                drawer.DrawGraph(x, y, false, GraphType.Dots, 0.0075);
            else
                drawer.DrawGraph(x, y, false, GraphType.Lines_Dots, 0.0075);
        }

        public int[] IntToCode(int _value)
        {
            int value = _value;
            int[] to_return = new int[16];

            if (value < 0)
            {
                to_return[0] = 1;
                value *= -1;
            }
            for (int i = 1; i < to_return.Length; i++)
            {
                if (value - (int)Math.Pow(2, to_return.Length - 1 - i) >= 0)
                {
                    to_return[i] = 1;
                    value -= (int)Math.Pow(2, to_return.Length - 1 - i);
                }
                else
                {
                    to_return[i] = 0;
                }
            }

            return to_return;
        }
        public int CodeToInt(int[] _code)
        {
            int value = 0;
            int[] code = _code;
            if (code[0] == 1)
                value -= (int)Math.Pow(2, code.Length - 1);
            for (int i = 1; i < code.Length; i++)
            {
                value += (int)Math.Pow(2, code.Length - 1 - i) * code[i];
            }
            return value;
        }
        private void Созвездие_FormClosing(object sender, FormClosingEventArgs e)
        {
            form1.f3_open = false;
        }
    }
}
