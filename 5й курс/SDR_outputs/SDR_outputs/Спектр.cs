﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace SDR_outputs
{
    public partial class Спектр : Form
    {
        Form1 form1;
        double[] I, Q;
        Complex[] cI, cQ;
        DrawerCollection dc;

        public Спектр()
        {
            InitializeComponent();
        }

        public Спектр(Form1 _form1, double[] _I, double[] _Q)
        {
            InitializeComponent();

            dc = new DrawerCollection();

            form1 = _form1;
            I = _I;
            Q = _Q;

            double[] I_arr, Q_arr;

            int pow = 0;
            for (int i = 0; i < 100; i++)
            {
                if (Math.Pow(2, i) > I.Length)
                {
                    pow = i;
                    break;
                }
            }

            I_arr = new double[(int)Math.Pow(2, pow)];
            Q_arr = new double[(int)Math.Pow(2, pow)];

            for (int i = 0; i < (int)Math.Pow(2, pow); i++)
            {
                if (i < I.Length)
                {
                    I_arr[i] = I[i];
                    Q_arr[i] = Q[i];
                }
                else
                {
                    I_arr[i] = 0;
                    Q_arr[i] = 0;
                }
            }

            cI = new Complex[I_arr.Length];
            cQ = new Complex[I_arr.Length];

            for (int i = 0; i < cI.Length; i++)
            {
                try
                {
                    cI[i] = new Complex(I_arr[i], 0);
                    cQ[i] = new Complex(Q_arr[i], 0);
                }
                catch (IndexOutOfRangeException)
                {
                    cI[i] = new Complex(0, 0);
                    cQ[i] = new Complex(0, 0);
                }
            }

            cI = Fourea(cI, cI.Length, -1);
            cQ = Fourea(cQ, cQ.Length, -1);

            Complex[] cI_inv = new Complex[cI.Length];
            Complex[] cQ_inv = new Complex[cQ.Length];

            for (int i = 0; i < cI_inv.Length / 2; i++)
            {
                cI_inv[i] = cI[cI.Length / 2 - 1 - i];
                cQ_inv[i] = cQ[cQ.Length / 2 - 1 - i];
            }
            for (int i = cI_inv.Length / 2; i < cI_inv.Length - 1; i++)
            {
                cI_inv[i] = cI[cI.Length - 1 - (i - cI.Length / 2)];
                cQ_inv[i] = cQ[cQ.Length - 1 - (i - cQ.Length / 2)];
            }


            Draw(cI_inv, cQ_inv);
        }

        public void Draw(Complex[] arr1, Complex[] arr2)
        {
            double[] mod_I, mod_Q;
            mod_I = new double[arr1.Length];
            mod_Q = new double[arr2.Length];
            for (int i = 0; i < cI.Length; i++)
            {
                mod_I[i] = arr1[i].Magnitude;
                mod_Q[i] = arr2[i].Magnitude;
            }

            double[] x = new double[arr1.Length];
            for (int i = 0; i < arr1.Length; i++)
            {
                x[i] = -1 + i * 2.0 / (double)arr1.Length;
            }

            var drwI = dc.DRWCreator(I_SpectreGraph, true, true, "Спектр I");
            var drwQ = dc.DRWCreator(Q_SpectreGraph, true, true, "Спектр Q");

            drwI.SetRanges(new double[] { -1, 1, 0, 5 * 1e5 });
            drwQ.SetRanges(new double[] { -1, 1, 0, 1e6 });
            drwI.DrawGraph(x, mod_I);
            drwQ.DrawGraph(x, mod_Q);
        }

        private Complex[] Fourea(Complex[] spectr, int n, int direct)
        {
            Complex buf;
            int i, j, istep;
            int m, mmax;
            double r, r1, theta, w_r, w_i, temp_r, temp_i;
            double pi = 3.1415926f;

            r = pi * direct;
            j = 0;
            for (i = 0; i < n; i++)
            {
                if (i < j)
                {
                    temp_r = spectr[j].Real;
                    temp_i = spectr[j].Imaginary;
                    buf = new Complex(spectr[i].Real, spectr[i].Imaginary);
                    spectr[j] = buf;
                    buf = new Complex(temp_r, temp_i);
                    spectr[i] = buf;
                }
                m = n >> 1;
                while (j >= m) { j -= m; m = (m + 1) / 2; }
                j += m;
            }

            mmax = 1;
            while (mmax < n)
            {
                istep = mmax << 1;
                r1 = r / (float)mmax;
                for (m = 0; m < mmax; m++)
                {
                    theta = r1 * m;
                    w_r = (float)Math.Cos((double)theta);
                    w_i = (float)Math.Sin((double)theta);
                    for (i = m; i < n; i += istep)
                    {
                        j = i + mmax;
                        temp_r = w_r * spectr[j].Real - w_i * spectr[j].Imaginary;
                        temp_i = w_r * spectr[j].Imaginary + w_i * spectr[j].Real;
                        buf = new Complex(spectr[i].Real - temp_r, spectr[i].Imaginary - temp_i);
                        spectr[j] = buf;
                        buf = new Complex(spectr[i].Real + temp_r, spectr[i].Imaginary + temp_i);
                        spectr[i] = buf;
                    }
                }
                mmax = istep;
            }

            if (direct > 0)
            {
                for (i = 0; i < n; i++)
                {
                    buf = new Complex(spectr[i].Real / (float)n, spectr[i].Imaginary / (float)n);
                    spectr[i] = buf;
                }
            }

            return spectr;
        }

        private void Спектр_FormClosing(object sender, FormClosingEventArgs e)
        {
            form1.f4_open = false;
        }
    }
}
