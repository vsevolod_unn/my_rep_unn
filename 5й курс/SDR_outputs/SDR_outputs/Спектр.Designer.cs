﻿namespace SDR_outputs
{
    partial class Спектр
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.I_SpectreGraph = new System.Windows.Forms.PictureBox();
            this.Q_SpectreGraph = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.I_SpectreGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q_SpectreGraph)).BeginInit();
            this.SuspendLayout();
            // 
            // I_SpectreGraph
            // 
            this.I_SpectreGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I_SpectreGraph.Location = new System.Drawing.Point(12, 12);
            this.I_SpectreGraph.Name = "I_SpectreGraph";
            this.I_SpectreGraph.Size = new System.Drawing.Size(776, 311);
            this.I_SpectreGraph.TabIndex = 0;
            this.I_SpectreGraph.TabStop = false;
            // 
            // Q_SpectreGraph
            // 
            this.Q_SpectreGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Q_SpectreGraph.Location = new System.Drawing.Point(12, 329);
            this.Q_SpectreGraph.Name = "Q_SpectreGraph";
            this.Q_SpectreGraph.Size = new System.Drawing.Size(776, 311);
            this.Q_SpectreGraph.TabIndex = 1;
            this.Q_SpectreGraph.TabStop = false;
            // 
            // Спектр
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 648);
            this.Controls.Add(this.Q_SpectreGraph);
            this.Controls.Add(this.I_SpectreGraph);
            this.Name = "Спектр";
            this.Text = "Спектр";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Спектр_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.I_SpectreGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Q_SpectreGraph)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox I_SpectreGraph;
        private System.Windows.Forms.PictureBox Q_SpectreGraph;
    }
}