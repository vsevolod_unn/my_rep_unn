﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace SDR_outputs
{
    public partial class Form1 : Form
    {
        double[] Iarr;
        double[] Qarr;
        double[] phase;
        public double[][] X, Y;
        public bool f2_open = false, f3_open = false, f4_open = false;
        TextForm form2;
        Созвездие form3;
        Спектр form4;
        DrawerCollection d;
        OpenFileDialog OD;
        string text;
        Thread F2Thread, F3Thread, F4Thread;

        public Form1()
        {
            InitializeComponent();
            DrawBT.Enabled = true;
        }

        private void OpenBT_Click(object sender, EventArgs e)
        {
            CloseThreads();

            OD = new OpenFileDialog();
            OD.Filter = "Текстовые файлы|*";
            var result = OD.ShowDialog();
            if (result != DialogResult.OK) return;

            try
            {
                text = File.ReadAllText(OD.FileName);
            }
            catch
            {
                MessageBox.Show("Error");
                return;
            }

            //text = File.ReadAllText("C:\\SDRSharp\\Recordings\\2.s");

            if (true/*result == DialogResult.OK*/)
            {
                if (form2 == null)
                {
                    F2Thread = new Thread(new ThreadStart(Form2Thread));
                    F2Thread.Start();
                }
                else
                {
                    form2.Close();
                    F2Thread.Abort();

                    F2Thread = new Thread(new ThreadStart(Form2Thread));
                    F2Thread.Start();
                }

                DrawBT.Enabled = true;
            }
        }

        private void DrawBT_Click(object sender, EventArgs e)
        {
            Iarr = new double[form2.Iarr.Length];
            Qarr = new double[form2.Iarr.Length];
            phase = new double[form2.Iarr.Length];

            for (int i = 0; i < form2.Iarr.Length; i++)
            {
                Iarr[i] = form2.Iarr[i];
                Qarr[i] = form2.Qarr[i];
                phase[i] = Math.Abs(Math.PI / 2 - Math.Abs(Math.Atan(Qarr[i] / Iarr[i])));
            }

            RangeX.Maximum = Iarr.Length - 250 - 1;
            RangeX.Minimum = 250;
            RangeX.TickFrequency = 10;

            X = new double[2][];
            Y = new double[2][];
            for (int n = 0; n < 2; n++)
            {
                X[n] = new double[Iarr.Length];
                Y[n] = new double[Iarr.Length];
            }
            for (int i = 0; i < Iarr.Length; i++)
            {
                X[1][i] = X[0][i] = i;
                Y[0][i] = Iarr[i];
                Y[1][i] = Qarr[i];
            }

            if (!f3_open)
            {
                f3_open = true;
                F3Thread = new Thread(new ThreadStart(Form3Thread));
                F3Thread.Start();
            }

            if (!f4_open)
            {
                f4_open = true;
                F4Thread = new Thread(new ThreadStart(Form4Thread));
                F4Thread.Start();
            }

            ReDraw();
        }

        private void RangeX_Scroll(object sender, EventArgs e)
        {
            ReDraw();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseThreads();
        }
        public void Form2Thread()
        {
            form2 = new TextForm(this, text);
            form2.ShowDialog();
        }
        public void Form3Thread()
        {
            form3 = new Созвездие(this, Iarr, Qarr);
            form3.text = text;
            form3.ShowDialog();
        }
        public void Form4Thread()
        {
            form4 = new Спектр(this, Iarr, Qarr);
            form4.ShowDialog();
        }
        public void ReDraw()
        {
            d = new DrawerCollection();
            var drw = d.DRWCreator(MainPicture, true, true, "Красный - I, синий - Q");
            drw.Resize(X, Y);
            //drw.ResizeX(RangeX.Value - 250, RangeX.Value);
            drw.SetOffsets(new int[] { 36, 30, 0 });
            drw.AxisConfig(new int[] { 19, 9, 7, 5, 0, 0 });
            drw.DrawGraphMulti(X, Y, false);
        }
        public void SwitchEnableDrawBT(bool enable)
        {
            //DrawBT.Enabled = enable;
        }
        public void CloseThreads()
        {
            if (form2 != null)
            {
                form2.Close();
            }
            if (form3 != null)
            {
                form3.Close();
            }
            if (form4 != null)
            {
                form4.Close();
            }

            if (F2Thread != null)
                if (F2Thread.IsAlive)
                    F2Thread.Abort();
            if (F3Thread != null)
                if (F3Thread.IsAlive)
                    F3Thread.Abort();
            if (F4Thread != null)
                if (F4Thread.IsAlive)
                    F4Thread.Abort();
        }
    }
}
