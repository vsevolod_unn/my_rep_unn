﻿namespace MonteCarlo
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.CommonRNG_TB = new System.Windows.Forms.TextBox();
            this.OurRNG_TB = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.CountTB = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.c_paramTB = new System.Windows.Forms.TextBox();
            this.MyRNG = new System.Windows.Forms.CheckBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.m_paramTB = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.x0_paramTB = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.a_paramTB = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.PictureMain = new System.Windows.Forms.PictureBox();
            this.TestBT = new System.Windows.Forms.Button();
            this.PiersonBT = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PeriodTB = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.Graph_N_paramTB = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.PiersonTheoreticalTB = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.PiersonResultTB = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.Pierson_N_paramTB = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.DownerTB = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.UpperTB = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.Correlation_ResultTB = new System.Windows.Forms.TextBox();
            this.Correlation_k_paramTB = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.Correlation_N_paramTB = new System.Windows.Forms.TextBox();
            this.CorrelationBT = new System.Windows.Forms.Button();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Avg_L_paramTB = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.GetAvgCB = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureMain)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox19);
            this.groupBox1.Controls.Add(this.textBox18);
            this.groupBox1.Controls.Add(this.textBox17);
            this.groupBox1.Controls.Add(this.CommonRNG_TB);
            this.groupBox1.Controls.Add(this.OurRNG_TB);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.CountTB);
            this.groupBox1.Controls.Add(this.textBox12);
            this.groupBox1.Controls.Add(this.c_paramTB);
            this.groupBox1.Controls.Add(this.MyRNG);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.m_paramTB);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.x0_paramTB);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.a_paramTB);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 296);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры ГСЧ (п.5)";
            // 
            // textBox19
            // 
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox19.Location = new System.Drawing.Point(107, 214);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(65, 15);
            this.textBox19.TabIndex = 22;
            this.textBox19.Text = "Наш";
            // 
            // textBox18
            // 
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox18.Location = new System.Drawing.Point(5, 214);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(65, 15);
            this.textBox18.TabIndex = 21;
            this.textBox18.Text = "Обычный";
            // 
            // textBox17
            // 
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.Location = new System.Drawing.Point(6, 191);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(163, 15);
            this.textBox17.TabIndex = 20;
            this.textBox17.Text = "Времени потрачено";
            // 
            // CommonRNG_TB
            // 
            this.CommonRNG_TB.Location = new System.Drawing.Point(6, 235);
            this.CommonRNG_TB.Name = "CommonRNG_TB";
            this.CommonRNG_TB.ReadOnly = true;
            this.CommonRNG_TB.Size = new System.Drawing.Size(68, 22);
            this.CommonRNG_TB.TabIndex = 19;
            // 
            // OurRNG_TB
            // 
            this.OurRNG_TB.Location = new System.Drawing.Point(107, 235);
            this.OurRNG_TB.Name = "OurRNG_TB";
            this.OurRNG_TB.ReadOnly = true;
            this.OurRNG_TB.Size = new System.Drawing.Size(68, 22);
            this.OurRNG_TB.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 263);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 26);
            this.button1.TabIndex = 14;
            this.button1.Text = "Генерация";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CountTB
            // 
            this.CountTB.Location = new System.Drawing.Point(107, 160);
            this.CountTB.Name = "CountTB";
            this.CountTB.Size = new System.Drawing.Size(68, 22);
            this.CountTB.TabIndex = 18;
            this.CountTB.Text = "100000";
            // 
            // textBox12
            // 
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.Location = new System.Drawing.Point(6, 164);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(90, 15);
            this.textBox12.TabIndex = 17;
            this.textBox12.Text = "Параметр N:";
            // 
            // c_paramTB
            // 
            this.c_paramTB.Location = new System.Drawing.Point(107, 49);
            this.c_paramTB.Name = "c_paramTB";
            this.c_paramTB.Size = new System.Drawing.Size(68, 22);
            this.c_paramTB.TabIndex = 7;
            this.c_paramTB.Text = "0";
            // 
            // MyRNG
            // 
            this.MyRNG.AutoSize = true;
            this.MyRNG.Location = new System.Drawing.Point(6, 133);
            this.MyRNG.Name = "MyRNG";
            this.MyRNG.Size = new System.Drawing.Size(125, 21);
            this.MyRNG.TabIndex = 16;
            this.MyRNG.Text = "Проверка ГСЧ";
            this.MyRNG.UseVisualStyleBackColor = true;
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(6, 53);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(90, 15);
            this.textBox8.TabIndex = 6;
            this.textBox8.Text = "Параметр c:";
            // 
            // m_paramTB
            // 
            this.m_paramTB.Location = new System.Drawing.Point(107, 77);
            this.m_paramTB.Name = "m_paramTB";
            this.m_paramTB.Size = new System.Drawing.Size(68, 22);
            this.m_paramTB.TabIndex = 5;
            this.m_paramTB.Text = "32768";
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(6, 81);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(90, 15);
            this.textBox6.TabIndex = 4;
            this.textBox6.Text = "Параметр m:";
            // 
            // x0_paramTB
            // 
            this.x0_paramTB.Location = new System.Drawing.Point(107, 105);
            this.x0_paramTB.Name = "x0_paramTB";
            this.x0_paramTB.Size = new System.Drawing.Size(68, 22);
            this.x0_paramTB.TabIndex = 3;
            this.x0_paramTB.Text = "11";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(6, 109);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(90, 15);
            this.textBox4.TabIndex = 2;
            this.textBox4.Text = "Параметр x0:";
            // 
            // a_paramTB
            // 
            this.a_paramTB.Location = new System.Drawing.Point(107, 21);
            this.a_paramTB.Name = "a_paramTB";
            this.a_paramTB.Size = new System.Drawing.Size(68, 22);
            this.a_paramTB.TabIndex = 1;
            this.a_paramTB.Text = "899";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(6, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(90, 15);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Параметр а:";
            // 
            // PictureMain
            // 
            this.PictureMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureMain.Location = new System.Drawing.Point(386, 15);
            this.PictureMain.Name = "PictureMain";
            this.PictureMain.Size = new System.Drawing.Size(828, 501);
            this.PictureMain.TabIndex = 1;
            this.PictureMain.TabStop = false;
            // 
            // TestBT
            // 
            this.TestBT.Location = new System.Drawing.Point(6, 74);
            this.TestBT.Name = "TestBT";
            this.TestBT.Size = new System.Drawing.Size(169, 26);
            this.TestBT.TabIndex = 2;
            this.TestBT.Text = "График f(n)";
            this.TestBT.UseVisualStyleBackColor = true;
            this.TestBT.Click += new System.EventHandler(this.TestBT_Click);
            // 
            // PiersonBT
            // 
            this.PiersonBT.Location = new System.Drawing.Point(6, 105);
            this.PiersonBT.Name = "PiersonBT";
            this.PiersonBT.Size = new System.Drawing.Size(169, 26);
            this.PiersonBT.TabIndex = 3;
            this.PiersonBT.Text = "Критерий Пирсона";
            this.PiersonBT.UseVisualStyleBackColor = true;
            this.PiersonBT.Click += new System.EventHandler(this.PiersonBT_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PeriodTB);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.Graph_N_paramTB);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Controls.Add(this.TestBT);
            this.groupBox2.Location = new System.Drawing.Point(12, 317);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(181, 107);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Пункт 2";
            // 
            // PeriodTB
            // 
            this.PeriodTB.Location = new System.Drawing.Point(107, 49);
            this.PeriodTB.Name = "PeriodTB";
            this.PeriodTB.ReadOnly = true;
            this.PeriodTB.Size = new System.Drawing.Size(68, 22);
            this.PeriodTB.TabIndex = 11;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(11, 53);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(90, 15);
            this.textBox3.TabIndex = 10;
            this.textBox3.Text = "Период:";
            // 
            // Graph_N_paramTB
            // 
            this.Graph_N_paramTB.Location = new System.Drawing.Point(107, 21);
            this.Graph_N_paramTB.Name = "Graph_N_paramTB";
            this.Graph_N_paramTB.Size = new System.Drawing.Size(68, 22);
            this.Graph_N_paramTB.TabIndex = 9;
            this.Graph_N_paramTB.Text = "2000";
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(11, 25);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(90, 15);
            this.textBox7.TabIndex = 8;
            this.textBox7.Text = "Параметр N:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.PiersonTheoreticalTB);
            this.groupBox3.Controls.Add(this.textBox10);
            this.groupBox3.Controls.Add(this.PiersonResultTB);
            this.groupBox3.Controls.Add(this.textBox5);
            this.groupBox3.Controls.Add(this.Pierson_N_paramTB);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.PiersonBT);
            this.groupBox3.Location = new System.Drawing.Point(199, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(181, 140);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Пункт 3";
            // 
            // PiersonTheoreticalTB
            // 
            this.PiersonTheoreticalTB.Location = new System.Drawing.Point(107, 77);
            this.PiersonTheoreticalTB.Name = "PiersonTheoreticalTB";
            this.PiersonTheoreticalTB.ReadOnly = true;
            this.PiersonTheoreticalTB.Size = new System.Drawing.Size(68, 22);
            this.PiersonTheoreticalTB.TabIndex = 13;
            // 
            // textBox10
            // 
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Location = new System.Drawing.Point(11, 81);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(102, 15);
            this.textBox10.TabIndex = 12;
            this.textBox10.Text = "χ по теории:";
            // 
            // PiersonResultTB
            // 
            this.PiersonResultTB.Location = new System.Drawing.Point(107, 49);
            this.PiersonResultTB.Name = "PiersonResultTB";
            this.PiersonResultTB.ReadOnly = true;
            this.PiersonResultTB.Size = new System.Drawing.Size(68, 22);
            this.PiersonResultTB.TabIndex = 11;
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(11, 53);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(102, 15);
            this.textBox5.TabIndex = 10;
            this.textBox5.Text = "χ расчетный:";
            // 
            // Pierson_N_paramTB
            // 
            this.Pierson_N_paramTB.Location = new System.Drawing.Point(107, 21);
            this.Pierson_N_paramTB.Name = "Pierson_N_paramTB";
            this.Pierson_N_paramTB.Size = new System.Drawing.Size(68, 22);
            this.Pierson_N_paramTB.TabIndex = 9;
            this.Pierson_N_paramTB.Text = "1000000";
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Location = new System.Drawing.Point(11, 25);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(90, 15);
            this.textBox9.TabIndex = 8;
            this.textBox9.Text = "Параметр N:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox26);
            this.groupBox4.Controls.Add(this.textBox23);
            this.groupBox4.Controls.Add(this.DownerTB);
            this.groupBox4.Controls.Add(this.textBox27);
            this.groupBox4.Controls.Add(this.UpperTB);
            this.groupBox4.Controls.Add(this.textBox25);
            this.groupBox4.Controls.Add(this.Correlation_ResultTB);
            this.groupBox4.Controls.Add(this.Correlation_k_paramTB);
            this.groupBox4.Controls.Add(this.textBox13);
            this.groupBox4.Controls.Add(this.textBox11);
            this.groupBox4.Controls.Add(this.Correlation_N_paramTB);
            this.groupBox4.Controls.Add(this.CorrelationBT);
            this.groupBox4.Controls.Add(this.textBox15);
            this.groupBox4.Location = new System.Drawing.Point(199, 161);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(181, 249);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Пункт 4";
            // 
            // textBox26
            // 
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox26.Location = new System.Drawing.Point(11, 105);
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.Size = new System.Drawing.Size(159, 15);
            this.textBox26.TabIndex = 19;
            this.textBox26.Text = "оба умножены на 1000";
            // 
            // textBox23
            // 
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox23.Location = new System.Drawing.Point(11, 84);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(159, 15);
            this.textBox23.TabIndex = 18;
            this.textBox23.Text = "Полная информация";
            // 
            // DownerTB
            // 
            this.DownerTB.Location = new System.Drawing.Point(107, 152);
            this.DownerTB.Name = "DownerTB";
            this.DownerTB.ReadOnly = true;
            this.DownerTB.Size = new System.Drawing.Size(68, 22);
            this.DownerTB.TabIndex = 17;
            // 
            // textBox27
            // 
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox27.Location = new System.Drawing.Point(11, 156);
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(102, 15);
            this.textBox27.TabIndex = 16;
            this.textBox27.Text = "Знаменатель:";
            // 
            // UpperTB
            // 
            this.UpperTB.Location = new System.Drawing.Point(107, 124);
            this.UpperTB.Name = "UpperTB";
            this.UpperTB.ReadOnly = true;
            this.UpperTB.Size = new System.Drawing.Size(68, 22);
            this.UpperTB.TabIndex = 15;
            // 
            // textBox25
            // 
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox25.Location = new System.Drawing.Point(11, 128);
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.Size = new System.Drawing.Size(102, 15);
            this.textBox25.TabIndex = 14;
            this.textBox25.Text = "Числитель:";
            // 
            // Correlation_ResultTB
            // 
            this.Correlation_ResultTB.Location = new System.Drawing.Point(107, 181);
            this.Correlation_ResultTB.Name = "Correlation_ResultTB";
            this.Correlation_ResultTB.ReadOnly = true;
            this.Correlation_ResultTB.Size = new System.Drawing.Size(68, 22);
            this.Correlation_ResultTB.TabIndex = 13;
            // 
            // Correlation_k_paramTB
            // 
            this.Correlation_k_paramTB.Location = new System.Drawing.Point(107, 49);
            this.Correlation_k_paramTB.Name = "Correlation_k_paramTB";
            this.Correlation_k_paramTB.Size = new System.Drawing.Size(68, 22);
            this.Correlation_k_paramTB.TabIndex = 11;
            this.Correlation_k_paramTB.Text = "4";
            // 
            // textBox13
            // 
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox13.Location = new System.Drawing.Point(11, 53);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(102, 15);
            this.textBox13.TabIndex = 10;
            this.textBox13.Text = "Параметр k:";
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Location = new System.Drawing.Point(11, 185);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(102, 15);
            this.textBox11.TabIndex = 12;
            this.textBox11.Text = "Значение C(k):";
            // 
            // Correlation_N_paramTB
            // 
            this.Correlation_N_paramTB.Location = new System.Drawing.Point(107, 21);
            this.Correlation_N_paramTB.Name = "Correlation_N_paramTB";
            this.Correlation_N_paramTB.Size = new System.Drawing.Size(68, 22);
            this.Correlation_N_paramTB.TabIndex = 9;
            this.Correlation_N_paramTB.Text = "10000";
            // 
            // CorrelationBT
            // 
            this.CorrelationBT.Location = new System.Drawing.Point(6, 217);
            this.CorrelationBT.Name = "CorrelationBT";
            this.CorrelationBT.Size = new System.Drawing.Size(169, 26);
            this.CorrelationBT.TabIndex = 3;
            this.CorrelationBT.Text = "Корреляции";
            this.CorrelationBT.UseVisualStyleBackColor = true;
            this.CorrelationBT.Click += new System.EventHandler(this.CorrelationBT_Click);
            // 
            // textBox15
            // 
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.Location = new System.Drawing.Point(11, 25);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(90, 15);
            this.textBox15.TabIndex = 8;
            this.textBox15.Text = "Параметр N:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Avg_L_paramTB);
            this.groupBox6.Controls.Add(this.textBox24);
            this.groupBox6.Controls.Add(this.GetAvgCB);
            this.groupBox6.Location = new System.Drawing.Point(12, 430);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(181, 82);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Прочее";
            // 
            // Avg_L_paramTB
            // 
            this.Avg_L_paramTB.Location = new System.Drawing.Point(138, 47);
            this.Avg_L_paramTB.Name = "Avg_L_paramTB";
            this.Avg_L_paramTB.Size = new System.Drawing.Size(32, 22);
            this.Avg_L_paramTB.TabIndex = 15;
            this.Avg_L_paramTB.Text = "10";
            // 
            // textBox24
            // 
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox24.Location = new System.Drawing.Point(6, 51);
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(126, 15);
            this.textBox24.TabIndex = 14;
            this.textBox24.Text = "Число усреднений:";
            // 
            // GetAvgCB
            // 
            this.GetAvgCB.AutoSize = true;
            this.GetAvgCB.Location = new System.Drawing.Point(5, 24);
            this.GetAvgCB.Name = "GetAvgCB";
            this.GetAvgCB.Size = new System.Drawing.Size(165, 21);
            this.GetAvgCB.TabIndex = 0;
            this.GetAvgCB.Text = "Несколько расчетов";
            this.GetAvgCB.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1226, 528);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.PictureMain);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Проверка генератора ГСЧ";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureMain)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox c_paramTB;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox m_paramTB;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox x0_paramTB;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox a_paramTB;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox PictureMain;
        private System.Windows.Forms.Button TestBT;
        private System.Windows.Forms.Button PiersonBT;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox Graph_N_paramTB;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox Pierson_N_paramTB;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox PiersonResultTB;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox PiersonTheoreticalTB;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox Correlation_ResultTB;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox Correlation_k_paramTB;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox Correlation_N_paramTB;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Button CorrelationBT;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox Avg_L_paramTB;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.CheckBox GetAvgCB;
        private System.Windows.Forms.TextBox DownerTB;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox UpperTB;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.CheckBox MyRNG;
        private System.Windows.Forms.TextBox PeriodTB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox CommonRNG_TB;
        private System.Windows.Forms.TextBox OurRNG_TB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox CountTB;
        private System.Windows.Forms.TextBox textBox12;
    }
}

