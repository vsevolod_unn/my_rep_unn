﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarlo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            rand = new Random();

            //Init();
        }

        public void Init()
        {
            a = Convert.ToDouble(a_paramTB.Text);
            c = Convert.ToDouble(c_paramTB.Text);
            m = Convert.ToDouble(m_paramTB.Text);
            x0 = Convert.ToDouble(x0_paramTB.Text);
            //N = Convert.ToInt32(N_paramTB.Text);
            L = Convert.ToInt32(Avg_L_paramTB.Text);
        }

        private void TestBT_Click(object sender, EventArgs e)
        {
            PeriodTB.Text = "";
            N = Convert.ToInt32(Graph_N_paramTB.Text);
            L = Convert.ToInt32(Avg_L_paramTB.Text);
            data2 = new double[N];
            for (int k = 0; k < N; k++)
                data2[k] = 0;

            if (GetAvgCB.Checked)
            {
                for (int step = 0; step < L; step++)
                {
                    for (int k = 0; k < N; k++)
                    {
                        double z = 0;
                        for (int i = 0; i < k; i++)
                        {
                            x0 = rand.NextDouble();
                            if (x0 > 0.5)
                            {
                                z++;
                            }
                            else
                            {
                                z--;
                            }
                        }
                        data2[k] += z / (double)L;
                    }
                }
            }
            else
            {
                data2[0] = 0;
                for (int k = 1; k < N; k++)
                {
                    double z = 0;
                    double[] data;
                    data = get_array(k, MyRNG.Checked, 0);
                    for (int i = 0; i < k; i++)
                    {
                        x0 = data[i];
                        if (x0 > 0.5)
                        {
                            z++;
                        }
                        else
                        {
                            z--;
                        }
                    }
                    data2[k] = z;
                }
            }

            for (int k = 10; k < N/2; k++)
            {
                bool tmp = true;
                for (int i = 0; i < k; i++)
                {
                    if (!(data2[0 + i] == data2[k + i])) tmp = false;
                    if (!tmp)
                    {
                        continue;
                    }
                }
                if (tmp)
                {
                    PeriodTB.Text = k.ToString();
                    break;
                }
            }

            MyDrawer drawer = new MyDrawer(PictureMain, data2, 0, N);
            drawer.redraw_collection();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(CountTB.Text);
            double[] data = new double[count];

            DateTime start = DateTime.Now;
            data = get_array(count, false, 0);
            DateTime end = DateTime.Now;
            CommonRNG_TB.Text = (end.Ticks - start.Ticks).ToString();

            start = DateTime.Now;
            data = get_array(count, true, 0);
            end = DateTime.Now;
            OurRNG_TB.Text = (end.Ticks - start.Ticks).ToString();
        }

        private void PiersonBT_Click(object sender, EventArgs e)
        {
            N = Convert.ToInt32(Pierson_N_paramTB.Text);
            L = Convert.ToInt32(Avg_L_paramTB.Text);
            Pierson = new double[M];
            double[] data3 = new double[N];
            double rand_x;
            double practical_xi = 0;

            if (GetAvgCB.Checked)
            {
                for (int step = 0; step < L; step++)
                {
                    for (int k = 0; k < M; k++)
                    {
                        Pierson[k] = 0;
                    }

                    data3 = get_array(N, MyRNG.Checked, 0);

                    for (int i = 0; i < N; i++)
                    {
                        rand_x = data3[i];
                        for (int k = 0; k < M; k++)
                        {
                            if (k * 0.04 < rand_x && rand_x < (k + 1) * 0.04)
                            {
                                Pierson[k]++;
                                break;
                            }
                        }
                    }

                    for (int k = 0; k < M; k++)
                    {
                        practical_xi += 1.0 / Pierson[k] * Math.Pow(Pierson[k] - (double)N / (double)M, 2);
                    }
                }
                practical_xi /= (double)L;
            }
            else
            {
                for (int k = 0; k < M; k++)
                {
                    Pierson[k] = 0;
                }

                data3 = get_array(N, MyRNG.Checked, 0);

                for (int i = 0; i < N; i++)
                {
                    rand_x = data3[i];
                    for (int k = 0; k < M; k++)
                    {
                        if (k * 0.04 < rand_x && rand_x < (k + 1) * 0.04)
                        {
                            Pierson[k]++;
                            break;
                        }
                    }
                }

                for (int k = 0; k < M; k++)
                {
                    practical_xi += 1.0 / Pierson[k] * Math.Pow(Pierson[k] - (double)N / (double)M, 2);
                }
            }

            PiersonResultTB.Text = practical_xi.ToString("f3");
            PiersonTheoreticalTB.Text = (42.98).ToString();
        }

        private void CorrelationBT_Click(object sender, EventArgs e)
        {
            N = Convert.ToInt32(Correlation_N_paramTB.Text);
            L = Convert.ToInt32(Avg_L_paramTB.Text);
            int K = Convert.ToInt32(Correlation_k_paramTB.Text);

            double[] data4 = new double[N];
            double correlation = 0;
            if (GetAvgCB.Checked)
            {
                for (int step = 0; step < L; step++)
                {
                    data4 = get_array(N, MyRNG.Checked, 0);
                    correlation += Get_Correlation(K, data4);
                }
                correlation /= (double)L;
            }
            else
            {
                data4 = get_array(N, MyRNG.Checked, 0);
                correlation = Get_Correlation(K, data4);
            }

            Correlation_ResultTB.Text = correlation.ToString("f3");
        }

        public double[] get_array(int Length, bool myRNG, double x_start)
        {
            Init();
            double[] to_return = new double[Length];

            if (myRNG)
            {
                if (x_start == 0) x_prev = x0;
                else x_prev = x_start;
                for (int i = 0; i < Length; i++)
                {
                    to_return[i] = RNG();
                }

                for (int i = 0; i < Length; i++)
                {
                    to_return[i] /= (double)m;
                }

            }
            else
            {
                for (int i = 0; i < Length; i++)
                {
                    to_return[i] = rand.NextDouble();
                }
            }

            return to_return;
        }

        public double Get_Correlation(int K, double[] data)
        {
            double XnXnk = 0;
            double Xn = 0;
            double Xnk = 0;
            double Xn2 = 0;
            double Xnk2 = 0;
            for (int n = 0; n < data.Length; n++)
            {
                if (n + K < data.Length)
                    XnXnk += 1.0 / (double)(data.Length - K) * data[n] * data[n + K];

                Xn += 1.0 / (double)data.Length * data[n];

                if (n + K < data.Length)
                    Xnk += 1.0 / (double)(data.Length - K) * data[n + K];

                Xn2 += 1.0 / (double)data.Length * Math.Pow(data[n], 2);

                if (n + K < data.Length)
                    Xnk2 += 1.0 / (double)(data.Length - K) * Math.Pow(data[n + K], 2);
            }

            double upper = (XnXnk - Xn * Xnk);
            double downer = (Xn2 - Xnk2);
            UpperTB.Text = (upper * 1000).ToString("f5");
            DownerTB.Text = (downer * 1000).ToString("f5");

            return upper / downer;
        }

        public double RNG()
        {
            x_prev = ((a * x_prev + c) % m);
            return x_prev;
        }

        double a, c, m, x0;
        double x_prev;

        int N, L, M = 25;

        double[] data2, Pierson;

        Random rand;
    }
}
