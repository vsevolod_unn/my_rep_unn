﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace MonteCarlo
{
    class MyDrawer
    {
        public double set_alfa
        {
            set { alfa = value; }
            get { return alfa; }
        }

        public MyDrawer(PictureBox pc, double[] dt, double minX, double maxX)
        {
            this.pic = pc;
            width = pc.Width;
            height = pc.Height;
            this.bmp = new Bitmap(width, height);

            this.g = Graphics.FromImage(bmp);
            this.g.Clear(Color.White);
            this.g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            this.pic.Image = bmp;

            this.data = dt;
            resize_area();
            this.minX = minX;
            this.maxX = maxX;

            ofsets_koef = 0.07;
            offsetX = (int)(ofsets_koef * width);
            offsetY = (int)(ofsets_koef * height);

            if (offsetX > offsetY) offsetX = offsetY;
            else offsetY = offsetX;

            sizeX = (int)(width - 2 * offsetX);
            sizeY = (int)(height - 2 * offsetY);

            pen = new Pen(new SolidBrush(Color.Red), 1);
            axis_pen = new Pen(new SolidBrush(Color.Black), 1);
        }

        public void set_angle(double A)
        {
            this.alfa = A;
        }

        public Point get_Point(double x, double y)
        {
            Point pt = new Point(0, 0);
            int x1 = (int)((offsetX + sizeX * (double)(x - minX) / (maxX - minX)));
            int y1 = (int)(height - (offsetY + sizeY * (double)(y - minY) / (maxY - minY)));
            pt.X = x1;
            pt.Y = y1;
            return pt;
        }

        public Point get_Point(int ix, double y)
        {
            Point pt = new Point(0, 0);
            double x = (double)(ix) * (maxX - minX) / (double)(data.Length);
            int x1 = (int)(offsetX + sizeX * (x - minX) / (maxX - minX));
            int y1 = (int)(height - (offsetY + sizeY * (double)(y - minY) / (maxY - minY)));
            pt.X = x1;
            pt.Y = y1;
            return pt;
        }

        public int get_x(double x, double z)
        {
            int x1 = (int)(multiplier * ((offsetX * 0.66 + (width - 2 * offsetX) * (double)(x - minX) / (maxX - minX)) + (int)(koef_y * (width - 2 * offsetX) * Math.Cos(alfa) * z)));
            return x1;
        }

        public int get_y(double y, double z)
        {
            int y1 = (int)(multiplier * (height - ((int)(offsetY * 0.5 + (height - 2 * offsetY) * (double)(y - minY) / (maxY - minY)) + (int)(koef_y * (height - 2 * offsetY) * Math.Sin(beta) * z))));
            return y1;
        }

        public void resize_area()
        {
            maxX = -Double.MaxValue;
            maxY = -Double.MaxValue;
            maxZ = -Double.MaxValue;

            minX = Double.MaxValue;
            minY = Double.MaxValue;
            minZ = Double.MaxValue;

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] < minY) minY = data[i];
                if (data[i] > maxY) maxY = data[i];
            }
        }

        public void redraw_collection()
        {
            Point pt1;
            Point pt2;
            g.Clear(Color.White);
            draw_axis();
            for (int i = 0; i < data.Length - 1; i++)
            {
                pt1 = get_Point(i, data[i]);
                pt2 = get_Point(i + 1, data[i + 1]);
                g.DrawLine(pen, pt1, pt2);
            }
        }

        public void draw_axis()
        {
            Point pt1;
            Point pt2;
            for (int i = 0; i < 9; i++)
            {
                pt1 = new Point((int)(offsetX + (double)(sizeX * i) / 8.0), offsetY);
                pt2 = new Point((int)(offsetX + (double)(sizeX * i) / 8.0), height - offsetY);
                g.DrawLine(axis_pen, pt1, pt2);
            }
            for (int i = 0; i < 9; i++)
            {
                pt1 = new Point(offsetX, (int)(offsetY + (double)(sizeY * i) / 8.0));
                pt2 = new Point(sizeX + offsetX, (int)(offsetY + (double)(sizeY * i) / 8.0));
                g.DrawLine(axis_pen, pt1, pt2);
            }

            g.DrawString("График функции f(n)", new Font("Colibri", 10), Brushes.Black, sizeX / 2-offsetX, offsetY / 4);


            g.DrawString(maxY.ToString("f0"), new Font("Colibri", 7), Brushes.Black, 0, offsetY);
            g.DrawString(((maxY + minY) / 2).ToString("f0"), new Font("Colibri", 7), Brushes.Black, 0, sizeY / 2 + offsetY);
            g.DrawString(minY.ToString("f0"), new Font("Colibri", 7), Brushes.Black, 0, sizeY + (int)(0.75 * offsetY));

            g.DrawString(minX.ToString(), new Font("Colibri", 7), Brushes.Black, offsetX, sizeY + offsetY);
            g.DrawString(((maxX + minX) / 2).ToString(), new Font("Colibri", 7), Brushes.Black, offsetX/2 + sizeX / 2, sizeY + offsetY);
            g.DrawString(maxX.ToString(), new Font("Colibri", 7), Brushes.Black, sizeX + offsetX/2, sizeY + offsetY);
        }

        PictureBox pic;
        Bitmap bmp;
        Graphics g;
        Pen pen, axis_pen;
        Brush brsh;

        int width, height, r, vidx, sizeX, sizeY, transformZ;

        double[] data;

        double maxX, maxY, maxZ;
        double minX, minY, minZ;
        int offsetX, offsetY;
        double ofsets_koef;

        double koef_y = 0.25;
        double multiplier;
        double beta = Math.PI / 4;
        double alfa;
    }
}
