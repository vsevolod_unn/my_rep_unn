﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Silicium
{
    public partial class Form1 : Form
    {
        int size_structure, vacancy_idx, TimeCount;
        double alfa = -Math.PI;
        double radius = 8, r, shift;

        double MX = -Double.MaxValue;
        double MY = -Double.MaxValue;
        double MZ = -Double.MaxValue;

        double MnX = Double.MaxValue;
        double MnY = Double.MaxValue;
        double MnZ = Double.MaxValue;

        double pot_energy = 0;
        double kin_energy = 0;

        Bitmap bmp;
        Graphics g;

        MyDraw drawer;

        Segregation sgr, sgr_buf;
        Random RandCreate;

        Vector[,] InTimeAtomsPos;
        bool IsVacancyClicked;

        int sizeX, sizeY, offsetX, offsetY;

        public Form1()
        {
            InitializeComponent();

            sizeX = 500;
            sizeY = 500;
            offsetX = 120;
            offsetY = 100;

            bmp = new Bitmap(sizeX, sizeY);
            g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            PicMain.Image = bmp;
            RandCreate = new Random();
            ShowTR.Enabled = false;
            AngleTR.Maximum = 0;
            AngleTR.Minimum = -180;
            AngleTR.Value = -180;
            IsVacancyClicked = false;
        }

        private void CreateBT_Click(object sender, EventArgs e)
        {
            ModelingGroup.Enabled = false;
            IsVacancyClicked = false;
            size_structure = Convert.ToInt16(SizeTB.Text);
            TimeCount = Convert.ToInt16(TimeCountTB.Text);

            sgr = new Segregation(
                (int)size_structure,    // размер структуры - n*a
                0,                      // доля Ge
                RandCreate);
            sgr.SetPotential(false);

            int AtomsCount = sgr.Structure.Length;
            CountTB.Text = AtomsCount.ToString();

            InTimeAtomsPos = new Vector[TimeCount, AtomsCount];

            MX = -Double.MaxValue;
            MY = -Double.MaxValue;
            MZ = -Double.MaxValue;

            MnX = Double.MaxValue;
            MnY = Double.MaxValue;
            MnZ = Double.MaxValue;
            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                if (sgr.Structure[i].Coordinate.x < MnX) MnX = sgr.Structure[i].Coordinate.x;
                if (sgr.Structure[i].Coordinate.x > MX) MX = sgr.Structure[i].Coordinate.x;

                if (sgr.Structure[i].Coordinate.y < MnY) MnY = sgr.Structure[i].Coordinate.y;
                if (sgr.Structure[i].Coordinate.y > MY) MY = sgr.Structure[i].Coordinate.y;

                if (sgr.Structure[i].Coordinate.z < MnZ) MnZ = sgr.Structure[i].Coordinate.z;
                if (sgr.Structure[i].Coordinate.z > MZ) MZ = sgr.Structure[i].Coordinate.z;
            }

            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                if (sgr.Structure[i].Coordinate.x == MnX || sgr.Structure[i].Coordinate.x == MX ||
                    sgr.Structure[i].Coordinate.y == MnY || sgr.Structure[i].Coordinate.y == MY ||
                    sgr.Structure[i].Coordinate.z == MnZ || sgr.Structure[i].Coordinate.z == MZ)
                    sgr.Structure[i].border = true;
            }
            sgr.FindFirstNeighbours();

            drawer = new MyDraw(PicMain,
                0, 0, 0,
                -0, -0, -0,
                sizeX, sizeY, offsetX, offsetY,
                (int)radius, Color.Red, -Math.PI);

            drawer.draw_collection(sgr.Structure.Length, sgr, BordersCB.Checked);
            vacancy_idx = 0;
            get_energy();
        }

        private void VacancionBT_Click(object sender, EventArgs e)
        {
            IsVacancyClicked = true;
            Random rand = new Random();
            int AtomsCount = sgr.Structure.Length;
            ShowTR.Enabled = true;
            ShowTR.Value = ShowTR.Maximum;
            r = 1.5 * ShowTR.Value * (MX - MnX) / (double)ShowTR.Maximum;

            if (vacancy_idx != 0)
            {
                sgr.Structure[vacancy_idx].vacancy = sgr.Structure[vacancy_idx].border = false;
            }

            while (true)
            {
                vacancy_idx = (int)(rand.NextDouble() * (AtomsCount - 1));
                if (sgr.Structure[vacancy_idx].Coordinate.x > (MX - MnX) / 4 && sgr.Structure[vacancy_idx].Coordinate.x < (MX - MnX) * 3 / 4 &&
                    sgr.Structure[vacancy_idx].Coordinate.y > (MY - MnY) / 4 && sgr.Structure[vacancy_idx].Coordinate.y < (MY - MnY) * 3 / 4 &&
                    sgr.Structure[vacancy_idx].Coordinate.z > (MZ - MnZ) / 4 && sgr.Structure[vacancy_idx].Coordinate.z < (MZ - MnZ) * 3 / 4 &&
                    !sgr.Structure[vacancy_idx].border)
                {
                    sgr.Structure[vacancy_idx].vacancy = sgr.Structure[vacancy_idx].border = true;
                    break;
                }
            }

            sgr.FindFirstNeighbours();
            sgr_buf = new Segregation((int)size_structure, 0, RandCreate);
            for (int i = 0; i < sgr.Structure.Length; i++)
                sgr_buf.Structure[i].Coordinate = sgr.Structure[i].Coordinate;
            redraw(sgr);
            get_energy();
            ModelingGroup.Enabled = true;
        }

        private void ShiftBT_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < sgr.Structure.Length; i++)
                sgr.Structure[i].Coordinate = sgr_buf.Structure[i].Coordinate;

            // Сдвиг
            shift = 0.01 * Convert.ToDouble(ShiftNumeric.Value);
            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                Vector shift_vec = new Vector(
                    (int)(RandCreate.NextDouble() * 10),
                    (int)(RandCreate.NextDouble() * 10),
                    (int)(RandCreate.NextDouble() * 10)) / 10 / Math.Sqrt(3);
                sgr.Structure[i].Coordinate += shift_vec * 0.1357675 * (-0.5 + RandCreate.NextDouble()) * shift;
            }

            get_energy();
            redraw(sgr);
        }

        private void StartSegregationBT_Click(object sender, EventArgs e)
        {
            /*for (int i = 0; i < sgr.Structure.Length; i++)
                sgr.Structure[i].Coordinate = sgr_buf.Structure[i].Coordinate;

            // Сдвиг
            shift = 0.01 * Convert.ToDouble(ShiftNumeric.Value);
            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                sgr.Structure[i].Coordinate += new Vector(1, 1, 1) * 0.1357675 * (-0.5 + RandCreate.NextDouble()) * shift;
            }
            
            get_energy();
            redraw(sgr);*/

            for (int t = 0; t < TimeCount; t++)
            {
                // Calculating
            }
        }

        private void BordersCB_CheckedChanged(object sender, EventArgs e)
        {
            if (!ShowTR.Enabled)
            {
                drawer = new MyDraw(PicMain, MX, MX, MX, MnX, MnX, MnX, sizeX, sizeY, offsetX, offsetY, (int)radius, Color.Red, alfa);
                drawer.draw_links(sgr);
                drawer.draw_collection(sgr.Structure.Length, sgr, BordersCB.Checked);
            }
            else
            {
                redraw(sgr);
            }
        }

        private void AngleTR_Scroll(object sender, EventArgs e)
        {
            alfa = Math.PI * 2 / 360 * (double)AngleTR.Value;

            if (!ShowTR.Enabled)
            {
                drawer = new MyDraw(PicMain, MX, MX, MX, MnX, MnX, MnX, sizeX, sizeY, offsetX, offsetY, (int)radius, Color.Red, alfa);
                drawer.draw_collection(sgr.Structure.Length, sgr, BordersCB.Checked);
            }
            else
            {
                redraw(sgr);
            }
        }

        private void ShowTR_Scroll(object sender, EventArgs e)
        {
            r = 1.5 * ShowTR.Value * (MX - MnX) / (double)ShowTR.Maximum;
            redraw(sgr);
        }

        public void get_energy()
        {
            pot_energy = 0;
            kin_energy = 0;
            sgr.Energy(out pot_energy, out kin_energy);
            if (IsVacancyClicked)
            {
                AtomEnergyTB.Text = (pot_energy / (double)(sgr.Structure.Length - 1)).ToString("f2") + " Эв";
                KinAtomEnergyTB.Text = (kin_energy / (double)(sgr.Structure.Length - 1)).ToString("f2") + " Эв";
            }
            else
            {
                AtomEnergyTB.Text = (pot_energy / (double)sgr.Structure.Length).ToString("f2") + " Эв";
                KinAtomEnergyTB.Text = (kin_energy / (double)(sgr.Structure.Length)).ToString("f2") + " Эв";
            }
            FullEnergyTB.Text = pot_energy.ToString("f2") + " Эв";
            KinFullEnergyTB.Text = kin_energy.ToString("f2") + " Эв";
        }

        public void redraw(Segregation s)
        {
            drawer = new MyDraw(PicMain, MX, MX, MX, MnX, MnX, MnX, sizeX, sizeY, offsetX, offsetY, (int)radius, Color.Red, alfa);
            drawer.draw_collection(s.Structure.Length, s, BordersCB.Checked, vacancy_idx, r);
        }
    }
}
