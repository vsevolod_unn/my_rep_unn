﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Segregation
{
    // Проект -> https://bitbucket.org/soroegin/segregation/

    /* -=< То, что нужно сделать >=-
     * [~+] Изменить методы подсчёта потенциальной энергии - через делагаты
     * [+] Разбить релаксацию на два метода - инициализацию и итерацию
     * [+] Заменить консольный вывод шага релаксации на вывод в listBox (на диалоговом окне)
     * [+] Переделать метод определения начальных скоростей атомов, пропорциональных заданной температуре
     * [+] Добавить метод суммирования подсчитанной энергии атомов структуры
     * [+] Метод перерасчёта энергии соседей
     * [~+] Тесты, тесты и ещё раз тесты!
     * [-] Переделать класс вывода информации в файлы
     * [+-] Добавить многопоточность с учётом переноса на Unity3D
     * [+-] Доработать библиотеку рисования LibDrawingGraphs (подписи к графикам)
     * [-] Перенести программу на Unity3D
     */

    public partial class Segregation
    {
        /// <summary>
        /// Потенциал Терсоффа.
        /// </summary>
        private PotentialTersoff pTersoff;

        /// <summary>
        /// Потенциал Стиллинджера-Вебера.
        /// </summary>
        private PotentialStillinger pStillinger;

        /// <summary>
        /// Выбранный потенциал.
        /// </summary>
        public Potential SelPotential
        {
            private set { selPotential = value; }
            get { return selPotential; }
        }
        private Potential selPotential;

        /// <summary>
        /// Кубическая кристаллическая решётка.
        /// </summary>
        public readonly Atom[] Structure;

        /// <summary>
        /// Количество атомов в элементарной атомной ячейке.
        /// </summary>
        public const int NumberAtoms = 8;

        /// <summary>
        /// Количество атомов в структуре.
        /// </summary>
        public readonly int StructNumAtoms;

        /// <summary>
        /// Размер структуры.
        /// </summary>
        public readonly int StructLength;

        /// <summary>
        /// Параметр решётки структуры.
        /// </summary>
        public readonly double StructLatPar;

        /// <summary>
        /// Создание кубической структуры.
        /// </summary>
        /// <param name="length">Размер структуры.</param>
        /// <param name="gePart">Доля германия в сплаве.</param>
        /// <param name="rand">Генератор случ. чисел.</param>
        public Segregation(int length, double gePart, Random rand)
        {
            this.StructLength = length;
            this.StructNumAtoms = TotalNumAtoms(this.StructLength);
            this.StructLatPar = AtomLatticeParam(gePart);
            this.Structure = new Atom[this.StructNumAtoms];

            this.pStillinger = new PotentialStillinger(AtomParams.LATPAR_SI, AtomParams.LATPAR_GE, this.StructLatPar);
            this.pTersoff = new PotentialTersoff();
            this.SetPotential(true);

            AtomType setType = AtomType.Si;
            if (gePart >= 0.5d) setType = AtomType.Ge;

            int atomIdx = -1;
            for (int x = 0; x < this.StructLength; ++x)
                for (int y = 0; y < this.StructLength; ++y)
                    for (int z = 0; z < this.StructLength; ++z)
                    {
                        double koeff = 0.25d * this.StructLatPar;
                        Vector posCell = new Vector(x, y, z) * this.StructLatPar;

                        this.Structure[++atomIdx] = new Atom(posCell, setType, atomIdx);
                        this.Structure[++atomIdx] = new Atom(new Vector(0, 2, 2) * koeff + posCell, setType, atomIdx);
                        this.Structure[++atomIdx] = new Atom(new Vector(2, 0, 2) * koeff + posCell, setType, atomIdx);
                        this.Structure[++atomIdx] = new Atom(new Vector(2, 2, 0) * koeff + posCell, setType, atomIdx);

                        this.Structure[++atomIdx] = new Atom(new Vector(1, 1, 1) * koeff + posCell, setType, atomIdx);
                        this.Structure[++atomIdx] = new Atom(new Vector(1, 3, 3) * koeff + posCell, setType, atomIdx);
                        this.Structure[++atomIdx] = new Atom(new Vector(3, 1, 3) * koeff + posCell, setType, atomIdx);
                        this.Structure[++atomIdx] = new Atom(new Vector(3, 3, 1) * koeff + posCell, setType, atomIdx);
                    }

            setType = AtomType.Ge;
            if (gePart >= 0.5d) setType = AtomType.Si;

            this.SetRandomAtomType(setType, gePart, rand);
            this.SearchNeighbours();
        }

        /// <summary>
        /// Количество атомов в структуре.
        /// </summary>
        /// <param name="length">Размер структуры.</param>
        /// <returns></returns>
        public static int TotalNumAtoms(int length)
        {
            return length * length * length * NumberAtoms;
        }

        /// <summary>
        /// Параметр решётки структуры.
        /// </summary>
        /// <param name="gePart">Доля германия в структуре.</param>
        /// <returns></returns>
        public static double AtomLatticeParam(double gePart)
        {
            return AtomParams.LATPAR_SI * (1.0d - gePart) + AtomParams.LATPAR_GE * gePart;
        }

        /// <summary>
        /// Энергия равновесного состояния (эВ).
        /// </summary>
        /// <param name="temperature">Температура (в Кельвинах).</param>
        /// <param name="numAtoms">Количество атомов структуры.</param>
        /// <returns></returns>
        public static double TemperatureEnergy(double temperature, int numAtoms)
        {
            return (3.0d / 2.0d) * AtomParams.BOLTZMANN * numAtoms * temperature;
        }

        /// <summary>
        /// Смена потенциала взаимодействия.
        /// </summary>
        /// <param name="tersoff">Потенциал Терсоффа или Стиллинджера-Вебера.</param>
        public void SetPotential(bool tersoff)
        {
            if (tersoff) this.SelPotential = pTersoff;
            else this.SelPotential = pStillinger;
        }

        /// <summary>
        /// Случайное распределение.
        /// </summary>
        /// <param name="setType">Распределяемый атом.</param>
        /// <param name="gePart">Доля германия.</param>
        /// <param name="rand">Генератор случайных чисел.</param>
        private void SetRandomAtomType(AtomType setType, double gePart, Random rand)
        {
            // Количество атомов выбранного типа (в зависимости от доли Германия).
            int numberOfAtoms = 0;

            if (setType == AtomType.Si) numberOfAtoms = (int)Math.Round((1.0d - gePart) * this.StructNumAtoms);
            else numberOfAtoms = (int)Math.Round(gePart * this.StructNumAtoms);

            for (int count = 0; count < numberOfAtoms;)
            {
                int a = rand.Next(0, this.StructNumAtoms);

                if (this.Structure[a].Type != setType)
                {
                    this.Structure[a].Type = setType;
                    count++;
                }
            }
        }

        /*
        //создание структуры с предопределённой конфигурацией
        private void SetConfiguration(AtomType type, double gePart, Random rand)
        {
            if (type == AtomType.Ge && gePart >= 0.5)
                for (int a = 0; a < this.StructNumAtoms; a++)
                    this.Structure[a].Type = AtomType.Si;

            int numberOfAtoms = 0;
            if (gePart >= 0.5) numberOfAtoms = (int)Math.Round((1.0 - gePart) * this.StructNumAtoms);
            else numberOfAtoms = (int)Math.Round(gePart * this.StructNumAtoms);

            for (int count = 0; count < numberOfAtoms;)
            {
                int a = rand.Next(0, this.StructNumAtoms);

                if (this.Structure[a].Type != type)
                {
                    this.Structure[a].Type = type;
                    count++;
                    for (int n = 0; n < this.Structure[a].FirstNeighbours.Count; n++)
                    {
                        if (count >= numberOfAtoms) break;
                        if (this.Structure[a].FirstNeighbours[n].Type != type)
                        {
                            this.Structure[a].FirstNeighbours[n].Type = type;
                            count++;
                        }
                    }
                }
            }
        }
        */

        /// <summary>
        /// Поиск соседей у всех атомов структуры.
        /// </summary>
        public void SearchNeighbours()
        {
            double L = this.StructLength * this.StructLatPar;
            double radiusOfNeighbours = new Vector(this.StructLatPar * 0.25d, this.StructLatPar * 0.25d, this.StructLatPar * 0.25d).Magnitude();
            radiusOfNeighbours += radiusOfNeighbours * 0.2d;

            for (int i = 0; i < this.StructNumAtoms; i++)
            {
                this.Structure[i].SearchFirstNeighbours(this.Structure, L, radiusOfNeighbours);
            }

            for (int j = 0; j < this.StructNumAtoms; j++)
            {
                this.Structure[j].SearchSecondNeighbours();
                this.Structure[j].SumNeighbours();
            }
        }

        /// <summary>
        /// Подсчёт полной потенциальной энергии системы.
        /// </summary>
        /// <returns></returns>
        public double PotentialEnergy()
        {
            double sumEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = 0; i < this.StructNumAtoms; i++)
            {
                this.Structure[i].PotentialEnergy(this.SelPotential, L, false);

                sumEnergy += this.Structure[i].PotEnergy;
            }

            return sumEnergy;
        }

        /// <summary>
        /// Подсчёт потенциальной и кинетической энергии.
        /// </summary>
        /// <param name="potEnergy">Потенциальная энергия.</param>
        /// <param name="kinEnergy">Кинетическая энергия.</param>
        public void Energy(out double potEnergy, out double kinEnergy)
        {
            potEnergy = 0.0d;
            kinEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = 0; i < this.StructNumAtoms; i++)// Перебор атомов в ячейке
            {
                this.Structure[i].PotentialEnergy(this.SelPotential, L, false);

                potEnergy += this.Structure[i].PotEnergy;
                kinEnergy += this.Structure[i].KineticEnergy();
            }
        }

        /// <summary>
        /// Подсчёт потенциальной и кинетической энергии части структуры.
        /// </summary>
        /// <param name="potEnergy">Потенциальная энергия.</param>
        /// <param name="kinEnergy">Кинетическая энергия.</param>
        /// <param name="startIdx">Начальный индекс атома.</param>
        /// <param name="endIdx">Конечный индекс атома.</param>
        public void Energy(out double potEnergy, out double kinEnergy, int startIdx, int endIdx)
        {
            potEnergy = 0.0d;
            kinEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = startIdx; i < endIdx; i++)
            {
                this.Structure[i].PotentialEnergy(this.SelPotential, L, false);

                potEnergy += this.Structure[i].PotEnergy;
                kinEnergy += this.Structure[i].KineticEnergy();
            }
        }

        /// <summary>
        /// Суммирование подсчитанной потенциальной энергии атомов структуры.
        /// </summary>
        /// <returns></returns>
        public double SumPotentialEnergy()
        {
            double potEnergy = 0.0d;

            for (int i = 0; i < this.StructNumAtoms; i++)
                potEnergy += this.Structure[i].PotEnergy;

            return potEnergy;
        }

        /// <summary>
        /// Вычисление распределения соседей атомов Si и Ge.
        /// </summary>
        /// <param name="probSi">Выходной массив распределения соседей Si.</param>
        /// <param name="probGe">Выходной массив распределения соседей Ge.</param>
        /// <returns>Текстовый вывод результатов.</returns>
        public string IdentificationAtoms(out float[] probSi, out float[] probGe)
        {
            int[] neighSi = new int[5] { 0, 0, 0, 0, 0 };
            int[] neighGe = new int[5] { 0, 0, 0, 0, 0 };
            probSi = new float[neighSi.Length];
            probGe = new float[neighGe.Length];

            int countSi = 0;
            int countGe = 0;
            for (int a = 0; a < this.StructNumAtoms; a++)
                if (this.Structure[a].Type == AtomType.Si)
                {
                    countSi++;

                    int count = 0;
                    for (int n = 0; n < this.Structure[a].FirstNeighbours.Count; n++)
                        if (this.Structure[a].FirstNeighbours[n].Type == this.Structure[a].Type)
                            count++;

                    switch (count)
                    {
                        case 0: ++neighSi[0]; break;
                        case 1: ++neighSi[1]; break;
                        case 2: ++neighSi[2]; break;
                        case 3: ++neighSi[3]; break;
                        case 4: ++neighSi[4]; break;
                        default: break;
                    }
                }
                else
                {
                    countGe++;

                    int count = 0;
                    for (int n = 0; n < this.Structure[a].FirstNeighbours.Count; n++)
                        if (this.Structure[a].FirstNeighbours[n].Type == this.Structure[a].Type)
                            count++;

                    switch (count)
                    {
                        case 0: ++neighGe[0]; break;
                        case 1: ++neighGe[1]; break;
                        case 2: ++neighGe[2]; break;
                        case 3: ++neighGe[3]; break;
                        case 4: ++neighGe[4]; break;
                        default: break;
                    }
                }

            for (int i = 0; i < neighSi.Length; i++)
            {
                probSi[i] = neighSi[i] / (float)countSi;
                probGe[i] = neighGe[i] / (float)countGe;
            }

            string outerText = "Анализ соседей атомов" + Environment.NewLine;
            outerText += "Распределение атомов соседей Si:" + Environment.NewLine;
            outerText += String.Format("  Кол-во атомов с 0 соседей - {0};" + Environment.NewLine +
                "  Кол-во атомов с 1 соседним Si - {1};" + Environment.NewLine +
                "  Кол-во атомов с 2 соседними Si - {2};" + Environment.NewLine +
                "  Кол-во атомов с 3 соседними Si - {3};" + Environment.NewLine +
                "  Кол-во атомов с 4 соседними Si - {4};" + Environment.NewLine,
                neighSi[0], neighSi[1], neighSi[2], neighSi[3], neighSi[4]);

            outerText += String.Format("Вероятность с 0 соседей - {0};" + Environment.NewLine +
                "  Вероятность с 1 соседним Si - {1};" + Environment.NewLine +
                "  Вероятность с 2 соседними Si - {2};" + Environment.NewLine +
                "  Вероятность с 3 соседними Si - {3};" + Environment.NewLine +
                "  Вероятность с 4 соседними Si - {4};" + Environment.NewLine +
                "  Общая вероятность - {5}" + Environment.NewLine,
                Math.Round(probSi[0], 4),
                Math.Round(probSi[1], 4),
                Math.Round(probSi[2], 4),
                Math.Round(probSi[3], 4),
                Math.Round(probSi[4], 4),
                Math.Round(probSi[0] + probSi[1] + probSi[2] + probSi[3] + probSi[4], 4));

            outerText += Environment.NewLine + "Распределение атомов соседей Ge:" + Environment.NewLine;
            outerText += String.Format("Кол-во атомов с 0 соседей - {0};" + Environment.NewLine +
                "  Кол-во атомов с 1 соседним Ge - {1};" + Environment.NewLine +
                "  Кол-во атомов с 2 соседними Ge - {2};" + Environment.NewLine +
                "  Кол-во атомов с 3 соседними Ge - {3};" + Environment.NewLine +
                "  Кол-во атомов с 4 соседними Ge - {4};" + Environment.NewLine,
                neighGe[0], neighGe[1], neighGe[2], neighGe[3], neighGe[4]);

            outerText += String.Format("Вероятность с 0 соседей - {0};" + Environment.NewLine +
                "  Вероятность с 1 соседним Ge - {1};" + Environment.NewLine +
                "  Вероятность с 2 соседними Ge - {2};" + Environment.NewLine +
                "  Вероятность с 3 соседними Ge - {3};" + Environment.NewLine +
                "  Вероятность с 4 соседними Ge - {4};" + Environment.NewLine +
                "  Общая вероятность - {5}" + Environment.NewLine,
                Math.Round(probGe[0], 4),
                Math.Round(probGe[1], 4),
                Math.Round(probGe[2], 4),
                Math.Round(probGe[3], 4),
                Math.Round(probGe[4], 4),
                Math.Round(probGe[0] + probGe[1] + probGe[2] + probGe[3] + probGe[4], 4));

            return outerText;
        }

        /// <summary>
        /// Парная корреляционная функция (ПКФ)
        /// </summary>
        /// <param name="masSiGe">Выходной массив значений функции g(r)</param>
        /// <returns></returns>
        public double[] PairCorrelationFunction(out double[,] masSiGe)
        {
            int length = 100;
            double rmax = StructLatPar;
            double dr = rmax / length;
            double l = dr;

            double[] ra = new double[length];
            masSiGe = new double[2, length];

            for (int s = 0; s < length; s++)
            {
                double r = dr * s;
                int typeSi = 0;
                int typeGe = 0;


                int half = StructNumAtoms / 8;
                int[] halfSize = new int[9] { 0, half, half * 2, half * 3, half * 4, half * 5, half * 6, half * 7, StructNumAtoms };
                int[] masTypeSi = new int[8];
                int[] masTypeGe = new int[8];
                Task[] tasks = new Task[8]
                {
                    new Task(delegate { CalcAtoms(out masTypeSi[0], out masTypeGe[0], r, r+l, halfSize[0], halfSize[1]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeSi[1], out masTypeGe[1], r, r+l, halfSize[1], halfSize[2]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeSi[2], out masTypeGe[2], r, r+l, halfSize[2], halfSize[3]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeSi[3], out masTypeGe[3], r, r+l, halfSize[3], halfSize[4]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeSi[4], out masTypeGe[4], r, r+l, halfSize[4], halfSize[5]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeSi[5], out masTypeGe[5], r, r+l, halfSize[5], halfSize[6]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeSi[6], out masTypeGe[6], r, r+l, halfSize[6], halfSize[7]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeSi[7], out masTypeGe[7], r, r+l, halfSize[7], halfSize[8]); }, TaskCreationOptions.AttachedToParent)
                };

                for (int task = 0; task < tasks.Length; task++)
                    tasks[task].Start();
                Task.WaitAll(tasks);

                for (int i = 0; i < masTypeSi.Length; i++)
                {
                    typeSi += masTypeSi[i];
                    typeGe += masTypeGe[i];
                }

                for (int t = 0; t < tasks.Length; t++)
                    tasks[t].Dispose();

                /*for (int i = 0; i < StructNumAtoms; i++)
                {
                    for (int j = 0; j < StructNumAtoms; j++)
                    {
                        if (i == j) continue;

                        double radius = Vector.Magnitude(Structure[i].Coordinate, Structure[j].Coordinate);
                        if (radius > r && radius <= r + l)
                        {
                            if (Structure[j].Type == AtomType.Si) typeSi++;
                            else if (Structure[j].Type == AtomType.Ge) typeGe++;
                        }
                    }
                }*/

                ra[s] = Math.Round(r / StructLatPar, 2);
                masSiGe[0, s] = typeSi / (double)StructNumAtoms;
                masSiGe[1, s] = typeGe / (double)StructNumAtoms;

                double add = 0;
                if (r != 0 && l != 0) add = (Math.Pow(StructLatPar * StructLength, 3) / StructNumAtoms) / (4 * Math.PI * l * r * r);
                masSiGe[0, s] *= add;
                masSiGe[1, s] *= add;
            }

            return ra;
        }

        /// <summary>
        /// Впомогательный метод для подсчёта атомов для ПКФ в определённой сфере
        /// </summary>
        /// <param name="typeSi">Полученное значение количества атомов кремния</param>
        /// <param name="typeGe">Полученное значение количества атомов германия</param>
        /// <param name="r">Радиус просмотра</param>
        /// <param name="rdr">Толщина сферы</param>
        /// <param name="startIdx">Начальный индекс атома в массиве перебираемых атомов</param>
        /// <param name="endIdx">Конечный индекс атома в массиве перебираемых атомов</param>
        private void CalcAtoms(out int typeSi, out int typeGe, double r, double rdr, int startIdx, int endIdx)
        {
            typeSi = typeGe = 0;
            double L = this.StructLength * this.StructLatPar;

            for (int i = startIdx; i < endIdx; i++)
            {
                for (int j = 0; j < StructNumAtoms; j++)
                {
                    if (i == j) continue;

                    double radius = Vector.MagnitudePeriod(Structure[i].Coordinate, Structure[j].Coordinate, L);
                    if (radius > r && radius <= rdr)
                    {
                        if (Structure[j].Type == AtomType.Si) typeSi++;
                        else if (Structure[j].Type == AtomType.Ge) typeGe++;
                    }
                }
            }


        }
    }
}