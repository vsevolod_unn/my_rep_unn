﻿using System;

namespace Segregation
{
    /// <summary>
    /// Потенциал взаимодействия.
    /// </summary>
    public abstract class Potential
    {
        public abstract double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false);
    }

    /// <summary>
    /// Потенциал Терсоффа.
    /// </summary>
    public class PotentialTersoff : Potential
    {
        /// <summary>
        /// Параметры потенциала Терсоффа.
        /// </summary>
        private struct ParamPotential
        {
            public double A, B, S, R, b, c, d, n, h, l1, l2, l3;

            public ParamPotential(double A, double B, double S, double R, double b, double c, double d, double n, double h, double l1, double l2, double l3)
            {
                this.A = A;
                this.B = B;
                this.S = S;
                this.R = R;
                this.b = b;
                this.c = c;
                this.d = d;
                this.n = n;
                this.h = h;
                this.l1 = l1;
                this.l2 = l2;
                this.l3 = l3;
            }

            /// <summary>
            /// Используется для получения параметров SiGe.
            /// </summary>
            /// <param name="potential1">Параметры потенциала для Si.</param>
            /// <param name="potential2">Параметры потенциала для Ge.</param>
            public ParamPotential(ParamPotential potential1, ParamPotential potential2)
            {
                this.A = Math.Sqrt(potential1.A * potential2.A);
                this.B = Math.Sqrt(potential1.B * potential2.B);
                this.S = Math.Sqrt(potential1.S * potential2.S);
                this.R = Math.Sqrt(potential1.R * potential2.R);
                this.b = 0.5 * (potential1.b + potential2.b);
                this.c = 0.5 * (potential1.c + potential2.c);
                this.d = 0.5 * (potential1.d + potential2.d);
                this.n = 0.5 * (potential1.n + potential2.n);
                this.h = 0.5 * (potential1.h + potential2.h);
                this.l1 = 0.5 * (potential1.l1 + potential2.l1);
                this.l2 = 0.5 * (potential1.l2 + potential2.l2);
                this.l3 = 0.5 * (potential1.l3 + potential2.l3);
            }

            public ParamPotential(ParamPotential potential1, ParamPotential potential2, ParamPotential potential3)
            {
                //this.A = 1 / 3.0 * (potential1.A + potential2.A + potential3.A);
                //this.B = 1 / 3.0 * (potential1.B + potential2.B + potential3.B);
                //this.S = 1 / 3.0 * (potential1.S + potential2.S + potential3.S);
                //this.R = 1 / 3.0 * (potential1.R + potential2.R + potential3.R);
                this.A = Math.Pow(potential1.A * potential2.A * potential3.A, 1 / 3.0);
                this.B = Math.Pow(potential1.B * potential2.B * potential3.B, 1 / 3.0);
                this.S = Math.Pow(potential1.S * potential2.S * potential3.S, 1 / 3.0);
                this.R = Math.Pow(potential1.R * potential2.R * potential3.R, 1 / 3.0);
                this.b = 1 / 3.0 * (potential1.b + potential2.b + potential3.b);
                this.c = 1 / 3.0 * (potential1.c + potential2.c + potential3.c);
                this.d = 1 / 3.0 * (potential1.d + potential2.d + potential3.d);
                this.n = 1 / 3.0 * (potential1.n + potential2.n + potential3.n);
                this.h = 1 / 3.0 * (potential1.h + potential2.h + potential3.h);
                this.l1 = 1 / 3.0 * (potential1.l1 + potential2.l1 + potential3.l1);
                this.l2 = 1 / 3.0 * (potential1.l2 + potential2.l2 + potential3.l2);
                this.l3 = 1 / 3.0 * (potential1.l3 + potential2.l3 + potential3.l3);
            }
        }

        /// <summary>
        /// Параметры для различных соединений атомов.
        /// </summary>
        private ParamPotential paramOfSi, paramOfGe, paramOfSiGe;

        public PotentialTersoff()
        {
            // ссылки на источники
            // https://books.google.ru/books?id=2KlVDwAAQBAJ&pg=PA239&lpg=PA239&dq=Tersoff+potential+parameters+for+Ge&source=bl&ots=MiKosGujIU&sig=ACfU3U3zUq0d2Tn7PX_E8lKIJB0Cfj3y0Q&hl=ru&sa=X&ved=2ahUKEwipg7OUpMLpAhVFDOwKHb8wADUQ6AEwBHoECAoQAQ#v=onepage&q=Tersoff%20potential%20parameters%20for%20Ge&f=false
            // https://books.google.ru/books?id=fnGRDQAAQBAJ&pg=PT204&lpg=PT204&dq=Stillinger-Weber+potential+constants+for+Ge&source=bl&ots=DrU9lZDej7&sig=ACfU3U3JDI0gH9ece1tKISxC9Vk0Z6ENJw&hl=ru&sa=X&ved=2ahUKEwjMt-SktcDpAhVC_aQKHYbSDG0Q6AEwAHoECAcQAQ#v=onepage&q=Stillinger-Weber%20potential%20constants%20for%20Ge&f=false
            // https://books.google.ru/books?id=szckDwAAQBAJ&pg=PA63&lpg=PA63&dq=Parameters+for+Stillinger-Weber+potential+for+silicon+and+germanium&source=bl&ots=p5hOCk02Wa&sig=ACfU3U0vw_085u9-cIvKoXWyUZZXOOEfYw&hl=ru&sa=X&ved=2ahUKEwjh8a2gssLpAhUR2aQKHabbCDc4ChDoATACegQICRAB#v=onepage&q=Parameters%20for%20Stillinger-Weber%20potential%20for%20silicon%20and%20germanium&f=false
            // https://pdfs.semanticscholar.org/1dfb/98f14dc635aab7354467337a0a7f8847515e.pdf


            paramOfSi = new ParamPotential(1830.8, 471.18, 0.30, 0.27, 1.1e-6, 100390, 16.217, 0.78734, -0.59825, 24.799, 17.322, 0);
            paramOfGe = new ParamPotential(1769, 419.23, 0.31, 0.28, 9.01e-7, 106430, 15.65, 0.75627, -0.43884, 24.451, 17.047, 0);
            paramOfSiGe = new ParamPotential(paramOfSi, paramOfGe);
        }

        /// <summary>
        /// Функция обрезания Fc.
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="radius">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_FunctionCutoff(ParamPotential potential, double radius)
        {
            if (radius < potential.R) return 1.0;
            else if (radius > potential.S) return 0.0;
            else return 0.5 + 0.5 * Math.Cos(Math.PI * (radius - potential.R) / (potential.S - potential.R));
        }

        /// <summary>
        /// Функция притяжения Fa(r).
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="Rij">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_FunctionA(ParamPotential potential, double Rij)
        {
            return potential.A * Math.Exp(-potential.l1 * Rij);
        }

        /// <summary>
        /// Функция отталкивания Fr(r).
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="Rij">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_FunctionB(ParamPotential potential, double Rij)
        {
            return -potential.B * Math.Exp(-potential.l2 * Rij);
        }

        /// <summary>
        /// Функция G(O).
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="Rij">Расстояние до первого атома-соседа.</param>
        /// <param name="Rik">Расстояние до второго атома-соседа.</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом<./param>
        /// <returns></returns>
        private double PE_FunctionG(ParamPotential potential, double Rij, double Rik, double Rjk)
        {
            // Косинус угла между связями i<->j и i<->k вычисляется по теореме косинусов:
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2.0 * Rij * Rik);

            // Возвели параметры в квадрат отдельно чтобы не писать ниже огромной формулой:
            double c2 = Math.Pow(potential.c, 2);//с^2
            double d2 = Math.Pow(potential.d, 2);//d^2

            return 1.0 + (c2 / d2) - (c2 / (d2 + Math.Pow(potential.h - cosTeta, 2)));
        }

        /// <summary>
        /// Функция Дзета_ij.
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="selAtom">Выбранный атом (i).</param>
        /// <param name="idxJ">Индекс в массиве соседей первого атома-соседа (j).</param>
        /// <param name="lengthSystem">Размер кубической расчётной ячейки.</param>
        /// <returns></returns>
        private double PE_FunctionDzeta(ParamPotential potential, Atom selAtom, int idxJ, double lengthSystem)
        {
            double sum = 0.0;
            double Rij = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[idxJ].Coordinate, lengthSystem);

            for (int k = 0; k < selAtom.Neighbours.Length; k++)
            {
                if (k == idxJ) continue;
                double Rik = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);
                double Rjk = Vector.MagnitudePeriod(selAtom.Neighbours[idxJ].Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);

                ParamPotential potentialIK = paramOfSiGe;
                if (selAtom.Type == AtomType.Si && selAtom.Neighbours[k].Type == AtomType.Si) potentialIK = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[k].Type == AtomType.Ge) potentialIK = paramOfGe;

                // potential.l3 == 0 ? 1 : Math.Exp(Math.Pow(potential.l3, 3) * Math.Pow(Rij - Rik, 3));
                double exponenta = 1; // Math.Exp(Math.Pow(potentialIK.l2, 3) * Math.Pow(Rij - Rik, 3));

                ParamPotential potentialI = paramOfSi;
                if (selAtom.Type == AtomType.Ge) potentialI = paramOfGe;

                sum += PE_FunctionCutoff(potentialIK, Rik) * PE_FunctionG(potentialI, Rij, Rik, Rjk) * exponenta;
            }

            return sum;
        }

        /// <summary>
        /// Функция bij.
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="selAtom">Выбранный атом (i).</param>
        /// <param name="idxJ">Индекс в массиве соседей первого атома-соседа (j).</param>
        /// <param name="lengthSystem">Размер кубической расчётной ячейки.</param>
        /// <returns></returns>
        private double PE_FunctionBeta(ParamPotential potential, Atom selAtom, int idxJ, double lengthSystem)
        {
            double dzeta = PE_FunctionDzeta(potential, selAtom, idxJ, lengthSystem);

            ParamPotential potentialI = paramOfSi;
            if (selAtom.Type == AtomType.Ge) potentialI = paramOfGe;
            return Math.Pow(1.0 + Math.Pow(potentialI.b * dzeta, potentialI.n), -1.0 / (2.0 * potentialI.n));
            //return Math.Pow(1.0 + Math.Pow(potential.b * dzeta, potential.n), -1.0 / (2.0 * potential.n));
        }

        /// <summary>
        /// Потенциальная энергия выбранного атома.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <param name="lengthSystem">Размер кубической расчётной ячейки.</param>
        /// <param name="force">Расчёт энергии взаимодействия выбранного атома.</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false)
        {
            double atomEnergy = 0.0;

            for (int j = 0; j < selAtom.Neighbours.Length; j++)
            {
                double Rij = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[j].Coordinate, lengthSystem);

                // В зависимости от типов атомов меняются параметры потенциала.
                ParamPotential potentialIJ = paramOfSiGe;
                if (selAtom.Type == AtomType.Si && selAtom.Neighbours[j].Type == AtomType.Si) potentialIJ = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[j].Type == AtomType.Ge) potentialIJ = paramOfGe;

                double beta = PE_FunctionBeta(potentialIJ, selAtom, j, lengthSystem);
                double Vij = PE_FunctionCutoff(potentialIJ, Rij) * (PE_FunctionA(potentialIJ, Rij) + beta * PE_FunctionB(potentialIJ, Rij));

                atomEnergy += Vij;
            }

            if (!force) atomEnergy /= 2.0;

            return atomEnergy;
        }
    }

    /// <summary>
    /// Потенциал Стиллинджера-Вебера.
    /// </summary>
    public class PotentialStillinger : Potential
    {
        /// <summary>
        /// Параметры потенциала Стиллинджера-Вебера.
        /// </summary>
        private struct ParamPotential
        {
            public double A, B, p, a, l, y, q, s, e;
            public ParamPotential(double A, double B, double p, double a, double l, double y, double q, double s, double e)
            {
                this.A = A;
                this.B = B;
                this.p = p;
                this.a = a;
                this.l = l;
                this.y = y;
                this.q = q;
                this.s = s;
                this.e = e;
            }

            public ParamPotential(ParamPotential p1, ParamPotential p2)
            {
                this.A = 0.5 * (p1.A + p2.A);
                this.B = 0.5 * (p1.B + p2.B);
                this.p = 0.5 * (p1.p + p2.p);
                this.a = Math.Pow((p1.a * p2.a), 1.0 / 2.0);//0.5 * (p1.a + p2.a);
                this.l = Math.Pow((p1.l * p2.l), 1.0 / 2.0);//0.5 * (p1.l + p2.l);
                this.y = 0.5 * (p1.y + p2.y);
                this.q = 0.5 * (p1.q + p2.q);
                this.s = 0.5 * (p1.s + p2.s);
                this.e = Math.Pow((p1.e * p2.e), 1.0 / 2.0);//0.5 * (p1.e + p2.e);
            }

            public ParamPotential(ParamPotential p1, ParamPotential p2, ParamPotential p3)
            {
                this.A = (p1.A + p2.A + p3.A) / 3.0;
                this.B = (p1.B + p2.B + p3.B) / 3.0;
                this.p = (p1.p + p2.p + p3.p) / 3.0;
                this.a = Math.Pow((p1.a * p2.a * p3.a), 1.0 / 3.0);//(p1.a + p2.a + p3.a) / 3.0;
                this.l = Math.Pow((p1.l * p2.l * p3.l), 1.0 / 3.0);//(p1.l + p2.l + p3.l) / 3.0;
                this.y = (p1.y + p2.y + p3.y) / 3.0;
                this.q = (p1.q + p2.q + p3.q) / 3.0;
                this.s = (p1.s + p2.s + p3.s) / 3.0;
                this.e = Math.Pow((p1.e * p2.e * p3.e), 1.0 / 3.0);//(p1.e + p2.e + p3.e) / 3.0;
            }
        }

        /// <summary>
        /// Параметры для различных соединений атомов.
        /// </summary>
        private ParamPotential paramOfSi, paramOfGe, paramOfSiSiGe, paramOfSiGeGe, paramOfSiGe;

        public PotentialStillinger(double latParSi, double latParGe, double latParSiGe)
        {
            // ссылки на источники
            // https://books.google.ru/books?id=fnGRDQAAQBAJ&pg=PT204&lpg=PT204&dq=Stillinger-Weber+potential+constants+for+Ge&source=bl&ots=DrU9lZDej7&sig=ACfU3U3JDI0gH9ece1tKISxC9Vk0Z6ENJw&hl=ru&sa=X&ved=2ahUKEwjMt-SktcDpAhVC_aQKHYbSDG0Q6AEwAHoECAcQAQ#v=onepage&q=Stillinger-Weber%20potential%20constants%20for%20Ge&f=false
            // https://books.google.ru/books?id=fnGRDQAAQBAJ&pg=PT204&lpg=PT204&dq=Stillinger-Weber+potential+constants+for+Ge&source=bl&ots=DrU9lZDej7&sig=ACfU3U3JDI0gH9ece1tKISxC9Vk0Z6ENJw&hl=ru&sa=X&ved=2ahUKEwjMt-SktcDpAhVC_aQKHYbSDG0Q6AEwAHoECAcQAQ#v=onepage&q=Stillinger-Weber%20potential%20constants%20for%20Ge&f=false
            // https://books.google.ru/books?id=YasPBgAAQBAJ&pg=PA44&lpg=PA44&dq=Parameters+for+Stillinger-Weber+potential+for+silicon+and+germanium&source=bl&ots=qSV6iEzxrT&sig=ACfU3U2P15HRyrIqSS8U4B2bAEJ7Ij_iqw&hl=ru&sa=X&ved=2ahUKEwjp1IG2rsLpAhXCjqQKHVcEDO4Q6AEwB3oECAoQAQ#v=onepage&q=Parameters%20for%20Stillinger-Weber%20potential%20for%20silicon%20and%20germanium&f=false
            // https://books.google.ru/books?id=szckDwAAQBAJ&pg=PA63&lpg=PA63&dq=Parameters+for+Stillinger-Weber+potential+for+silicon+and+germanium&source=bl&ots=p5hOCk02Wa&sig=ACfU3U0vw_085u9-cIvKoXWyUZZXOOEfYw&hl=ru&sa=X&ved=2ahUKEwjh8a2gssLpAhUR2aQKHabbCDc4ChDoATACegQICRAB#v=onepage&q=Parameters%20for%20Stillinger-Weber%20potential%20for%20silicon%20and%20germanium&f=false
            // https://pdfs.semanticscholar.org/1dfb/98f14dc635aab7354467337a0a7f8847515e.pdf


            double si = 0.2095; //latParSi * Math.Sqrt(3) / (4 * Math.Pow(2, 1 / 6.0));
            double sg = 0.2181;//latParGe * Math.Sqrt(3) / (4 * Math.Pow(2, 1 / 6.0));
            double sig = latParSiGe * Math.Sqrt(3) / (4 * Math.Pow(2, 1 / 6.0));

            paramOfSi = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * si, 21.0, 1.20, 0, si, 2.17); //2.1672381
            paramOfGe = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * sg, 31.0, 1.20, 0, sg, 1.93);

            //paramOfSiSiGe = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * sig, 24.33333, 1.20, 0, sig, 2.050);
            //paramOfSiGeGe = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * sig, 27.66667, 1.20, 0, sig, 2.050);
            //paramOfSiGe = new ParamPotential(7.049556277, 0.6022245584, 4, 1.80 * sig, 26, 1.20, 0, sig, 2.050);

            paramOfSiGe = new ParamPotential(paramOfSi, paramOfGe);
            paramOfSiSiGe = new ParamPotential(paramOfSi, paramOfSi, paramOfGe);
            paramOfSiGeGe = new ParamPotential(paramOfSi, paramOfGe, paramOfGe);
        }

        private ParamPotential AngleFunction(AtomType at1, AtomType at2, AtomType at3)
        {
            if (at1 == AtomType.Si && at2 == AtomType.Si && at3 == AtomType.Si)
            {
                return paramOfSi;
            }
            if (at1 == AtomType.Ge && at2 == AtomType.Ge && at3 == AtomType.Ge)
            {
                return paramOfGe;
            }
            if (at1 == AtomType.Si && at2 == AtomType.Ge && at3 == AtomType.Si)
            {
                return paramOfSiGe;
            }
            if (at1 == AtomType.Ge && at2 == AtomType.Si && at3 == AtomType.Ge)
            {
                return paramOfSiGe;
            }

            if (at1 == AtomType.Si && at2 == AtomType.Si && at3 == AtomType.Ge)
            {
                return new ParamPotential(paramOfSi, paramOfSiGe);
            }
            if (at1 == AtomType.Ge && at2 == AtomType.Si && at3 == AtomType.Si)
            {
                return new ParamPotential(paramOfSi, paramOfSiGe);
            }

            if (at1 == AtomType.Ge && at2 == AtomType.Ge && at3 == AtomType.Si)
            {
                return new ParamPotential(paramOfGe, paramOfSiGe);
            }
            if (at1 == AtomType.Si && at2 == AtomType.Ge && at3 == AtomType.Ge)
            {
                return new ParamPotential(paramOfGe, paramOfSiGe);
            }


            return new ParamPotential(0, 0, 0, 0, 0, 0, 0, 0, 0);
        }

        /// <summary>
        /// Функция двухчастичного взаимодействия ф(r).
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="radius">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_FunctionCutoff(ParamPotential potential, double radius)
        {
            if (radius > potential.a) return 0.0;
            else return potential.e * potential.A * (potential.B * Math.Pow(potential.s / radius, potential.p) - Math.Pow(potential.s / radius, potential.q))
                    * Math.Exp(potential.s / (radius - potential.a));
        }

        /// <summary>
        /// Затухающая функция, критический радиус которой находится между первым и вторым ближайшим соседом.
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="Rij">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_FunctionH(ParamPotential potentialIJ, ParamPotential potentialIK, double Rij, double Rik)
        {
            if (Rij > potentialIJ.a) return 0.0;
            else if (Rik > potentialIK.a) return 0.0;
            else
            {
                double exponenta = (potentialIJ.y * potentialIJ.s / (Rij - potentialIJ.a)) + (potentialIK.y * potentialIK.s / (Rik - potentialIK.a));
                return Math.Exp(exponenta);
            }
        }

        /// <summary>
        /// Функция (cos + 1/3)^2.
        /// </summary>
        /// <param name="Rij">Расстояние до первого атома-соседа.</param>
        /// <param name="Rik">Расстояние до второго атома-соседа.</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом.</param>
        /// <returns></returns>
        private double PE_FunctionCos(double Rij, double Rik, double Rjk)
        {
            // Косинус угла между связями i<->j и i<->k вычисляется по теореме косинусов.
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2.0 * Rij * Rik);
            return Math.Pow((1.0 / 3.0) + cosTeta, 2);
        }

        /// <summary>
        /// Потенциальная энергия выбранного атома.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <param name="lengthSystem">Размер кубической расчётной ячейки.</param>
        /// <param name="force">Расчёт энергии взаимодействия выбранного атома.</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false)
        {
            double atomEnergyDuo = 0.0, atomEnergyTrio = 0.0;

            /*for (int j = 0; j < selAtom.Neighbours.Length; j++)
            {
                double Rij = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[j].Coordinate, lengthSystem);

                ParamPotential potentialIJ = paramOfSiGe;
                if (selAtom.Type == AtomType.Si && selAtom.Neighbours[j].Type == AtomType.Si) potentialIJ = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[j].Type == AtomType.Ge) potentialIJ = paramOfGe;

                atomEnergyDuo += potentialIJ.e * PE_FunctionCutoff(potentialIJ, Rij);

                for (int k = 0; k < selAtom.Neighbours.Length; k++)
                {
                    if (k == j) continue;
                    double Rik = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);
                    double Rjk = Vector.MagnitudePeriod(selAtom.Neighbours[j].Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);

                    double cosTeta = PE_FunctionCos(Rij, Rik, Rjk);

                    int typeGe = 0;
                    if (selAtom.Type == AtomType.Ge) ++typeGe;
                    if (selAtom.Neighbours[j].Type == AtomType.Ge) ++typeGe;
                    if (selAtom.Neighbours[k].Type == AtomType.Ge) ++typeGe;

                    ParamPotential potentialIJK = paramOfSi;
                    if (typeGe == 3) potentialIJK = paramOfGe;
                    else if (typeGe == 2) potentialIJK = paramOfSiGeGe;
                    else if (typeGe == 1) potentialIJK = paramOfSiSiGe;

                    ParamPotential potentialIK = paramOfSiGe;
                    if (selAtom.Type == AtomType.Si && selAtom.Neighbours[k].Type == AtomType.Si) potentialIK = paramOfSi;
                    if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[k].Type == AtomType.Ge) potentialIK = paramOfGe;

                    atomEnergyTrio += potentialIJK.e * potentialIJK.l * PE_FunctionH(potentialIJ, potentialIK, Rij, Rik) * cosTeta;
                }
            }*/

            for (int j = 0; j < selAtom.Neighbours.Length; j++)
            {
                double Rij = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[j].Coordinate, lengthSystem);

                ParamPotential potentialIJ = paramOfSiGe;
                if (selAtom.Type == AtomType.Si && selAtom.Neighbours[j].Type == AtomType.Si) potentialIJ = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[j].Type == AtomType.Ge) potentialIJ = paramOfGe;

                atomEnergyDuo += PE_FunctionCutoff(potentialIJ, Rij);

                for (int k = 0; k < selAtom.Neighbours.Length; k++)
                {
                    if (k == j) continue;
                    double Rik = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);
                    double Rjk = Vector.MagnitudePeriod(selAtom.Neighbours[j].Coordinate, selAtom.Neighbours[k].Coordinate, lengthSystem);

                    double cosTetaJIK = PE_FunctionCos(Rij, Rik, Rjk);
                    double cosTetaIJK = PE_FunctionCos(Rij, Rjk, Rik);
                    double cosTetaIKJ = PE_FunctionCos(Rik, Rjk, Rij);

                    int typeGe = 0;
                    if (selAtom.Type == AtomType.Ge) ++typeGe;
                    if (selAtom.Neighbours[j].Type == AtomType.Ge) ++typeGe;
                    if (selAtom.Neighbours[k].Type == AtomType.Ge) ++typeGe;

                    ParamPotential potentialIJK = paramOfSi;
                    if (typeGe == 3) potentialIJK = paramOfGe;
                    else if (typeGe == 2) potentialIJK = paramOfSiGeGe;
                    else if (typeGe == 1) potentialIJK = paramOfSiSiGe;

                    ParamPotential potentialIK = paramOfSiGe;
                    if (selAtom.Type == AtomType.Si && selAtom.Neighbours[k].Type == AtomType.Si) potentialIK = paramOfSi;
                    if (selAtom.Type == AtomType.Ge && selAtom.Neighbours[k].Type == AtomType.Ge) potentialIK = paramOfGe;

                    ParamPotential potentialJK = paramOfSiGe;
                    if (selAtom.Neighbours[j].Type == AtomType.Si && selAtom.Neighbours[k].Type == AtomType.Si) potentialJK = paramOfSi;
                    if (selAtom.Neighbours[j].Type == AtomType.Ge && selAtom.Neighbours[k].Type == AtomType.Ge) potentialJK = paramOfGe;

                    atomEnergyTrio += potentialIJK.e * potentialIJK.l * (PE_FunctionH(potentialIJ, potentialIK, Rij, Rik) * cosTetaJIK
                    + PE_FunctionH(potentialIJ, potentialJK, Rij, Rjk) * cosTetaIJK + PE_FunctionH(potentialIK, potentialJK, Rik, Rjk) * cosTetaIKJ);

                    //ParamPotential potentialI = AngleFunction(selAtom.Type, selAtom.Neighbours[j].Type, selAtom.Neighbours[k].Type);
                    //ParamPotential potentialJ = AngleFunction(selAtom.Neighbours[j].Type, selAtom.Type, selAtom.Neighbours[k].Type);
                    //ParamPotential potentialK = AngleFunction(selAtom.Neighbours[k].Type, selAtom.Type, selAtom.Neighbours[j].Type);

                    /*ParamPotential potentialI = paramOfSi;
                    if (selAtom.Type == AtomType.Ge) potentialI = paramOfGe;
                    ParamPotential potentialJ = paramOfSi;
                    if (selAtom.Neighbours[j].Type == AtomType.Ge) potentialJ = paramOfGe;
                    ParamPotential potentialK = paramOfSi;
                    if (selAtom.Neighbours[k].Type == AtomType.Ge) potentialK = paramOfGe;*/

                    //atomEnergyTrio += potentialI.e * potentialI.l * PE_FunctionH(potentialIJ, potentialIK, Rij, Rik) * cosTetaJIK
                    //+ potentialJ.e * potentialJ.l * PE_FunctionH(potentialIJ, potentialJK, Rij, Rjk) * cosTetaIJK
                    //+ potentialK.e * potentialK.l * PE_FunctionH(potentialIK, potentialJK, Rik, Rjk) * cosTetaIKJ;
                }
            }

            double atomEnergy = 0.0;
            if (!force) atomEnergy = atomEnergyDuo / 2.0 + atomEnergyTrio / 3.0;
            else atomEnergy = atomEnergyDuo + atomEnergyTrio;

            return atomEnergy;
        }
    }

    /// <summary>
    /// Потенциал Вуксевича.
    /// </summary>
    public class PotentialVukcevich : Potential
    {
        /// <summary>
        /// Параметры потенциала Вуксевича.
        /// </summary>
        private struct ParamPotential
        {
            public double H, a, b, d, c, r0;
            public ParamPotential(double H, double a, double b, double d, double c, double r0)
            {
                this.H = H;
                this.a = a;
                this.b = b;
                this.d = d;
                this.c = c;
                this.r0 = r0;
            }

            public ParamPotential(ParamPotential p1, ParamPotential p2)
            {
                this.H = 0.5 * (p1.H + p2.H);
                this.a = 0.5 * (p1.a + p2.a);
                this.b = 0.5 * (p1.b + p2.b);
                this.d = 0.5 * (p1.d + p2.d);
                this.c = 0.5 * (p1.c + p2.c);
                this.r0 = 0.5 * (p1.r0 + p2.r0);
            }
        }

        /// <summary>
        /// Параметры для различных соединений атомов.
        /// </summary>
        private ParamPotential paramOfSi, paramOfGe, paramOfSiGe;

        public PotentialVukcevich(double latParSi, double latParGe)
        {
            double si = latParSi * Math.Sqrt(3) / 4.0;
            double sg = latParGe * Math.Sqrt(3) / 4.0;
            //double sig = latParSiGe * Math.Sqrt(3) / 4.0;

            paramOfSi = new ParamPotential(4.67, 5.43, 0.0956, 0.0239, 0.192, si);
            paramOfGe = new ParamPotential(3.84, 5.64, 0.0968, 0.0242, 0.175, sg);
            paramOfSiGe = new ParamPotential(paramOfSi, paramOfGe);
        }

        /// <summary>
        /// Косинус угла по теореме косинусов.
        /// </summary>
        /// <param name="Rij">Расстояние до первого атома-соседа.</param>
        /// <param name="Rik">Расстояние до второго атома-соседа.</param>
        /// <param name="Rjk">Расстояние между первым атомом-соседом и вторым атомом-соседом.</param>
        /// <returns></returns>
        private double PE_FunctionCos(double Rij, double Rik, double Rjk)
        {
            // Косинус угла между связями i<->j и i<->k вычисляется по теореме косинусов.
            double cosTeta = (Math.Pow(Rik, 2) + Math.Pow(Rij, 2) - Math.Pow(Rjk, 2)) / (2.0 * Rij * Rik);
            return cosTeta;
        }

        /// <summary>
        /// Потенциальная энергия выбранного атома.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <param name="lengthSystem">Размер кубической расчётной ячейки.</param>
        /// <param name="force">Расчёт энергии взаимодействия выбранного атома.</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false)
        {
            double energy = 0.0;

            for (int i = 0; i < selAtom.FirstNeighbours.Count; ++i)
            {
                ParamPotential potential = paramOfSi;
                if (selAtom.Type == AtomType.Ge) potential = paramOfGe;

                ParamPotential potentialI = paramOfSiGe;
                if (selAtom.Type == AtomType.Si && selAtom.FirstNeighbours[i].Type == AtomType.Si) potentialI = paramOfSi;
                if (selAtom.Type == AtomType.Ge && selAtom.FirstNeighbours[i].Type == AtomType.Ge) potentialI = paramOfGe;

                double ri = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.FirstNeighbours[i].Coordinate, lengthSystem);
                double exponenta = -potentialI.b * Math.Exp((potentialI.r0 - ri) / potentialI.b) + potentialI.d * Math.Exp((potentialI.r0 - ri) / potentialI.d);

                double sum = 0.0;
                for (int j = 0; j < selAtom.FirstNeighbours.Count; ++j)
                {
                    if (j == i) continue;

                    double rj = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.FirstNeighbours[j].Coordinate, lengthSystem);
                    double rk = Vector.MagnitudePeriod(selAtom.FirstNeighbours[i].Coordinate, selAtom.FirstNeighbours[j].Coordinate, lengthSystem);

                    double cos2 = Math.Pow(PE_FunctionCos(ri, rj, rk), 2);
                    sum += Math.Exp(-Math.Pow(cos2 - (1 / 9.0), 2) / potential.c);
                }

                for (int n = 0; n < selAtom.FirstNeighbours[i].FirstNeighbours.Count; ++n)
                {
                    Atom neigh = selAtom.FirstNeighbours[i].FirstNeighbours[n];
                    if (neigh.Index == selAtom.Index) continue;

                    double rj = Vector.MagnitudePeriod(selAtom.FirstNeighbours[i].Coordinate, neigh.Coordinate, lengthSystem);
                    double rk = Vector.MagnitudePeriod(selAtom.Coordinate, neigh.Coordinate, lengthSystem);

                    double cos2 = Math.Pow(PE_FunctionCos(ri, rj, rk), 2);
                    sum += Math.Exp(-Math.Pow(cos2 - (1 / 9.0), 2) / potential.c);
                }

                energy += potential.H / (2 * (potential.b - potential.d)) * exponenta * sum / 6.0;
            }

            if (!force) energy /= 2.0;

            return energy;
        }

    }
}