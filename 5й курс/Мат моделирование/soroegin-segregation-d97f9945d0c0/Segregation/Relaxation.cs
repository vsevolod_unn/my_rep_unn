﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Segregation
{
    public class Relaxation
    {
        Segregation sgr;
        Atom[] atoms;
        Random rand, randShift;
        public TimeSpan ProcTime; //1 итерация/100 перестановок
        public double TimeIter, TimeSwap;

        private Metropolis algSwap;
        public Metropolis GetAlgSwap
        {
            get { return algSwap; }
        }

        double[] masTimeStep;
        double[] masKinEnergy, masPotEnergy;
        List<double> listKinEnergy, listPotEnergy, listTimeStep;
        int[] timeDevStep;
        int[] halfSize;
        double[] kE;
        double[] pE;

        double temperature, timeRelax, valueStep, Ekravn, time;
        double shiftAtom;
        int numStep, step, periodSwap, countSwap, periodIdent, countIdent, normStep;
        bool removeRelax, tmprVeloc;

        string listText = string.Empty;
        public bool End = false, BREAK = false, corrFunc = false;

        public int GetNumSwap
        {
            get
            {
                if (periodSwap != 0) return (numStep - step) / periodSwap;
                else return 0;
            }
        }

        public string GetListText
        {
            get { return listText; }
        }

        public int GetNumStep
        {
            get { return numStep; }
        }

        public int GetStep
        {
            get { return step; }
        }

        public double[] GetMasKinEnergy
        {
            get { return masKinEnergy; }
        }
        public double[] GetMasPotEnergy
        {
            get { return masPotEnergy; }
        }
        public double[] GetMasTimeStep
        {
            get { return masTimeStep; }
        }

        public Relaxation(Segregation sgr)
        {
            this.sgr = sgr;
        }

        public void CalculatedTime()
        {
            atoms = new Atom[sgr.Structure.Length];
            for (int a = 0; a < sgr.Structure.Length; a++)
            {
                atoms[a] = new Atom(sgr.Structure[a].Coordinate, sgr.Structure[a].Type, sgr.Structure[a].Index);
                atoms[a].Coordinate = sgr.Structure[a].Coordinate;
                atoms[a].Velocity = sgr.Structure[a].Velocity;
                atoms[a].Type = sgr.Structure[a].Type;
                atoms[a].KinEnergy = sgr.Structure[a].KinEnergy;
                atoms[a].PotEnergy = sgr.Structure[a].PotEnergy;
            }

            if (shiftAtom != 0.0d)
                sgr.ShiftAtoms(shiftAtom, rand);
            if (tmprVeloc)
                sgr.TemperatureVelocity(temperature);

            double pot = 0.0d, kin = 0.0d;
            sgr.Energy(out pot, out kin);

            // Перестановки атомов
            DateTime startTimeSwap = DateTime.Now;
            if (periodSwap != 0)
            {
                algSwap.Initialization(100, temperature);
                algSwap.Iterations(false);
            }
            DateTime endTimeSwap = DateTime.Now;

            // Алгоритм Верле
            for (int x = 0; x < sgr.StructNumAtoms; x++)
                sgr.AlgorithmVerlet(ref sgr.Structure[x], valueStep);

            // Подсчёт энергии
            double kinEnergy = 0.0d, potEnergy = 0.0d;
            Task[] calcEnergy = new Task[8]
            {
                    new Task(delegate { sgr.Energy(out pE[0], out kE[0], halfSize[0], halfSize[1]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[1], out kE[1], halfSize[1], halfSize[2]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[2], out kE[2], halfSize[2], halfSize[3]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[3], out kE[3], halfSize[3], halfSize[4]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[4], out kE[4], halfSize[4], halfSize[5]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[5], out kE[5], halfSize[5], halfSize[6]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[6], out kE[6], halfSize[6], halfSize[7]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[7], out kE[7], halfSize[7], halfSize[8]); }, TaskCreationOptions.AttachedToParent)
            };

            for (int task = 0; task < calcEnergy.Length; task++)
                calcEnergy[task].Start();
            Task.WaitAll(calcEnergy);

            for (int e = 0; e < kE.Length; e++)
            {
                kinEnergy += kE[e];
                potEnergy += pE[e];
            }

            for (int t = 0; t < calcEnergy.Length; t++)
                calcEnergy[t].Dispose();


            DateTime endTimeIter = DateTime.Now;

            double timeSwap, timeIter;

            TimeSpan time = endTimeSwap - startTimeSwap;
            timeSwap = time.TotalMilliseconds;
            time = endTimeIter - endTimeSwap;
            timeIter = time.TotalMilliseconds;

            for (int a = 0; a < sgr.Structure.Length; a++)
            {
                sgr.Structure[a].Coordinate = atoms[a].Coordinate;
                sgr.Structure[a].Velocity = atoms[a].Velocity;
                sgr.Structure[a].Type = atoms[a].Type;
                sgr.Structure[a].KinEnergy = atoms[a].KinEnergy;
                sgr.Structure[a].PotEnergy = atoms[a].PotEnergy;
            };

            //Время тестирования
            TimeIter = timeIter;
            timeIter *= numStep;

            if (periodSwap != 0)
            {
                TimeSwap = timeSwap * countSwap / 100.0d;
                timeSwap *= (numStep / periodSwap) * (countSwap / 100.0d);
            }
            else
            {
                timeSwap = 0.0d;
                TimeSwap = 0.0d;
            }

            ProcTime = new TimeSpan(0, 0, 0, 0, (int)(timeIter + timeSwap));

            if (periodSwap != 0) algSwap.Initialization(countSwap, temperature);
            else algSwap.Initialization(0, temperature);
        }

        public void Initialization(Random randSwap, Random randShift, double temperature, double shiftAtom, bool tmprVeloc,
            int[] timeDevStep, double modStep, int numStep, int normStep, bool removeRelax,
            int countSwap, int periodSwap, int periodIdent, bool corrFunc)
        {
            this.rand = randSwap;
            this.randShift = randShift;
            this.temperature = temperature;
            this.numStep = numStep;
            this.removeRelax = removeRelax;
            this.periodSwap = periodSwap;
            this.periodIdent = periodIdent;
            this.countSwap = countSwap;
            this.timeDevStep = timeDevStep;
            this.normStep = normStep;
            this.shiftAtom = shiftAtom;
            this.tmprVeloc = tmprVeloc;
            this.corrFunc = corrFunc;

            time = AtomParams.PERIOD_OSCILL;// 1.018e-13 //расчёт по формуле
            valueStep = modStep * time;// Чем выше температура, тем меньше шаг!

            Ekravn = Segregation.TemperatureEnergy(temperature, sgr.StructNumAtoms);

            listKinEnergy = new List<double>();
            listPotEnergy = new List<double>();
            listTimeStep = new List<double>();

            timeRelax = 0;
            countIdent = 0;

            algSwap = new Metropolis(sgr, rand);

            // разбиение индексов атомов, для передачи их в потоковые методы
            int half = sgr.StructNumAtoms / 8;
            halfSize = new int[9] { 0, half, half * 2, half * 3, half * 4, half * 5, half * 6, half * 7, sgr.StructNumAtoms };
            kE = new double[8];
            pE = new double[8];
        }

        public void Iterations()
        {
            listText = "[Запуск моделирования]" + Environment.NewLine + Environment.NewLine;

            double[] ra = null;
            double[,] masSiGe1 = null, masSiGe2 = null;

            // вычисление ПКФ идеального кристалла
            if (corrFunc)
            {
                listText += ">> Расчёт ПКФ..." + Environment.NewLine + Environment.NewLine;
                ra = sgr.PairCorrelationFunction(out masSiGe1);
            }

            // начальное условие: либо сдвиг по координате, либо задание скорости
            if (shiftAtom != 0)
                sgr.ShiftAtoms(shiftAtom, randShift);
            if (tmprVeloc)
                sgr.TemperatureVelocity(temperature);

            float[] probSi;
            float[] probGe;

            // начальный анализ атомов и инициализация алгоритма Метрополиса
            if (periodSwap != 0) algSwap.Initialization(countSwap, temperature);
            if (periodIdent != 0)
            {
                sgr.IdentificationAtoms(out probSi, out probGe);
                OutputInfo.WriteDistrFile(countIdent, probSi, probGe, false);
                OutputInfo.WriteDistrFile(countIdent, probSi, probGe, true);
                countIdent++;
            }

            // подсчёт начальной энергии
            double pot = 0, kin = 0;
            sgr.Energy(out pot, out kin);
            listKinEnergy.Add(kin);
            listPotEnergy.Add(pot);
            listTimeStep.Add(0);
            listText += Segregation.OutputConsole(0, 0, 0, 0, kin, pot, 0, pot);

            for (step = 1; step <= numStep; step++)
            {
                if (BREAK) break;

                // Уменьшение шага
                for (int s = 0; s < timeDevStep.Length; s++)
                    if (step == timeDevStep[s]) valueStep /= 2;

                // Перестановки атомов
                if (periodSwap != 0 && step % periodSwap == 0)
                {
                    listText += ">> Перестановки атомов..." + Environment.NewLine + Environment.NewLine;
                    algSwap.Iterations(true);
                }

                // Алгоритм Верле
                for (int x = 0; x < sgr.StructNumAtoms; x++)
                    sgr.AlgorithmVerlet(ref sgr.Structure[x], valueStep);

                // Подсчёт энергии
                double kinEnergy = 0, potEnergy = 0;
                Task[] calcEnergy = new Task[8]
                {
                    new Task(delegate { sgr.Energy(out pE[0], out kE[0], halfSize[0], halfSize[1]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[1], out kE[1], halfSize[1], halfSize[2]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[2], out kE[2], halfSize[2], halfSize[3]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[3], out kE[3], halfSize[3], halfSize[4]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[4], out kE[4], halfSize[4], halfSize[5]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[5], out kE[5], halfSize[5], halfSize[6]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[6], out kE[6], halfSize[6], halfSize[7]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { sgr.Energy(out pE[7], out kE[7], halfSize[7], halfSize[8]); }, TaskCreationOptions.AttachedToParent)
                };

                for (int task = 0; task < calcEnergy.Length; task++)
                    calcEnergy[task].Start();
                Task.WaitAll(calcEnergy);

                for (int e = 0; e < kE.Length; e++)
                {
                    kinEnergy += kE[e];
                    potEnergy += pE[e];
                }

                for (int t = 0; t < calcEnergy.Length; t++)
                    calcEnergy[t].Dispose();

                double E1 = listKinEnergy[step - 1] + listPotEnergy[step - 1];
                double E2 = kinEnergy + potEnergy;
                double deltaEnergy = E2 - E1;

                double changeKineticEnergy = kinEnergy - listKinEnergy[step - 1];
                double changePotentialEnergy = potEnergy - listPotEnergy[step - 1];

                timeRelax += valueStep / time;

                listKinEnergy.Add(kinEnergy);
                listPotEnergy.Add(potEnergy);
                listTimeStep.Add(timeRelax);

                listText += Segregation.OutputConsole(step, timeRelax, changeKineticEnergy, changePotentialEnergy, kinEnergy, potEnergy, deltaEnergy, pot);

                // Нормировка скоростей
                if (normStep != 0 && step % normStep == 0)
                {
                    double beta = Math.Sqrt(Ekravn / kinEnergy);
                    if (temperature == 0) beta = 0.5;
                    sgr.NormalizeVelocity(beta);
                }

                // Анализ соседей
                if (periodIdent != 0)
                    if (step % periodIdent == 0 || step == numStep)
                    {
                        sgr.IdentificationAtoms(out probSi, out probGe);
                        OutputInfo.WriteDistrFile(countIdent, probSi, probGe, false);
                        OutputInfo.WriteDistrFile(countIdent, probSi, probGe, true);
                        countIdent++;
                    }
            }

            // записываем в массивы энергию и шаг по времени
            masKinEnergy = listKinEnergy.ToArray();
            masPotEnergy = listPotEnergy.ToArray();
            masTimeStep = listTimeStep.ToArray();

            // сброс структуры
            if (removeRelax)
            {
                for (int a = 0; a < sgr.Structure.Length; a++)
                {
                    sgr.Structure[a].Coordinate = atoms[a].Coordinate;
                    sgr.Structure[a].Velocity = atoms[a].Velocity;
                    sgr.Structure[a].Type = atoms[a].Type;
                    sgr.Structure[a].KinEnergy = atoms[a].KinEnergy;
                    sgr.Structure[a].PotEnergy = atoms[a].PotEnergy;
                }
            }

            // вычисление и запись ПКФ
            if(corrFunc && !BREAK)
            {
                listText += ">> Расчёт ПКФ..." + Environment.NewLine + Environment.NewLine;
                sgr.PairCorrelationFunction(out masSiGe2);
                OutputInfo.WriteCorrFunction(temperature, ra, masSiGe1, masSiGe2);
            }

            listText += Environment.NewLine + "[Завершение моделирования]";

            // мой любимый звуковой сигнал
            Console.Beep(600, 1000);

            TimeIter = 0;
            TimeSwap = 0;

            // обязательное поле, для закрытия окна информации
            End = true;
        }



    }

    partial class Segregation
    {
        /// <summary>
        /// Вывод информации о релаксации и запись в Excel
        /// </summary>
        /// <param name="step">Номер шага по времени</param>
        /// <param name="timeStep">Шаг по времени</param>
        /// <param name="changeKinEnergy">Изменение кинетической энергии</param>
        /// <param name="changePotEnergy">Изменение потенциальной энергии</param>
        /// <param name="kinEnergy">Кинетическая энергия</param>
        /// <param name="potEnergy">Потенциальная энергия</param>
        /// <param name="deltaEnergy">Разница между полными энергиями</param>
        /// <param name="startPotEnergy">Начальная потенциальная энергия</param>
        public static string OutputConsole(int step, double timeStep, double changeKinEnergy, double changePotEnergy,
    double kinEnergy, double potEnergy, double deltaEnergy, double startPotEnergy)
        {
            // выравнивание столбцов энергий
            string ep = String.Format("  Ep = {0:f6} эВ", potEnergy);
            string ek = String.Format("  Ek = {0:f6} эВ", kinEnergy);
            if (ek.Length < ep.Length)
            {
                ek = ek.PadRight(ep.Length, ' ');
            }
            else if (ek.Length > ep.Length)
            {
                ep = ep.PadRight(ek.Length, ' ');
            }

            string text = String.Format("Шаг №{0}:" + Environment.NewLine, step);
            text += String.Format("{0}\tdEp = {1:E} эВ" + Environment.NewLine,
                ep, changePotEnergy);
            text += String.Format("{0}\tdEk = {1:E} эВ" + Environment.NewLine,
                ek, changeKinEnergy);
            text += String.Format("  E2 - E1 = {0:f6} эВ" + Environment.NewLine + Environment.NewLine,
                deltaEnergy);

            OutputInfo.WriteEnergyFile(step, timeStep, kinEnergy, potEnergy, startPotEnergy, false);
            OutputInfo.WriteEnergyFile(step, timeStep, kinEnergy, potEnergy, startPotEnergy, true);

            return text;
        }

        /// <summary>
        /// Формула трёхточечного дифференцирования для расчёта производной потенциальной энергии.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <param name="delta">Смещение.</param>
        /// <returns></returns>
        private double ThreePointDifferentiation(Atom selAtom, Vector delta)
        {
            double L = this.StructLength * this.StructLatPar;
            Vector coord = selAtom.Coordinate;
            double potentialEnergyRight, potentialEnergyLeft;

            selAtom.Coordinate = new Vector(selAtom.Coordinate.x, selAtom.Coordinate.y, selAtom.Coordinate.z) + delta;
            potentialEnergyRight = selAtom.PotentialEnergy(this.SelPotential, L, true);

            selAtom.Coordinate = new Vector(selAtom.Coordinate.x, selAtom.Coordinate.y, selAtom.Coordinate.z) - (2 * delta);
            potentialEnergyLeft = selAtom.PotentialEnergy(this.SelPotential, L, true);

            selAtom.Coordinate = coord;

            return (potentialEnergyRight - potentialEnergyLeft) / (2.0 * delta.MaxElement()); // эВ/нм
        }

        /// <summary>
        /// Сила, действующая на выбранный атом.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <returns></returns>
        private Vector Force(Atom selAtom)
        {
            Vector force = new Vector();
            double valueDelta = 0.01;// (нм)

            Atom a1 = (Atom)selAtom.Clone();
            Atom a2 = (Atom)selAtom.Clone();

            Task[] tasks = new Task[3]
            {
                new Task(delegate{force.x = -ThreePointDifferentiation(a1, new Vector(valueDelta, 0.0, 0.0)); }, TaskCreationOptions.AttachedToParent),
                new Task(delegate{force.y = -ThreePointDifferentiation(a2, new Vector(0.0, valueDelta, 0.0)); }, TaskCreationOptions.AttachedToParent),
                new Task(delegate{force.z = -ThreePointDifferentiation(selAtom, new Vector(0.0, 0.0, valueDelta)); }, TaskCreationOptions.AttachedToParent)
            };
            for (int t = 0; t < tasks.Length; t++)
                tasks[t].Start();
            Task.WaitAll(tasks);

            for (int t = 0; t < tasks.Length; t++)
                tasks[t].Dispose();

            return force * 0.16021766208; // перевод из эВ в Дж => (нм * кг / с^2)
        }

        /// <summary>
        /// Случайный сдвиг атомов в структуре.
        /// </summary>
        /// <param name="shift">Значение сдвига (нм).</param>
        /// <param name="rand">Генератор случайных чисел.</param>
        public void ShiftAtoms(double shift, Random rand)
        {
            if (shift == 0) return;

            double L = this.StructLength * this.StructLatPar;

            for (int a = 0; a < this.StructNumAtoms; a++)
            {
                Vector coord = this.Structure[a].Coordinate + new Vector(rand.Next(-1, 2) * shift, rand.Next(-1, 2) * shift, rand.Next(-1, 2) * shift);
                this.Structure[a].SetCoordinate(coord, L);
            }
        }
        /// <summary>
        /// Нормировка скоростей атомов структуры.
        /// </summary>
        /// <param name="beta">Коэффициент нормировки.</param>
        public void NormalizeVelocity(double beta)
        {
            for (int a = 0; a < this.StructNumAtoms; a++)
            {
                this.Structure[a].Velocity *= beta;
            }
        }

        /// <summary>
        /// Определение начальных скоростей атомов, пропорциональных заданной температуре (НЕКОРРЕКТНО РАБОТАЕТ!)
        /// </summary>
        /// <param name="temperature">Температура</param>
        public void TemperatureVelocity(double temperature)
        {
            double Ek = (3.0 / 2.0) * 8.6142e-5 * temperature / 6.24e+18;
            double mass = (AtomParams.MASS_SI + AtomParams.MASS_GE) / 2.0;
            double Vmax = Math.Sqrt(Ek / mass);
            Vmax *= 0.68e+9;

            for (int a = 0; a < this.StructNumAtoms; a++)
            {
                double x = Vmax;
                double y = Vmax;
                double z = Vmax;

                this.Structure[a].Velocity = new Vector(x, y, z);
            }
        }

        /// <summary>
        /// Алгоритм Верле.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <param name="stepTime">Шаг по времени.</param>
        public void AlgorithmVerlet(ref Atom selAtom, double stepTime)
        {
            Vector speedup = Force(selAtom);// An(м/с^2)
            double koeff = Math.Pow(stepTime, 2) / (2.0 * selAtom.Mass);
            Vector coordinate = (selAtom.Coordinate) + (selAtom.Velocity * stepTime) + (speedup * koeff);// нм

            double L = this.StructLength * this.StructLatPar;
            selAtom.SetCoordinate(coordinate, L);

            Vector speedup2 = Force(selAtom);// An+1
            speedup += speedup2;

            koeff = stepTime / (2.0 * selAtom.Mass);
            Vector velocity = selAtom.Velocity + (speedup * koeff);
            selAtom.Velocity = velocity;// нм/с
        }
    }
}
