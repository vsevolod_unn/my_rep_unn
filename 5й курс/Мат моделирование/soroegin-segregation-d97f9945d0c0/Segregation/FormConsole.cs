﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Segregation
{
    public partial class FormConsole : Form
    {
        Relaxation relax;

        public FormConsole(Relaxation relax)
        {
            InitializeComponent();

            this.relax = relax;
            txtBox_time.Text = String.Format("{0}h {1}m", relax.ProcTime.Hours, relax.ProcTime.Minutes);
            txtBox_lastTime.Text = txtBox_time.Text;
            btn_break.Visible = true;

            groupBox1.Enabled = false;
            txt_maxIter.Text = relax.GetAlgSwap.GetMaxStep.ToString();
            txt_iter.Text = "0";
        }

        private void timer_update_Tick(object sender, EventArgs e)
        {
            //вывод текста
            if (txtBox_output.Text.Length != relax.GetListText.Length && !check_outputPause.Checked)
                txtBox_output.AppendText(relax.GetListText.Substring(txtBox_output.Text.Length));

            //оставшееся время
            TimeSpan timeLast = new TimeSpan(0, 0, 0, 0, (int)(relax.TimeIter * (relax.GetNumStep - relax.GetStep) + relax.GetNumSwap * relax.TimeSwap));
            txtBox_lastTime.Text = String.Format("{0}h {1}m", timeLast.Hours, timeLast.Minutes);

            //окно перестановок
            if (relax.GetAlgSwap.GetStep == relax.GetAlgSwap.GetMaxStep && relax.GetAlgSwap.GetMaxStep != 0)
            {
                if (groupBox1.Enabled) groupBox1.Enabled = false;
                txt_iter.Text = relax.GetAlgSwap.GetMaxStep.ToString();
            }
            else if (relax.GetAlgSwap.GetStep != 0)
            {
                if (!groupBox1.Enabled) groupBox1.Enabled = true;

                txt_iter.Text = relax.GetAlgSwap.GetStep.ToString();
            }

            //прогресс бар
            if (pgsBar_time.Value != relax.GetStep)
            {
                pgsBar_time.Value = relax.GetStep;
                label_progress.Text = String.Format("{0}%", pgsBar_time.Value * 100 / pgsBar_time.Maximum);
            }

            //завершение вычислений
            if (relax.End)
            {
                timer_update.Stop();

                // если завершено, то выводим всё в текстбокс
                if (check_outputPause.Checked && txtBox_output.Text.Length != relax.GetListText.Length)
                {
                    txtBox_output.AppendText(relax.GetListText.Substring(txtBox_output.Text.Length));
                }
                check_outputPause.Enabled = false;

                pgsBar_time.Value = pgsBar_time.Maximum;
                label_progress.Text = "100%";

                btn_break.Enabled = true;
                btn_break.Text = "Закрыть";
                btn_break.BackColor = Color.White;
                btn_break.Select();
            }
        }

        private void FormConsole_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!relax.End)
            {
                MessageBox.Show("Дождитесь завершения процесса", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void FormConsole_Load(object sender, EventArgs e)
        {
            this.Activate();
            this.DoubleBuffered = true;

            pgsBar_time.Maximum = relax.GetNumStep + 1;
            timer_update.Start();
        }

        private void btn_break_Click(object sender, EventArgs e)
        {
            if (relax.End)
            {
                Close();
                return;
            }

            if (MessageBox.Show("Прервать процесс?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                btn_break.Enabled = false;
                btn_break.BackColor = Color.Green;
                relax.BREAK = true;
            }
        }
    }
}
