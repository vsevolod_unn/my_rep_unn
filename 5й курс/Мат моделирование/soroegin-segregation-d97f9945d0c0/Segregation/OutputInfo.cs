﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace Segregation
{
    public static class OutputInfo
    {
        private static string dirData = Environment.CurrentDirectory + "\\Data\\";
        private static string dirTests = Environment.CurrentDirectory + "\\Data\\Tests\\";
        private static string pathTestNum = dirData + "TestNumber.log";
        private static string pathEnergy = dirData + "Energy.csv";
        private static string pathDistr = dirData + "Distribution.csv";
        private static string pathCorrFunc = dirData + "PairCorrelation.csv";

        private static string pathTestEnergy;
        private static string pathTestDistr;
        private static string pathTestCorrFunc;

        /// <summary>
        /// Номер текущего теста. Необходимо для шаблона записи тестов
        /// </summary>
        private static int TestNum = 0;

        /// <summary>
        /// Получить номер текущего теста
        /// </summary>
        public static int GetTestNum
        {
            get { return TestNum; }
        }

        /// <summary>
        /// Проверяет наличие каталогов
        /// </summary>
        public static void Start()
        {
            DirectoryInfo diData = new DirectoryInfo(dirData);
            DirectoryInfo diTests = new DirectoryInfo(dirTests);

            if (diData.Exists)
            {
                if (diTests.Exists)
                {
                    return;
                }
                else diTests.Create();
            }
            else
            {
                diData.Create();
                diTests.Create();
            }
        }

        /// <summary>
        /// Записывает в конфигурационный файл информацию о номере текущего теста
        /// </summary>
        /// <param name="testNum">Номер текущего теста</param>
        /// <returns></returns>
        private static bool WriteTestNum(int testNum)
        {
            try
            {
                TestNum = testNum;
                using (StreamWriter sw = new StreamWriter(pathTestNum, false, Encoding.Default))
                {
                    sw.WriteLine("Не редактировать, не удалять и не перемещать данный файл, т.к. он является необходимым для сохранения тестов!");
                    sw.WriteLine("Текущая дата; номер предыдущего теста:");
                    sw.WriteLine("{0}; {1}", DateTime.Today.Date.ToString("dd.MM.yyyy"), testNum);
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        /// <summary>
        /// Считывает информацию из конфигурационного файла об дате и номере теста. Возвращает номер текущего теста.
        /// Если -1, то поток записи занят.
        /// </summary>
        /// <param name="read">Не инкрементирует считываемое значение, т.е. возращает номер предыдущего теста (за день)</param>
        /// <returns></returns>
        public static int ReadTestNum(bool read)
        {
            try
            {
                int num = 0;
                string text;
                using (StreamReader sw = new StreamReader(pathTestNum))
                {
                    sw.ReadLine();
                    sw.ReadLine();
                    text = sw.ReadLine();
                }

                string[] dateTest = text.Split(';');
                DateTime date = Convert.ToDateTime(dateTest[0]);
                if (date.Year != DateTime.Today.Year || date.Month != DateTime.Today.Month || date.Day != DateTime.Today.Day)
                {
                    if (read) return 0;
                    else
                    {
                        WriteTestNum(1);
                        return 1;
                    }
                }

                if (read) return Convert.ToInt32(dateTest[1]);

                num = Convert.ToInt32(dateTest[1]);
                ++num;
                WriteTestNum(num);

                return num;
            }
            catch (System.IO.IOException)
            {
                if (read) return -1;
                if (!WriteTestNum(1)) return -1;

                return 1;
            }
        }

        /// <summary>
        /// Открывает файл с энергией
        /// </summary>
        /// <returns></returns>
        public static bool OpenGraphEnergy()
        {
            try
            {
                Process.Start(pathEnergy);
                return true;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Открывает файл с распределением соседей
        /// </summary>
        /// <returns></returns>
        public static bool OpenDistr()
        {
            try
            {
                Process.Start(pathDistr);
                return true;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Открывает папку, где лежат сохранённые файлы
        /// </summary>
        public static void OpenStorage()
        {
            Process.Start("explorer", Environment.CurrentDirectory + "\\Data\\");
        }

        /// <summary>
        /// Записать энергию в файл
        /// </summary>
        /// <param name="step">Номер шага по времени</param>
        /// <param name="timeStep">Шаг по времени</param>
        /// <param name="kinEnergy">Кинетическая энергия на данном шаге</param>
        /// <param name="potEnergy">Потенциальная энергия на данном шаге</param>
        /// <param name="startPotEnergy">Начальная потенциальная энергия</param>
        /// <param name="test">Это файл из ряда тестов? Если true, то он сохраняется по шаблону в папке с тестами</param>
        /// <returns></returns>
        public static bool WriteEnergyFile(int step, double timeStep, double kinEnergy, double potEnergy, double startPotEnergy, bool test)
        {
            string path;
            if (test) path = pathTestEnergy;
            else path = pathEnergy;

            using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
            {
                sw.WriteLine("{0};{1};;{2};{3};{4};{5}", step, timeStep, potEnergy, kinEnergy, potEnergy - startPotEnergy, kinEnergy + potEnergy - startPotEnergy);
            }
            return true;
        }

        /// <summary>
        /// Очищает и создает новый файл
        /// </summary>
        /// <param name="test">Это файл из ряда тестов? Если true, то он сохраняется по шаблону в папке с тестами</param>
        /// <param name="tempr">Температура. Необходимо для файла из ряда тестов</param>
        /// <returns></returns>
        public static bool ClearEnergyFile(bool test, double tempr = 0)
        {
            try
            {
                string path;
                if (test) path = pathTestEnergy = dirTests + String.Format("energy_{0}_{1}_{2}K.csv", DateTime.Today.Date.ToString("yyMMdd"), TestNum, tempr);
                else path = pathEnergy;

                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("Номер шага по времени;Шаг по времени;;Потенциальная энергия Ep, эВ;Кинетическая энергия Ek, эВ;Потенциальная относ. E0p, эВ;Суммарная Es, эВ");
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        /// <summary>
        /// Записывает распределение соседей атомов структуры в файл
        /// </summary>
        /// <param name="step">Номер подсчёта распределения атомов</param>
        /// <param name="probSi">Массив распределения соседей Si</param>
        /// <param name="probGe">Массив распределения соседей Ge</param>
        /// <param name="test">Это файл из ряда тестов? Если true, то он сохраняется по шаблону в папке с тестами</param>
        /// <returns></returns>
        public static bool WriteDistrFile(int step, float[] probSi, float[] probGe, bool test)
        {
            string path;
            if (test) path = pathTestDistr;
            else path = pathDistr;

            using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
            {
                sw.WriteLine("{10};{0};{1};{2};{3};{4};{11};;{10};{5};{6};{7};{8};{9};{12}", probSi[0], probSi[1], probSi[2], probSi[3], probSi[4],
                    probGe[0], probGe[1], probGe[2], probGe[3], probGe[4], step,
                    probSi[0] + probSi[1] + probSi[2] + probSi[3] + probSi[4],
                    probGe[0] + probGe[1] + probGe[2] + probGe[3] + probGe[4]);
            }
            return true;
        }

        /// <summary>
        /// Очищает и создает новый файл
        /// </summary>
        /// <param name="test">Это файл из ряда тестов? Если true, то он сохраняется по шаблону в папке с тестами</param>
        /// <param name="tempr">Температура. Необходимо для файла из ряда тестов</param>
        /// <returns></returns>
        public static bool ClearDistrFile(bool test, double tempr = 0)
        {
            try
            {
                string path;
                if (test) path = pathTestDistr = dirTests + String.Format("distr_{0}_{1}_{2}K.csv", DateTime.Today.Date.ToString("yyMMdd"), TestNum, tempr);
                else path = pathDistr;

                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("si;0;1;2;3;4;sum;;ge;0;1;2;3;4;sum");
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        /// <summary>
        /// Очищает или создаёт новый файл для ПКФ
        /// </summary>
        /// <param name="tempr">Температура</param>
        /// <returns></returns>
        public static bool ClearCorrFunction(double tempr)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(pathCorrFunc, false, Encoding.Default))
                {
                    sw.WriteLine("r/a;Si;Ge;До/после релаксации;r/a;Si;Ge");
                }

                pathTestCorrFunc = dirTests + String.Format("corr_{0}_{1}_{2}K.csv", DateTime.Today.Date.ToString("yyMMdd"), TestNum, tempr);
                using (StreamWriter sw = new StreamWriter(pathTestCorrFunc, false, Encoding.Default))
                {
                    sw.WriteLine("r/a;Si;Ge;До/после релаксации;r/a;Si;Ge");
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }
        /// <summary>
        /// Записывает ПКФ
        /// </summary>
        /// <param name="tempr">Температура</param>
        /// <param name="ra">Массив значений r/a</param>
        /// <param name="masSiGe1">Массив ПКФ до релаксации</param>
        /// <param name="masSiGe2">Массив ПКФ после релаксации</param>
        /// <returns></returns>
        public static bool WriteCorrFunction(double tempr, double[] ra, double[,] masSiGe1, double[,] masSiGe2)
        {
            using (StreamWriter sw = new StreamWriter(pathCorrFunc, true, Encoding.Default))
            {
                for (int i = 0; i < ra.Length; i++)
                {
                    sw.WriteLine("{0};{1};{2};;{3};{4};{5}", ra[i], masSiGe1[0, i], masSiGe1[1, i], ra[i], masSiGe2[0, i], masSiGe2[1, i]);
                }
            }

            //string path = dirTests + String.Format("corr_{0}_{1}_{2}K.csv", DateTime.Today.Date.ToString("yyMMdd"), TestNum, tempr);
            using (StreamWriter sw = new StreamWriter(pathTestCorrFunc, true, Encoding.Default))
            {
                for (int i = 0; i < ra.Length; i++)
                {
                    sw.WriteLine("{0};{1};{2};;{3};{4};{5}", ra[i], masSiGe1[0, i], masSiGe1[1, i], ra[i], masSiGe2[0, i], masSiGe2[1, i]);
                }
            }
            return true;
        }
    }
}
