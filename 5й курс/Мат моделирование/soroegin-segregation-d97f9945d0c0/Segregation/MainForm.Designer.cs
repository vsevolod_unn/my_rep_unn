﻿namespace Segregation
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.GroupBox groupBox1;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label15;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label18;
            System.Windows.Forms.Label label19;
            System.Windows.Forms.Label label22;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label label23;
            System.Windows.Forms.Label label_countSwap;
            System.Windows.Forms.Label label26;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.PictureBox pictureBox2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.PictureBox pictureBox1;
            System.Windows.Forms.Label label27;
            System.Windows.Forms.Label label25;
            this.rdBtn_STW = new System.Windows.Forms.RadioButton();
            this.rdBtn_TRS = new System.Windows.Forms.RadioButton();
            this.label_swap = new System.Windows.Forms.Label();
            this.grpBox_swap = new System.Windows.Forms.GroupBox();
            this.btn_openNeigh = new System.Windows.Forms.Button();
            this.txt_graphsSwap = new System.Windows.Forms.TextBox();
            this.pict_swap = new System.Windows.Forms.PictureBox();
            this.numUD_graphsSwap = new System.Windows.Forms.NumericUpDown();
            this.numUD_swap = new System.Windows.Forms.NumericUpDown();
            this.label_testNum = new System.Windows.Forms.Label();
            this.numUD_ident = new System.Windows.Forms.NumericUpDown();
            this.btn_calculateEnergy = new System.Windows.Forms.Button();
            this.txt_sumEnergy = new System.Windows.Forms.TextBox();
            this.numUD_sizeStructure = new System.Windows.Forms.NumericUpDown();
            this.numUD_partGe = new System.Windows.Forms.NumericUpDown();
            this.btn_relaxation = new System.Windows.Forms.Button();
            this.numUD_numStepRelax = new System.Windows.Forms.NumericUpDown();
            this.numUD_multStepRelax = new System.Windows.Forms.NumericUpDown();
            this.grpBox_create = new System.Windows.Forms.GroupBox();
            this.txt_latPar = new System.Windows.Forms.TextBox();
            this.txt_numAtoms = new System.Windows.Forms.TextBox();
            this.btn_createStructure = new System.Windows.Forms.Button();
            this.label_status = new System.Windows.Forms.Label();
            this.numUD_shiftAtoms = new System.Windows.Forms.NumericUpDown();
            this.grpBox_energy = new System.Windows.Forms.GroupBox();
            this.txt_atomEnergy = new System.Windows.Forms.TextBox();
            this.grpBox_relax = new System.Windows.Forms.GroupBox();
            this.check_corrFunc = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_timetau = new System.Windows.Forms.Label();
            this.txt_time = new System.Windows.Forms.TextBox();
            this.checkBox_justRelax = new System.Windows.Forms.CheckBox();
            this.numUD_step = new System.Windows.Forms.NumericUpDown();
            this.btn_clearStep = new System.Windows.Forms.Button();
            this.btn_deleteStep = new System.Windows.Forms.Button();
            this.btn_addStep = new System.Windows.Forms.Button();
            this.checkBox_startVelocity = new System.Windows.Forms.CheckBox();
            this.numUD_periodSwap = new System.Windows.Forms.NumericUpDown();
            this.listBox_step = new System.Windows.Forms.ListBox();
            this.numUD_stepNorm = new System.Windows.Forms.NumericUpDown();
            this.txt_tmprEnergy = new System.Windows.Forms.TextBox();
            this.numUD_tmpr = new System.Windows.Forms.NumericUpDown();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formulasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escapeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grpBox_graph = new System.Windows.Forms.GroupBox();
            this.checkBox_timeRelax = new System.Windows.Forms.CheckBox();
            this.btn_openGraph = new System.Windows.Forms.Button();
            this.pict_3 = new System.Windows.Forms.PictureBox();
            this.pict_kinEnergy = new System.Windows.Forms.PictureBox();
            this.pict_potEnergy = new System.Windows.Forms.PictureBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.timer_screen = new System.Windows.Forms.Timer(this.components);
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            groupBox1 = new System.Windows.Forms.GroupBox();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label15 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label18 = new System.Windows.Forms.Label();
            label19 = new System.Windows.Forms.Label();
            label22 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            label23 = new System.Windows.Forms.Label();
            label_countSwap = new System.Windows.Forms.Label();
            label26 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            pictureBox2 = new System.Windows.Forms.PictureBox();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            label27 = new System.Windows.Forms.Label();
            label25 = new System.Windows.Forms.Label();
            groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
            this.grpBox_swap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict_swap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_graphsSwap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_swap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_ident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_sizeStructure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_partGe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_numStepRelax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_multStepRelax)).BeginInit();
            this.grpBox_create.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_shiftAtoms)).BeginInit();
            this.grpBox_energy.SuspendLayout();
            this.grpBox_relax.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_step)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_periodSwap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_stepNorm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_tmpr)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.grpBox_graph.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict_kinEnergy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict_potEnergy)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(5, 76);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(100, 13);
            label1.TabIndex = 2;
            label1.Text = "Энергия системы:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Cursor = System.Windows.Forms.Cursors.Help;
            label2.Location = new System.Drawing.Point(6, 24);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(49, 13);
            label2.TabIndex = 4;
            label2.Text = "Размер:";
            this.toolTip.SetToolTip(label2, "Количество ячеек структуры N^3");
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(this.rdBtn_STW);
            groupBox1.Controls.Add(this.rdBtn_TRS);
            groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            groupBox1.Location = new System.Drawing.Point(6, 19);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new System.Drawing.Size(238, 49);
            groupBox1.TabIndex = 1;
            groupBox1.TabStop = false;
            groupBox1.Text = "Тип потенциала";
            // 
            // rdBtn_STW
            // 
            this.rdBtn_STW.AutoSize = true;
            this.rdBtn_STW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdBtn_STW.Location = new System.Drawing.Point(89, 21);
            this.rdBtn_STW.Name = "rdBtn_STW";
            this.rdBtn_STW.Size = new System.Drawing.Size(138, 17);
            this.rdBtn_STW.TabIndex = 2;
            this.rdBtn_STW.Text = "Стиллинджера-Вебера";
            this.rdBtn_STW.UseVisualStyleBackColor = true;
            // 
            // rdBtn_TRS
            // 
            this.rdBtn_TRS.AutoSize = true;
            this.rdBtn_TRS.Checked = true;
            this.rdBtn_TRS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdBtn_TRS.Location = new System.Drawing.Point(6, 21);
            this.rdBtn_TRS.Name = "rdBtn_TRS";
            this.rdBtn_TRS.Size = new System.Drawing.Size(77, 17);
            this.rdBtn_TRS.TabIndex = 1;
            this.rdBtn_TRS.TabStop = true;
            this.rdBtn_TRS.Text = "Терсоффа";
            this.rdBtn_TRS.UseVisualStyleBackColor = true;
            this.rdBtn_TRS.CheckedChanged += new System.EventHandler(this.rdBtn_TRS_CheckedChanged);
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(6, 76);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(54, 13);
            label3.TabIndex = 12;
            label3.Text = "Доля Ge:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(6, 100);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(107, 13);
            label4.TabIndex = 16;
            label4.Text = "Параметр решётки:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(6, 24);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(103, 13);
            label6.TabIndex = 23;
            label6.Text = "Количество шагов:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Cursor = System.Windows.Forms.Cursors.Help;
            label7.Location = new System.Drawing.Point(6, 50);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(96, 13);
            label7.TabIndex = 25;
            label7.Text = "Множитель шага:";
            this.toolTip.SetToolTip(label7, "Множитель шага по времени m, т.е. шаг по времени - m*τ,\r\nгде τ - период оптически" +
        "х колебаний атомов в решётке");
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(6, 50);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(84, 13);
            label8.TabIndex = 22;
            label8.Text = "Кол-во атомов:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(6, 76);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(108, 13);
            label9.TabIndex = 25;
            label9.Text = "Случ. сдвиг атомов:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(217, 76);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(20, 13);
            label10.TabIndex = 14;
            label10.Text = "эВ";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(185, 100);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(21, 13);
            label11.TabIndex = 15;
            label11.Text = "нм";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(185, 76);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(21, 13);
            label12.TabIndex = 26;
            label12.Text = "нм";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new System.Drawing.Point(5, 102);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(101, 13);
            label13.TabIndex = 16;
            label13.Text = "Энергия на атом ≈";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(217, 102);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(20, 13);
            label14.TabIndex = 17;
            label14.Text = "эВ";
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label15.ForeColor = System.Drawing.SystemColors.ControlText;
            label15.Location = new System.Drawing.Point(9, 39);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(172, 13);
            label15.TabIndex = 34;
            label15.Text = "График потенциальной энергии:";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(6, 238);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(165, 13);
            label16.TabIndex = 35;
            label16.Text = "График кинетической энергии:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(154, 102);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(14, 13);
            label5.TabIndex = 29;
            label5.Text = "К";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(6, 102);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(77, 13);
            label17.TabIndex = 28;
            label17.Text = "Температура:";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(185, 128);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(20, 13);
            label18.TabIndex = 20;
            label18.Text = "эВ";
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Cursor = System.Windows.Forms.Cursors.Help;
            label19.Location = new System.Drawing.Point(6, 128);
            label19.Name = "label19";
            label19.Size = new System.Drawing.Size(82, 13);
            label19.TabIndex = 19;
            label19.Text = "Равн. энергия:";
            this.toolTip.SetToolTip(label19, "Энергия равновесного состояния структуры");
            // 
            // label22
            // 
            label22.AutoSize = true;
            label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label22.ForeColor = System.Drawing.SystemColors.ControlText;
            label22.Location = new System.Drawing.Point(9, 437);
            label22.Name = "label22";
            label22.Size = new System.Drawing.Size(317, 13);
            label22.TabIndex = 36;
            label22.Text = "График кинетической, потенциальной и суммарной энергии:";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Cursor = System.Windows.Forms.Cursors.Help;
            label20.Location = new System.Drawing.Point(252, 102);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(83, 13);
            label20.TabIndex = 34;
            label20.Text = "Деление шага:";
            this.toolTip.SetToolTip(label20, "На каких шагах шаг по времени будет уменьшен в 2 раза");
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Cursor = System.Windows.Forms.Cursors.Help;
            label23.Location = new System.Drawing.Point(252, 50);
            label23.Name = "label23";
            label23.Size = new System.Drawing.Size(169, 13);
            label23.TabIndex = 40;
            label23.Text = "Период нормировки скоростей:";
            this.toolTip.SetToolTip(label23, "Каждые N шагов скорости атомов нормируются в зависимости от равновесной энергии");
            // 
            // label_countSwap
            // 
            label_countSwap.AutoSize = true;
            label_countSwap.BackColor = System.Drawing.SystemColors.Control;
            label_countSwap.Location = new System.Drawing.Point(6, 24);
            label_countSwap.Name = "label_countSwap";
            label_countSwap.Size = new System.Drawing.Size(143, 13);
            label_countSwap.TabIndex = 43;
            label_countSwap.Text = "Количество перестановок:";
            // 
            // label26
            // 
            label26.AutoSize = true;
            label26.Cursor = System.Windows.Forms.Cursors.Help;
            label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label26.Location = new System.Drawing.Point(252, 24);
            label26.Name = "label26";
            label26.Size = new System.Drawing.Size(178, 13);
            label26.TabIndex = 46;
            label26.Text = "Период анализа соседей атомов:";
            this.toolTip.SetToolTip(label26, "Каждые N шагов вычисляется распределение соседей атомов");
            // 
            // label21
            // 
            label21.AutoSize = true;
            label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label21.ForeColor = System.Drawing.SystemColors.ControlText;
            label21.Location = new System.Drawing.Point(6, 50);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(166, 13);
            label21.TabIndex = 39;
            label21.Text = "График энергии перестановок:";
            // 
            // pictureBox2
            // 
            pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            pictureBox2.Location = new System.Drawing.Point(394, 337);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new System.Drawing.Size(150, 45);
            pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            pictureBox2.TabIndex = 48;
            pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = global::Segregation.Properties.Resources.latter1;
            pictureBox1.Location = new System.Drawing.Point(213, 12);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(68, 68);
            pictureBox1.TabIndex = 47;
            pictureBox1.TabStop = false;
            // 
            // label27
            // 
            label27.AutoSize = true;
            label27.BackColor = System.Drawing.SystemColors.Control;
            label27.Location = new System.Drawing.Point(236, 50);
            label27.Name = "label27";
            label27.Size = new System.Drawing.Size(19, 13);
            label27.TabIndex = 53;
            label27.Text = "из";
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Location = new System.Drawing.Point(79, 24);
            label25.Name = "label25";
            label25.Size = new System.Drawing.Size(19, 13);
            label25.TabIndex = 56;
            label25.Text = "пс";
            // 
            // label_swap
            // 
            this.label_swap.AutoSize = true;
            this.label_swap.Cursor = System.Windows.Forms.Cursors.Help;
            this.label_swap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_swap.Location = new System.Drawing.Point(252, 76);
            this.label_swap.Name = "label_swap";
            this.label_swap.Size = new System.Drawing.Size(162, 13);
            this.label_swap.TabIndex = 45;
            this.label_swap.Text = "Период перестановок атомов:";
            this.toolTip.SetToolTip(this.label_swap, "Каждые N шагов атомы переставляются по алгоритму Метрополиса,\r\nколичество переста" +
        "новок подсвечено красным");
            this.label_swap.MouseEnter += new System.EventHandler(this.label_swap_MouseEnter);
            this.label_swap.MouseLeave += new System.EventHandler(this.label_swap_MouseLeave);
            // 
            // grpBox_swap
            // 
            this.grpBox_swap.Controls.Add(this.btn_openNeigh);
            this.grpBox_swap.Controls.Add(label21);
            this.grpBox_swap.Controls.Add(this.txt_graphsSwap);
            this.grpBox_swap.Controls.Add(label27);
            this.grpBox_swap.Controls.Add(this.pict_swap);
            this.grpBox_swap.Controls.Add(this.numUD_graphsSwap);
            this.grpBox_swap.Controls.Add(label_countSwap);
            this.grpBox_swap.Controls.Add(this.numUD_swap);
            this.grpBox_swap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_swap.Location = new System.Drawing.Point(6, 210);
            this.grpBox_swap.Name = "grpBox_swap";
            this.grpBox_swap.Size = new System.Drawing.Size(378, 265);
            this.grpBox_swap.TabIndex = 3;
            this.grpBox_swap.TabStop = false;
            this.grpBox_swap.Text = "Алгоритм Метрополиса";
            // 
            // btn_openNeigh
            // 
            this.btn_openNeigh.BackColor = System.Drawing.Color.White;
            this.btn_openNeigh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openNeigh.Location = new System.Drawing.Point(261, 19);
            this.btn_openNeigh.Name = "btn_openNeigh";
            this.btn_openNeigh.Size = new System.Drawing.Size(107, 23);
            this.btn_openNeigh.TabIndex = 18;
            this.btn_openNeigh.TabStop = false;
            this.btn_openNeigh.Text = "Открыть в Excel";
            this.btn_openNeigh.UseVisualStyleBackColor = false;
            this.btn_openNeigh.Click += new System.EventHandler(this.btn_openNeigh_Click);
            // 
            // txt_graphsSwap
            // 
            this.txt_graphsSwap.BackColor = System.Drawing.SystemColors.Control;
            this.txt_graphsSwap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_graphsSwap.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_graphsSwap.Location = new System.Drawing.Point(261, 48);
            this.txt_graphsSwap.Name = "txt_graphsSwap";
            this.txt_graphsSwap.ReadOnly = true;
            this.txt_graphsSwap.Size = new System.Drawing.Size(36, 20);
            this.txt_graphsSwap.TabIndex = 54;
            this.txt_graphsSwap.TabStop = false;
            this.txt_graphsSwap.Text = "0";
            this.txt_graphsSwap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pict_swap
            // 
            this.pict_swap.BackColor = System.Drawing.Color.White;
            this.pict_swap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pict_swap.Location = new System.Drawing.Point(8, 74);
            this.pict_swap.Name = "pict_swap";
            this.pict_swap.Size = new System.Drawing.Size(360, 180);
            this.pict_swap.TabIndex = 38;
            this.pict_swap.TabStop = false;
            // 
            // numUD_graphsSwap
            // 
            this.numUD_graphsSwap.BackColor = System.Drawing.SystemColors.Window;
            this.numUD_graphsSwap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_graphsSwap.Location = new System.Drawing.Point(181, 48);
            this.numUD_graphsSwap.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numUD_graphsSwap.Name = "numUD_graphsSwap";
            this.numUD_graphsSwap.Size = new System.Drawing.Size(49, 20);
            this.numUD_graphsSwap.TabIndex = 52;
            this.numUD_graphsSwap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numUD_graphsSwap.ValueChanged += new System.EventHandler(this.numUD_graphsSwap_ValueChanged);
            // 
            // numUD_swap
            // 
            this.numUD_swap.BackColor = System.Drawing.SystemColors.Window;
            this.numUD_swap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_swap.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUD_swap.Location = new System.Drawing.Point(155, 22);
            this.numUD_swap.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numUD_swap.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUD_swap.Name = "numUD_swap";
            this.numUD_swap.Size = new System.Drawing.Size(75, 20);
            this.numUD_swap.TabIndex = 0;
            this.numUD_swap.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // label_testNum
            // 
            this.label_testNum.AutoSize = true;
            this.label_testNum.Cursor = System.Windows.Forms.Cursors.Help;
            this.label_testNum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label_testNum.ForeColor = System.Drawing.Color.Navy;
            this.label_testNum.Location = new System.Drawing.Point(6, 482);
            this.label_testNum.Name = "label_testNum";
            this.label_testNum.Size = new System.Drawing.Size(157, 13);
            this.label_testNum.TabIndex = 48;
            this.label_testNum.Text = "Проведено экспериментов: 0";
            this.label_testNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip.SetToolTip(this.label_testNum, "Количество проведённых тестов за день");
            // 
            // numUD_ident
            // 
            this.numUD_ident.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_ident.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_ident.Location = new System.Drawing.Point(436, 22);
            this.numUD_ident.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_ident.Name = "numUD_ident";
            this.numUD_ident.Size = new System.Drawing.Size(44, 20);
            this.numUD_ident.TabIndex = 6;
            this.numUD_ident.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btn_calculateEnergy
            // 
            this.btn_calculateEnergy.BackColor = System.Drawing.Color.White;
            this.btn_calculateEnergy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_calculateEnergy.Location = new System.Drawing.Point(6, 134);
            this.btn_calculateEnergy.Name = "btn_calculateEnergy";
            this.btn_calculateEnergy.Size = new System.Drawing.Size(75, 23);
            this.btn_calculateEnergy.TabIndex = 0;
            this.btn_calculateEnergy.Text = "Подсчёт";
            this.btn_calculateEnergy.UseVisualStyleBackColor = false;
            this.btn_calculateEnergy.Click += new System.EventHandler(this.btn_calculateEnergy_Click);
            // 
            // txt_sumEnergy
            // 
            this.txt_sumEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_sumEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_sumEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_sumEnergy.Location = new System.Drawing.Point(111, 74);
            this.txt_sumEnergy.Name = "txt_sumEnergy";
            this.txt_sumEnergy.ReadOnly = true;
            this.txt_sumEnergy.Size = new System.Drawing.Size(100, 20);
            this.txt_sumEnergy.TabIndex = 1;
            this.txt_sumEnergy.TabStop = false;
            this.txt_sumEnergy.Text = "0";
            this.txt_sumEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numUD_sizeStructure
            // 
            this.numUD_sizeStructure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_sizeStructure.Location = new System.Drawing.Point(66, 22);
            this.numUD_sizeStructure.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numUD_sizeStructure.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUD_sizeStructure.Name = "numUD_sizeStructure";
            this.numUD_sizeStructure.Size = new System.Drawing.Size(42, 20);
            this.numUD_sizeStructure.TabIndex = 0;
            this.numUD_sizeStructure.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numUD_sizeStructure.ValueChanged += new System.EventHandler(this.numUD_sizeStructure_ValueChanged);
            // 
            // numUD_partGe
            // 
            this.numUD_partGe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_partGe.DecimalPlaces = 2;
            this.numUD_partGe.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numUD_partGe.Location = new System.Drawing.Point(66, 74);
            this.numUD_partGe.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_partGe.Name = "numUD_partGe";
            this.numUD_partGe.Size = new System.Drawing.Size(43, 20);
            this.numUD_partGe.TabIndex = 1;
            this.numUD_partGe.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numUD_partGe.ValueChanged += new System.EventHandler(this.numUD_partGe_ValueChanged);
            // 
            // btn_relaxation
            // 
            this.btn_relaxation.BackColor = System.Drawing.Color.White;
            this.btn_relaxation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_relaxation.Location = new System.Drawing.Point(469, 477);
            this.btn_relaxation.Name = "btn_relaxation";
            this.btn_relaxation.Size = new System.Drawing.Size(80, 23);
            this.btn_relaxation.TabIndex = 10;
            this.btn_relaxation.Text = "Запуск";
            this.btn_relaxation.UseVisualStyleBackColor = false;
            this.btn_relaxation.Click += new System.EventHandler(this.btn_relaxation_Click);
            // 
            // numUD_numStepRelax
            // 
            this.numUD_numStepRelax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_numStepRelax.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUD_numStepRelax.Location = new System.Drawing.Point(120, 22);
            this.numUD_numStepRelax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_numStepRelax.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUD_numStepRelax.Name = "numUD_numStepRelax";
            this.numUD_numStepRelax.Size = new System.Drawing.Size(59, 20);
            this.numUD_numStepRelax.TabIndex = 1;
            this.numUD_numStepRelax.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numUD_numStepRelax.ValueChanged += new System.EventHandler(this.numUD_numStepRelax_ValueChanged);
            // 
            // numUD_multStepRelax
            // 
            this.numUD_multStepRelax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_multStepRelax.DecimalPlaces = 3;
            this.numUD_multStepRelax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_multStepRelax.Location = new System.Drawing.Point(120, 48);
            this.numUD_multStepRelax.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_multStepRelax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_multStepRelax.Name = "numUD_multStepRelax";
            this.numUD_multStepRelax.Size = new System.Drawing.Size(59, 20);
            this.numUD_multStepRelax.TabIndex = 2;
            this.numUD_multStepRelax.Value = new decimal(new int[] {
            10,
            0,
            0,
            196608});
            this.numUD_multStepRelax.ValueChanged += new System.EventHandler(this.numUD_numStepRelax_ValueChanged);
            // 
            // grpBox_create
            // 
            this.grpBox_create.Controls.Add(pictureBox1);
            this.grpBox_create.Controls.Add(label11);
            this.grpBox_create.Controls.Add(this.txt_latPar);
            this.grpBox_create.Controls.Add(this.txt_numAtoms);
            this.grpBox_create.Controls.Add(label8);
            this.grpBox_create.Controls.Add(this.btn_createStructure);
            this.grpBox_create.Controls.Add(this.numUD_sizeStructure);
            this.grpBox_create.Controls.Add(label2);
            this.grpBox_create.Controls.Add(this.numUD_partGe);
            this.grpBox_create.Controls.Add(label3);
            this.grpBox_create.Controls.Add(label4);
            this.grpBox_create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_create.Location = new System.Drawing.Point(12, 36);
            this.grpBox_create.Name = "grpBox_create";
            this.grpBox_create.Size = new System.Drawing.Size(287, 163);
            this.grpBox_create.TabIndex = 0;
            this.grpBox_create.TabStop = false;
            this.grpBox_create.Text = "Параметры создания структуры";
            // 
            // txt_latPar
            // 
            this.txt_latPar.BackColor = System.Drawing.SystemColors.Control;
            this.txt_latPar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_latPar.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_latPar.Location = new System.Drawing.Point(119, 98);
            this.txt_latPar.Name = "txt_latPar";
            this.txt_latPar.ReadOnly = true;
            this.txt_latPar.Size = new System.Drawing.Size(60, 20);
            this.txt_latPar.TabIndex = 24;
            this.txt_latPar.TabStop = false;
            this.txt_latPar.Text = "0,554535";
            this.txt_latPar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_numAtoms
            // 
            this.txt_numAtoms.BackColor = System.Drawing.SystemColors.Control;
            this.txt_numAtoms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_numAtoms.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_numAtoms.Location = new System.Drawing.Point(96, 48);
            this.txt_numAtoms.Name = "txt_numAtoms";
            this.txt_numAtoms.ReadOnly = true;
            this.txt_numAtoms.Size = new System.Drawing.Size(57, 20);
            this.txt_numAtoms.TabIndex = 23;
            this.txt_numAtoms.TabStop = false;
            this.txt_numAtoms.Text = "1000";
            this.txt_numAtoms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_createStructure
            // 
            this.btn_createStructure.BackColor = System.Drawing.Color.White;
            this.btn_createStructure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_createStructure.Location = new System.Drawing.Point(6, 134);
            this.btn_createStructure.Name = "btn_createStructure";
            this.btn_createStructure.Size = new System.Drawing.Size(75, 23);
            this.btn_createStructure.TabIndex = 2;
            this.btn_createStructure.Text = "Создать";
            this.btn_createStructure.UseVisualStyleBackColor = false;
            this.btn_createStructure.Click += new System.EventHandler(this.btn_createStructure_Click);
            // 
            // label_status
            // 
            this.label_status.BackColor = System.Drawing.SystemColors.Control;
            this.label_status.Cursor = System.Windows.Forms.Cursors.Default;
            this.label_status.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_status.ForeColor = System.Drawing.Color.Red;
            this.label_status.Location = new System.Drawing.Point(6, 16);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(147, 32);
            this.label_status.TabIndex = 46;
            this.label_status.Text = "STATUS: WAIT";
            this.label_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numUD_shiftAtoms
            // 
            this.numUD_shiftAtoms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_shiftAtoms.DecimalPlaces = 3;
            this.numUD_shiftAtoms.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_shiftAtoms.Location = new System.Drawing.Point(120, 74);
            this.numUD_shiftAtoms.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_shiftAtoms.Name = "numUD_shiftAtoms";
            this.numUD_shiftAtoms.Size = new System.Drawing.Size(59, 20);
            this.numUD_shiftAtoms.TabIndex = 3;
            this.numUD_shiftAtoms.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numUD_shiftAtoms.ValueChanged += new System.EventHandler(this.numUD_shiftAtoms_ValueChanged);
            // 
            // grpBox_energy
            // 
            this.grpBox_energy.Controls.Add(label14);
            this.grpBox_energy.Controls.Add(this.txt_atomEnergy);
            this.grpBox_energy.Controls.Add(label13);
            this.grpBox_energy.Controls.Add(label10);
            this.grpBox_energy.Controls.Add(groupBox1);
            this.grpBox_energy.Controls.Add(this.txt_sumEnergy);
            this.grpBox_energy.Controls.Add(label1);
            this.grpBox_energy.Controls.Add(this.btn_calculateEnergy);
            this.grpBox_energy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_energy.Location = new System.Drawing.Point(305, 36);
            this.grpBox_energy.Name = "grpBox_energy";
            this.grpBox_energy.Size = new System.Drawing.Size(262, 163);
            this.grpBox_energy.TabIndex = 1;
            this.grpBox_energy.TabStop = false;
            this.grpBox_energy.Text = "Параметры рассчёта энергии";
            // 
            // txt_atomEnergy
            // 
            this.txt_atomEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_atomEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_atomEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_atomEnergy.Location = new System.Drawing.Point(111, 100);
            this.txt_atomEnergy.Name = "txt_atomEnergy";
            this.txt_atomEnergy.ReadOnly = true;
            this.txt_atomEnergy.Size = new System.Drawing.Size(100, 20);
            this.txt_atomEnergy.TabIndex = 15;
            this.txt_atomEnergy.TabStop = false;
            this.txt_atomEnergy.Text = "0";
            this.txt_atomEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpBox_relax
            // 
            this.grpBox_relax.Controls.Add(this.check_corrFunc);
            this.grpBox_relax.Controls.Add(this.groupBox3);
            this.grpBox_relax.Controls.Add(this.groupBox2);
            this.grpBox_relax.Controls.Add(this.grpBox_swap);
            this.grpBox_relax.Controls.Add(this.label_testNum);
            this.grpBox_relax.Controls.Add(pictureBox2);
            this.grpBox_relax.Controls.Add(label26);
            this.grpBox_relax.Controls.Add(this.checkBox_justRelax);
            this.grpBox_relax.Controls.Add(this.numUD_ident);
            this.grpBox_relax.Controls.Add(this.numUD_step);
            this.grpBox_relax.Controls.Add(this.btn_clearStep);
            this.grpBox_relax.Controls.Add(this.label_swap);
            this.grpBox_relax.Controls.Add(this.btn_deleteStep);
            this.grpBox_relax.Controls.Add(this.btn_addStep);
            this.grpBox_relax.Controls.Add(this.checkBox_startVelocity);
            this.grpBox_relax.Controls.Add(this.numUD_periodSwap);
            this.grpBox_relax.Controls.Add(label20);
            this.grpBox_relax.Controls.Add(this.listBox_step);
            this.grpBox_relax.Controls.Add(this.numUD_stepNorm);
            this.grpBox_relax.Controls.Add(label18);
            this.grpBox_relax.Controls.Add(label23);
            this.grpBox_relax.Controls.Add(label5);
            this.grpBox_relax.Controls.Add(this.txt_tmprEnergy);
            this.grpBox_relax.Controls.Add(label19);
            this.grpBox_relax.Controls.Add(label17);
            this.grpBox_relax.Controls.Add(this.numUD_tmpr);
            this.grpBox_relax.Controls.Add(label12);
            this.grpBox_relax.Controls.Add(this.btn_relaxation);
            this.grpBox_relax.Controls.Add(this.numUD_numStepRelax);
            this.grpBox_relax.Controls.Add(label9);
            this.grpBox_relax.Controls.Add(label6);
            this.grpBox_relax.Controls.Add(this.numUD_multStepRelax);
            this.grpBox_relax.Controls.Add(label7);
            this.grpBox_relax.Controls.Add(this.numUD_shiftAtoms);
            this.grpBox_relax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_relax.Location = new System.Drawing.Point(12, 205);
            this.grpBox_relax.Name = "grpBox_relax";
            this.grpBox_relax.Size = new System.Drawing.Size(555, 505);
            this.grpBox_relax.TabIndex = 2;
            this.grpBox_relax.TabStop = false;
            this.grpBox_relax.Text = "Параметры моделирования";
            // 
            // check_corrFunc
            // 
            this.check_corrFunc.AutoSize = true;
            this.check_corrFunc.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.check_corrFunc.Cursor = System.Windows.Forms.Cursors.Help;
            this.check_corrFunc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check_corrFunc.Location = new System.Drawing.Point(428, 149);
            this.check_corrFunc.Name = "check_corrFunc";
            this.check_corrFunc.Size = new System.Drawing.Size(87, 17);
            this.check_corrFunc.TabIndex = 60;
            this.check_corrFunc.Text = "Расчёт ПКФ";
            this.toolTip.SetToolTip(this.check_corrFunc, "Расчёт парной корреляционной функции (ПКФ) до и после релаксации");
            this.check_corrFunc.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label_status);
            this.groupBox3.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox3.Location = new System.Drawing.Point(390, 271);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(159, 55);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Статус моделирования";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_timetau);
            this.groupBox2.Controls.Add(this.txt_time);
            this.groupBox2.Controls.Add(label25);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Help;
            this.groupBox2.Location = new System.Drawing.Point(390, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(159, 55);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Время моделирования";
            this.toolTip.SetToolTip(this.groupBox2, "Физическое время моделирования");
            // 
            // txt_timetau
            // 
            this.txt_timetau.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txt_timetau.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_timetau.Location = new System.Drawing.Point(105, 24);
            this.txt_timetau.Name = "txt_timetau";
            this.txt_timetau.Size = new System.Drawing.Size(47, 13);
            this.txt_timetau.TabIndex = 59;
            this.txt_timetau.Text = "(0τ)";
            this.txt_timetau.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip.SetToolTip(this.txt_timetau, "Период оптических колебаний атомов в решётке τ = 1.018e-13 с");
            // 
            // txt_time
            // 
            this.txt_time.BackColor = System.Drawing.SystemColors.Control;
            this.txt_time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_time.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_time.Location = new System.Drawing.Point(12, 22);
            this.txt_time.Name = "txt_time";
            this.txt_time.ReadOnly = true;
            this.txt_time.Size = new System.Drawing.Size(61, 20);
            this.txt_time.TabIndex = 31;
            this.txt_time.TabStop = false;
            this.txt_time.Text = "0";
            this.txt_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBox_justRelax
            // 
            this.checkBox_justRelax.AutoSize = true;
            this.checkBox_justRelax.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox_justRelax.Cursor = System.Windows.Forms.Cursors.Help;
            this.checkBox_justRelax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_justRelax.Location = new System.Drawing.Point(428, 118);
            this.checkBox_justRelax.Name = "checkBox_justRelax";
            this.checkBox_justRelax.Size = new System.Drawing.Size(109, 17);
            this.checkBox_justRelax.TabIndex = 9;
            this.checkBox_justRelax.Text = "Сброс структуры";
            this.toolTip.SetToolTip(this.checkBox_justRelax, "Если TRUE, то структура не меняется после моделирования,\r\nт.е. возвращается в исх" +
        "одное состояние");
            this.checkBox_justRelax.UseVisualStyleBackColor = true;
            // 
            // numUD_step
            // 
            this.numUD_step.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_step.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_step.Location = new System.Drawing.Point(255, 118);
            this.numUD_step.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_step.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_step.Name = "numUD_step";
            this.numUD_step.Size = new System.Drawing.Size(80, 20);
            this.numUD_step.TabIndex = 39;
            this.numUD_step.TabStop = false;
            this.numUD_step.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // btn_clearStep
            // 
            this.btn_clearStep.BackColor = System.Drawing.Color.White;
            this.btn_clearStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_clearStep.Location = new System.Drawing.Point(341, 177);
            this.btn_clearStep.Name = "btn_clearStep";
            this.btn_clearStep.Size = new System.Drawing.Size(75, 23);
            this.btn_clearStep.TabIndex = 38;
            this.btn_clearStep.TabStop = false;
            this.btn_clearStep.Text = "Очистить";
            this.btn_clearStep.UseVisualStyleBackColor = false;
            this.btn_clearStep.Click += new System.EventHandler(this.btn_clearStep_Click);
            // 
            // btn_deleteStep
            // 
            this.btn_deleteStep.BackColor = System.Drawing.Color.White;
            this.btn_deleteStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_deleteStep.Location = new System.Drawing.Point(341, 146);
            this.btn_deleteStep.Name = "btn_deleteStep";
            this.btn_deleteStep.Size = new System.Drawing.Size(75, 23);
            this.btn_deleteStep.TabIndex = 37;
            this.btn_deleteStep.TabStop = false;
            this.btn_deleteStep.Text = "Удалить";
            this.btn_deleteStep.UseVisualStyleBackColor = false;
            this.btn_deleteStep.Click += new System.EventHandler(this.btn_deleteStep_Click);
            // 
            // btn_addStep
            // 
            this.btn_addStep.BackColor = System.Drawing.Color.White;
            this.btn_addStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_addStep.Location = new System.Drawing.Point(341, 115);
            this.btn_addStep.Name = "btn_addStep";
            this.btn_addStep.Size = new System.Drawing.Size(75, 23);
            this.btn_addStep.TabIndex = 36;
            this.btn_addStep.TabStop = false;
            this.btn_addStep.Text = "Добавить";
            this.btn_addStep.UseVisualStyleBackColor = false;
            this.btn_addStep.Click += new System.EventHandler(this.btn_addStep_Click);
            // 
            // checkBox_startVelocity
            // 
            this.checkBox_startVelocity.AutoSize = true;
            this.checkBox_startVelocity.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox_startVelocity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_startVelocity.Location = new System.Drawing.Point(4, 152);
            this.checkBox_startVelocity.Name = "checkBox_startVelocity";
            this.checkBox_startVelocity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBox_startVelocity.Size = new System.Drawing.Size(186, 30);
            this.checkBox_startVelocity.TabIndex = 5;
            this.checkBox_startVelocity.Text = "Начальные скорости\r\nпропорциональны температуре:";
            this.checkBox_startVelocity.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox_startVelocity.UseVisualStyleBackColor = true;
            this.checkBox_startVelocity.CheckedChanged += new System.EventHandler(this.checkBox_startVelocity_CheckedChanged);
            // 
            // numUD_periodSwap
            // 
            this.numUD_periodSwap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_periodSwap.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_periodSwap.Location = new System.Drawing.Point(436, 74);
            this.numUD_periodSwap.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_periodSwap.Name = "numUD_periodSwap";
            this.numUD_periodSwap.Size = new System.Drawing.Size(44, 20);
            this.numUD_periodSwap.TabIndex = 8;
            this.numUD_periodSwap.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // listBox_step
            // 
            this.listBox_step.FormattingEnabled = true;
            this.listBox_step.Location = new System.Drawing.Point(255, 144);
            this.listBox_step.Name = "listBox_step";
            this.listBox_step.Size = new System.Drawing.Size(80, 56);
            this.listBox_step.TabIndex = 33;
            this.listBox_step.TabStop = false;
            // 
            // numUD_stepNorm
            // 
            this.numUD_stepNorm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_stepNorm.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_stepNorm.Location = new System.Drawing.Point(436, 48);
            this.numUD_stepNorm.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_stepNorm.Name = "numUD_stepNorm";
            this.numUD_stepNorm.Size = new System.Drawing.Size(44, 20);
            this.numUD_stepNorm.TabIndex = 7;
            this.numUD_stepNorm.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // txt_tmprEnergy
            // 
            this.txt_tmprEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_tmprEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_tmprEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_tmprEnergy.Location = new System.Drawing.Point(94, 126);
            this.txt_tmprEnergy.Name = "txt_tmprEnergy";
            this.txt_tmprEnergy.ReadOnly = true;
            this.txt_tmprEnergy.Size = new System.Drawing.Size(85, 20);
            this.txt_tmprEnergy.TabIndex = 18;
            this.txt_tmprEnergy.TabStop = false;
            this.txt_tmprEnergy.Text = "0";
            this.txt_tmprEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numUD_tmpr
            // 
            this.numUD_tmpr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_tmpr.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUD_tmpr.Location = new System.Drawing.Point(94, 100);
            this.numUD_tmpr.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_tmpr.Name = "numUD_tmpr";
            this.numUD_tmpr.Size = new System.Drawing.Size(54, 20);
            this.numUD_tmpr.TabIndex = 4;
            this.numUD_tmpr.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numUD_tmpr.ValueChanged += new System.EventHandler(this.numUD_tmpr_ValueChanged);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.formulasToolStripMenuItem,
            this.infoToolStripMenuItem,
            this.escapeToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip.Size = new System.Drawing.Size(964, 24);
            this.menuStrip.TabIndex = 30;
            this.menuStrip.Text = "menuStrip";
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.dataToolStripMenuItem.Text = "Папка данных";
            this.dataToolStripMenuItem.Click += new System.EventHandler(this.dataToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.optionsToolStripMenuItem.Text = "Настройки";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // formulasToolStripMenuItem
            // 
            this.formulasToolStripMenuItem.Name = "formulasToolStripMenuItem";
            this.formulasToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.formulasToolStripMenuItem.Text = "Формулы";
            this.formulasToolStripMenuItem.Click += new System.EventHandler(this.formulasToolStripMenuItem_Click);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.infoToolStripMenuItem.Text = "О программе";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // escapeToolStripMenuItem
            // 
            this.escapeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.escapeToolStripMenuItem.Name = "escapeToolStripMenuItem";
            this.escapeToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.escapeToolStripMenuItem.Text = "Выход";
            this.escapeToolStripMenuItem.Click += new System.EventHandler(this.escapeToolStripMenuItem_Click);
            // 
            // grpBox_graph
            // 
            this.grpBox_graph.Controls.Add(this.checkBox_timeRelax);
            this.grpBox_graph.Controls.Add(label22);
            this.grpBox_graph.Controls.Add(this.btn_openGraph);
            this.grpBox_graph.Controls.Add(this.pict_3);
            this.grpBox_graph.Controls.Add(label16);
            this.grpBox_graph.Controls.Add(label15);
            this.grpBox_graph.Controls.Add(this.pict_kinEnergy);
            this.grpBox_graph.Controls.Add(this.pict_potEnergy);
            this.grpBox_graph.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_graph.Location = new System.Drawing.Point(573, 36);
            this.grpBox_graph.Name = "grpBox_graph";
            this.grpBox_graph.Size = new System.Drawing.Size(379, 674);
            this.grpBox_graph.TabIndex = 4;
            this.grpBox_graph.TabStop = false;
            this.grpBox_graph.Text = "Графики энергий";
            // 
            // checkBox_timeRelax
            // 
            this.checkBox_timeRelax.AutoSize = true;
            this.checkBox_timeRelax.Cursor = System.Windows.Forms.Cursors.Help;
            this.checkBox_timeRelax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_timeRelax.Location = new System.Drawing.Point(261, 23);
            this.checkBox_timeRelax.Name = "checkBox_timeRelax";
            this.checkBox_timeRelax.Size = new System.Drawing.Size(108, 17);
            this.checkBox_timeRelax.TabIndex = 37;
            this.checkBox_timeRelax.TabStop = false;
            this.checkBox_timeRelax.Text = "Выводить время";
            this.toolTip.SetToolTip(this.checkBox_timeRelax, "Отобразить на графиках шаг по времени,\r\nвместо номера шага по времени");
            this.checkBox_timeRelax.UseVisualStyleBackColor = true;
            this.checkBox_timeRelax.CheckedChanged += new System.EventHandler(this.radioBtn_graphFull_CheckedChanged);
            // 
            // btn_openGraph
            // 
            this.btn_openGraph.BackColor = System.Drawing.Color.White;
            this.btn_openGraph.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openGraph.Location = new System.Drawing.Point(267, 644);
            this.btn_openGraph.Name = "btn_openGraph";
            this.btn_openGraph.Size = new System.Drawing.Size(102, 23);
            this.btn_openGraph.TabIndex = 19;
            this.btn_openGraph.TabStop = false;
            this.btn_openGraph.Text = "Открыть в Excel";
            this.btn_openGraph.UseVisualStyleBackColor = false;
            this.btn_openGraph.Click += new System.EventHandler(this.btn_openGraph_Click);
            // 
            // pict_3
            // 
            this.pict_3.BackColor = System.Drawing.Color.White;
            this.pict_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pict_3.Location = new System.Drawing.Point(9, 453);
            this.pict_3.Name = "pict_3";
            this.pict_3.Size = new System.Drawing.Size(360, 180);
            this.pict_3.TabIndex = 35;
            this.pict_3.TabStop = false;
            // 
            // pict_kinEnergy
            // 
            this.pict_kinEnergy.BackColor = System.Drawing.Color.White;
            this.pict_kinEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pict_kinEnergy.Location = new System.Drawing.Point(9, 254);
            this.pict_kinEnergy.Name = "pict_kinEnergy";
            this.pict_kinEnergy.Size = new System.Drawing.Size(360, 180);
            this.pict_kinEnergy.TabIndex = 21;
            this.pict_kinEnergy.TabStop = false;
            // 
            // pict_potEnergy
            // 
            this.pict_potEnergy.BackColor = System.Drawing.Color.White;
            this.pict_potEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pict_potEnergy.Location = new System.Drawing.Point(9, 55);
            this.pict_potEnergy.Name = "pict_potEnergy";
            this.pict_potEnergy.Size = new System.Drawing.Size(360, 180);
            this.pict_potEnergy.TabIndex = 20;
            this.pict_potEnergy.TabStop = false;
            // 
            // timer_screen
            // 
            this.timer_screen.Interval = 300;
            this.timer_screen.Tick += new System.EventHandler(this.timer_screen_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(964, 722);
            this.Controls.Add(this.grpBox_graph);
            this.Controls.Add(this.grpBox_relax);
            this.Controls.Add(this.grpBox_energy);
            this.Controls.Add(this.grpBox_create);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Моделирование сегрегации в сплаве SiGe";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
            this.grpBox_swap.ResumeLayout(false);
            this.grpBox_swap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict_swap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_graphsSwap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_swap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_ident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_sizeStructure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_partGe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_numStepRelax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_multStepRelax)).EndInit();
            this.grpBox_create.ResumeLayout(false);
            this.grpBox_create.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_shiftAtoms)).EndInit();
            this.grpBox_energy.ResumeLayout(false);
            this.grpBox_energy.PerformLayout();
            this.grpBox_relax.ResumeLayout(false);
            this.grpBox_relax.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_step)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_periodSwap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_stepNorm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_tmpr)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.grpBox_graph.ResumeLayout(false);
            this.grpBox_graph.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pict_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict_kinEnergy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pict_potEnergy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_calculateEnergy;
        private System.Windows.Forms.TextBox txt_sumEnergy;
        private System.Windows.Forms.NumericUpDown numUD_sizeStructure;
        private System.Windows.Forms.RadioButton rdBtn_TRS;
        private System.Windows.Forms.RadioButton rdBtn_STW;
        private System.Windows.Forms.NumericUpDown numUD_partGe;
        private System.Windows.Forms.Button btn_relaxation;
        private System.Windows.Forms.PictureBox pict_potEnergy;
        private System.Windows.Forms.PictureBox pict_kinEnergy;
        private System.Windows.Forms.NumericUpDown numUD_numStepRelax;
        private System.Windows.Forms.NumericUpDown numUD_multStepRelax;
        private System.Windows.Forms.GroupBox grpBox_create;
        private System.Windows.Forms.Button btn_createStructure;
        private System.Windows.Forms.NumericUpDown numUD_shiftAtoms;
        private System.Windows.Forms.GroupBox grpBox_energy;
        private System.Windows.Forms.GroupBox grpBox_relax;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formulasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escapeToolStripMenuItem;
        private System.Windows.Forms.TextBox txt_latPar;
        private System.Windows.Forms.TextBox txt_numAtoms;
        private System.Windows.Forms.TextBox txt_atomEnergy;
        private System.Windows.Forms.GroupBox grpBox_graph;
        private System.Windows.Forms.Button btn_openGraph;
        private System.Windows.Forms.NumericUpDown numUD_tmpr;
        private System.Windows.Forms.TextBox txt_tmprEnergy;
        private System.Windows.Forms.PictureBox pict_3;
        private System.Windows.Forms.ListBox listBox_step;
        private System.Windows.Forms.CheckBox checkBox_startVelocity;
        private System.Windows.Forms.Button btn_clearStep;
        private System.Windows.Forms.Button btn_deleteStep;
        private System.Windows.Forms.Button btn_addStep;
        private System.Windows.Forms.NumericUpDown numUD_step;
        private System.Windows.Forms.NumericUpDown numUD_stepNorm;
        private System.Windows.Forms.CheckBox checkBox_justRelax;
        private System.Windows.Forms.NumericUpDown numUD_swap;
        private System.Windows.Forms.CheckBox checkBox_timeRelax;
        private System.Windows.Forms.PictureBox pict_swap;
        private System.Windows.Forms.NumericUpDown numUD_periodSwap;
        private System.Windows.Forms.NumericUpDown numUD_ident;
        private System.Windows.Forms.GroupBox grpBox_swap;
        private System.Windows.Forms.Label label_status;
        private System.Windows.Forms.Button btn_openNeigh;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label label_swap;
        private System.Windows.Forms.Label label_testNum;
        private System.Windows.Forms.Timer timer_screen;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown numUD_graphsSwap;
        private System.Windows.Forms.TextBox txt_graphsSwap;
        private System.Windows.Forms.TextBox txt_time;
        private System.Windows.Forms.Label txt_timetau;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox check_corrFunc;
    }
}

