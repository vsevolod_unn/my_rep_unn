﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Segregation
{
    /// <summary>
    /// Тип атома.
    /// </summary>
    public enum AtomType
    {
        Si,
        Ge
    }

    /// <summary>
    /// Параметры атомов (масса, постоянная решётки).
    /// </summary>
    public static class AtomParams
    {
        /// <summary>
        /// Масса атома кремния (кг).
        /// </summary>
        public const double MASS_SI = 28.085d * 1.660539e-27;

        /// <summary>
        /// Масса атома германия (кг).
        /// </summary>
        public const double MASS_GE = 72.630d * 1.660539e-27;

        /// <summary>
        /// Параметр решётки атома кремния (нм).
        /// </summary>
        public const double LATPAR_SI = 0.54307d;

        /// <summary>
        /// Параметр решётки атома германия (нм).
        /// </summary>
        public const double LATPAR_GE = 0.5660d;

        /// <summary>
        /// Постоянная Больцмана (эВ/К).
        /// </summary>
        public const double BOLTZMANN = 8.6142e-5;

        /// <summary>
        /// Период оптических колебаний атомов в решётке сплава (сек).
        /// </summary>
        public const double PERIOD_OSCILL = 1.018e-13;
    }

    /// <summary>
    /// Атом.
    /// </summary>
    public class Atom : ICloneable
    {
        /// <summary>
        /// Координата атома (нм);
        /// для передачи значения с использованием периодических граничных условий вызвать <see cref="Atom.SetCoordinate(Vector, double)"/>.
        /// </summary>
        public Vector Coordinate
        {
            set { coordinate = value; }
            get { return coordinate; }
        }
        private Vector coordinate;

        /// <summary>
        /// Скорость атома (нм/с).
        /// </summary>
        public Vector Velocity
        {
            set
            {
                velocity = value;
            }
            get { return velocity; }
        }
        private Vector velocity;

        /// <summary>
        /// Масса атома (кг), изменяется вместе с типом атома <see cref="Atom.Type"/>.
        /// </summary>
        public double Mass
        {
            get { return mass; }
            private set { mass = value; }
        }
        private double mass;

        /// <summary>
        /// Тип атома.
        /// </summary>
        public AtomType Type
        {
            set
            {
                atomType = value;
                if (atomType == AtomType.Si)
                {
                    this.Mass = AtomParams.MASS_SI;
                }
                else if (atomType == AtomType.Ge)
                {
                    this.Mass = AtomParams.MASS_GE;
                }
            }
            get { return atomType; }
        }
        private AtomType atomType;

        /// <summary>
        /// Индекс атома.
        /// </summary>
        public int Index
        {
            get { return index; }
            private set { index = value; }
        }
        private int index;

        /// <summary>
        /// Первые и вторые соседи атома (<see cref="Atom.FirstNeighbours"/> и <see cref="Atom.SecondNeighbours"/>).
        /// </summary>
        public Atom[] Neighbours
        {
            private set { neighbours = value; }
            get { return neighbours; }
        }
        private Atom[] neighbours;

        /// <summary>
        /// Потенциальная энергия атома (эВ), вычисляется в методе PotentialEnergy(impact = false).
        /// </summary>
        public double PotEnergy;

        /// <summary>
        /// Кинетическая энергия атома (эВ), вычисляется в методе KineticEnergy.
        /// </summary>
        public double KinEnergy;

        /// <summary>
        /// Первые соседи атома.
        /// </summary>
        public readonly List<Atom> FirstNeighbours = new List<Atom>();

        /// <summary>
        /// Вторые соседи атома.
        /// </summary>
        public readonly List<Atom> SecondNeighbours = new List<Atom>();

        /// <summary>
        /// Cоздание атома.
        /// </summary>
        /// <param name="coord">Координата атома.</param>
        /// <param name="type">Тип атома.</param>
        /// <param name="idx">Индекс атома в системе.</param>
        public Atom(Vector coord, AtomType type, int idx)
        {
            coordinate = coord;
            velocity = Vector.Zero;
            this.Type = type;
            this.Index = idx;
        }

        /// <summary>
        /// Клонировать объект с сохранением ссылочных переменных.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Кинетическая энергия атома (эВ).
        /// </summary>
        /// <returns>Кинетическая энергия.</returns>
        public double KineticEnergy()
        {
            double sum = 0.0d;

            sum += Math.Pow(velocity.x, 2);
            sum += Math.Pow(velocity.y, 2);
            sum += Math.Pow(velocity.z, 2);

            // Энергия в (эВ).
            double energy = sum * this.Mass * 6.24151d / 2.0d;
            this.KinEnergy = energy;

            return energy;
        }

        /// <summary>
        /// Подсчёт потенцальной энергии выбранного атома.
        /// </summary>
        /// <param name="pot">Тип потенциала.</param>
        /// <param name="L">Размер кубической расчётной ячейки.</param>
        /// <param name="impact">Подсчёт энергии взаимодействия (для расчёта силы).</param>
        /// <returns></returns>
        public double PotentialEnergy(Potential pot, double L, bool impact)
        {
            double energy = pot.PotentialEnergy(this, L, impact);
            if (!impact) this.PotEnergy = energy;

            return energy;
        }

        /// <summary>
        /// Подсчёт потенциальной энергии выбранного атома и его соседей.
        /// </summary>
        /// <param name="pot">Тип потенциала.</param>
        /// <param name="L">Размер кубической расчётной ячейки.</param>
        public void PotentialEnergy(Potential pot, double L)
        {
            this.PotentialEnergy(pot, L, false);

            for (int i = 0; i < this.Neighbours.Length; i++)
            {
                this.Neighbours[i].PotentialEnergy(pot, L, false);
            }
        }

        /// <summary>
        /// Поиск первых соседей выбранного атома.
        /// </summary>
        /// <param name="sys">Система атомов в которой находится выбранный атом.</param>
        /// <param name="L">Размер кубической расчётной ячейки.</param>
        /// <param name="searchRadius">Радиус поиска соседей.</param>
        public void SearchFirstNeighbours(Atom[] sys, double L, double searchRadius)
        {
            this.FirstNeighbours.Clear();

            for (int a = 0; a < sys.Length; a++)
            {
                if (sys[a].Index == this.Index) continue;

                double radius = Vector.MagnitudePeriod(this.Coordinate, sys[a].Coordinate, L);

                if (radius <= searchRadius)
                {
                    this.FirstNeighbours.Add(sys[a]);
                }
            }
        }

        /// <summary>
        /// Присваивание нового значения координаты атома с использованием периодических граничных условий.
        /// </summary>
        /// <param name="coord">Новая координата.</param>
        /// <param name="L">Размер кубической расчётной ячейки.</param>
        public void SetCoordinate(Vector coord, double L)
        {
            if (coord.x > L) this.coordinate.x = coord.x - L;
            else if (coord.x < 0.0d) this.coordinate.x = L + coord.x;
            else this.coordinate.x = coord.x;

            if (coord.y > L) this.coordinate.y = coord.y - L;
            else if (coord.y < 0.0d) this.coordinate.y = L + coord.y;
            else this.coordinate.y = coord.y;

            if (coord.z > L) this.coordinate.z = coord.z - L;
            else if (coord.z < 0.0d) this.coordinate.z = L + coord.z;
            else this.coordinate.z = coord.z;
        }

        /// <summary>
        /// Поиск вторых соседей выбранного атома.
        /// </summary>
        public void SearchSecondNeighbours()
        {
            this.SecondNeighbours.Clear();

            for (int a = 0; a < this.FirstNeighbours.Count; a++)
            {
                Atom firstNeigh = this.FirstNeighbours[a];

                for (int n = 0; n < firstNeigh.FirstNeighbours.Count; n++)
                {
                    if (firstNeigh.FirstNeighbours[n].Index == this.Index) continue;

                    if (!this.SecondNeighbours.Contains(firstNeigh.FirstNeighbours[n]))
                    {
                        this.SecondNeighbours.Add(firstNeigh.FirstNeighbours[n]);
                    }
                }
            }
        }

        /// <summary>
        /// Вычисление первых и вторых соседей атома.
        /// </summary>
        public void SumNeighbours()
        {
            List<Atom> neigh = new List<Atom>(this.FirstNeighbours);
            neigh.AddRange(this.SecondNeighbours);
            this.Neighbours = neigh.ToArray();
        }
    }
}