﻿namespace Segregation
{
    partial class FormLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLoad));
            this.picBox_anim = new System.Windows.Forms.PictureBox();
            this.timer_load = new System.Windows.Forms.Timer(this.components);
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_anim)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            label1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label1.Location = new System.Drawing.Point(86, 12);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(204, 68);
            label1.TabIndex = 0;
            label1.Text = "Пожалуйста, подождите...";
            label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picBox_anim
            // 
            this.picBox_anim.Image = global::Segregation.Properties.Resources.latter1;
            this.picBox_anim.Location = new System.Drawing.Point(12, 12);
            this.picBox_anim.Name = "picBox_anim";
            this.picBox_anim.Size = new System.Drawing.Size(68, 68);
            this.picBox_anim.TabIndex = 48;
            this.picBox_anim.TabStop = false;
            // 
            // timer_load
            // 
            this.timer_load.Interval = 600;
            this.timer_load.Tick += new System.EventHandler(this.timer_load_Tick);
            // 
            // FormLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 91);
            this.ControlBox = false;
            this.Controls.Add(this.picBox_anim);
            this.Controls.Add(label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormLoad";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Загрузка";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormLoad_FormClosing);
            this.Load += new System.EventHandler(this.FormLoad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBox_anim)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer_load;
        private System.Windows.Forms.PictureBox picBox_anim;
    }
}