﻿namespace Silicium
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label1;
            this.PicMain = new System.Windows.Forms.PictureBox();
            this.SizeTB = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.CreateBT = new System.Windows.Forms.Button();
            this.VacancionBT = new System.Windows.Forms.Button();
            this.TimeCountTB = new System.Windows.Forms.NumericUpDown();
            this.numUD_multStepRelax = new System.Windows.Forms.NumericUpDown();
            this.StartSegregationBT = new System.Windows.Forms.Button();
            this.AngleTR = new System.Windows.Forms.TrackBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.CountTB = new System.Windows.Forms.TextBox();
            this.ShowTR = new System.Windows.Forms.TrackBar();
            this.BordersCB = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ModelingGroup = new System.Windows.Forms.GroupBox();
            this.ShiftNumeric = new System.Windows.Forms.NumericUpDown();
            this.ShiftBT = new System.Windows.Forms.Button();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.KinFullEnergyTB = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.KinAtomEnergyTB = new System.Windows.Forms.TextBox();
            this.FullEnergyTB = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.AtomEnergyTB = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TimeMomentTrack = new System.Windows.Forms.TrackBar();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.PotEnergyGraphPic = new System.Windows.Forms.PictureBox();
            this.KinEnergyGraphPic = new System.Windows.Forms.PictureBox();
            this.FullEnergyGraphPic = new System.Windows.Forms.PictureBox();
            this.VacancyEnergyTB = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeCountTB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_multStepRelax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AngleTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowTR)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.ModelingGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftNumeric)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeMomentTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PotEnergyGraphPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KinEnergyGraphPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullEnergyGraphPic)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(32, 93);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(96, 17);
            label6.TabIndex = 53;
            label6.Text = "Число шагов:";
            label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Cursor = System.Windows.Forms.Cursors.Help;
            label7.Location = new System.Drawing.Point(6, 124);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(122, 17);
            label7.TabIndex = 55;
            label7.Text = "Множитель шага:";
            label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(192, 93);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(76, 17);
            label1.TabIndex = 74;
            label1.Text = "Сдвиг в %:";
            label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PicMain
            // 
            this.PicMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PicMain.Location = new System.Drawing.Point(8, 8);
            this.PicMain.Name = "PicMain";
            this.PicMain.Size = new System.Drawing.Size(792, 700);
            this.PicMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicMain.TabIndex = 0;
            this.PicMain.TabStop = false;
            // 
            // SizeTB
            // 
            this.SizeTB.Location = new System.Drawing.Point(141, 28);
            this.SizeTB.Name = "SizeTB";
            this.SizeTB.Size = new System.Drawing.Size(49, 22);
            this.SizeTB.TabIndex = 1;
            this.SizeTB.Text = "3";
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(5, 31);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(130, 15);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "Размер структуры:";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CreateBT
            // 
            this.CreateBT.Location = new System.Drawing.Point(196, 27);
            this.CreateBT.Name = "CreateBT";
            this.CreateBT.Size = new System.Drawing.Size(135, 25);
            this.CreateBT.TabIndex = 3;
            this.CreateBT.Text = "Создать";
            this.CreateBT.UseVisualStyleBackColor = true;
            this.CreateBT.Click += new System.EventHandler(this.CreateBT_Click);
            // 
            // VacancionBT
            // 
            this.VacancionBT.Enabled = false;
            this.VacancionBT.Location = new System.Drawing.Point(196, 59);
            this.VacancionBT.Name = "VacancionBT";
            this.VacancionBT.Size = new System.Drawing.Size(136, 25);
            this.VacancionBT.TabIndex = 4;
            this.VacancionBT.Text = "Вакансия";
            this.VacancionBT.UseVisualStyleBackColor = true;
            this.VacancionBT.Click += new System.EventHandler(this.VacancionBT_Click);
            // 
            // TimeCountTB
            // 
            this.TimeCountTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TimeCountTB.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.TimeCountTB.Location = new System.Drawing.Point(127, 91);
            this.TimeCountTB.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.TimeCountTB.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.TimeCountTB.Name = "TimeCountTB";
            this.TimeCountTB.Size = new System.Drawing.Size(59, 22);
            this.TimeCountTB.TabIndex = 47;
            this.TimeCountTB.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // numUD_multStepRelax
            // 
            this.numUD_multStepRelax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_multStepRelax.DecimalPlaces = 2;
            this.numUD_multStepRelax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_multStepRelax.Location = new System.Drawing.Point(127, 122);
            this.numUD_multStepRelax.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_multStepRelax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_multStepRelax.Name = "numUD_multStepRelax";
            this.numUD_multStepRelax.Size = new System.Drawing.Size(59, 22);
            this.numUD_multStepRelax.TabIndex = 48;
            this.numUD_multStepRelax.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // StartSegregationBT
            // 
            this.StartSegregationBT.Location = new System.Drawing.Point(195, 151);
            this.StartSegregationBT.Name = "StartSegregationBT";
            this.StartSegregationBT.Size = new System.Drawing.Size(136, 25);
            this.StartSegregationBT.TabIndex = 59;
            this.StartSegregationBT.Text = "Старт";
            this.StartSegregationBT.UseVisualStyleBackColor = true;
            this.StartSegregationBT.Click += new System.EventHandler(this.StartSegregationBT_Click);
            // 
            // AngleTR
            // 
            this.AngleTR.Location = new System.Drawing.Point(5, 51);
            this.AngleTR.Maximum = 0;
            this.AngleTR.Minimum = -180;
            this.AngleTR.Name = "AngleTR";
            this.AngleTR.Size = new System.Drawing.Size(326, 56);
            this.AngleTR.TabIndex = 60;
            this.AngleTR.Scroll += new System.EventHandler(this.AngleTR_Scroll);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(6, 63);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(129, 15);
            this.textBox1.TabIndex = 62;
            this.textBox1.Text = "Число атомов:";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CountTB
            // 
            this.CountTB.Enabled = false;
            this.CountTB.Location = new System.Drawing.Point(141, 60);
            this.CountTB.Name = "CountTB";
            this.CountTB.Size = new System.Drawing.Size(49, 22);
            this.CountTB.TabIndex = 61;
            this.CountTB.Text = "0";
            // 
            // ShowTR
            // 
            this.ShowTR.Enabled = false;
            this.ShowTR.Location = new System.Drawing.Point(9, 122);
            this.ShowTR.Maximum = 50;
            this.ShowTR.Name = "ShowTR";
            this.ShowTR.Size = new System.Drawing.Size(319, 56);
            this.ShowTR.TabIndex = 63;
            this.ShowTR.Value = 1;
            this.ShowTR.Scroll += new System.EventHandler(this.ShowTR_Scroll);
            // 
            // BordersCB
            // 
            this.BordersCB.AutoSize = true;
            this.BordersCB.Checked = true;
            this.BordersCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.BordersCB.Location = new System.Drawing.Point(241, 91);
            this.BordersCB.Name = "BordersCB";
            this.BordersCB.Size = new System.Drawing.Size(88, 21);
            this.BordersCB.TabIndex = 64;
            this.BordersCB.Text = "Границы";
            this.BordersCB.UseVisualStyleBackColor = true;
            this.BordersCB.CheckedChanged += new System.EventHandler(this.BordersCB_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.VacancionBT);
            this.groupBox1.Controls.Add(this.BordersCB);
            this.groupBox1.Controls.Add(this.SizeTB);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.CountTB);
            this.groupBox1.Controls.Add(this.CreateBT);
            this.groupBox1.Location = new System.Drawing.Point(806, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 125);
            this.groupBox1.TabIndex = 65;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Генерация структуры";
            // 
            // ModelingGroup
            // 
            this.ModelingGroup.Controls.Add(this.ShiftNumeric);
            this.ModelingGroup.Controls.Add(this.ShiftBT);
            this.ModelingGroup.Controls.Add(label1);
            this.ModelingGroup.Controls.Add(this.textBox9);
            this.ModelingGroup.Controls.Add(this.KinFullEnergyTB);
            this.ModelingGroup.Controls.Add(this.textBox10);
            this.ModelingGroup.Controls.Add(this.KinAtomEnergyTB);
            this.ModelingGroup.Controls.Add(this.TimeCountTB);
            this.ModelingGroup.Controls.Add(this.StartSegregationBT);
            this.ModelingGroup.Controls.Add(this.FullEnergyTB);
            this.ModelingGroup.Controls.Add(label6);
            this.ModelingGroup.Controls.Add(this.textBox6);
            this.ModelingGroup.Controls.Add(this.textBox4);
            this.ModelingGroup.Controls.Add(this.numUD_multStepRelax);
            this.ModelingGroup.Controls.Add(label7);
            this.ModelingGroup.Controls.Add(this.AtomEnergyTB);
            this.ModelingGroup.Enabled = false;
            this.ModelingGroup.Location = new System.Drawing.Point(1154, 8);
            this.ModelingGroup.Name = "ModelingGroup";
            this.ModelingGroup.Size = new System.Drawing.Size(337, 184);
            this.ModelingGroup.TabIndex = 66;
            this.ModelingGroup.TabStop = false;
            this.ModelingGroup.Text = "Моделирование ";
            // 
            // ShiftNumeric
            // 
            this.ShiftNumeric.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ShiftNumeric.DecimalPlaces = 1;
            this.ShiftNumeric.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ShiftNumeric.Location = new System.Drawing.Point(269, 93);
            this.ShiftNumeric.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.ShiftNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ShiftNumeric.Name = "ShiftNumeric";
            this.ShiftNumeric.Size = new System.Drawing.Size(59, 22);
            this.ShiftNumeric.TabIndex = 76;
            this.ShiftNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ShiftBT
            // 
            this.ShiftBT.Location = new System.Drawing.Point(195, 120);
            this.ShiftBT.Name = "ShiftBT";
            this.ShiftBT.Size = new System.Drawing.Size(136, 25);
            this.ShiftBT.TabIndex = 75;
            this.ShiftBT.Text = "Добавить сдвиг";
            this.ShiftBT.UseVisualStyleBackColor = true;
            this.ShiftBT.Click += new System.EventHandler(this.ShiftBT_Click);
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Location = new System.Drawing.Point(214, 60);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(31, 15);
            this.textBox9.TabIndex = 72;
            this.textBox9.Text = "Кин.";
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // KinFullEnergyTB
            // 
            this.KinFullEnergyTB.Enabled = false;
            this.KinFullEnergyTB.Location = new System.Drawing.Point(247, 57);
            this.KinFullEnergyTB.Name = "KinFullEnergyTB";
            this.KinFullEnergyTB.Size = new System.Drawing.Size(81, 22);
            this.KinFullEnergyTB.TabIndex = 70;
            this.KinFullEnergyTB.Text = "3";
            // 
            // textBox10
            // 
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Location = new System.Drawing.Point(214, 28);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(31, 15);
            this.textBox10.TabIndex = 71;
            this.textBox10.Text = "Кин.";
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // KinAtomEnergyTB
            // 
            this.KinAtomEnergyTB.Enabled = false;
            this.KinAtomEnergyTB.Location = new System.Drawing.Point(247, 25);
            this.KinAtomEnergyTB.Name = "KinAtomEnergyTB";
            this.KinAtomEnergyTB.Size = new System.Drawing.Size(81, 22);
            this.KinAtomEnergyTB.TabIndex = 69;
            this.KinAtomEnergyTB.Text = "3";
            // 
            // FullEnergyTB
            // 
            this.FullEnergyTB.Enabled = false;
            this.FullEnergyTB.Location = new System.Drawing.Point(127, 57);
            this.FullEnergyTB.Name = "FullEnergyTB";
            this.FullEnergyTB.Size = new System.Drawing.Size(81, 22);
            this.FullEnergyTB.TabIndex = 67;
            this.FullEnergyTB.Text = "3";
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(3, 60);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(119, 15);
            this.textBox6.TabIndex = 68;
            this.textBox6.Text = "Полная энергия:";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(3, 28);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(119, 15);
            this.textBox4.TabIndex = 66;
            this.textBox4.Text = "Энергия на атом:";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AtomEnergyTB
            // 
            this.AtomEnergyTB.Enabled = false;
            this.AtomEnergyTB.Location = new System.Drawing.Point(127, 25);
            this.AtomEnergyTB.Name = "AtomEnergyTB";
            this.AtomEnergyTB.Size = new System.Drawing.Size(81, 22);
            this.AtomEnergyTB.TabIndex = 65;
            this.AtomEnergyTB.Text = "3";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.TimeMomentTrack);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.ShowTR);
            this.groupBox3.Controls.Add(this.textBox8);
            this.groupBox3.Controls.Add(this.textBox7);
            this.groupBox3.Controls.Add(this.AngleTR);
            this.groupBox3.Location = new System.Drawing.Point(806, 136);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(337, 266);
            this.groupBox3.TabIndex = 67;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Отрисовка";
            // 
            // TimeMomentTrack
            // 
            this.TimeMomentTrack.Enabled = false;
            this.TimeMomentTrack.Location = new System.Drawing.Point(9, 193);
            this.TimeMomentTrack.Maximum = 499;
            this.TimeMomentTrack.Name = "TimeMomentTrack";
            this.TimeMomentTrack.Size = new System.Drawing.Size(319, 56);
            this.TimeMomentTrack.TabIndex = 71;
            this.TimeMomentTrack.Scroll += new System.EventHandler(this.TimeMomentTrack_Scroll);
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(9, 167);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(226, 15);
            this.textBox3.TabIndex = 72;
            this.textBox3.Text = "Момент времени t:";
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(9, 97);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(226, 15);
            this.textBox8.TabIndex = 70;
            this.textBox8.Text = "Отображение соседей:";
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(9, 27);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(119, 15);
            this.textBox7.TabIndex = 69;
            this.textBox7.Text = "Угол обзора:";
            // 
            // PotEnergyGraphPic
            // 
            this.PotEnergyGraphPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PotEnergyGraphPic.Location = new System.Drawing.Point(806, 453);
            this.PotEnergyGraphPic.Name = "PotEnergyGraphPic";
            this.PotEnergyGraphPic.Size = new System.Drawing.Size(337, 255);
            this.PotEnergyGraphPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PotEnergyGraphPic.TabIndex = 68;
            this.PotEnergyGraphPic.TabStop = false;
            // 
            // KinEnergyGraphPic
            // 
            this.KinEnergyGraphPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.KinEnergyGraphPic.Location = new System.Drawing.Point(1154, 453);
            this.KinEnergyGraphPic.Name = "KinEnergyGraphPic";
            this.KinEnergyGraphPic.Size = new System.Drawing.Size(337, 255);
            this.KinEnergyGraphPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.KinEnergyGraphPic.TabIndex = 69;
            this.KinEnergyGraphPic.TabStop = false;
            // 
            // FullEnergyGraphPic
            // 
            this.FullEnergyGraphPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FullEnergyGraphPic.Location = new System.Drawing.Point(1154, 192);
            this.FullEnergyGraphPic.Name = "FullEnergyGraphPic";
            this.FullEnergyGraphPic.Size = new System.Drawing.Size(337, 255);
            this.FullEnergyGraphPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FullEnergyGraphPic.TabIndex = 70;
            this.FullEnergyGraphPic.TabStop = false;
            // 
            // VacancyEnergyTB
            // 
            this.VacancyEnergyTB.Enabled = false;
            this.VacancyEnergyTB.Location = new System.Drawing.Point(1062, 408);
            this.VacancyEnergyTB.Name = "VacancyEnergyTB";
            this.VacancyEnergyTB.Size = new System.Drawing.Size(81, 22);
            this.VacancyEnergyTB.TabIndex = 77;
            this.VacancyEnergyTB.Text = "0";
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Location = new System.Drawing.Point(806, 411);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(251, 15);
            this.textBox11.TabIndex = 78;
            this.textBox11.Text = "Энергия возникновения вакансии";
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1503, 716);
            this.Controls.Add(this.VacancyEnergyTB);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.FullEnergyGraphPic);
            this.Controls.Add(this.KinEnergyGraphPic);
            this.Controls.Add(this.PotEnergyGraphPic);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.ModelingGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.PicMain);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PicMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeCountTB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_multStepRelax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AngleTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShowTR)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ModelingGroup.ResumeLayout(false);
            this.ModelingGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShiftNumeric)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeMomentTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PotEnergyGraphPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KinEnergyGraphPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullEnergyGraphPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PicMain;
        private System.Windows.Forms.TextBox SizeTB;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button CreateBT;
        private System.Windows.Forms.Button VacancionBT;
        private System.Windows.Forms.NumericUpDown TimeCountTB;
        private System.Windows.Forms.NumericUpDown numUD_multStepRelax;
        private System.Windows.Forms.Button StartSegregationBT;
        private System.Windows.Forms.TrackBar AngleTR;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox CountTB;
        private System.Windows.Forms.TrackBar ShowTR;
        private System.Windows.Forms.CheckBox BordersCB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox ModelingGroup;
        private System.Windows.Forms.TextBox FullEnergyTB;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox AtomEnergyTB;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox KinFullEnergyTB;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox KinAtomEnergyTB;
        private System.Windows.Forms.Button ShiftBT;
        private System.Windows.Forms.NumericUpDown ShiftNumeric;
        private System.Windows.Forms.TrackBar TimeMomentTrack;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.PictureBox PotEnergyGraphPic;
        private System.Windows.Forms.PictureBox KinEnergyGraphPic;
        private System.Windows.Forms.PictureBox FullEnergyGraphPic;
        private System.Windows.Forms.TextBox VacancyEnergyTB;
        private System.Windows.Forms.TextBox textBox11;
    }
}

