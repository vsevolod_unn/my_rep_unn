﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;


namespace Silicium
{
    class MyDraw
    {
        public double set_alfa
        {
            set { alfa = value; }
            get { return alfa; }
        }

        public MyDraw(
            PictureBox pc,
            double MxX, double MxY, double MxZ,
            double MnX, double MnY, double MnZ,
            int SX, int SY,
            int OSX, int OSY,
            int radius, Color cl, double A)
        {
            this.pic = pc;
            this.bmp = new Bitmap(SX, SY);
            this.g = Graphics.FromImage(bmp);
            this.g.Clear(Color.White);
            this.g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            this.pic.Image = bmp;

            maxX = MxX;
            maxY = MxY;
            maxZ = MxZ;

            minX = MnX;
            minY = MnY;
            minZ = MnZ;

            sizeX = SX;
            sizeY = SY;
            offsetX = OSX;
            offsetY = OSY;
            r = radius;

            alfa = A;

            vidx = 0;
            multiplier = 1.0;

            transformX = (int)(offsetX * 0.66 + (sizeX - 2 * offsetX));
            transformY = (int)(offsetY * 1.25 + (sizeY - 2 * offsetY));
            //transformZ = 

            pen = new Pen(new SolidBrush(Color.Black), 1);
        }

        public void set_angle(double A)
        {
            this.alfa = A;
        }

        public int get_x(double x, double z)
        {
            int x1 = (int)(multiplier * ((offsetX * 0.66 + (sizeX - 2 * offsetX) * (double)(x - minX) / (maxX - minX)) + (int)(koef_y * (sizeX - 2 * offsetX) * Math.Cos(alfa) * z)));
            return x1;
        }

        public int get_y(double y, double z)
        {
            int y1 = (int)(multiplier * (sizeY - ((int)(offsetY * 0.5 + (sizeY - 2 * offsetY) * (double)(y - minY) / (maxY - minY)) + (int)(koef_y * (sizeY - 2 * offsetY) * Math.Sin(beta) * z))));
            return y1;
        }

        public void resize_area(Segregation s)
        {
            double MX = maxX = -Double.MaxValue;
            double MY = maxY = -Double.MaxValue;
            double MZ = maxZ = -Double.MaxValue;

            double MnX = minX = Double.MaxValue;
            double MnY = minY = Double.MaxValue;
            double MnZ = minZ = Double.MaxValue;

            for (int i = 0; i < s.StructLength; i++)
            {
                if (s.Structure[i].Coordinate.x < MnX) MnX = s.Structure[i].Coordinate.x;
                if (s.Structure[i].Coordinate.x > MX) MX = s.Structure[i].Coordinate.x;

                if (s.Structure[i].Coordinate.y < MnY) MnY = s.Structure[i].Coordinate.y;
                if (s.Structure[i].Coordinate.y > MY) MY = s.Structure[i].Coordinate.y;

                if (s.Structure[i].Coordinate.z < MnZ) MnZ = s.Structure[i].Coordinate.z;
                if (s.Structure[i].Coordinate.z > MZ) MZ = s.Structure[i].Coordinate.z;
            }

            maxX = MX;
            maxY = MY;
            maxZ = MZ;

            minX = MnX;
            minY = MnY;
            minZ = MnZ;
        }

        public void draw_collection(int count, Segregation s, bool show_borders)
        {
            double MX = maxX = -Double.MaxValue;
            double MY = maxY = -Double.MaxValue;
            double MZ = maxZ = -Double.MaxValue;

            double MnX = minX = Double.MaxValue;
            double MnY = minY = Double.MaxValue;
            double MnZ = minZ = Double.MaxValue;

            for (int i = 0; i < count; i++)
            {
                if (s.Structure[i].Coordinate.x < MnX) MnX = s.Structure[i].Coordinate.x;
                if (s.Structure[i].Coordinate.x > MX) MX = s.Structure[i].Coordinate.x;

                if (s.Structure[i].Coordinate.y < MnY) MnY = s.Structure[i].Coordinate.y;
                if (s.Structure[i].Coordinate.y > MY) MY = s.Structure[i].Coordinate.y;

                if (s.Structure[i].Coordinate.z < MnZ) MnZ = s.Structure[i].Coordinate.z;
                if (s.Structure[i].Coordinate.z > MZ) MZ = s.Structure[i].Coordinate.z;
            }

            maxX = MX;
            maxY = MY;
            maxZ = MZ;

            minX = MnX;
            minY = MnY;
            minZ = MnZ;

            draw_links(s, show_borders);

            for (int i = 0; i < count; i++)
            {
                double z = (double)(s.Structure[i].Coordinate.z - MnZ) / (MZ - MnZ);
                int x = get_x(s.Structure[i].Coordinate.x, z);
                int y = get_y(s.Structure[i].Coordinate.y, z);

                if (s.Structure[i].vacancy)
                {
                    SolidBrush brsh = new SolidBrush((Color.FromArgb(0, 255, 0)));
                    g.DrawEllipse(pen, x - r / 2, y - r / 2, r, r);
                }
                else
                {
                    if (!s.Structure[i].border)
                    {
                        SolidBrush brsh = new SolidBrush((Color.FromArgb((int)(64 + (1 - z) * 191), 255, 0, 0)));
                        g.FillEllipse(brsh, x - r / 2, y - r / 2, r, r);
                    }
                    else if (show_borders)
                    {
                        SolidBrush brsh = new SolidBrush((Color.FromArgb((int)(64 + (1 - z) * 191), 0, 0, 255)));
                        g.FillEllipse(brsh, x - r / 2, y - r / 2, r, r);
                    }
                }
            }
        }

        public void draw_collection(int count, Segregation s, bool show_borders, int vacancy_idx, double radius)
        {
            double MX = maxX = -Double.MaxValue;
            double MY = maxY = -Double.MaxValue;
            double MZ = maxZ = -Double.MaxValue;

            double MnX = minX = Double.MaxValue;
            double MnY = minY = Double.MaxValue;
            double MnZ = minZ = Double.MaxValue;

            for (int i = 0; i < count; i++)
            {
                if (s.Structure[i].Coordinate.x < MnX) MnX = s.Structure[i].Coordinate.x;
                if (s.Structure[i].Coordinate.x > MX) MX = s.Structure[i].Coordinate.x;

                if (s.Structure[i].Coordinate.y < MnY) MnY = s.Structure[i].Coordinate.y;
                if (s.Structure[i].Coordinate.y > MY) MY = s.Structure[i].Coordinate.y;

                if (s.Structure[i].Coordinate.z < MnZ) MnZ = s.Structure[i].Coordinate.z;
                if (s.Structure[i].Coordinate.z > MZ) MZ = s.Structure[i].Coordinate.z;
            }

            maxX = MX;
            maxY = MY;
            maxZ = MZ;

            minX = MnX;
            minY = MnY;
            minZ = MnZ;

            vidx = vacancy_idx;

            draw_links(s, vacancy_idx, radius, show_borders);

            double vz = (double)(s.Structure[vacancy_idx].Coordinate.z - minZ) / (maxZ - minZ);
            int vx = get_x(s.Structure[vacancy_idx].Coordinate.x, vz);
            int vy = get_y(s.Structure[vacancy_idx].Coordinate.y, vz);
            double z;
            int x, y;

            SolidBrush brsh = new SolidBrush((Color.FromArgb(0, 0, 0)));
            g.DrawEllipse(pen, vx - r / 2, vy - r / 2, r, r);

            for (int i = 0; i < count; i++)
            {
                if (i != vacancy_idx)
                {
                    if (Math.Sqrt(
                        Math.Pow(s.Structure[vacancy_idx].Coordinate.x - s.Structure[i].Coordinate.x, 2) +
                        Math.Pow(s.Structure[vacancy_idx].Coordinate.y - s.Structure[i].Coordinate.y, 2) +
                        Math.Pow(s.Structure[vacancy_idx].Coordinate.z - s.Structure[i].Coordinate.z, 2)) < radius)
                    {
                        z = (double)(s.Structure[i].Coordinate.z - minZ) / (maxZ - minZ);
                        x = get_x(s.Structure[i].Coordinate.x, z);
                        y = get_y(s.Structure[i].Coordinate.y, z);

                        if (!s.Structure[i].border)
                        {
                            brsh = new SolidBrush((Color.FromArgb((int)(64 + (1 - z) * 191), 255, 0, 0)));
                            g.FillEllipse(brsh, x - r / 2, y - r / 2, r, r);
                        }
                        else if (show_borders && !s.Structure[i].vacancy)
                        {
                            brsh = new SolidBrush((Color.FromArgb((int)(64 + (1 - z) * 191), 0, 0, 255)));
                            g.FillEllipse(brsh, x - r / 2, y - r / 2, r, r);
                        }
                    }
                }
            }
        }

        public void draw_line(Vector v1, Vector v2)
        {
            double MX = maxX;
            double MY = maxY;
            double MZ = maxZ;

            double MnX = minX;
            double MnY = minY;
            double MnZ = minZ;

            Vector coord1 = v1;
            Vector coord2 = v2;

            double z1 = (double)(coord1.z - MnZ) / (MZ - MnZ);
            int x1 = get_x(coord1.x, z1);
            int y1 = get_y(coord1.y, z1);

            double z2 = (double)(coord2.z - MnZ) / (MZ - MnZ);
            int x2 = get_x(coord2.x, z2);
            int y2 = get_y(coord2.y, z2);

            if (coord1.x >= (MnX) && coord1.x <= (MX - MnX) &&
                coord1.y >= (MnY) && coord1.y <= (MY - MnY) &&
                coord1.z >= (MnZ) && coord1.z <= (MZ - MnZ) &&
                coord2.x >= (MnX) && coord2.x <= (MX - MnX) &&
                coord2.y >= (MnY) && coord2.y <= (MY - MnY) &&
                coord2.z >= (MnZ) && coord2.z <= (MZ - MnZ) || true)
                g.DrawLine(pen, x1, y1, x2, y2);
        }

        public void draw_links(Segregation s, bool show_borders)
        {
            for (int i = 0; i < s.Structure.Length; i++)
            {
                for (int j = 0; j < s.Structure[i].FirstNeighbours.Count; j++)
                {
                    if (!s.Structure[i].border && !s.Structure[i].FirstNeighbours[j].border || show_borders)
                        draw_line(s.Structure[i].Coordinate, s.Structure[i].FirstNeighbours[j].Coordinate);
                }
            }
        }

        public void draw_links(Segregation s, int vacancy_idx, double radius, bool show_borders)
        {
            for (int i = 0; i < s.Structure.Length; i++)
            {
                for (int j = 0; j < s.Structure[i].FirstNeighbours.Count; j++)
                {
                    if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate) < radius &&
                        Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].FirstNeighbours[j].Coordinate) < radius &&
                        !s.Structure[i].vacancy && !s.Structure[i].FirstNeighbours[j].vacancy &&
                        (!s.Structure[i].border && !s.Structure[i].FirstNeighbours[j].border || show_borders))
                    {
                        draw_line(s.Structure[i].Coordinate, s.Structure[i].FirstNeighbours[j].Coordinate);
                    }
                }
            }
            /// Legacy
            {
                /*
                for (int i = 0; i < s.Structure.Length; i += 8)
                {
                    if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec111) < radius &&
                        Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec111) != 0)
                    {
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec000) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec000) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(0, 0, 0), new Vector(1, 1, 1));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec022) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec022) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(0, 2, 2), new Vector(1, 1, 1));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec202) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec202) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 0, 2), new Vector(1, 1, 1));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec220) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec220) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 2, 0), new Vector(1, 1, 1));
                        }
                    }

                    if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec133) < radius &&
                        Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec133) != 0)
                    {
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec022) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec022) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(0, 2, 2), new Vector(1, 3, 3));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec224) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec224) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 2, 4), new Vector(1, 3, 3));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec242) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec242) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 4, 2), new Vector(1, 3, 3));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec044) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec044) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(0, 4, 4), new Vector(1, 3, 3));
                        }
                    }

                    if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec331) < radius &&
                        Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec331) != 0)
                    {
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec220) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec220) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 2, 0), new Vector(3, 3, 1));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec440) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec440) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(4, 4, 0), new Vector(3, 3, 1));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec422) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec422) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(4, 2, 2), new Vector(3, 3, 1));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec242) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec242) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 4, 2), new Vector(3, 3, 1));
                        }
                    }

                    if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec313) < radius &&
                        Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec313) != 0)
                    {
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec202) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec202) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 0, 2), new Vector(3, 1, 3));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec404) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec404) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(4, 0, 4), new Vector(3, 1, 3));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec224) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec224) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(2, 2, 4), new Vector(3, 1, 3));
                        }
                        if (Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec422) < radius &&
                            Vector.Magnitude(s.Structure[vacancy_idx].Coordinate, s.Structure[i].Coordinate + vec422) != 0)
                        {
                            draw_line(s.Structure[i].Coordinate, new Vector(4, 2, 2), new Vector(3, 1, 3));
                        }
                    }

                }*/
            }
        }

        public void draw_links_and_find_neighbours(Segregation s)
        {
            for (int i = 0; i < s.Structure.Length; i++)
            {
                //s.Structure[i].FirstNeighbours.Clear();
                for (int j = 0; j < s.Structure.Length; j++)
                {
                    double mag = Vector.Magnitude(s.Structure[i].Coordinate, s.Structure[j].Coordinate);
                    if (mag <= constant * 1.1)
                    {
                        draw_line(s.Structure[i].Coordinate, s.Structure[j].Coordinate);
                        //s.Structure[i].FirstNeighbours.Add(s.Structure[j]);
                    }
                }
            }
        }

        double constant = 0.1357675 * Math.Sqrt(3);
        PictureBox pic;
        Bitmap bmp;
        Graphics g;
        Pen pen;
        Brush brsh;

        int sizeX, sizeY, offsetX, offsetY, r, vidx, transformX, transformY, transformZ;

        double maxX, maxY, maxZ;
        double minX, minY, minZ;

        double koef_y = 0.25;
        double multiplier;
        double beta = Math.PI / 4;
        double alfa;

        /*
        Vector vec000 = new Vector(0, 0, 0) * 0.1357675;
        Vector vec022 = new Vector(0, 2, 2) * 0.1357675;
        Vector vec202 = new Vector(2, 0, 2) * 0.1357675;
        Vector vec220 = new Vector(2, 2, 0) * 0.1357675;
        Vector vec111 = new Vector(1, 1, 1) * 0.1357675;
        Vector vec224 = new Vector(2, 2, 4) * 0.1357675;
        Vector vec242 = new Vector(2, 4, 2) * 0.1357675;
        Vector vec044 = new Vector(0, 4, 4) * 0.1357675;
        Vector vec133 = new Vector(1, 3, 3) * 0.1357675;
        Vector vec440 = new Vector(4, 4, 0) * 0.1357675;
        Vector vec422 = new Vector(4, 2, 2) * 0.1357675;
        Vector vec331 = new Vector(3, 3, 1) * 0.1357675;
        Vector vec404 = new Vector(4, 0, 4) * 0.1357675;
        Vector vec313 = new Vector(3, 1, 3) * 0.1357675;
        */
    }
}
