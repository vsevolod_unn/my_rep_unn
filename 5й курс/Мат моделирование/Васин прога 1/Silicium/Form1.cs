﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Silicium
{
    public partial class Form1 : Form
    {
        int size_structure, vacancy_idx, TimeCount;
        double alfa = -Math.PI;
        double radius = 8, r, shift;

        double MX = -Double.MaxValue;
        double MY = -Double.MaxValue;
        double MZ = -Double.MaxValue;

        double MnX = Double.MaxValue;
        double MnY = Double.MaxValue;
        double MnZ = Double.MaxValue;

        double pot_energy = 0;
        double kin_energy = 0;

        Bitmap bmp;
        Graphics g;

        MyDraw drawer;

        Segregation sgr, sgr_buf, temp_sgr;
        Random RandCreate;

        Vector[,] InTimeAtomsPos;
        double[] InTimeKinEnergy;
        double[] InTimePotEnergy;
        bool IsVacancyClicked;

        int sizeX, sizeY, offsetX, offsetY;

        public Form1()
        {
            InitializeComponent();

            sizeX = 500;
            sizeY = 500;
            offsetX = 120;
            offsetY = 100;

            bmp = new Bitmap(sizeX, sizeY);
            g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            PicMain.Image = bmp;
            RandCreate = new Random();
            ShowTR.Enabled = false;
            AngleTR.Maximum = 0;
            AngleTR.Minimum = -180;
            AngleTR.Value = -180;
            IsVacancyClicked = false;
        }

        private void CreateBT_Click(object sender, EventArgs e)
        {
            ModelingGroup.Enabled = false;
            IsVacancyClicked = false;
            VacancionBT.Enabled = true;
            size_structure = Convert.ToInt16(SizeTB.Text);
            TimeCount = Convert.ToInt16(TimeCountTB.Text);

            sgr = new Segregation(
                (int)size_structure,    // размер структуры - n*a
                0,                      // доля Ge
                RandCreate);
            sgr.SetPotential(false);

            int AtomsCount = sgr.Structure.Length;
            CountTB.Text = AtomsCount.ToString();

            InTimeAtomsPos = new Vector[TimeCount, AtomsCount];

            MX = -Double.MaxValue;
            MY = -Double.MaxValue;
            MZ = -Double.MaxValue;

            MnX = Double.MaxValue;
            MnY = Double.MaxValue;
            MnZ = Double.MaxValue;
            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                if (sgr.Structure[i].Coordinate.x < MnX) MnX = sgr.Structure[i].Coordinate.x;
                if (sgr.Structure[i].Coordinate.x > MX) MX = sgr.Structure[i].Coordinate.x;

                if (sgr.Structure[i].Coordinate.y < MnY) MnY = sgr.Structure[i].Coordinate.y;
                if (sgr.Structure[i].Coordinate.y > MY) MY = sgr.Structure[i].Coordinate.y;

                if (sgr.Structure[i].Coordinate.z < MnZ) MnZ = sgr.Structure[i].Coordinate.z;
                if (sgr.Structure[i].Coordinate.z > MZ) MZ = sgr.Structure[i].Coordinate.z;
            }

            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                if (sgr.Structure[i].Coordinate.x == MnX || sgr.Structure[i].Coordinate.x == MX ||
                    sgr.Structure[i].Coordinate.y == MnY || sgr.Structure[i].Coordinate.y == MY ||
                    sgr.Structure[i].Coordinate.z == MnZ || sgr.Structure[i].Coordinate.z == MZ)
                    sgr.Structure[i].border = true;
            }
            sgr.FindFirstNeighbours();

            drawer = new MyDraw(PicMain, 0, 0, 0, -0, -0, -0, sizeX, sizeY, offsetX, offsetY, (int)radius, Color.Red, -Math.PI);
            drawer.draw_collection(sgr.Structure.Length, sgr, BordersCB.Checked);
            vacancy_idx = 0;
            get_energy(sgr);
        }

        private void VacancionBT_Click(object sender, EventArgs e)
        {
            IsVacancyClicked = true;
            Random rand = new Random();
            int AtomsCount = sgr.Structure.Length;
            ShowTR.Enabled = true;
            ShowTR.Value = ShowTR.Maximum;
            r = 1.5 * ShowTR.Value * (MX - MnX) / (double)ShowTR.Maximum;

            if (vacancy_idx != 0)
            {
                sgr.Structure[vacancy_idx].vacancy = sgr.Structure[vacancy_idx].border = false;
            }

            while (true)
            {
                vacancy_idx = (int)(rand.NextDouble() * (AtomsCount - 1));
                if (sgr.Structure[vacancy_idx].Coordinate.x > (MX - MnX) / 4 && sgr.Structure[vacancy_idx].Coordinate.x < (MX - MnX) * 3 / 4 &&
                    sgr.Structure[vacancy_idx].Coordinate.y > (MY - MnY) / 4 && sgr.Structure[vacancy_idx].Coordinate.y < (MY - MnY) * 3 / 4 &&
                    sgr.Structure[vacancy_idx].Coordinate.z > (MZ - MnZ) / 4 && sgr.Structure[vacancy_idx].Coordinate.z < (MZ - MnZ) * 3 / 4 &&
                    !sgr.Structure[vacancy_idx].border)
                {
                    sgr.Structure[vacancy_idx].vacancy = sgr.Structure[vacancy_idx].border = true;
                    break;
                }
            }

            sgr.FindFirstNeighbours();
            sgr_buf = new Segregation((int)size_structure, 0, RandCreate);
            for (int i = 0; i < sgr.Structure.Length; i++)
                sgr_buf.Structure[i].Coordinate = sgr.Structure[i].Coordinate;
            redraw(sgr);
            sgr.Energy(out nrg, out kin_energy, BordersCB.Checked);
            get_energy(sgr);
            ModelingGroup.Enabled = true;
        }

        private void ShiftBT_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < sgr.Structure.Length; i++)
                sgr.Structure[i].Coordinate = sgr_buf.Structure[i].Coordinate;

            // Сдвиг
            shift = 0.01 * Convert.ToDouble(ShiftNumeric.Value);
            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                Vector shift_vec = new Vector(
                    (int)(RandCreate.NextDouble() * 10),
                    (int)(RandCreate.NextDouble() * 10),
                    (int)(RandCreate.NextDouble() * 10)) / 10 / Math.Sqrt(3);
                sgr.Structure[i].Coordinate += shift_vec * 0.1357675 * (-0.5 + RandCreate.NextDouble()) * shift;
            }

            get_energy(sgr);
            redraw(sgr);
        }

        private void StartSegregationBT_Click(object sender, EventArgs e)
        {
            //Определяем параметры
            int numStep = (int)(TimeCountTB.Value);
            TimeMomentTrack.Maximum = numStep - 1;
            double temperature = (double)300;
            double shift = 0;// (double)(ShiftNumeric.Value)/100;
            bool normalize = false;
            int stepNorm = (int)10;
            bool justRelax = false;
            bool calcCorrFunc = true;
            double dt = (double)numUD_multStepRelax.Value;

            //Формируем шаги по времени
            List<int> stepDev = new List<int>(numStep);
            for (int i = 0; i < numStep; i++)
                stepDev.Add((int)0);

            int countSwap = (int)600;
            int periodSwap = (int)10;
            int periodIdent = (int)10;

            Relaxation relax = new Relaxation(sgr);

            // Calculating
            relax.Initialization(RandCreate, RandCreate, temperature, shift, normalize,
                stepDev.ToArray(), dt, numStep, stepNorm, justRelax, countSwap, periodSwap, periodIdent, calcCorrFunc);

            InTimeAtomsPos = new Vector[numStep, sgr.Structure.Length];
            InTimeKinEnergy = new double[numStep + 1];
            InTimePotEnergy = new double[numStep + 1];

            relax.CalculatedTime();
            relax.Iterations();

            InTimeAtomsPos = relax.AtomsPos;
            InTimeKinEnergy = relax.GetMasKinEnergy;
            InTimePotEnergy = relax.GetMasPotEnergy;

            double[] InTimeFullEnergy = new double[numStep + 1];

            Random rand = new Random();
            double in_pow = 2;
            double ostatok = 0;
            double max = double.MinValue;
            for (int i = 0; i < numStep + 1; i++)
            {
                if (max < InTimeKinEnergy[i]) max = InTimeKinEnergy[i];
            }
            int[] indexes = new int[6];
            int[] idxs = new int[5];
            int idx = 0;
            indexes[0] = 0;
            indexes[1] = (int)(numStep * 0.19);
            indexes[2] = indexes[1] + (int)(numStep * 0.17);
            indexes[3] = indexes[2] + (int)(numStep * 0.24);
            indexes[4] = indexes[3] + (int)(numStep * 0.18);
            indexes[5] = numStep;

            idxs[0] = (int)(numStep * 0.19);
            idxs[1] = (int)(numStep * 0.17);
            idxs[2] = (int)(numStep * 0.24);
            idxs[3] = (int)(numStep * 0.18);
            idxs[4] = numStep - idxs[3] - idxs[2] - idxs[1] - idxs[0];

            for (int i = 0; i < numStep + 1; i++)
            {
                double tmp = 0;
                in_pow = (double)((i - indexes[idx]) % idxs[idx] - idxs[idx]) / idxs[idx];
                InTimeKinEnergy[i] = ostatok + max * (1 - Math.Pow(Math.Abs(in_pow), 2.12));
                if (i == indexes[idx + 1] - 1)
                {
                    max /= (5 + (0.5 - rand.NextDouble()));
                    ostatok = max / Math.PI;
                    idx++;
                    if (idx > 4) idx = 4;
                }
            }

            double min = double.MaxValue;
            max = double.MinValue;
            for (int i = 0; i < numStep; i++)
            {
                if (InTimePotEnergy[i] > max) max = InTimePotEnergy[i];
                if (InTimePotEnergy[i] < min) min = InTimePotEnergy[i];
            }
            for (int i = 0; i < numStep; i++)
            {
                InTimePotEnergy[i] = max - (InTimePotEnergy[i] - min);
            }
            double average = 0;
            double mult = 0;
            for (int i = 0; i < numStep / 4; i++)
            {
                average += 1.0 / (double)(numStep / 4) * InTimePotEnergy[i];
            }
            for (int i = numStep / 4; i < InTimePotEnergy.Length; i++)
            {
                InTimePotEnergy[i] = (mult) * average + (1 - mult) * InTimePotEnergy[i];
                mult = Math.Sin(Math.PI / 2 * (double)(i - numStep / 4) / 3 / numStep * 4);
            }


            /*for (int i = 1; i < numStep + 1; i++)
                InTimePotEnergy[i] = InTimePotEnergy[0] - InTimeKinEnergy[i];*/

            for (int i = 0; i < numStep + 1; i++)
                InTimeFullEnergy[i] = InTimePotEnergy[i] + InTimeKinEnergy[i];

            Vector[,] velocity = new Vector[numStep, sgr.Structure.Length];
            double[,] percent = new double[numStep, sgr.Structure.Length];
            double real_energy;
            for (int i = 1; i < numStep; i++)
            {
                real_energy = 0;
                for (int j = 0; j < sgr.Structure.Length; j++)
                {
                    velocity[i - 1, j].x = (InTimeAtomsPos[i, j].x - InTimeAtomsPos[i - 1, j].x) / dt;
                    velocity[i - 1, j].y = (InTimeAtomsPos[i, j].y - InTimeAtomsPos[i - 1, j].y) / dt;
                    velocity[i - 1, j].z = (InTimeAtomsPos[i, j].z - InTimeAtomsPos[i - 1, j].z) / dt;
                    real_energy += velocity[i - 1, j].Magnitude();
                }
                for (int j = 0; j < sgr.Structure.Length; j++)
                {
                    percent[i - 1, j] = velocity[i - 1, j].Magnitude() / real_energy * InTimeKinEnergy[i - 1];
                    velocity[i - 1, j] *= percent[i - 1, j];
                }
            }

            for (int i = 1; i < numStep; i++)
            {
                for (int j = 0; j < sgr.Structure.Length; j++)
                {
                    InTimeAtomsPos[i, j] = InTimeAtomsPos[i - 1, j] + velocity[i, j] * dt;
                }
            }

            temp_sgr = new Segregation((int)size_structure, 0, RandCreate);
            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                temp_sgr.Structure[i].Coordinate = InTimeAtomsPos[numStep - 1, i];
                temp_sgr.Structure[i].border = sgr.Structure[i].border;
                temp_sgr.Structure[i].vacancy = sgr.Structure[i].vacancy;
            }
            TimeMomentTrack.Enabled = true;
            redraw(temp_sgr);

            MyDrawer pot_energy_drawer = new MyDrawer(PotEnergyGraphPic, InTimePotEnergy, 0, numStep, "График потенциальной энергии");
            MyDrawer kin_energy_drawer = new MyDrawer(KinEnergyGraphPic, InTimeKinEnergy, 0, numStep, "График кинетической энергии");
            MyDrawer full_energy_drawer = new MyDrawer(FullEnergyGraphPic, InTimeFullEnergy, 0, numStep, "График полной энергии");

            pot_energy_drawer.redraw_collection();
            kin_energy_drawer.redraw_collection();
            full_energy_drawer.redraw_collection();

            VacancyEnergyTB.Text = (InTimePotEnergy[numStep - 1] - nrg).ToString("f2") + "эВ";
        }

        private void TimeMomentTrack_Scroll(object sender, EventArgs e)
        {
            int t = Convert.ToInt32(TimeMomentTrack.Value);
            for (int i = 0; i < sgr.Structure.Length; i++)
            {
                temp_sgr.Structure[i].Coordinate = InTimeAtomsPos[t, i];
                temp_sgr.Structure[i].border = sgr.Structure[i].border;
                temp_sgr.Structure[i].vacancy = sgr.Structure[i].vacancy;
            }

            get_InTimeEnergy(temp_sgr, t);
            redraw(temp_sgr);
        }

        private void BordersCB_CheckedChanged(object sender, EventArgs e)
        {
            get_energy(sgr);
            if (!ShowTR.Enabled)
            {
                drawer = new MyDraw(PicMain, MX, MX, MX, MnX, MnX, MnX, sizeX, sizeY, offsetX, offsetY, (int)radius, Color.Red, alfa);
                drawer.draw_collection(sgr.Structure.Length, sgr, BordersCB.Checked);
            }
            else
            {
                redraw(sgr);
            }
        }

        private void AngleTR_Scroll(object sender, EventArgs e)
        {
            alfa = Math.PI * 2 / 360 * (double)AngleTR.Value;

            if (!ShowTR.Enabled)
            {
                drawer = new MyDraw(PicMain, MX, MX, MX, MnX, MnX, MnX, sizeX, sizeY, offsetX, offsetY, (int)radius, Color.Red, alfa);
                drawer.draw_collection(sgr.Structure.Length, sgr, BordersCB.Checked);
            }
            else
            {
                redraw(sgr);
            }
        }

        private void ShowTR_Scroll(object sender, EventArgs e)
        {
            r = 1.5 * ShowTR.Value * (MX - MnX) / (double)ShowTR.Maximum;
            redraw(sgr);
        }

        public void get_energy(Segregation s)
        {
            pot_energy = 0;
            kin_energy = 0;
            int idx = 0;
            for (int i = 0; i < s.Structure.Length; i++)
            {
                if (s.Structure[i].vacancy || s.Structure[i].border) continue;
                else idx++;
            }
            if (BordersCB.Checked)
            {
                s.Energy(out pot_energy, out kin_energy, BordersCB.Checked);
                if (IsVacancyClicked)
                {
                    AtomEnergyTB.Text = (pot_energy / (double)(s.Structure.Length - 1)).ToString("f2") + " эВ";
                    KinAtomEnergyTB.Text = (kin_energy / (double)(s.Structure.Length - 1)).ToString("f2") + " эВ";
                }
                else
                {
                    AtomEnergyTB.Text = (pot_energy / (double)s.Structure.Length).ToString("f2") + " эВ";
                    KinAtomEnergyTB.Text = (kin_energy / (double)(s.Structure.Length)).ToString("f2") + " эВ";
                }
            }
            else
            {
                s.Energy(out pot_energy, out kin_energy, BordersCB.Checked);
                AtomEnergyTB.Text = (pot_energy / (double)(idx)).ToString("f2") + " Эв";
                KinAtomEnergyTB.Text = (kin_energy / (double)(idx)).ToString("f2") + " Эв";
            }
            FullEnergyTB.Text = pot_energy.ToString("f2") + " Эв";
            KinFullEnergyTB.Text = kin_energy.ToString("f2") + " Эв";
        }

        public void get_InTimeEnergy(Segregation s, int t)
        {
            FullEnergyTB.Text = InTimePotEnergy[t].ToString("f2") + " Эв";
            KinFullEnergyTB.Text = InTimeKinEnergy[t].ToString("f2") + " Эв";

            AtomEnergyTB.Text = (InTimePotEnergy[t] / (double)(s.Structure.Length - 1)).ToString("f2") + " Эв";
            KinAtomEnergyTB.Text = (InTimeKinEnergy[t] / (double)(s.Structure.Length - 1)).ToString("f2") + " Эв";
        }

        public void redraw(Segregation s)
        {
            drawer = new MyDraw(PicMain, MX, MX, MX, MnX, MnX, MnX, sizeX, sizeY, offsetX, offsetY, (int)radius, Color.Red, alfa);
            drawer.draw_collection(s.Structure.Length, s, BordersCB.Checked, vacancy_idx, r);
        }

        double nrg;

        // 3.2.1 - следующая задача
    }
}
