﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silicium
{
    public class Metropolis
    {
        /// <summary>
        /// Шаг алгоритма.
        /// </summary>
        private int step;
        /// <summary>
        /// Максимальный шаг алгоритма.
        /// </summary>
        private int maxStep;
        /// <summary>
        /// Постоянная Больцмана помноженная на температуру.
        /// </summary>
        private double constKT;
        /// <summary>
        /// Лист массивов энергий.
        /// </summary>
        private List<double[]> listMasEnergy;
        /// <summary>
        /// Структура сплава.
        /// </summary>
        private Segregation sgr;
        /// <summary>
        /// Генератор случайных чисел.
        /// </summary>
        private Random rand;

        /// <summary>
        /// Текущий шаг цикла перестановок.
        /// </summary>
        public int GetStep
        {
            get { return step; }
        }

        /// <summary>
        /// Максимальный шаг цикла перестановок.
        /// </summary>
        public int GetMaxStep
        {
            get { return maxStep; }
        }

        /// <summary>
        /// Размер листа массивов энергий.
        /// </summary>
        public int GetLengthList
        {
            get { return listMasEnergy.Count; }
        }

        /// <summary>
        /// Массив энергии.
        /// </summary>
        /// <param name="index">Индекс массива в листе.</param>
        /// <returns></returns>
        public double[] GetMasEnergy(int index)
        {
            if (index < 0 || index >= listMasEnergy.Count) return null;

            return listMasEnergy[index];
        }

        /// <summary>
        /// Преобразование листа массивов в массив массивов.
        /// </summary>
        /// <returns></returns>
        public double[][] GetListMasEnergy()
        {
            if (listMasEnergy.Count <= 0) return null;

            return listMasEnergy.ToArray();
        }

        /// <summary>
        /// Создание объекта класса.
        /// </summary>
        /// <param name="sgr">Структура сплава.</param>
        /// <param name="rand">Генератор случайных чисел.</param>
        public Metropolis(Segregation sgr, Random rand)
        {
            this.sgr = sgr;
            this.rand = rand;

            listMasEnergy = new List<double[]>();

            step = maxStep = 0;
        }

        /// <summary>
        /// Инициализация алгоритма.
        /// </summary>
        /// <param name="maxStep">Максимальное количество шагов.</param>
        /// <param name="tempr">Температура (К).</param>
        public void Initialization(int maxStep, double tempr)
        {
            if (maxStep < 0 || tempr < 0.0) return;

            this.maxStep = maxStep;
            this.step = 0;
            constKT = AtomParams.BOLTZMANN * tempr;
        }

        /// <summary>
        /// Алгоритм Метрополиса.
        /// </summary>
        /// <param name="write">True, если нужно записывать полученный массив энергии в лист массивов энергий.</param>
        /// <returns></returns>
        public void Iterations(bool write)
        {
            if (sgr.StructLatPar == AtomParams.LATPAR_SI || sgr.StructLatPar == AtomParams.LATPAR_GE) return;

            double L = sgr.StructLength * sgr.StructLatPar;
            List<double> masEnergy = new List<double>();

            for (step = 1; step < maxStep; step++)
            {
                int idxAtom1 = rand.Next(0, sgr.StructNumAtoms);
                AtomType atom1 = sgr.Structure[idxAtom1].Type;

                AtomType atom2;
                int idxAtom2;
                while (true)
                {
                    idxAtom2 = rand.Next(0, sgr.StructNumAtoms);
                    atom2 = sgr.Structure[idxAtom2].Type;
                    if (atom1 != atom2) break;
                }

                //Если атомы разные:
                //если измен. энергии меньше 0, то меняем,
                //если измен. энергии больше 0 и exp(-dE/kT) > r, r = [0, 1), тогда меняем
                //Если атомы одинаковые, то не меняем


                //Подсчёт энергии до перестановки
                double energyBefore = sgr.SumPotentialEnergy();

                //Перестановка
                sgr.Structure[idxAtom1].Type = atom2;
                sgr.Structure[idxAtom2].Type = atom1;

                //Подсчёт энергии после перестановки

                Task[] tasks = new Task[2]
                {
                   new Task(delegate{ sgr.Structure[idxAtom1].PotentialEnergy(sgr.SelPotential, L); }, TaskCreationOptions.AttachedToParent),
                   new Task(delegate{ sgr.Structure[idxAtom2].PotentialEnergy(sgr.SelPotential, L); }, TaskCreationOptions.AttachedToParent),
                };
                for (int t = 0; t < tasks.Length; t++)
                    tasks[t].Start();
                Task.WaitAll(tasks);

                double energyAfter = sgr.SumPotentialEnergy();

                double residual = energyAfter - energyBefore;

                if (residual <= 0.0d)
                {
                    if (write)
                        masEnergy.Add(energyAfter);
                }
                else
                {
                    double exponenta = Math.Exp(-residual / constKT);
                    double r = rand.NextDouble();
                    if (exponenta > r)
                    {
                        if (write)
                            masEnergy.Add(energyAfter);
                    }
                    else
                    {
                        sgr.Structure[idxAtom1].Type = atom1;
                        sgr.Structure[idxAtom2].Type = atom2;

                        //Подсчёт энергии после перестановки

                        tasks = new Task[2]
                        {
                           new Task(delegate{ sgr.Structure[idxAtom1].PotentialEnergy(sgr.SelPotential, L); }, TaskCreationOptions.AttachedToParent),
                           new Task(delegate{ sgr.Structure[idxAtom2].PotentialEnergy(sgr.SelPotential, L); }, TaskCreationOptions.AttachedToParent),
                        };
                        for (int t = 0; t < tasks.Length; t++)
                            tasks[t].Start();
                        Task.WaitAll(tasks);
                    }
                }

                for (int t = 0; t < tasks.Length; t++)
                    tasks[t].Dispose();
            }

            if (write)
                listMasEnergy.Add(masEnergy.ToArray());
        }

    }
}
