﻿namespace _3D
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Picture_1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Picture_2 = new System.Windows.Forms.PictureBox();
            this.StartBT = new System.Windows.Forms.Button();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Picture_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_2)).BeginInit();
            this.SuspendLayout();
            // 
            // Picture_1
            // 
            this.Picture_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture_1.Location = new System.Drawing.Point(6, 5);
            this.Picture_1.Name = "Picture_1";
            this.Picture_1.Size = new System.Drawing.Size(497, 527);
            this.Picture_1.TabIndex = 0;
            this.Picture_1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(901, 538);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "Тык";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Picture_2
            // 
            this.Picture_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture_2.Location = new System.Drawing.Point(509, 5);
            this.Picture_2.Name = "Picture_2";
            this.Picture_2.Size = new System.Drawing.Size(497, 527);
            this.Picture_2.TabIndex = 2;
            this.Picture_2.TabStop = false;
            // 
            // StartBT
            // 
            this.StartBT.Location = new System.Drawing.Point(790, 538);
            this.StartBT.Name = "StartBT";
            this.StartBT.Size = new System.Drawing.Size(105, 28);
            this.StartBT.TabIndex = 3;
            this.StartBT.Text = "Тык";
            this.StartBT.UseVisualStyleBackColor = true;
            this.StartBT.Click += new System.EventHandler(this.StartBT_Click);
            // 
            // MainTimer
            // 
            this.MainTimer.Interval = 1;
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 573);
            this.Controls.Add(this.StartBT);
            this.Controls.Add(this.Picture_2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Picture_1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Picture_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture_1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox Picture_2;
        private System.Windows.Forms.Button StartBT;
        private System.Windows.Forms.Timer MainTimer;
    }
}

