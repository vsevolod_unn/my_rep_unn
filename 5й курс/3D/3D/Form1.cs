﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3D
{
    public partial class Form1 : Form
    {
        double angleX, angleY, angleZ, mult;
        int N = 100;
        double[] x, y;
        double[][] z;

        double minX, minY, minZ;
        double maxX, maxY, maxZ;

        public Form1()
        {
            InitializeComponent();
            StartBT.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            angleX = 35;
            angleY = 35;
            angleZ = 35;
            mult = 0.6;

            minX = -2;
            maxX = 2;
            minY = -2;
            maxY = 2;
            minZ = 0;
            maxZ = 4;

            double dx = (maxX - minX) / (double)N;
            double dy = (maxY - minY) / (double)N;

            /// Создаем массивы координат - так удобнее, чем разбираться с ними внутри класса
            x = new double[N];
            y = new double[N];
            z = new double[N][];
            for (int i = 0; i < N; i++)
            {
                z[i] = new double[N];
                x[i] = minX + i * dx;
                for (int j = 0; j < N; j++)
                {
                    y[j] = minY + j * dy;
                    z[i][j] = func(x[i], y[j]);
                }
            }
            /// Можно передавать размеры области отрисовки, можно не передавать - все равно отрисует.
            /// Также можно передавать только часть параметров - например только по Х и по У - тогда 
            /// z зададутся по умолчанию (0, 1).
            /// Примеры:
            /// Не передаем размеры области - картинка слева.
            Drawer3D drawer1 = new Drawer3D(Picture_1);
            drawer1.Draw(x, y, z, angleX, angleY, angleZ, mult);
            /// Передаем размеры области - картинка справа.
            Drawer3D drawer2 = new Drawer3D(Picture_2, (new double[] { minX, maxX, minY, maxY, minZ, maxZ }).ToArray());
            drawer2.Draw(x, y, z, angleX, angleY, angleZ, mult);

            StartBT.Enabled = true;
        }

        /// <summary>
        /// Сюда пишем что угодно, чтобы получать массив данных z[][].
        /// Можно использовать и под z[,], но для рисовалки придется переделать в z[][].
        /// </summary>
        /// <param name="x"> тут и так все понятно </param>
        /// <param name="y"> тут и так все понятно </param>
        /// <returns> вернет z = f(x, y) </returns>
        public double func(double x, double y)
        {
            if (x * x + y * y < 4)
            {
                double z = Math.Sqrt(4 - x * x - y * y);
                //double z = Math.Cos(x * x + y * y) / (x * x + y * y + 1);
                //double z = Math.Pow((2 - x*x) * (2 - y*y), 1);
                return z;
            }
            else
                return 0;
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            angleX += 1;
            Drawer3D drawer1 = new Drawer3D(Picture_1);
            drawer1.Draw(x, y, z, angleX, angleY, angleZ, mult);
        }

        private void StartBT_Click(object sender, EventArgs e)
        {
            MainTimer.Start();
        }
    }
}
