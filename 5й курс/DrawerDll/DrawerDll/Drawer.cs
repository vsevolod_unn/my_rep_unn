﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows;

public class Drawer
{
    string graph_name;
    public PictureBox pic_box;
    public Bitmap bmp;
    public Graphics g;
    public Pen pen, axis_pen;

    public Color
        ColorBackGround = Color.Black,
        ColorPen = Color.Green,
        ColorBrush = Color.Black;

    public int width, height;
    public double minX, maxX, minY, maxY, minZ, maxZ;
    public int sizeX, sizeY, sizeZ;
    public int offsetX, offsetY, offsetZ;

    public Drawer() { }
    public Drawer(PictureBox pb, string _graph_name)
    {
        graph_name = _graph_name;
        pic_box = pb;
        width = pic_box.Width;
        height = pic_box.Height;

        minX = 0;
        minY = 0;
        minZ = 0;
        maxX = 1;
        maxY = 1;
        maxZ = 1;

        InitGraphics();
    }
    public Drawer(PictureBox pb, Array _sizes, string _graph_name)
    {
        graph_name = _graph_name;
        pic_box = pb;
        width = pic_box.Width;
        height = pic_box.Height;

        InitGraphics();
        if (_sizes == null || _sizes.Length == 0)
        {
            minX = 0;
            minY = 0;
            minZ = 0;
            maxX = 1;
            maxY = 1;
            maxZ = 1;
        }
        else
        {
            Resize(_sizes);
        }
    }
    public void InitGraphics()
    {
        bmp = new Bitmap(width, height);
        g = Graphics.FromImage(bmp);
        g.Clear(Color.Black);
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
    }
    public void Resize(Array _sizes)
    {
        double[] sizes = new double[6];
        for (int i = 0; i < 6; i++)
        {
            sizes[i] = (int)(i * 2) / 6; ;
        }
        for (int i = 0; i < _sizes.Length; i++)
        {
            sizes[i] = ((double[])_sizes)[i];
        }
        minX = sizes[0];
        maxX = sizes[1];
        minY = sizes[2];
        maxY = sizes[3];
        minZ = sizes[4];
        maxZ = sizes[5];
    }
    public void Resize(double[] x, double[] y, double[][] z)
    {
        minX = double.MaxValue;
        maxX = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] < minX) minX = x[i];
            if (x[i] > maxX) maxX = x[i];
        }

        minY = double.MaxValue;
        maxY = double.MinValue;
        for (int i = 0; i < y.Length; i++)
        {
            if (y[i] < minY) minY = y[i];
            if (y[i] > maxY) maxY = y[i];
        }

        minZ = double.MaxValue;
        maxZ = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            for (int j = 0; j < y.Length; j++)
            {
                if (z[i][j] < minZ) minZ = z[i][j];
                if (z[i][j] > maxZ) maxZ = z[i][j];
            }
        }
    }
    public void ResizeX(double _minX, double _maxX)
    {
        minX = _minX;
        maxX = _maxX;
    }
    public void GraphicsSetTransform()
    {
        g.ResetTransform();
        g.TranslateTransform((float)offsetX, (float)offsetY);
        g.ScaleTransform((float)((double)(width - 2 * offsetX) / (maxX - minX)), -(float)((double)(height - 2 * offsetY) / (maxY - minY)));
        g.TranslateTransform(-(float)minX, (float)minY);
    }
    public void SetOffsets(Array _offsets)
    {
        offsetX = ((int[])_offsets)[0];
        offsetY = ((int[])_offsets)[1];
        offsetZ = ((int[])_offsets)[2];

        sizeX = (int)(width - 2 * offsetX);
        sizeY = (int)(height - 2 * offsetY);
    }

    public void DrawGraph(double[] x, double[] y)
    {
        g.Clear(Color.White);

        g.ResetTransform();
        DrawAxis();
        GraphicsSetTransform();

        pen = new Pen(Brushes.Red, 0);
        for (int i = 0; i < y.Length - 1; i++)
        {
            {
                if (y[i] <= maxY && y[i] >= minY && y[i + 1] < maxY && y[i + 1] >= minY)
                    g.DrawLine(pen, (float)x[i], (float)y[i], (float)x[i + 1], (float)y[i + 1]);
            }
        }
        pic_box.Image = bmp;
    }
    public void DrawAxis()
    {
        axis_pen = new Pen(Brushes.Gray, 0);
        float[] dash_attributes = new float[] { 6, 4, 2, 4 };
        axis_pen.DashPattern = dash_attributes;

        Point pt1;
        Point pt2;
        for (int i = 0; i < 9; i++)
        {
            pt1 = new Point((int)(offsetX + (double)(sizeX * i) / 8.0), offsetY);
            pt2 = new Point((int)(offsetX + (double)(sizeX * i) / 8.0), height - offsetY);
            g.DrawLine(axis_pen, pt1, pt2);
        }
        for (int i = 0; i < 9; i++)
        {
            pt1 = new Point(offsetX, (int)(offsetY + (double)(sizeY * i) / 8.0));
            pt2 = new Point(sizeX + offsetX, (int)(offsetY + (double)(sizeY * i) / 8.0));
            g.DrawLine(axis_pen, pt1, pt2);
        }

        g.DrawString(graph_name, new Font("Colibri", 10), Brushes.Black, sizeX / 2 - offsetX, offsetY / 4);

        g.DrawString(maxY.ToString("f2"), new Font("Colibri", 7), Brushes.Black, 0, offsetY);
        g.DrawString(((maxY + minY) / 2).ToString("f2"), new Font("Colibri", 7), Brushes.Black, 0, sizeY / 2 + offsetY);
        g.DrawString(minY.ToString("f2"), new Font("Colibri", 7), Brushes.Black, 0, sizeY + (int)(0.75 * offsetY));

        g.DrawString(minX.ToString("f2"), new Font("Colibri", 7), Brushes.Black, offsetX, sizeY + offsetY);
        g.DrawString(((maxX + minX) / 2).ToString("f2"), new Font("Colibri", 7), Brushes.Black, offsetX / 2 + sizeX / 2, sizeY + offsetY);
        g.DrawString(maxX.ToString("f2"), new Font("Colibri", 7), Brushes.Black, sizeX + offsetX / 2, sizeY + offsetY);
    }
}



