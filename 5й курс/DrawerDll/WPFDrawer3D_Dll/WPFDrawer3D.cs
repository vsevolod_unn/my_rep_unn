﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Controls;
using System.IO;

public class WPFDrawer3D
{
    string graph_name;
    public System.Windows.Controls.Image image;
    public System.Drawing.Bitmap bmp;
    public System.Drawing.Graphics g;
    public System.Drawing.Pen pen, axis_pen;

    public System.Drawing.Color
        ColorBackGround = System.Drawing.Color.White,
        ColorPen = System.Drawing.Color.Red,
        ColorBrush = System.Drawing.Color.Black;

    public int width, height;
    public double minX, maxX, minY, maxY, minZ, maxZ;
    public int sizeX, sizeY, sizeZ;
    public int offsetX, offsetY, offsetZ;
    public int axis_countX = 9, axis_countY = 9, axis_values_countX = 3, axis_values_countY = 3;

    public WPFDrawer3D() { }
    public WPFDrawer3D(System.Windows.Controls.Image _img, string _graph_name)
    {
        graph_name = _graph_name;
        image = _img;
        width = (int)image.Width;
        height = (int)image.Height;

        minX = 0;
        minY = 0;
        minZ = 0;
        maxX = 1;
        maxY = 1;
        maxZ = 1;

        InitGraphics();
    }
    public WPFDrawer3D(System.Windows.Controls.Image _img, Array _sizes, string _graph_name)
    {
        graph_name = _graph_name;
        image = _img;
        width = (int)image.Width;
        height = (int)image.Height;

        InitGraphics();
        if (_sizes == null || _sizes.Length == 0)
        {
            minX = 0;
            minY = 0;
            minZ = 0;
            maxX = 1;
            maxY = 1;
            maxZ = 1;
        }
        else
        {
            Resize(_sizes);
        }
    }
    public WPFDrawer3D(System.Windows.Controls.Image _img, Array _sizes, string _graph_name, Array _offsets)
    {
        graph_name = _graph_name;
        image = _img;
        width = (int)image.Width;
        height = (int)image.Height;

        InitGraphics();
        if (_sizes == null || _sizes.Length == 0)
        {
            minX = 0;
            minY = 0;
            minZ = 0;
            maxX = 1;
            maxY = 1;
            maxZ = 1;
        }
        else
        {
            Resize(_sizes);
        }
        SetOffsets(_offsets);
    }
    public void InitGraphics()
    {
        bmp = new Bitmap(width, height);
        g = Graphics.FromImage(bmp);
        g.Clear(Color.Black);
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
    }
    public void Resize(Array _sizes)
    {
        double[] sizes = new double[6];
        for (int i = 0; i < 6; i++)
        {
            sizes[i] = (int)(i * 2) / 6; ;
        }
        for (int i = 0; i < _sizes.Length; i++)
        {
            sizes[i] = ((double[])_sizes)[i];
        }
        minX = sizes[0];
        maxX = sizes[1];
        minY = sizes[2];
        maxY = sizes[3];
        minZ = sizes[4];
        maxZ = sizes[5];
    }
    public void Resize(double[] x, double[] y, double[][] z)
    {
        minX = double.MaxValue;
        maxX = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] < minX) minX = x[i];
            if (x[i] > maxX) maxX = x[i];
        }

        minY = double.MaxValue;
        maxY = double.MinValue;
        for (int i = 0; i < y.Length; i++)
        {
            if (y[i] < minY) minY = y[i];
            if (y[i] > maxY) maxY = y[i];
        }

        minZ = double.MaxValue;
        maxZ = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            for (int j = 0; j < y.Length; j++)
            {
                if (z[i][j] < minZ) minZ = z[i][j];
                if (z[i][j] > maxZ) maxZ = z[i][j];
            }
        }
    }
    public void GraphicsSetTransform()
    {
        g.ResetTransform();
        g.ScaleTransform((float)((double)width / (maxX - minX)), -(float)((double)height / (maxY - minY)));
        g.TranslateTransform(-(float)minX, (float)minY);
    }
    public void SetOffsets(Array _offsets)
    {
        offsetX = ((int[])_offsets)[0];
        offsetY = ((int[])_offsets)[1];
        offsetZ = ((int[])_offsets)[2];

        sizeX = (int)(width - 2 * offsetX);
        sizeY = (int)(height - 2 * offsetY);
    }
    public void AxisConfig(Array _axis_count)
    {
        if (_axis_count.Length >= 2)
        {
            axis_countX = ((int[])_axis_count)[0];
            axis_countY = ((int[])_axis_count)[1];
        }
        if (_axis_count.Length >= 4)
        {
            axis_values_countX = ((int[])_axis_count)[2];
            axis_values_countY = ((int[])_axis_count)[3];
        }
    }
    public void DrawAxis()
    {
        axis_pen = new System.Drawing.Pen(System.Drawing.Brushes.Gray, 0);
        float[] dash_attributes = new float[] { 6, 4, 2, 4 };
        axis_pen.DashPattern = dash_attributes;

        System.Drawing.Point pt1;
        System.Drawing.Point pt2;
        for (int i = 0; i < axis_countX; i++)
        {
            pt1 = new System.Drawing.Point((int)(offsetX + (double)(sizeX * i) / (double)(axis_countX - 1)), offsetY);
            pt2 = new System.Drawing.Point((int)(offsetX + (double)(sizeX * i) / (double)(axis_countX - 1)), height - offsetY);
            g.DrawLine(axis_pen, pt1, pt2);
        }
        for (int i = 0; i < axis_countY; i++)
        {
            pt1 = new System.Drawing.Point(offsetX, (int)(offsetY + (double)(sizeY * i) / (double)(axis_countY - 1)));
            pt2 = new System.Drawing.Point(sizeX + offsetX, (int)(offsetY + (double)(sizeY * i) / (double)(axis_countY - 1)));
            g.DrawLine(axis_pen, pt1, pt2);
        }

        g.DrawString(graph_name, new System.Drawing.Font("Colibri", 10), System.Drawing.Brushes.Black, sizeX / 2 - offsetX, offsetY / 4);

        for (int i = 0; i < axis_values_countX; i++)
        {
            g.DrawString(
                ((maxX - minX) * (double)i / (double)(axis_values_countX - 1) + minX).ToString("f2"),
                new System.Drawing.Font("Colibri", 7), System.Drawing.Brushes.Black,
                (int)(sizeX * (double)i / (double)(axis_values_countX - 1)) +
                (int)(offsetX * ((axis_values_countX - i * 0.25 - 1) / (axis_values_countX - 1))),
                sizeY + offsetY);
        }

        for (int i = 0; i < axis_values_countY; i++)
        {
            g.DrawString(
                ((minY - maxY) * (double)i / (double)(axis_values_countY - 1) + maxY).ToString("f2"),
                new System.Drawing.Font("Colibri", 7), System.Drawing.Brushes.Black, 0,
                (int)(sizeY * (double)i / (double)(axis_values_countY - 1)) +
                (int)(offsetY * ((axis_values_countY - i * 0.25 - 1) / (axis_values_countY - 1)))
                );
        }
    }
    public static System.Windows.Media.Imaging.BitmapImage convert_to_WPFImage(System.Drawing.Image _img)
    {
        MemoryStream ms = new MemoryStream();  // no using here! BitmapImage will dispose the stream after loading
        _img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);

        System.Windows.Media.Imaging.BitmapImage img = new System.Windows.Media.Imaging.BitmapImage();
        img.BeginInit();
        img.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
        img.StreamSource = ms;
        img.EndInit();
        return img;
    }

    /// <summary>
    /// Отрисовка 3D фигуры, вид которой определяется уравнением z = f(x, y);
    /// </summary>
    /// <param name="x"></param> массив х координат
    /// <param name="y"></param> массив у координат
    /// <param name="z"></param> массив значений функции f (координаты z)
    /// <param name="angleX"></param> угол по оси ох
    /// <param name="angleY"></param> угол по оси оy
    /// <param name="angleZ"></param> угол по оси оz
    /// <param name="mult"></param> величина зумирования
    public void Draw(double[] x, double[] y, double[][] z, double angleX, double angleY, double angleZ, double mult)
    {
        Resize(x, y, z);
        g.Clear(ColorBackGround);
        
        g.ResetTransform();
        DrawAxis();
        GraphicsSetTransform();

        int l1 = x.Length;
        int l2 = y.Length;

        double x1 = 0, y1 = 0, z1 = 0;
        double x2 = 0, y2 = 0, z2 = 0;
        double x3 = 0, y3 = 0, z3 = 0;
        double x4 = 0, y4 = 0, z4 = 0;

        SolidBrush brush = new SolidBrush(ColorBrush);
        Pen pen = new Pen(ColorPen, (float)0);

        mat4 m4 = new mat4();
        m4.translate(0, 0, -maxZ / 2);
        m4.scale(mult, mult, mult);
        m4.perspective(10);
        m4.rotateZ(angleX);
        m4.rotateY(angleY);
        m4.rotateZ(angleZ);

        for (int i = 1; i < l1; i++)
        {
            x1 = x[i - 1];
            x2 = x[i];
            x3 = x[i];
            x4 = x[i - 1];
            for (int j = 1; j < l2; j++)
            {
                y1 = y[j - 1];
                y2 = y[j - 1];
                y3 = y[j];
                y4 = y[j];

                z1 = z[i - 1][j - 1];
                z2 = z[i][j - 1];
                z3 = z[i][j];
                z4 = z[i - 1][j];

                vec4 v1 = new vec4(x1, y1, z1);
                vec4 v2 = new vec4(x2, y2, z2);
                vec4 v3 = new vec4(x3, y3, z3);
                vec4 v4 = new vec4(x4, y4, z4);

                v1 = v1 * m4;
                v2 = v2 * m4;
                v3 = v3 * m4;
                v4 = v4 * m4;

                PointF[] matr = new PointF[]
                {
                        new PointF(v1.y(), v1.z()),
                        new PointF(v2.y(), v2.z()),
                        new PointF(v3.y(), v3.z()),
                        new PointF(v4.y(), v4.z())
                };

                g.DrawPolygon(pen, matr);
                g.FillPolygon(brush, matr);
            }
        }
        System.Drawing.Image img = bmp;
        System.Windows.Media.Imaging.BitmapImage bitmapImage = convert_to_WPFImage(img);
        image.Source = bitmapImage;
    }
}

class mat4
{
    // Ранг матрицы
    int _rank = 4;
    double M_PI = Math.PI;

    // Элементы матрицы
    public double[][] _matrix;

    // Конструктор по умолчанию
    public mat4()
    {
        _rank = 4;
        _matrix = new double[_rank][];
        for (int i = 0; i < _rank; i++)
            _matrix[i] = new double[_rank];

        reset();
    }

    // Конструктор
    public mat4(mat4 matr)
    {
        _matrix = matr._matrix;
    }

    // Конструктор
    public mat4(double m11, double m12, double m13,
        double m21, double m22, double m23,
        double m31, double m32, double m33,
        double dx = 0, double dy = 0, double dz = 0)
    {
        reset();
        _matrix[0][0] = m11;
        _matrix[0][1] = m12;
        _matrix[0][2] = m13;
        _matrix[0][3] = 0;

        _matrix[1][0] = m21;
        _matrix[1][1] = m22;
        _matrix[1][2] = m23;
        _matrix[1][3] = 0;

        _matrix[2][0] = m31;
        _matrix[2][1] = m32;
        _matrix[2][2] = m33;
        _matrix[2][3] = 0;

        _matrix[3][0] = dx;
        _matrix[3][1] = dy;
        _matrix[3][2] = dz;
        _matrix[3][3] = 1;
    }

    // Операция умножения на матрицу
    public static mat4 operator *(mat4 left, mat4 right)
    {
        mat4 _newmatrix = new mat4();
        for (int i = 0; i < _newmatrix._rank; i++)//строка 
        {
            for (int j = 0; j < _newmatrix._rank; j++)//столбцы
            {
                _newmatrix._matrix[i][j] = 0;//создаём новую матрицу
                for (int k = 0; k < _newmatrix._rank; k++)//для умножения 2-ух матриц
                {
                    _newmatrix._matrix[i][j] += (left._matrix[i][k]) * (right._matrix[k][j]);
                }
            }
        }
        return _newmatrix;
    }

    // Сброс матрицы в единичную
    public void reset()
    {
        this._matrix = new double[4][];
        this._matrix[0] = new double[4];
        this._matrix[1] = new double[4];
        this._matrix[2] = new double[4];
        this._matrix[3] = new double[4];
        for (int i = 0; i < 4; i++)
        {
            _matrix[i][i] = 1;
        }
    }

    // Поворот относительно оси X. Угол в градусах!
    public void rotateX(double angle)
    {
        double fi = angle * M_PI / 180;

        mat4 rmatr = new mat4(
            1.0, 0.0, 0.0,
            0.0, Math.Cos(fi), Math.Sin(fi),
            0.0, -Math.Sin(fi), Math.Cos(fi));

        mat4 matr = new mat4(this * rmatr);
        this._matrix = matr._matrix;
    }

    // Поворот относительно оси Y. Угол в градусах!
    public void rotateY(double angle)
    {
        double fi = angle * M_PI / 180;

        mat4 rmatr = new mat4(
            Math.Cos(fi), 0.0, -Math.Sin(fi),
            0.0, 1.0, 0.0,
            Math.Sin(fi), 0.0, Math.Cos(fi));

        mat4 matr = new mat4(this * rmatr);
        this._matrix = matr._matrix;
    }

    // Поворот относительно оси Z. Угол в градусах!
    public void rotateZ(double angle)
    {
        double fi = angle * M_PI / 180;

        mat4 rmatr = new mat4(
            Math.Cos(fi), Math.Sin(fi), 0.0,
            -Math.Sin(fi), Math.Cos(fi), 0.0,
            0.0, 0.0, 1.0);

        mat4 matr = new mat4(this * rmatr);
        this._matrix = matr._matrix;
    }

    // Перенос (сдвиг)
    public void translate(double x, double y, double z)
    {
        mat4 tmatr = new mat4(
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0,
            x, y, z);

        mat4 matr = new mat4(this * tmatr);
        this._matrix = matr._matrix;
    }

    // Масштабирование
    public void scale(double x, double y, double z)
    {
        mat4 smatr = new mat4(
            x, 0.0, 0.0,
            0.0, y, 0.0,
            0.0, 0.0, z);

        mat4 matr = new mat4(this * smatr);
        this._matrix = matr._matrix;
    }

    // Перспективное преобразование вдоль оси X
    public void perspective(double center)
    {
        mat4 pmatr = new mat4(
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0);
        pmatr._matrix[0][3] = -1 / center;

        mat4 matr = new mat4(this * pmatr);
        this._matrix = matr._matrix;
    }
};

// ------------------------------------------------------------

///////////////////////////////////////////////////////////////
// Вектор - однородные координаты точки в 3-мерном пространстве
///////////////////////////////////////////////////////////////

class vec4
{
    // Размерность вектора
    int _size = 4;

    // Элементы вектора
    public double[] _vector;

    // Конструктор
    public vec4(double x = 0, double y = 0, double z = 0)
    {
        _vector = new double[_size];
        _vector[0] = x;
        _vector[1] = y;
        _vector[2] = z;
        _vector[3] = 1;
    }

    public vec4(vec4 vect)
    {
        this._vector = vect._vector;
    }

    // Операция умножения на матрицу
    public static vec4 operator *(vec4 left, mat4 right)
    {
        vec4 _newvector = new vec4();
        for (int i = 0; i < left._size; i++)
        {
            _newvector._vector[i] = 0;
            for (int k = 0; k < left._size; k++)
            {
                _newvector._vector[i] += (left._vector[k]) * (right._matrix[k][i]);
            }
        }
        for (int j = 0; j < left._size; j++)
        {
            _newvector._vector[j] /= _newvector._vector[left._size - 1];
        }
        return _newvector;
    }

    public float x()
    {
        return (float)_vector[0];
    }
    public float y()
    {
        return (float)_vector[1];
    }
    public float z()
    {
        return (float)_vector[2];
    }
};
