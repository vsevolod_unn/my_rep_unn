﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


abstract public class IDrawer
{
    public IDrawer() { }

    /// --------------------------------------------------------------------------------------------------------------------
    /// 
    /// Общие параметры
    /// Отрисовка всегда идет с использованием GDI+
    public string GraphName;
    protected System.Drawing.Bitmap bmp;
    protected System.Drawing.Graphics g;

    /// Цвета, кисти, шрифты
    public System.Drawing.Pen PenAxis;
    public System.Drawing.Pen[] PenGraphics = new System.Drawing.Pen[10];
    public System.Drawing.Color
        ColorBackGround = System.Drawing.Color.White;
    public System.Drawing.Color[]
        ColorPen = new System.Drawing.Color[10];
    public System.Drawing.Brush
        ColorAxisPen = System.Drawing.Brushes.Gray,
        ColorGraphNameText = System.Drawing.Brushes.Black,
        ColorSignaturesText = System.Drawing.Brushes.Black;
    protected System.Drawing.Font
        FontGraphicName = new System.Drawing.Font("Colibri", 10),
        FontSignaturesText = new System.Drawing.Font("Colibi", 7);

    /// Все в писелях
    protected int width, height;
    protected int sizeX, sizeY, sizeZ;
    protected int offsetX, offsetY, offsetZ;
    /// Мировые координаты 
    protected double minX, maxX, minY, maxY, minZ, maxZ;

    /// Параметры оформления графика
    protected int X_accuracy = 2, Y_accuracy = 2;
    protected int axis_countX = 9, axis_countY = 9, axis_values_countX = 3, axis_values_countY = 3;
    public bool GRAPHIC, AXIS, GRAPH_NAME, X_SIGNATURES, Y_SIGNATURES, BORDER, AUTOSIZE;

    /// --------------------------------------------------------------------------------------------------------------------
    /// 
    /// Управление рисованием
    public void InitGraphics()
    {
        bmp = new System.Drawing.Bitmap(width, height);
        g = System.Drawing.Graphics.FromImage(bmp);
        g.Clear(System.Drawing.Color.Black);
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

        /// Создание цветов - их можно будет переопределить
        {
            ColorPen[0] = System.Drawing.Color.Red;
            ColorPen[1] = System.Drawing.Color.Navy;
            ColorPen[2] = System.Drawing.Color.DarkGreen;
            ColorPen[3] = System.Drawing.Color.Purple;
            ColorPen[4] = System.Drawing.Color.DarkOrange;
            ColorPen[5] = System.Drawing.Color.DarkOrchid;
            ColorPen[6] = System.Drawing.Color.Black;
            ColorPen[7] = System.Drawing.Color.DarkCyan;
            ColorPen[8] = System.Drawing.Color.YellowGreen;
            ColorPen[9] = System.Drawing.Color.Coral;
        }
        /// Создание ручек - их можно будет переопределить
        {
            PenGraphics[0] = new System.Drawing.Pen(System.Drawing.Color.Red, 0);
            PenGraphics[1] = new System.Drawing.Pen(System.Drawing.Color.Navy, 0);
            PenGraphics[2] = new System.Drawing.Pen(System.Drawing.Color.DarkGreen, 0);
            PenGraphics[3] = new System.Drawing.Pen(System.Drawing.Color.Purple, 0);
            PenGraphics[4] = new System.Drawing.Pen(System.Drawing.Color.DarkOrange, 0);
            PenGraphics[5] = new System.Drawing.Pen(System.Drawing.Color.DarkOrchid, 0);
            PenGraphics[6] = new System.Drawing.Pen(System.Drawing.Color.Black, 0);
            PenGraphics[7] = new System.Drawing.Pen(System.Drawing.Color.DarkCyan, 0);
            PenGraphics[8] = new System.Drawing.Pen(System.Drawing.Color.YellowGreen, 0);
            PenGraphics[9] = new System.Drawing.Pen(System.Drawing.Color.Coral, 0);
        }
    }
    public void GraphicsSetTransform()
    {
        g.ResetTransform();
        g.TranslateTransform((float)offsetX, (float)offsetY);
        g.ScaleTransform((float)((double)(width - 2 * offsetX) / (maxX - minX)), -(float)((double)(height - 2 * offsetY) / (maxY - minY)));
        g.TranslateTransform(-(float)minX, -(float)maxY);
    }

    /// --------------------------------------------------------------------------------------------------------------------
    /// 
    /// Управление областями графика
    public void Resize(Array _sizes)
    {
        double[] sizes = new double[6];
        for (int i = 0; i < 6; i++)
        {
            sizes[i] = (int)(i * 2) / 6; ;
        }
        for (int i = 0; i < _sizes.Length; i++)
        {
            sizes[i] = ((double[])_sizes)[i];
        }
        minX = sizes[0];
        maxX = sizes[1];
        minY = sizes[2];
        maxY = sizes[3];
        minZ = sizes[4];
        maxZ = sizes[5];
    }
    public void Resize(double[] x, double[] y)
    {
        minX = double.MaxValue;
        maxX = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] < minX) minX = x[i];
            if (x[i] > maxX) maxX = x[i];
        }

        minY = double.MaxValue;
        maxY = double.MinValue;
        for (int i = 0; i < y.Length; i++)
        {
            if (y[i] < minY) minY = y[i];
            if (y[i] > maxY) maxY = y[i];
        }
    }
    public void Resize(double[][] x, double[][] y)
    {
        double _minX = minX;
        double _maxX = maxX;
        double _minY = minY;
        double _maxY = maxY;

        for (int i = 0; i < x.GetLength(0); i++)
        {
            Resize(x[i], y[i]);
            if (i == 0)
            {
                _minX = minX;
                _maxX = maxX;
                _minY = minY;
                _maxY = maxY;
            }
            else
            {
                if (_minX < minX) minX = _minX;
                if (_minY < minY) minY = _minY;
                if (_maxX > maxX) maxX = _maxX;
                if (_maxY > maxY) maxY = _maxY;
            }
        }
    }
    public void Resize(double[] x, double[] y, double[][] z)
    {
        minX = double.MaxValue;
        maxX = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] < minX) minX = x[i];
            if (x[i] > maxX) maxX = x[i];
        }

        minY = double.MaxValue;
        maxY = double.MinValue;
        for (int i = 0; i < y.Length; i++)
        {
            if (y[i] < minY) minY = y[i];
            if (y[i] > maxY) maxY = y[i];
        }

        minZ = double.MaxValue;
        maxZ = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            for (int j = 0; j < y.Length; j++)
            {
                if (z[i][j] < minZ) minZ = z[i][j];
                if (z[i][j] > maxZ) maxZ = z[i][j];
            }
        }
    }
    public void Resize(double[][] x, double[][] y, double[][] z)
    {
        double _minX = minX;
        double _maxX = maxX;
        double _minY = minY;
        double _maxY = maxY;
        double _minZ = minZ;
        double _maxZ = maxZ;

        for (int i = 0; i < x.GetLength(0); i++)
        {
            Resize(x[i], y[i], z);
            if (_minX < minX) minX = _minX;
            if (_minY < minY) minY = _minY;
            if (_minZ < minZ) minZ = _minZ;
            if (_maxX > maxX) maxX = _maxX;
            if (_maxY > maxY) maxY = _maxY;
            if (_maxZ > maxZ) maxZ = _maxZ;
        }
    }
    public void ResizeX(double _minX, double _maxX)
    {
        minX = _minX;
        maxX = _maxX;
    }

    /// --------------------------------------------------------------------------------------------------------------------
    /// 
    /// Настройка отрисовки
    public void SetRanges(Array _sizes)
    {
        double[] sizes = new double[6];
        for (int i = 0; i < 6; i++)
        {
            sizes[i] = (int)(i * 2) / 6; ;
        }
        for (int i = 0; i < _sizes.Length; i++)
        {
            sizes[i] = ((double[])_sizes)[i];
        }
        minX = sizes[0];
        maxX = sizes[1];
        minY = sizes[2];
        maxY = sizes[3];
        minZ = sizes[4];
        maxZ = sizes[5];
    }
    public void SetOffsets(Array _offsets)
    {
        offsetX = ((int[])_offsets)[0];
        offsetY = ((int[])_offsets)[1];
        offsetZ = ((int[])_offsets)[2];

        sizeX = (int)(width - 2 * offsetX);
        sizeY = (int)(height - 2 * offsetY);
    }
    public void AxisConfig(Array _axis_count)
    {
        if (_axis_count.Length >= 2)
        {
            axis_countX = ((int[])_axis_count)[0];
            axis_countY = ((int[])_axis_count)[1];
        }
        if (_axis_count.Length >= 4)
        {
            axis_values_countX = ((int[])_axis_count)[2];
            axis_values_countY = ((int[])_axis_count)[3];
        }
        if (_axis_count.Length >= 6)
        {
            X_accuracy = ((int[])_axis_count)[4];
            Y_accuracy = ((int[])_axis_count)[5];
        }
    }
    public void SetFonts(
        System.Drawing.Brush _color_of_graph_name,
        System.Drawing.Font _font_of_graph_name,
        System.Drawing.Brush _color_of_signatures,
        System.Drawing.Font _font_of_signatures)
    {
        ColorGraphNameText = _color_of_graph_name;
        FontGraphicName = _font_of_graph_name;
        ColorSignaturesText = _color_of_signatures;
        FontSignaturesText = _font_of_signatures;
    }
    public void SetSignaturesFormat(
        System.Drawing.Brush _color_of_graph_name,
        System.Drawing.Font _font_of_graph_name)
    {
        ColorGraphNameText = _color_of_graph_name;
        FontGraphicName = _font_of_graph_name;
    }
    public void SetGraphNameFormat(
        System.Drawing.Brush _color_of_signatures,
        System.Drawing.Font _font_of_signatures)
    {
        ColorSignaturesText = _color_of_signatures;
        FontSignaturesText = _font_of_signatures;
    }

    /// --------------------------------------------------------------------------------------------------------------------
    /// 
    /// Отрисовка осей и нанесение подписей
    private void DrawAxis()
    {
        if (AXIS)
        {
            PenAxis = new System.Drawing.Pen(ColorAxisPen, 0);
            float[] dash_attributes = new float[] { 6, 4, 2, 4 };
            PenAxis.DashPattern = dash_attributes;

            System.Drawing.Point pt1;
            System.Drawing.Point pt2;
            for (int i = 0; i < axis_countX; i++)
            {
                pt1 = new System.Drawing.Point((int)(offsetX + (double)(sizeX * i) / (double)(axis_countX - 1)), offsetY);
                pt2 = new System.Drawing.Point((int)(offsetX + (double)(sizeX * i) / (double)(axis_countX - 1)), height - offsetY);
                g.DrawLine(PenAxis, pt1, pt2);
            }
            for (int i = 0; i < axis_countY; i++)
            {
                pt1 = new System.Drawing.Point(offsetX, (int)(offsetY + (double)(sizeY * i) / (double)(axis_countY - 1)));
                pt2 = new System.Drawing.Point(sizeX + offsetX, (int)(offsetY + (double)(sizeY * i) / (double)(axis_countY - 1)));
                g.DrawLine(PenAxis, pt1, pt2);
            }
        }
        if (GRAPH_NAME)
            g.DrawString(GraphName, FontGraphicName, ColorGraphNameText, sizeX / 2 - offsetX, offsetY / 4);

        if (X_SIGNATURES)
            for (int i = 0; i < axis_values_countX; i++)
            {
                g.DrawString(
                    ((maxX - minX) * (double)i / (double)(axis_values_countX - 1) + minX).ToString("f" + X_accuracy.ToString()),
                    FontSignaturesText, ColorSignaturesText,
                    (int)(sizeX * (double)i / (double)(axis_values_countX - 1)) +
                    (int)(offsetX * ((axis_values_countX - i * 0.25 - 1) / (axis_values_countX - 1))),
                    sizeY + offsetY);
            }

        if (Y_SIGNATURES)
            for (int i = 0; i < axis_values_countY; i++)
            {
                g.DrawString(
                    ((minY - maxY) * (double)i / (double)(axis_values_countY - 1) + maxY).ToString("f" + Y_accuracy.ToString()),
                    FontSignaturesText, ColorSignaturesText, 0,
                    (int)(sizeY * (double)i / (double)(axis_values_countY - 1)) +
                    (int)(offsetY * ((axis_values_countY - i * 0.25 - 1) / (axis_values_countY - 1)))
                    );
            }
    }

    /// --------------------------------------------------------------------------------------------------------------------
    /// 
    /// Общие методы рисования
    /// Каждый может реализовать дополнительные
    public void DrawGraph(double[] x, double[] y, bool resize = true, GraphType type = GraphType.Lines, double dots_size = 0.0025)
    {
        g.Clear(ColorBackGround);

        if (resize && AUTOSIZE)
            Resize(x, y);

        g.ResetTransform();
        SetOffsets(new int[] { offsetX, offsetY, offsetZ });
        DrawAxis();
        GraphicsSetTransform();

        PenGraphics[0] = new System.Drawing.Pen(ColorPen[0], 0);
        for (int i = 0; i < y.Length - 1; i++)
        {
            if (y[i] <= maxY && y[i] >= minY && y[i + 1] < maxY && y[i + 1] >= minY &&
                x[i] <= maxX && x[i] >= minX && x[i + 1] < maxX && x[i + 1] >= minX)
                switch ((int)type)
                {
                    case 1:
                        g.DrawLine(PenGraphics[0], (float)x[i], (float)y[i], (float)x[i + 1], (float)y[i + 1]);
                        break;
                    case 2:
                        g.FillEllipse(new System.Drawing.SolidBrush(ColorPen[0]),
                            (float)(x[i] - (maxX - minX) * dots_size),
                            (float)(y[i] - (maxY - minY) * dots_size),
                            (float)((maxX - minX) * dots_size),
                            (float)((maxY - minY) * dots_size));
                        break;
                    case 3:
                        g.DrawLine(PenGraphics[0], (float)x[i], (float)y[i], (float)x[i + 1], (float)y[i + 1]);
                        g.FillEllipse(new System.Drawing.SolidBrush(ColorPen[1]),
                            (float)(x[i] - (maxX - minX) * dots_size),
                            (float)(y[i] - (maxY - minY) * dots_size),
                            (float)((maxX - minX) * dots_size),
                            (float)((maxY - minY) * dots_size));
                        break;
                    default:
                        break;
                }
        }

        UploadImage();
    }
    public void DrawGraphLines(double[] x, double[] y, bool resize = true)
    {
        g.Clear(ColorBackGround);

        if (resize && AUTOSIZE)
            Resize(x, y);

        g.ResetTransform();
        SetOffsets(new int[] { offsetX, offsetY, offsetZ });
        DrawAxis();
        GraphicsSetTransform();

        PenGraphics[0] = new System.Drawing.Pen(ColorPen[0], 0);
        for (int i = 0; i < y.Length - 1; i++)
        {
            {
                if (y[i] <= maxY && y[i] >= minY && y[i + 1] < maxY && y[i + 1] >= minY &&
                    x[i] <= maxX && x[i] >= minX && x[i + 1] < maxX && x[i + 1] >= minX)
                    g.DrawLine(PenGraphics[0], (float)x[i], (float)y[i], (float)x[i + 1], (float)y[i + 1]);
            }
        }

        UploadImage();
    }
    public void DrawGraphDots(double[] x, double[] y, bool resize = true)
    {
        g.Clear(ColorBackGround);

        if (resize && AUTOSIZE)
            Resize(x, y);

        g.ResetTransform();
        SetOffsets(new int[] { offsetX, offsetY, offsetZ });
        DrawAxis();
        GraphicsSetTransform();

        PenGraphics[0] = new System.Drawing.Pen(ColorPen[0], 0);
        for (int i = 0; i < y.Length - 1; i++)
        {
            {
                if (y[i] <= maxY && y[i] >= minY && y[i + 1] < maxY && y[i + 1] >= minY &&
                    x[i] <= maxX && x[i] >= minX && x[i + 1] < maxX && x[i + 1] >= minX)
                    g.DrawEllipse(PenGraphics[0],
                        (float)(x[i] - (maxX - minX) * 0.0025),
                        (float)(y[i] - (maxY - minY) * 0.0025),
                        (float)((maxX - minX) * 0.0025),
                        (float)((maxY - minY) * 0.0025));
            }
        }

        UploadImage();
    }
    public void DrawGraphMulti(double[][] x, double[][] y, bool resize = true, GraphType type = GraphType.Lines, double dots_size = 0.0025)
    {
        int count = x.GetLength(0);

        g.Clear(ColorBackGround);

        if (resize)
            Resize(x, y);

        g.ResetTransform();
        SetOffsets(new int[] { offsetX, offsetY, offsetZ });
        DrawAxis();
        GraphicsSetTransform();

        for (int n = 0; n < count; n++)
        {
            PenGraphics[n] = new System.Drawing.Pen(ColorPen[n], 0);
            for (int i = 0; i < y[n].Length - 1; i++)
            {
                if (y[n][i] <= maxY && y[n][i] >= minY && y[n][i + 1] < maxY && y[n][i + 1] >= minY &&
                    x[n][i] <= maxX && x[n][i] >= minX && x[n][i + 1] < maxX && x[n][i + 1] >= minX)
                    switch ((int)type)
                    {
                        case 1:
                            g.DrawLine(
                                PenGraphics[n],
                                (float)x[n][i], (float)y[n][i],
                                (float)x[n][i + 1], (float)y[n][i + 1]);
                            break;
                        case 2:
                            g.FillEllipse(new System.Drawing.SolidBrush(ColorPen[n]),
                                (float)(x[n][i] - (maxX - minX) * dots_size),
                                (float)(y[n][i] - (maxY - minY) * dots_size),
                                (float)((maxX - minX) * dots_size),
                                (float)((maxY - minY) * dots_size));
                            break;
                        case 3:
                            g.DrawLine(
                                PenGraphics[n],
                                (float)x[n][i], (float)y[n][i],
                                (float)x[n][i + 1], (float)y[n][i + 1]);

                            int DotsColorNumber;
                            if (n + 1 < ColorPen.Length)
                                DotsColorNumber = n + 1;
                            else
                                DotsColorNumber = ColorPen.Length - 1 - n;
                            g.FillEllipse(new System.Drawing.SolidBrush(ColorPen[DotsColorNumber]),
                                (float)(x[n][i] - (maxX - minX) * dots_size),
                                (float)(y[n][i] - (maxY - minY) * dots_size),
                                (float)((maxX - minX) * dots_size),
                                (float)((maxY - minY) * dots_size));
                            break;
                        default:
                            break;
                    }
            }
        }
        UploadImage();
    }

    /// --------------------------------------------------------------------------------------------------------------------
    /// 
    /// Каждый наследник переопределит:
    /// метод загрузки области отрисовки 
    abstract public void GetImage(Object img);
    /// метод загрузки bmp в область отрисовки
    abstract public void UploadImage();
}

public enum GraphType : int
{
    Lines = 1,
    Dots = 2,
    Lines_Dots = 3
}








