﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Windows;

public class DRWDrawer : IDrawer 
{
    public PictureBox pic_box;

    public override void GetImage(Object img)
    {
        pic_box = (System.Windows.Forms.PictureBox)img;
        width = pic_box.Width;
        height = pic_box.Height;
    }

    public override void UploadImage()
    {
        pic_box.Image = bmp;
    }
}



