﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Строители
// Батя люого строителя - его интерфейс
public abstract class IBuilder
{
    public static int[] base_offsets = new int[] { 30, 30, 0 };
    public static int[] base_axis_conf = new int[] { 19, 9, 7, 5, 2, 2 };

    public abstract void InitImage();
    public abstract void InitGraphic(bool yn = true);
    public abstract void InitAutosize(bool yn = true);
    public abstract void InitAxis(bool yn = true);
    public abstract void InitBorder(bool yn = true);
    public abstract void SetAxisConf();
    public abstract void SetOffsets();

    public abstract void InitX_Signatures(bool yn = true);
    public abstract void InitY_Signatures(bool yn = true);
    public abstract void InitGraphicName(bool yn = true, string name = "");

    public void Graphic()
    {
        InitAutosize();
        InitGraphic();
    }
    public void Axis()
    {
        SetOffsets();
        SetAxisConf();
        InitAxis();
        InitBorder();
    }
    public void Signatures()
    {
        SetOffsets();
        SetAxisConf();
        InitGraphicName();
        InitX_Signatures();
        InitY_Signatures();
    }
}

// 2D рисовалка для WPF
public class WPFDrawerBuilder : IBuilder
{
    public WPFDrawer drw;
    System.Windows.Controls.Image image;

    public WPFDrawerBuilder(System.Windows.Controls.Image img)
    {
        drw = new WPFDrawer();
        image = img;
        InitImage();
    }

    public override void InitImage()
    {
        drw.GetImage(image);
        drw.InitGraphics();
    }

    public override void InitGraphic(bool yn = true)
    {
        drw.GRAPHIC = yn;
    }
    public override void InitAutosize(bool yn = true)
    {
        drw.AUTOSIZE = yn;
    }

    public override void InitBorder(bool yn = true)
    {
        drw.BORDER = yn;
    }
    public override void InitAxis(bool yn = true)
    {
        drw.AxisConfig(base_axis_conf);
        drw.AXIS = yn;
    }

    public override void SetAxisConf()
    {
        drw.AxisConfig(base_axis_conf);
    }
    public override void SetOffsets()
    {
        drw.SetOffsets(base_offsets);
    }

    public override void InitX_Signatures(bool yn = true)
    {
        drw.X_SIGNATURES = yn;
    }
    public override void InitY_Signatures(bool yn = true)
    {
        drw.Y_SIGNATURES = yn;
    }
    public override void InitGraphicName(bool yn = true, string name = "")
    {
        drw.GraphName = name;
        drw.GRAPH_NAME = yn;
    }
}

// Псевдо 3D рисовалка для WPF
public class WPFDrawer3DBuilder : IBuilder
{
    public WPFDrawer3D drw;
    System.Windows.Controls.Image image;

    public WPFDrawer3DBuilder(System.Windows.Controls.Image img)
    {
        drw = new WPFDrawer3D();
        image = img;
        InitImage();
    }

    public override void InitImage()
    {
        drw.GetImage(image);
        drw.InitGraphics();
    }

    public override void InitGraphic(bool yn = true)
    {
        drw.GRAPHIC = yn;
    }
    public override void InitAutosize(bool yn = true)
    {
        drw.AUTOSIZE = yn;
    }

    public override void InitBorder(bool yn = true)
    {
        drw.BORDER = yn;
    }
    public override void InitAxis(bool yn = true)
    {
        drw.AxisConfig(base_axis_conf);
        drw.AXIS = yn;
    }

    public override void SetAxisConf()
    {
        drw.AxisConfig(base_axis_conf);
    }
    public override void SetOffsets()
    {
        drw.SetOffsets(base_offsets);
    }

    public override void InitX_Signatures(bool yn = true)
    {
        drw.X_SIGNATURES = yn;
    }
    public override void InitY_Signatures(bool yn = true)
    {
        drw.Y_SIGNATURES = yn;
    }
    public override void InitGraphicName(bool yn = true, string name = "")
    {
        drw.graph_name = name;
        drw.GRAPH_NAME = yn;
    }
}

// 2D рисовалка для Forms
public class DrawerBuilder : IBuilder
{
    public DRWDrawer drw;
    System.Windows.Forms.PictureBox image;

    public DrawerBuilder(System.Windows.Forms.PictureBox img)
    {
        drw = new DRWDrawer();
        image = img;
        InitImage();
    }

    public override void InitImage()
    {
        drw.GetImage(image);
        drw.InitGraphics();
    }

    public override void InitGraphic(bool yn = true)
    {
        drw.GRAPHIC = yn;
    }
    public override void InitAutosize(bool yn = true)
    {
        drw.AUTOSIZE = yn;
    }

    public override void InitBorder(bool yn = true)
    {
        drw.BORDER = yn;
    }
    public override void InitAxis(bool yn = true)
    {
        drw.AxisConfig(base_axis_conf);
        drw.AXIS = yn;
    }

    public override void SetAxisConf()
    {
        drw.AxisConfig(base_axis_conf);
    }
    public override void SetOffsets()
    {
        drw.SetOffsets(base_offsets);
    }

    public override void InitX_Signatures(bool yn = true)
    {
        drw.X_SIGNATURES = yn;
    }
    public override void InitY_Signatures(bool yn = true)
    {
        drw.Y_SIGNATURES = yn;
    }
    public override void InitGraphicName(bool yn = true, string name = "")
    {
        drw.GraphName = name;
        drw.GRAPH_NAME = yn;
    }
}

// Псевдо 3D рисовалка для Forms

