﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


// Директор
public class DrawerCollection
{
    public WPFDrawer WPFCreater(Object img = null, bool axis = false, bool signatures = false, string graph_name = "")
    {
        var builder = new WPFDrawerBuilder((System.Windows.Controls.Image)img);

        builder.Graphic();
        if (axis)
            builder.Axis();
        if (signatures)
            builder.Signatures();
        if (graph_name != "")
            builder.InitGraphicName(true, graph_name);

        return builder.drw;
    }

    public WPFDrawer3D WPF3DCreater(Object img = null, bool axis = false, bool signatures = false, string graph_name = "")
    {
        var builder = new WPFDrawer3DBuilder((System.Windows.Controls.Image)img);

        builder.Graphic();
        if (axis)
            builder.Axis();
        if (signatures)
            builder.Signatures();
        if (graph_name != "")
            builder.InitGraphicName(true, graph_name);

        return builder.drw;
    }

    public DRWDrawer DRWCreator(Object img = null, bool axis = false, bool signatures = false, string graph_name = "")
    {
        var builder = new DrawerBuilder((System.Windows.Forms.PictureBox)img);

        builder.Graphic();
        if (axis)
            builder.Axis();
        if (signatures)
            builder.Signatures();
        if (graph_name != "")
            builder.InitGraphicName(true, graph_name);

        return builder.drw;
    }
}