﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


public class WPFDrawer : IDrawer
{
    public System.Windows.Controls.Image image;

    public override void GetImage(object img)
    {
        image = (System.Windows.Controls.Image)img;
        width = (int)image.Width;
        height = (int)image.Height;
    }

    public override void UploadImage()
    {
        System.Drawing.Image img = bmp;
        System.Windows.Media.Imaging.BitmapImage bitmapImage = convert_to_WPFImage(img);
        image.Source = bitmapImage;
    }

    public static BitmapImage convert_to_WPFImage(System.Drawing.Image _img)
    {
        MemoryStream ms = new MemoryStream();  // no using here! BitmapImage will dispose the stream after loading
        _img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);

        BitmapImage img = new BitmapImage();
        img.BeginInit();
        img.CacheOption = BitmapCacheOption.OnLoad;
        img.StreamSource = ms;
        img.EndInit();
        return img;
    }
}







