﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Строитель WPFDrawer
public class WPFDrawerBuilder
{
    public WPFDrawer drw;

    public WPFDrawerBuilder() { drw = new WPFDrawer(); }

    public void InitImage(System.Windows.Controls.Image img = null)
    {
        drw.GetImage(img);
        drw.InitGraphics();
    }
    public void InitAutosize(bool yn = true)
    {
        drw.AUTOSIZE = yn;
    }
    public void InitAxis(bool yn = true)
    {
        drw.AXIS = yn;
    }
    public void InitBorder(bool yn = true)
    {
        drw.BORDER = yn;
    }
    public void InitGraphic(bool yn = true)
    {
        drw.GRAPHIC = yn;
    }
    public void InitX_Signatures(bool yn = true)
    {
        drw.X_SIGNATURES = yn;
    }
    public void InitY_Signatures(bool yn = true)
    {
        drw.Y_SIGNATURES = yn;
    }
    public void InitGraphicName(bool yn = true)
    {
        drw.GRAPH_NAME = yn;
    }

    public void SimpleInit(System.Windows.Controls.Image img = null)
    {
        InitAutosize();
        InitGraphic();
        InitImage(img);
    }
}

// Директор
public class WPFDrawerCollection
{
    private WPFDrawerBuilder builder;

    public WPFDrawer CreateOnlyGraphic(Object img = null)
    {
        builder = new WPFDrawerBuilder();

        builder.SimpleInit((System.Windows.Controls.Image)img);

        return builder.drw;
    }
    public WPFDrawer get_drawer()
    {
        return builder.drw;
    }
}