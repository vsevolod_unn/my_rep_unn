﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


public class WPFDrawer
{
    string graph_name;
    public System.Windows.Controls.Image image;
    public System.Drawing.Bitmap bmp;
    public System.Drawing.Graphics g;
    public System.Drawing.Pen pen, axis_pen;

    public System.Drawing.Color
        ColorBackGround = System.Drawing.Color.White,
        ColorPen = System.Drawing.Color.Red,
        ColorBrush = System.Drawing.Color.Black;

    public int width, height;
    public double minX, maxX, minY, maxY, minZ, maxZ;
    public int sizeX, sizeY, sizeZ;
    public int offsetX, offsetY, offsetZ;

    public int axis_countX = 9, axis_countY = 9, axis_values_countX = 3, axis_values_countY = 3;
    public bool GRAPHIC, AXIS, GRAPH_NAME, X_SIGNATURES, Y_SIGNATURES, BORDER, AUTOSIZE;

    public WPFDrawer() { }
    public WPFDrawer(System.Windows.Controls.Image _img, string _graph_name)
    {
        graph_name = _graph_name;
        image = _img;
        width = (int)image.Width;
        height = (int)image.Height;

        minX = 0;
        minY = 0;
        minZ = 0;
        maxX = 1;
        maxY = 1;
        maxZ = 1;

        InitGraphics();
    }
    public WPFDrawer(System.Windows.Controls.Image _img, Array _sizes, string _graph_name)
    {
        graph_name = _graph_name;
        image = _img;
        width = (int)image.Width;
        height = (int)image.Height;

        InitGraphics();
        if (_sizes == null || _sizes.Length == 0)
        {
            minX = 0;
            minY = 0;
            minZ = 0;
            maxX = 1;
            maxY = 1;
            maxZ = 1;
        }
        else
        {
            Resize(_sizes);
        }
    }
    public WPFDrawer(System.Windows.Controls.Image _img, Array _sizes, string _graph_name, Array _offsets)
    {
        graph_name = _graph_name;
        image = _img;
        width = (int)image.Width;
        height = (int)image.Height;

        InitGraphics();
        if (_sizes == null || _sizes.Length == 0)
        {
            minX = 0;
            minY = 0;
            minZ = 0;
            maxX = 1;
            maxY = 1;
            maxZ = 1;
        }
        else
        {
            Resize(_sizes);
        }
        SetOffsets(_offsets);
    }
    public void GetImage(System.Windows.Controls.Image _img)
    {
        image = _img;
        width = (int)image.Width;
        height = (int)image.Height;
    }

    public void InitGraphics()
    {
        bmp = new System.Drawing.Bitmap(width, height);
        g = System.Drawing.Graphics.FromImage(bmp);
        g.Clear(System.Drawing.Color.Black);
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
    }
    public void Resize(Array _sizes)
    {
        double[] sizes = new double[6];
        for (int i = 0; i < 6; i++)
        {
            sizes[i] = (int)(i * 2) / 6; ;
        }
        for (int i = 0; i < _sizes.Length; i++)
        {
            sizes[i] = ((double[])_sizes)[i];
        }
        minX = sizes[0];
        maxX = sizes[1];
        minY = sizes[2];
        maxY = sizes[3];
        minZ = sizes[4];
        maxZ = sizes[5];
    }
    public void Resize(double[] x, double[] y)
    {
        minX = double.MaxValue;
        maxX = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] < minX) minX = x[i];
            if (x[i] > maxX) maxX = x[i];
        }

        minY = double.MaxValue;
        maxY = double.MinValue;
        for (int i = 0; i < y.Length; i++)
        {
            if (y[i] < minY) minY = y[i];
            if (y[i] > maxY) maxY = y[i];
        }
    }
    public void Resize(double[][] x, double[][] y)
    {
        double _minX = minX;
        double _maxX = maxX;
        double _minY = minY;
        double _maxY = maxY;

        for (int i = 0; i < x.GetLength(0); i++)
        {
            Resize(x[i], y[i]);
            if (i == 0)
            {
                _minX = minX;
                _maxX = maxX;
                _minY = minY;
                _maxY = maxY;
            }
            else
            {
                if (_minX < minX) minX = _minX;
                if (_minY < minY) minY = _minY;
                if (_maxX > maxX) maxX = _maxX;
                if (_maxY > maxY) maxY = _maxY;
            }
        }
    }
    public void Resize(double[] x, double[] y, double[][] z)
    {
        minX = double.MaxValue;
        maxX = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] < minX) minX = x[i];
            if (x[i] > maxX) maxX = x[i];
        }

        minY = double.MaxValue;
        maxY = double.MinValue;
        for (int i = 0; i < y.Length; i++)
        {
            if (y[i] < minY) minY = y[i];
            if (y[i] > maxY) maxY = y[i];
        }

        minZ = double.MaxValue;
        maxZ = double.MinValue;
        for (int i = 0; i < x.Length; i++)
        {
            for (int j = 0; j < y.Length; j++)
            {
                if (z[i][j] < minZ) minZ = z[i][j];
                if (z[i][j] > maxZ) maxZ = z[i][j];
            }
        }
    }
    public void Resize(double[][] x, double[][] y, double[][] z)
    {
        double _minX = minX;
        double _maxX = maxX;
        double _minY = minY;
        double _maxY = maxY;
        double _minZ = minZ;
        double _maxZ = maxZ;

        for (int i = 0; i < x.GetLength(0); i++)
        {
            Resize(x[i], y[i], z);
            if (_minX < minX) minX = _minX;
            if (_minY < minY) minY = _minY;
            if (_minZ < minZ) minZ = _minZ;
            if (_maxX > maxX) maxX = _maxX;
            if (_maxY > maxY) maxY = _maxY;
            if (_maxZ > maxZ) maxZ = _maxZ;
        }
    }
    public void ResizeX(double _minX, double _maxX)
    {
        minX = _minX;
        maxX = _maxX;
    }
    public void GraphicsSetTransform()
    {
        g.ResetTransform();
        g.TranslateTransform((float)offsetX, (float)offsetY);
        g.ScaleTransform((float)((double)(width - 2 * offsetX) / (maxX - minX)), -(float)((double)(height - 2 * offsetY) / (maxY - minY)));
        g.TranslateTransform(-(float)minX, -(float)maxY);
    }
    public void SetOffsets(Array _offsets)
    {
        offsetX = ((int[])_offsets)[0];
        offsetY = ((int[])_offsets)[1];
        offsetZ = ((int[])_offsets)[2];

        sizeX = (int)(width - 2 * offsetX);
        sizeY = (int)(height - 2 * offsetY);
    }
    public void AxisConfig(Array _axis_count)
    {
        if (_axis_count.Length >= 2)
        {
            axis_countX = ((int[])_axis_count)[0];
            axis_countY = ((int[])_axis_count)[1];
        }
        if (_axis_count.Length >= 4)
        {
            axis_values_countX = ((int[])_axis_count)[2];
            axis_values_countY = ((int[])_axis_count)[3];
        }
    }

    public void DrawGraph(double[] x, double[] y, bool resize = true)
    {
        g.Clear(ColorBackGround);

        if (resize && AUTOSIZE)
            Resize(x, y);

        g.ResetTransform();
        DrawAxis();
        GraphicsSetTransform();

        pen = new System.Drawing.Pen(ColorPen, 0);
        for (int i = 0; i < y.Length - 1; i++)
        {
            {
                if (y[i] <= maxY && y[i] >= minY && y[i + 1] < maxY && y[i + 1] >= minY)
                    g.DrawLine(pen, (float)x[i], (float)y[i], (float)x[i + 1], (float)y[i + 1]);
            }
        }
        System.Drawing.Image img = bmp;
        BitmapImage bitmapImage = convert_to_WPFImage(img);
        image.Source = bitmapImage;
    }
    public void DrawGraphMulti(double[][] x, double[][] y, System.Drawing.Color[] colors, bool resize = false)
    {
        int count = colors.Length;

        g.Clear(ColorBackGround);

        if (resize)
            Resize(x, y);

        g.ResetTransform();
        DrawAxis();
        GraphicsSetTransform();

        System.Drawing.Pen[] pens = new System.Drawing.Pen[count];

        for (int graph_number = 0; graph_number < count; graph_number++)
        {
            pens[graph_number] = new System.Drawing.Pen(colors[graph_number], 0);
            for (int i = 0; i < y[graph_number].Length - 1; i++)
            {
                if (y[graph_number][i] <= maxY && y[graph_number][i] >= minY && y[graph_number][i + 1] < maxY && y[graph_number][i + 1] >= minY)
                    g.DrawLine(
                        pens[graph_number],
                        (float)x[graph_number][i], (float)y[graph_number][i],
                        (float)x[graph_number][i + 1], (float)y[graph_number][i + 1]);
            }
        }
        System.Drawing.Image img = bmp;
        BitmapImage bitmapImage = convert_to_WPFImage(img);
        image.Source = bitmapImage;

    }
    private void DrawAxis()
    {
        if (AXIS)
        {
            axis_pen = new System.Drawing.Pen(System.Drawing.Brushes.Gray, 0);
            float[] dash_attributes = new float[] { 6, 4, 2, 4 };
            axis_pen.DashPattern = dash_attributes;

            System.Drawing.Point pt1;
            System.Drawing.Point pt2;
            for (int i = 0; i < axis_countX; i++)
            {
                pt1 = new System.Drawing.Point((int)(offsetX + (double)(sizeX * i) / (double)(axis_countX - 1)), offsetY);
                pt2 = new System.Drawing.Point((int)(offsetX + (double)(sizeX * i) / (double)(axis_countX - 1)), height - offsetY);
                g.DrawLine(axis_pen, pt1, pt2);
            }
            for (int i = 0; i < axis_countY; i++)
            {
                pt1 = new System.Drawing.Point(offsetX, (int)(offsetY + (double)(sizeY * i) / (double)(axis_countY - 1)));
                pt2 = new System.Drawing.Point(sizeX + offsetX, (int)(offsetY + (double)(sizeY * i) / (double)(axis_countY - 1)));
                g.DrawLine(axis_pen, pt1, pt2);
            }
        }
        if (GRAPH_NAME)
            g.DrawString(graph_name, new System.Drawing.Font("Colibri", 10), System.Drawing.Brushes.Black, sizeX / 2 - offsetX, offsetY / 4);

        if (X_SIGNATURES)
            for (int i = 0; i < axis_values_countX; i++)
            {
                g.DrawString(
                    ((maxX - minX) * (double)i / (double)(axis_values_countX - 1) + minX).ToString("f2"),
                    new System.Drawing.Font("Colibri", 7), System.Drawing.Brushes.Black,
                    (int)(sizeX * (double)i / (double)(axis_values_countX - 1)) +
                    (int)(offsetX * ((axis_values_countX - i * 0.25 - 1) / (axis_values_countX - 1))),
                    sizeY + offsetY);
            }

        if (Y_SIGNATURES)
            for (int i = 0; i < axis_values_countY; i++)
            {
                g.DrawString(
                    ((minY - maxY) * (double)i / (double)(axis_values_countY - 1) + maxY).ToString("f2"),
                    new System.Drawing.Font("Colibri", 7), System.Drawing.Brushes.Black, 0,
                    (int)(sizeY * (double)i / (double)(axis_values_countY - 1)) +
                    (int)(offsetY * ((axis_values_countY - i * 0.25 - 1) / (axis_values_countY - 1)))
                    );
            }
    }
    public static BitmapImage convert_to_WPFImage(System.Drawing.Image _img)
    {
        MemoryStream ms = new MemoryStream();  // no using here! BitmapImage will dispose the stream after loading
        _img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);

        BitmapImage img = new BitmapImage();
        img.BeginInit();
        img.CacheOption = BitmapCacheOption.OnLoad;
        img.StreamSource = ms;
        img.EndInit();
        return img;
    }
}







