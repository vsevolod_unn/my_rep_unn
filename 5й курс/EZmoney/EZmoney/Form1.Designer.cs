﻿namespace EZmoney
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.MoneyTB = new System.Windows.Forms.RichTextBox();
            this.StartBT = new System.Windows.Forms.Button();
            this.StopBT = new System.Windows.Forms.Button();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            this.GetTB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MoneyTB
            // 
            this.MoneyTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MoneyTB.EnableAutoDragDrop = true;
            this.MoneyTB.Location = new System.Drawing.Point(0, 0);
            this.MoneyTB.Name = "MoneyTB";
            this.MoneyTB.Size = new System.Drawing.Size(490, 493);
            this.MoneyTB.TabIndex = 0;
            this.MoneyTB.Text = "";
            // 
            // StartBT
            // 
            this.StartBT.Location = new System.Drawing.Point(248, 464);
            this.StartBT.Name = "StartBT";
            this.StartBT.Size = new System.Drawing.Size(118, 29);
            this.StartBT.TabIndex = 1;
            this.StartBT.Text = "Начать";
            this.StartBT.UseVisualStyleBackColor = true;
            this.StartBT.Click += new System.EventHandler(this.StartBT_Click);
            // 
            // StopBT
            // 
            this.StopBT.Location = new System.Drawing.Point(372, 464);
            this.StopBT.Name = "StopBT";
            this.StopBT.Size = new System.Drawing.Size(118, 29);
            this.StopBT.TabIndex = 2;
            this.StopBT.Text = "Стоп";
            this.StopBT.UseVisualStyleBackColor = true;
            this.StopBT.Click += new System.EventHandler(this.StopBT_Click);
            // 
            // MainTimer
            // 
            this.MainTimer.Interval = 1000;
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // GetTB
            // 
            this.GetTB.Location = new System.Drawing.Point(0, 464);
            this.GetTB.Name = "GetTB";
            this.GetTB.Size = new System.Drawing.Size(118, 29);
            this.GetTB.TabIndex = 3;
            this.GetTB.Text = "Вывести";
            this.GetTB.UseVisualStyleBackColor = true;
            this.GetTB.Click += new System.EventHandler(this.GetTB_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 493);
            this.Controls.Add(this.GetTB);
            this.Controls.Add(this.StopBT);
            this.Controls.Add(this.StartBT);
            this.Controls.Add(this.MoneyTB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Бабосозарабатыватель v 1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox MoneyTB;
        private System.Windows.Forms.Button StartBT;
        private System.Windows.Forms.Button StopBT;
        private System.Windows.Forms.Timer MainTimer;
        private System.Windows.Forms.Button GetTB;
    }
}

