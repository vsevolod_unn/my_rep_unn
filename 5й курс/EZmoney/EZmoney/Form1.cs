﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;
using WMPLib;

namespace EZmoney
{
    public partial class Form1 : Form
    {
        int BalanceSession;
        int BalanceTotal;
        bool GetBT_Pressed;
        WMPLib.WindowsMediaPlayer wplayer;

        public Form1()
        {
            InitializeComponent();
            StopBT.Enabled = false;
            GetTB.Enabled = false;

            BalanceTotal = Convert.ToInt32(File.ReadAllText("win32/BalanceTotal"));
            BalanceSession = 0;
            GetBT_Pressed = false;

            wplayer = new WMPLib.WindowsMediaPlayer();
            wplayer.URL = "win32/money";
            wplayer.controls.play();
        }

        private void StartBT_Click(object sender, EventArgs e)
        {
            if (GetBT_Pressed)
            {
                MoneyTB.Text = "";
                GetBT_Pressed = false;
            }
            GetTB.Enabled = false;
            StopBT.Enabled = true;
            MainTimer.Start();
        }

        private void StopBT_Click(object sender, EventArgs e)
        {
            StartBT.Text = "Продолжить";
            MainTimer.Stop();
            GetTB.Enabled = true;
        }

        private void GetTB_Click(object sender, EventArgs e)
        {
            MoneyTB.Text += "\nВы успешно вывели " + Format(BalanceSession) + " P\n";

            BalanceTotal += BalanceSession;
            BalanceSession = 0;

            MoneyTB.Text += "Ваш баланс: " + Format(BalanceTotal) + " P\n\n";
            StartBT.Text = "Начать";
            GetBT_Pressed = true;
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            BalanceSession += 32000;
            MoneyTB.Text += "За эту секунду вы заработали 32.000 Р, итого: " + Format(BalanceSession) + " P\n";
            wplayer.controls.play();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            File.WriteAllText("win32/BalanceTotal", BalanceTotal.ToString());
        }

        public string Format(int bank)
        {
            string str = bank.ToString();
            string str1 = "";
            for (int i = 0; i < str.Length; i++)
            {
                str1 += str[str.Length - 1 - i];
                if (i % 3 == 2 && i != str.Length - 1)
                    str1 += ".";
            }
            str = "";
            for (int i = 0; i < str1.Length; i++)
            {
                str += str1[str1.Length - 1 - i];
            }
            return str;
        }
    }
}
