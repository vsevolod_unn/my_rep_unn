﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HadamarMatrixNS;

namespace IOTexts
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text ="";

            HadamarMatrix Hm = new HadamarMatrix(Convert.ToInt32(textBox2.Text));

            for (int i = 0; i < Hm.size; i++)
            {
                for (int j = 0; j < Hm.size; j++)
                {
                    if (Hm.M[i][j] < 0)
                        richTextBox1.Text += Hm.M[i][j].ToString() + " ";
                    else
                        richTextBox1.Text += " " + Hm.M[i][j].ToString() + " ";
                }
                richTextBox1.Text += "\n";
            }
        }
    }
}
