﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Windows.Forms;

namespace IT_task_1
{
    public partial class Form1 : Form
    {
        int M, sizeW, sizeH, pH, pW, last_radius;
        double dmin, dmax, Amin, Amax, percent, filter_percent, sko;
        double[,] pic, noised_pic, new_pic, expanded_pic, noise, spectre_to_draw;

        Complex[] line, column;
        Complex buf;
        Complex[,] c_pic, spectre, filtred_spectre, spectre_buf;

        bool draw_noise_flag = false;
        bool draw_filtered_flag = true;

        bool filtred;

        Random rand;

        OpenFileDialog OD;

        Image loaded_pic, img2, img3;

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public Form1()
        {
            InitializeComponent();
            FilterBT.Enabled = false;
        }

        private void LoadBT_Click(object sender, EventArgs e)
        {
            OD = new OpenFileDialog();
            OD.Filter = "Файлы изображений|*.bmp;*.jpg;*.png";

            if (OD.ShowDialog() != DialogResult.OK) return;

            try
            {
                loaded_pic = Image.FromFile(OD.FileName);
                PictureCB.Checked = true;
            }
            catch
            {
                MessageBox.Show("Error");
                return;
            }

            PictureCB.Enabled = true;
            FilterBT.Enabled = false;
            Picture_1.Image = loaded_pic;
        }

        private void InitBT_Click(object sender, EventArgs e)
        {
            // ------------------------------------------------------------------------------------------------------------------
            // Начальная инициализация
            // ------------------------------------------------------------------------------------------------------------------
            dmin = Convert.ToInt32(d_minTB.Text);
            dmax = Convert.ToInt32(d_maxTB.Text);
            Amin = Convert.ToInt32(A_minTB.Text);
            Amax = Convert.ToInt32(A_maxTB.Text);
            percent = Convert.ToDouble(pTB.Text) / 100;
            filter_percent = Convert.ToDouble(FilterPercentTB.Text) / 100;

            M = Convert.ToInt32(MTB.Text);
            if (PictureCB.Checked)
            {
                sizeW = loaded_pic.Width;
                sizeH = loaded_pic.Height;

                SizeWTB.Text = sizeW.ToString();
                SizeHTB.Text = sizeH.ToString();
            }
            else
            {
                sizeW = Convert.ToInt32(SizeWTB.Text);
                sizeH = Convert.ToInt32(SizeHTB.Text);
            }

            // Проверка на степени двойки
            if ((int)Math.Log(sizeW, 2) != Math.Log(sizeW, 2))
                pW = (int)Math.Pow(2, (int)Math.Log(sizeW, 2) + 1);
            else
                pW = (int)Math.Pow(2, (int)Math.Log(sizeW, 2));
            if ((int)Math.Log(sizeH, 2) != Math.Log(sizeH, 2))
                pH = (int)Math.Pow(2, (int)Math.Log(sizeH, 2) + 1);
            else
                pH = (int)Math.Pow(2, (int)Math.Log(sizeH, 2));

            rand = new Random();

            pic = new double[sizeH, sizeW];
            noise = new double[sizeH, sizeW];
            noised_pic = new double[sizeH, sizeW];

            spectre_to_draw = new double[pH, pW];
            expanded_pic = new double[pH, pW];
            c_pic = new Complex[pH, pW];
            spectre = new Complex[pH, pW];
            spectre_buf = new Complex[pH, pW];
            filtred_spectre = new Complex[pH, pW];
            line = new Complex[pW];
            column = new Complex[pH];

            double temp;
            draw_noise_flag = false;
            draw_filtered_flag = true;
            filtred = false;

            if (PictureCB.Checked)
            {
                double[,] picture = new double[sizeW, sizeH];
                Bitmap bmp = new Bitmap(loaded_pic);
                for (int i = 0; i < sizeH; i++)
                    for (int j = 0; j < sizeW; j++)
                    {
                        temp = 0;
                        for (int k = 0; k < 12; k++)
                            temp += rand.NextDouble() / 12;
                        noise[i, j] = temp;
                        picture[j, i] = pic[i, j] = 1.0 / 3.0 / 255.0 * (bmp.GetPixel(j, i).R +
                                                         bmp.GetPixel(j, i).G +
                                                         bmp.GetPixel(j, i).B);
                    }
            }
            else
            {
                // Создание куполов, шумов и зашумленной картинки
                for (int i = 0; i < sizeH; i++)
                    for (int j = 0; j < sizeW; j++)
                    {
                        temp = 0;
                        for (int k = 0; k < 12; k++)
                            temp += rand.NextDouble() / 12;
                        noise[i, j] = temp;
                        pic[i, j] = 0;
                    }
                for (int k = 0; k < M; k++)
                {
                    double a, dx, dy;
                    int x0, y0;
                    a = Amin + rand.NextDouble() * (Amax - Amin);
                    dx = dmin + rand.NextDouble() * (dmax - dmin);
                    dy = dmin + rand.NextDouble() * (dmax - dmin);
                    x0 = (int)(rand.NextDouble() * sizeH);
                    y0 = (int)(rand.NextDouble() * sizeW);
                    for (int i = 0; i < sizeH; i++)
                        for (int j = 0; j < sizeW; j++)
                        {
                            pic[i, j] += a * Math.Exp(-Math.Pow(i - x0, 2) / 2 / dx / dx - Math.Pow(j - y0, 2) / 2 / dy / dy);
                        }
                }
            }
            // Отрисовка картины
            noise = get_real_noise_arr(pic, noise, sizeH, sizeW, percent);
            for (int i = 0; i < sizeH; i++)
                for (int j = 0; j < sizeW; j++)
                    noised_pic[i, j] = pic[i, j] + noise[i, j];

            if (PictureCB.Checked)
            {
                double[,] nsd_pic = new double[sizeW, sizeH];
                for (int i = 0; i < sizeH; i++)
                    for (int j = 0; j < sizeW; j++)
                        nsd_pic[j, i] = pic[i, j] + noise[i, j];
                draw(Picture_1, nsd_pic, sizeW, sizeH);
            }
            else
                draw(Picture_1, noised_pic, sizeH, sizeW);

            // ------------------------------------------------------------------------------------------------------------------
            // Формирование спектра
            // ------------------------------------------------------------------------------------------------------------------
            expanded_pic = ExpandPic(noised_pic, pH, pW);
            Complex c_buf;
            for (int i = 0; i < pH; i++)
                for (int j = 0; j < pW; j++)
                {
                    c_buf = new Complex(expanded_pic[i, j], 0);
                    spectre[i, j] = c_pic[i, j] = c_buf;
                }

            // Все строки
            for (int i = 0; i < pH; i++)
            {
                for (int j = 0; j < pW; j++)
                {
                    line[j] = spectre[i, j];
                }
                line = Fourea(line, pW, -1);
                for (int j = 0; j < pW; j++)
                {
                    spectre[i, j] = line[j];
                }
            }

            // Все столбцы
            for (int j = 0; j < pW; j++)
            {
                for (int i = 0; i < pH; i++)
                {
                    column[i] = spectre[i, j];
                }
                column = Fourea(column, pH, -1);
                for (int i = 0; i < pH; i++)
                {
                    spectre[i, j] = column[i];
                }
            }

            // Копирование спектра
            for (int i = 0; i < pH; i++)
                for (int j = 0; j < pW; j++)
                    spectre_buf[i, j] = spectre[i, j];
            // Отрисовка спектра
            draw(Picture_2, convert_for_drawing(spectre, pH, pW), pH, pW);

            PictureCB.Enabled = false;
            LogMashtabCB.Enabled = true;
            FilterBT.Enabled = true;
        }

        private void FilterBT_Click(object sender, EventArgs e)
        {
            // ------------------------------------------------------------------------------------------------------------------
            // Фильтрация спектра
            // ------------------------------------------------------------------------------------------------------------------
            filtred = true;
            filter_percent = Convert.ToDouble(FilterPercentTB.Text) / 100;
            draw_filtered_flag = true;
            sko = 0;

            // Копируем спектр
            for (int i = 0; i < pH; i++)
                for (int j = 0; j < pW; j++)
                    filtred_spectre[i, j] = spectre_buf[i, j] = spectre[i, j];

            // Считаем энергию
            double full_energy = 0;
            for (int i = 0; i < pH; i++)
                for (int j = 0; j < pW; j++)
                    full_energy += Math.Pow(spectre_buf[i, j].Magnitude, 2);

            // Начинаем отчистку спектра
            double energy = 0;
            last_radius = (int)(pW / 2 * 1.4);
            for (int radius = 1; radius < pW / 2; radius++)
            {
                for (int i = 0; i < pH / 2; i++)
                    for (int j = 0; j < pW / 2; j++)
                    {
                        if (Math.Sqrt(i * i + j * j) <= radius)
                        {
                            energy += Math.Pow(spectre[i, j].Magnitude, 2);
                            energy += Math.Pow(spectre[pH - 1 - i, j].Magnitude, 2);
                            energy += Math.Pow(spectre[pH - 1 - i, pW - 1 - j].Magnitude, 2);
                            energy += Math.Pow(spectre[i, pW - 1 - j].Magnitude, 2);
                        }
                    }
                if (energy < full_energy * filter_percent) energy = 0;
                else
                {
                    last_radius = radius;
                    break;
                }
            }
            for (int i = 0; i < pH / 2; i++)
                for (int j = 0; j < pW / 2; j++)
                    if (Math.Sqrt(i * i + j * j) > last_radius)
                    {
                        buf = new Complex(0, 0);
                        filtred_spectre[i, j] = spectre_buf[i, j] = buf;
                        filtred_spectre[pH - 1 - i, j] = spectre_buf[pH - 1 - i, j] = buf;
                        filtred_spectre[pH - 1 - i, pW - 1 - j] = spectre_buf[pH - 1 - i, pW - 1 - j] = buf;
                        filtred_spectre[i, pW - 1 - j] = spectre_buf[i, pW - 1 - j] = buf;
                    }
            draw_special(Picture_2, convert_for_drawing(filtred_spectre, pH, pW), pH, pW);
            // ------------------------------------------------------------------------------------------------------------------
            // Получение результатов
            // ------------------------------------------------------------------------------------------------------------------
            // Все строки
            for (int i = 0; i < pH; i++)
            {
                for (int j = 0; j < pW; j++)
                {
                    line[j] = spectre_buf[i, j];
                }
                line = Fourea(line, pW, 1);
                for (int j = 0; j < pW; j++)
                {
                    spectre_buf[i, j] = line[j];
                }
            }

            // Все столбцы
            for (int j = 0; j < pW; j++)
            {
                for (int i = 0; i < pH; i++)
                {
                    column[i] = spectre_buf[i, j];
                }
                column = Fourea(column, pH, 1);
                for (int i = 0; i < pH; i++)
                {
                    spectre_buf[i, j] = column[i];
                }
            }

            // Вывод результатов
            //if ()
            new_pic = new double[pW, pH];
            double[,] new_pic2 = new double[pH, pW];
            for (int i = 0; i < pH; i++)
                for (int j = 0; j < pW; j++)
                    if (PictureCB.Checked)
                        new_pic[j, i] = spectre_buf[i, j].Magnitude;
                    else
                        new_pic2[i, j] = spectre_buf[i, j].Magnitude;
            if (PictureCB.Checked)
                draw(Picture_3, new_pic, pW, pH);
            else
                draw(Picture_3, new_pic2, pH, pW);

            // Расчет СКО
            for (int i = 0; i < sizeH; i++)
                for (int j = 0; j < sizeW; j++)
                    if (PictureCB.Checked)
                        sko += 1.0 / pW / pH * Math.Pow(new_pic[j, i] - expanded_pic[i, j], 2);
                    else
                        sko += 1.0 / pW / pH * Math.Pow(new_pic2[i, j] - expanded_pic[i, j], 2);

            PictureCB.Enabled = true;
            string buf1 = sko.ToString("f8");
            SKO_TB.Text = buf1;
        }

        /// <summary>
        /// Растягивает матрицу до указанных значений ширины/высоты.
        /// </summary>
        /// <param name="src"> Исходная матрица </param>
        /// <param name="reqH"> Необходимая ширина </param>
        /// <param name="reqW"> Необходимая высота </param>
        /// <returns> Растянутая до указанных значений матрица </returns>
        private double[,] ExpandPic(double[,] src, int reqH, int reqW)
        {
            double[,] result = new double[reqH, reqW];

            // Идем по строкам
            double[] bufferBefore = new double[src.GetLength(0)];
            double[] bufferAfter = new double[reqH];

            for (int i = 0; i < src.GetLength(1); i++)
            {
                for (int j = 0; j < src.GetLength(0); j++)
                {
                    bufferBefore[j] = src[j, i];
                }

                bufferAfter = ExpandArr(bufferBefore, reqH);

                for (int j = 0; j < reqH; j++)
                {
                    result[j, i] = bufferAfter[j];
                }
            }

            // Идем по столбцам
            bufferBefore = new double[src.GetLength(1)];
            bufferAfter = new double[reqW];

            for (int i = 0; i < reqH; i++)
            {
                for (int j = 0; j < src.GetLength(1); j++)
                {
                    bufferBefore[j] = result[i, j];
                }

                bufferAfter = ExpandArr(bufferBefore, reqW);

                for (int j = 0; j < reqW; j++)
                {
                    result[i, j] = bufferAfter[j];
                }
            }
            return result;
        }

        /// <summary>
        /// Растягивает массив данных
        /// </summary>
        /// <param name="src"> Исходный массив </param>
        /// <param name="requiredLength"> Требуемая длина нового массива </param>
        /// <returns> Интерполированный массив </returns>
        private double[] ExpandArr(double[] src, int requiredLength)
        {
            double[] destination = new double[requiredLength];

            destination[0] = src[0];
            destination[src.Length - 1] = src[src.Length - 1];

            for (int i = 1; i < requiredLength - 1; i++)
            {
                double jd = ((double)i * (double)(src.Length - 1) / (double)(requiredLength - 1));
                int j = (int)jd;
                destination[i] = src[j] + (src[j + 1] - src[j]) * (jd - (double)j);
            }
            return destination;
        }

        /*private void ExpandedPicCB_CheckedChanged(object sender, EventArgs e)
{
    if (ExpandedPicCB.Checked)
    {
        new_pic = new double[pW, pH];
        double[,] new_pic2 = new double[pH, pW];
        for (int i = 0; i < pH; i++)
            for (int j = 0; j < pW; j++)
                if (PictureCB.Checked)
                    new_pic[j, i] = spectre_buf[i + 0 * (int)(pH - sizeH) / 2, j + 0 * (int)(pW - sizeW) / 2].Magnitude;
                else
                    new_pic2[i, j] = spectre_buf[i + 0 * (int)(pH - sizeH) / 2, j + 0 * (int)(pW - sizeW) / 2].Magnitude;
        if (PictureCB.Checked)
            draw(Picture_3, new_pic, pW, pH);
        else
            draw(Picture_3, new_pic2, pH, pW);
        draw(Picture_1, expanded_pic, pH, pW);
    }
    else
    {
        new_pic = new double[sizeW, sizeH];
        double[,] new_pic2 = new double[sizeH, sizeW];
        for (int i = 0; i < sizeH; i++)
            for (int j = 0; j < sizeW; j++)
                if (PictureCB.Checked)
                    new_pic[j, i] = spectre_buf[i + 0 * (int)(pH - sizeH) / 2, j + 0 * (int)(pW - sizeW) / 2].Magnitude;
                else
                    new_pic2[i, j] = spectre_buf[i + 0 * (int)(pH - sizeH) / 2, j + 0 * (int)(pW - sizeW) / 2].Magnitude;
        if (PictureCB.Checked)
            draw(Picture_3, new_pic, sizeW, sizeH);
        else
            draw(Picture_3, new_pic2, sizeH, sizeW);
        draw(Picture_1, pic, sizeH, sizeW);
    }
}*/

        private void LogMashtabCB_CheckedChanged(object sender, EventArgs e)
        {
            if (filtred)
                draw_special(Picture_2, convert_for_drawing(filtred_spectre, pH, pW), pH, pW);
            else
                draw_special(Picture_2, convert_for_drawing(spectre, pH, pW), pH, pW);
        }


        private Complex[] Fourea(Complex[] spectr, int n, int direct)
        {
            Complex buf;
            int i, j, istep;
            int m, mmax;
            double r, r1, theta, w_r, w_i, temp_r, temp_i;
            double pi = 3.1415926f;

            r = pi * direct;
            j = 0;
            for (i = 0; i < n; i++)
            {
                if (i < j)
                {
                    temp_r = spectr[j].Real;
                    temp_i = spectr[j].Imaginary;
                    buf = new Complex(spectr[i].Real, spectr[i].Imaginary);
                    spectr[j] = buf;
                    buf = new Complex(temp_r, temp_i);
                    spectr[i] = buf;
                }
                m = n >> 1;
                while (j >= m) { j -= m; m = (m + 1) / 2; }
                j += m;
            }

            mmax = 1;
            while (mmax < n)
            {
                istep = mmax << 1;
                r1 = r / (float)mmax;
                for (m = 0; m < mmax; m++)
                {
                    theta = r1 * m;
                    w_r = (float)Math.Cos((double)theta);
                    w_i = (float)Math.Sin((double)theta);
                    for (i = m; i < n; i += istep)
                    {
                        j = i + mmax;
                        temp_r = w_r * spectr[j].Real - w_i * spectr[j].Imaginary;
                        temp_i = w_r * spectr[j].Imaginary + w_i * spectr[j].Real;
                        buf = new Complex(spectr[i].Real - temp_r, spectr[i].Imaginary - temp_i);
                        spectr[j] = buf;
                        buf = new Complex(spectr[i].Real + temp_r, spectr[i].Imaginary + temp_i);
                        spectr[i] = buf;
                    }
                }
                mmax = istep;
            }

            if (direct > 0)
            {
                for (i = 0; i < n; i++)
                {
                    buf = new Complex(spectr[i].Real / (float)n, spectr[i].Imaginary / (float)n);
                    spectr[i] = buf;
                }
            }

            return spectr;
        }

        double[,] get_real_noise_arr(double[,] arr, double[,] nois, int N, int M, double percent)
        {
            double d;
            double energy = 0;
            double n_energy = 0;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    energy += arr[i, j] * arr[i, j];
                    n_energy += nois[i, j] * nois[i, j];
                }
            d = energy / n_energy * percent;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                    nois[i, j] *= Math.Sqrt(d);
            return nois;
        }

        public void draw(PictureBox pic_box, double[,] to_draw, int N, int M)
        {
            Color clr = new Color();
            Bitmap bmp = new Bitmap(N, M);
            to_draw = flat(N, M, to_draw);
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    clr = Color.FromArgb(
                        (int)(to_draw[i, j] * 255),
                        (int)(to_draw[i, j] * 255),
                        (int)(to_draw[i, j] * 255));
                    bmp.SetPixel(i, j, clr);
                }
            pic_box.Image = bmp;
        }

        public void draw_special(PictureBox pic_box, double[,] to_draw, int N, int M)
        {
            Color clr = new Color();
            Bitmap bmp = new Bitmap(N, M);
            to_draw = flat(N, M, to_draw);
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    clr = Color.FromArgb(
                        (int)(to_draw[i, j] * 255),
                        (int)(to_draw[i, j] * 255),
                        (int)(to_draw[i, j] * 255));
                    bmp.SetPixel(i, j, clr);
                }
            pic_box.Image = bmp;

            if (filter_percent < 1)
            {
                int r = last_radius;
                Graphics g;
                g = Graphics.FromImage(pic_box.Image);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                Pen pen = new Pen(Color.Red, 2);
                g.DrawEllipse(pen, -r, -r, r * 2, r * 2);
                g.DrawEllipse(pen, N - r, M - r, r * 2, r * 2);
                g.DrawEllipse(pen, N - r, -r, r * 2, r * 2);
                g.DrawEllipse(pen, -r, M - r, r * 2, r * 2);
            }
        }

        double[,] flat(int N, int M, double[,] to_flat)
        {
            double[,] flatten = new double[N, M];
            double max = 0;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    if (to_flat[i, j] > max) max = to_flat[i, j];
                }
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    flatten[i, j] = to_flat[i, j] / max;
                }

            return flatten;
        }

        double[,] convert_for_drawing(Complex[,] arr, int N, int M)
        {
            double[,] to_return = new double[N, M];
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    if (LogMashtabCB.Checked)
                    {
                        if (arr[i, j].Magnitude == 0)
                            to_return[i, j] = 0;
                        else
                            to_return[i, j] = Math.Log10(1 + arr[i, j].Magnitude);
                    }
                    else
                        to_return[i, j] = Math.Sqrt(arr[i, j].Magnitude);
                }
            to_return[0, 0] = 0;
            return to_return;
        }
    }
}

