﻿namespace IT_task_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Picture_1 = new System.Windows.Forms.PictureBox();
            this.Picture_3 = new System.Windows.Forms.PictureBox();
            this.Picture_2 = new System.Windows.Forms.PictureBox();
            this.LoadBT = new System.Windows.Forms.Button();
            this.MTB = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.A_minTB = new System.Windows.Forms.TextBox();
            this.A_maxTB = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.d_minTB = new System.Windows.Forms.TextBox();
            this.d_maxTB = new System.Windows.Forms.TextBox();
            this.InitBT = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SizeHTB = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.SizeWTB = new System.Windows.Forms.TextBox();
            this.pTB = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.FilterBT = new System.Windows.Forms.Button();
            this.FilterPercentTB = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.SKO_TB = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.PictureCB = new System.Windows.Forms.CheckBox();
            this.LogMashtabCB = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Picture_1
            // 
            this.Picture_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture_1.Location = new System.Drawing.Point(12, 12);
            this.Picture_1.Name = "Picture_1";
            this.Picture_1.Size = new System.Drawing.Size(400, 400);
            this.Picture_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Picture_1.TabIndex = 0;
            this.Picture_1.TabStop = false;
            // 
            // Picture_3
            // 
            this.Picture_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture_3.Location = new System.Drawing.Point(824, 12);
            this.Picture_3.Name = "Picture_3";
            this.Picture_3.Size = new System.Drawing.Size(400, 400);
            this.Picture_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Picture_3.TabIndex = 1;
            this.Picture_3.TabStop = false;
            // 
            // Picture_2
            // 
            this.Picture_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture_2.Location = new System.Drawing.Point(418, 12);
            this.Picture_2.Name = "Picture_2";
            this.Picture_2.Size = new System.Drawing.Size(400, 400);
            this.Picture_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Picture_2.TabIndex = 2;
            this.Picture_2.TabStop = false;
            // 
            // LoadBT
            // 
            this.LoadBT.Location = new System.Drawing.Point(242, 19);
            this.LoadBT.Name = "LoadBT";
            this.LoadBT.Size = new System.Drawing.Size(162, 50);
            this.LoadBT.TabIndex = 3;
            this.LoadBT.Text = "Открыть картинку";
            this.LoadBT.UseVisualStyleBackColor = true;
            this.LoadBT.Click += new System.EventHandler(this.LoadBT_Click);
            // 
            // MTB
            // 
            this.MTB.Location = new System.Drawing.Point(80, 19);
            this.MTB.Name = "MTB";
            this.MTB.Size = new System.Drawing.Size(54, 22);
            this.MTB.TabIndex = 4;
            this.MTB.Text = "5";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(11, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(63, 15);
            this.textBox1.TabIndex = 5;
            this.textBox1.Text = "Count";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.A_minTB);
            this.groupBox1.Controls.Add(this.A_maxTB);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.d_minTB);
            this.groupBox1.Controls.Add(this.d_maxTB);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.MTB);
            this.groupBox1.Location = new System.Drawing.Point(12, 418);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 108);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры куполов";
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(140, 75);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(14, 15);
            this.textBox6.TabIndex = 13;
            this.textBox6.Text = "-";
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(11, 75);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(64, 15);
            this.textBox7.TabIndex = 12;
            this.textBox7.Text = "A min-max";
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // A_minTB
            // 
            this.A_minTB.Location = new System.Drawing.Point(80, 75);
            this.A_minTB.Name = "A_minTB";
            this.A_minTB.Size = new System.Drawing.Size(54, 22);
            this.A_minTB.TabIndex = 11;
            this.A_minTB.Text = "1";
            // 
            // A_maxTB
            // 
            this.A_maxTB.Location = new System.Drawing.Point(154, 75);
            this.A_maxTB.Name = "A_maxTB";
            this.A_maxTB.Size = new System.Drawing.Size(54, 22);
            this.A_maxTB.TabIndex = 10;
            this.A_maxTB.Text = "2";
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(140, 47);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(14, 15);
            this.textBox5.TabIndex = 9;
            this.textBox5.Text = "-";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(11, 47);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(64, 15);
            this.textBox4.TabIndex = 8;
            this.textBox4.Text = "d min-max";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // d_minTB
            // 
            this.d_minTB.Location = new System.Drawing.Point(80, 47);
            this.d_minTB.Name = "d_minTB";
            this.d_minTB.Size = new System.Drawing.Size(54, 22);
            this.d_minTB.TabIndex = 7;
            this.d_minTB.Text = "5";
            // 
            // d_maxTB
            // 
            this.d_maxTB.Location = new System.Drawing.Point(154, 47);
            this.d_maxTB.Name = "d_maxTB";
            this.d_maxTB.Size = new System.Drawing.Size(54, 22);
            this.d_maxTB.TabIndex = 6;
            this.d_maxTB.Text = "7";
            // 
            // InitBT
            // 
            this.InitBT.Location = new System.Drawing.Point(653, 437);
            this.InitBT.Name = "InitBT";
            this.InitBT.Size = new System.Drawing.Size(164, 50);
            this.InitBT.TabIndex = 7;
            this.InitBT.Text = "Инициализация";
            this.InitBT.UseVisualStyleBackColor = true;
            this.InitBT.Click += new System.EventHandler(this.InitBT_Click);
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(9, 23);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(45, 15);
            this.textBox2.TabIndex = 15;
            this.textBox2.Text = "Size H";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SizeHTB
            // 
            this.SizeHTB.Location = new System.Drawing.Point(60, 19);
            this.SizeHTB.Name = "SizeHTB";
            this.SizeHTB.Size = new System.Drawing.Size(54, 22);
            this.SizeHTB.TabIndex = 14;
            this.SizeHTB.Text = "220";
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Location = new System.Drawing.Point(9, 51);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(45, 15);
            this.textBox8.TabIndex = 16;
            this.textBox8.Text = "Size W";
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SizeWTB
            // 
            this.SizeWTB.Location = new System.Drawing.Point(60, 47);
            this.SizeWTB.Name = "SizeWTB";
            this.SizeWTB.Size = new System.Drawing.Size(54, 22);
            this.SizeWTB.TabIndex = 17;
            this.SizeWTB.Text = "240";
            // 
            // pTB
            // 
            this.pTB.Location = new System.Drawing.Point(182, 19);
            this.pTB.Name = "pTB";
            this.pTB.Size = new System.Drawing.Size(54, 22);
            this.pTB.TabIndex = 19;
            this.pTB.Text = "5";
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Location = new System.Drawing.Point(128, 23);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(48, 15);
            this.textBox9.TabIndex = 18;
            this.textBox9.Text = "noise %";
            // 
            // FilterBT
            // 
            this.FilterBT.Location = new System.Drawing.Point(824, 437);
            this.FilterBT.Name = "FilterBT";
            this.FilterBT.Size = new System.Drawing.Size(164, 50);
            this.FilterBT.TabIndex = 20;
            this.FilterBT.Text = "Фильтрация";
            this.FilterBT.UseVisualStyleBackColor = true;
            this.FilterBT.Click += new System.EventHandler(this.FilterBT_Click);
            // 
            // FilterPercentTB
            // 
            this.FilterPercentTB.Location = new System.Drawing.Point(182, 47);
            this.FilterPercentTB.Name = "FilterPercentTB";
            this.FilterPercentTB.Size = new System.Drawing.Size(54, 22);
            this.FilterPercentTB.TabIndex = 22;
            this.FilterPercentTB.Text = "99";
            // 
            // textBox10
            // 
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Location = new System.Drawing.Point(131, 51);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(45, 15);
            this.textBox10.TabIndex = 21;
            this.textBox10.Text = "filter %";
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SKO_TB
            // 
            this.SKO_TB.Location = new System.Drawing.Point(60, 75);
            this.SKO_TB.MaxLength = 5;
            this.SKO_TB.Name = "SKO_TB";
            this.SKO_TB.Size = new System.Drawing.Size(116, 22);
            this.SKO_TB.TabIndex = 24;
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Location = new System.Drawing.Point(9, 79);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(45, 15);
            this.textBox11.TabIndex = 23;
            this.textBox11.Text = "СКО";
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PictureCB
            // 
            this.PictureCB.AutoSize = true;
            this.PictureCB.Enabled = false;
            this.PictureCB.Location = new System.Drawing.Point(655, 492);
            this.PictureCB.Name = "PictureCB";
            this.PictureCB.Size = new System.Drawing.Size(93, 21);
            this.PictureCB.TabIndex = 25;
            this.PictureCB.Text = "Картинка";
            this.PictureCB.UseVisualStyleBackColor = true;
            // 
            // LogMashtabCB
            // 
            this.LogMashtabCB.AutoSize = true;
            this.LogMashtabCB.Enabled = false;
            this.LogMashtabCB.Location = new System.Drawing.Point(182, 76);
            this.LogMashtabCB.Name = "LogMashtabCB";
            this.LogMashtabCB.Size = new System.Drawing.Size(213, 21);
            this.LogMashtabCB.TabIndex = 26;
            this.LogMashtabCB.Text = "Логарифмический масштаб";
            this.LogMashtabCB.UseVisualStyleBackColor = true;
            this.LogMashtabCB.CheckedChanged += new System.EventHandler(this.LogMashtabCB_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LogMashtabCB);
            this.groupBox2.Controls.Add(this.SizeHTB);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.SKO_TB);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.LoadBT);
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.SizeWTB);
            this.groupBox2.Controls.Add(this.FilterPercentTB);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.textBox10);
            this.groupBox2.Controls.Add(this.pTB);
            this.groupBox2.Location = new System.Drawing.Point(239, 418);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(410, 108);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры изображения";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 532);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.PictureCB);
            this.Controls.Add(this.FilterBT);
            this.Controls.Add(this.InitBT);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Picture_2);
            this.Controls.Add(this.Picture_3);
            this.Controls.Add(this.Picture_1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Picture_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture_1;
        private System.Windows.Forms.PictureBox Picture_3;
        private System.Windows.Forms.PictureBox Picture_2;
        private System.Windows.Forms.Button LoadBT;
        private System.Windows.Forms.TextBox MTB;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox A_minTB;
        private System.Windows.Forms.TextBox A_maxTB;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox d_minTB;
        private System.Windows.Forms.TextBox d_maxTB;
        private System.Windows.Forms.Button InitBT;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox SizeHTB;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox SizeWTB;
        private System.Windows.Forms.TextBox pTB;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Button FilterBT;
        private System.Windows.Forms.TextBox FilterPercentTB;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox SKO_TB;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.CheckBox PictureCB;
        private System.Windows.Forms.CheckBox LogMashtabCB;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

