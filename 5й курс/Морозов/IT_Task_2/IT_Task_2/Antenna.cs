﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Numerics;

namespace IT_Task_2
{
    class Antenna
    {
        int width, height, size_x, size_y;
        public double Amp, k, lambda, w, r, v;
        public double posX, posY;
        public Point pos;
        Vector pos3D;

        public Antenna(Point p, double lambd, double a, double K, int sz_x, int sz_y, int W, int H)
        {
            k = K;
            lambda = lambd;
            Amp = a;
            pos = p;
            size_x = sz_x;
            size_y = sz_y;
            width = W;
            height = H;
            get_x();
            get_y();
            pos3D = new Vector(posX, posY, 0);
        }

        public void get_intense(double x, double y, double z, out Vector re, out Vector im)
        {
            Vector r = new Vector(x - this.pos3D.x, y - this.pos3D.y, z - this.pos3D.z);
            double R = r.Magnitude();
            Vector n = new Vector(r.x, r.y, r.z) / R;
            re = n * this.Amp * Math.Cos(2 * Math.PI / this.lambda * R - R * this.k) * Math.Exp(-R);
            im = n * this.Amp * Math.Sin(2 * Math.PI / this.lambda * R - R * this.k) * Math.Exp(-R);
        }

        public Vector get_intense(double x, double y, double z)
        {
            Vector r = new Vector(x - this.pos3D.x, y - this.pos3D.y, z - this.pos3D.z);
            double R = r.Magnitude();
            Vector n = new Vector(r.x, r.y, r.z) / R;
            return n * this.Amp * Math.Cos(2 * Math.PI / this.lambda * R - R * this.k) * Math.Exp(-R);
        }


        public void get_x()
        {
            posX = lambda * k * size_x / width * pos.X;
        }

        public void get_y()
        {
            posY = lambda * k * size_y / height * pos.Y;
        }
    }
}
