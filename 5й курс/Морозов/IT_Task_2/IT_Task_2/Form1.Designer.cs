﻿namespace IT_Task_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Picture_1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SizeY_TB = new System.Windows.Forms.TextBox();
            this.K_TB = new System.Windows.Forms.TextBox();
            this.d_TB = new System.Windows.Forms.TextBox();
            this.InitBT = new System.Windows.Forms.Button();
            this.Lambda_TB = new System.Windows.Forms.TextBox();
            this.textbox11 = new System.Windows.Forms.TextBox();
            this.GroupBox_2 = new System.Windows.Forms.GroupBox();
            this.GroupBox_1 = new System.Windows.Forms.GroupBox();
            this.SizeX_TB = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.SizeR_TB = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.ChooseColor = new System.Windows.Forms.ColorDialog();
            this.ChooseColorBT = new System.Windows.Forms.Button();
            this.ClearBT = new System.Windows.Forms.Button();
            this.Picture_2 = new System.Windows.Forms.PictureBox();
            this.CalculateBT = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DrawAccuracyTB = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.AngleZ_Track = new System.Windows.Forms.TrackBar();
            this.AngleY_Track = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_1)).BeginInit();
            this.GroupBox_2.SuspendLayout();
            this.GroupBox_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AngleZ_Track)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AngleY_Track)).BeginInit();
            this.SuspendLayout();
            // 
            // Picture_1
            // 
            this.Picture_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture_1.Enabled = false;
            this.Picture_1.Location = new System.Drawing.Point(12, 12);
            this.Picture_1.Name = "Picture_1";
            this.Picture_1.Size = new System.Drawing.Size(428, 427);
            this.Picture_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Picture_1.TabIndex = 0;
            this.Picture_1.TabStop = false;
            this.Picture_1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Picture_1_MouseClick);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(19, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(135, 15);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Число узлов (верт.)";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SizeY_TB
            // 
            this.SizeY_TB.Location = new System.Drawing.Point(160, 21);
            this.SizeY_TB.Name = "SizeY_TB";
            this.SizeY_TB.Size = new System.Drawing.Size(45, 22);
            this.SizeY_TB.TabIndex = 2;
            this.SizeY_TB.Text = "9";
            // 
            // K_TB
            // 
            this.K_TB.Location = new System.Drawing.Point(160, 48);
            this.K_TB.Name = "K_TB";
            this.K_TB.Size = new System.Drawing.Size(45, 22);
            this.K_TB.TabIndex = 4;
            this.K_TB.Text = "0,5";
            // 
            // d_TB
            // 
            this.d_TB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d_TB.Location = new System.Drawing.Point(14, 51);
            this.d_TB.Name = "d_TB";
            this.d_TB.ReadOnly = true;
            this.d_TB.Size = new System.Drawing.Size(140, 15);
            this.d_TB.TabIndex = 3;
            this.d_TB.Text = "Коэффициент k";
            this.d_TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // InitBT
            // 
            this.InitBT.Location = new System.Drawing.Point(311, 530);
            this.InitBT.Name = "InitBT";
            this.InitBT.Size = new System.Drawing.Size(129, 26);
            this.InitBT.TabIndex = 5;
            this.InitBT.Text = "Инициализация";
            this.InitBT.UseVisualStyleBackColor = true;
            this.InitBT.Click += new System.EventHandler(this.InitBT_Click);
            // 
            // Lambda_TB
            // 
            this.Lambda_TB.Location = new System.Drawing.Point(160, 21);
            this.Lambda_TB.Name = "Lambda_TB";
            this.Lambda_TB.Size = new System.Drawing.Size(45, 22);
            this.Lambda_TB.TabIndex = 7;
            this.Lambda_TB.Text = "0,1";
            // 
            // textbox11
            // 
            this.textbox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textbox11.Location = new System.Drawing.Point(17, 24);
            this.textbox11.Name = "textbox11";
            this.textbox11.ReadOnly = true;
            this.textbox11.Size = new System.Drawing.Size(137, 15);
            this.textbox11.TabIndex = 6;
            this.textbox11.Text = "Длина волны";
            this.textbox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // GroupBox_2
            // 
            this.GroupBox_2.Controls.Add(this.Lambda_TB);
            this.GroupBox_2.Controls.Add(this.d_TB);
            this.GroupBox_2.Controls.Add(this.K_TB);
            this.GroupBox_2.Controls.Add(this.textbox11);
            this.GroupBox_2.Location = new System.Drawing.Point(229, 445);
            this.GroupBox_2.Name = "GroupBox_2";
            this.GroupBox_2.Size = new System.Drawing.Size(211, 79);
            this.GroupBox_2.TabIndex = 8;
            this.GroupBox_2.TabStop = false;
            this.GroupBox_2.Text = "Параметры излучателя";
            // 
            // GroupBox_1
            // 
            this.GroupBox_1.Controls.Add(this.SizeX_TB);
            this.GroupBox_1.Controls.Add(this.textBox3);
            this.GroupBox_1.Controls.Add(this.SizeY_TB);
            this.GroupBox_1.Controls.Add(this.textBox1);
            this.GroupBox_1.Location = new System.Drawing.Point(12, 445);
            this.GroupBox_1.Name = "GroupBox_1";
            this.GroupBox_1.Size = new System.Drawing.Size(211, 79);
            this.GroupBox_1.TabIndex = 9;
            this.GroupBox_1.TabStop = false;
            this.GroupBox_1.Text = "Параметры пространства";
            // 
            // SizeX_TB
            // 
            this.SizeX_TB.Location = new System.Drawing.Point(160, 48);
            this.SizeX_TB.Name = "SizeX_TB";
            this.SizeX_TB.Size = new System.Drawing.Size(45, 22);
            this.SizeX_TB.TabIndex = 6;
            this.SizeX_TB.Text = "8";
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(34, 51);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(120, 15);
            this.textBox3.TabIndex = 5;
            this.textBox3.Text = "Число узлов (гор.)";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SizeR_TB
            // 
            this.SizeR_TB.Location = new System.Drawing.Point(160, 21);
            this.SizeR_TB.Name = "SizeR_TB";
            this.SizeR_TB.Size = new System.Drawing.Size(45, 22);
            this.SizeR_TB.TabIndex = 4;
            this.SizeR_TB.Text = "100";
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(56, 24);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(98, 15);
            this.textBox5.TabIndex = 3;
            this.textBox5.Text = "Расстояние r";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ChooseColorBT
            // 
            this.ChooseColorBT.Location = new System.Drawing.Point(229, 530);
            this.ChooseColorBT.Name = "ChooseColorBT";
            this.ChooseColorBT.Size = new System.Drawing.Size(76, 91);
            this.ChooseColorBT.TabIndex = 10;
            this.ChooseColorBT.Text = "Цвет";
            this.ChooseColorBT.UseVisualStyleBackColor = true;
            this.ChooseColorBT.Click += new System.EventHandler(this.ChooseColorBT_Click);
            // 
            // ClearBT
            // 
            this.ClearBT.Location = new System.Drawing.Point(311, 562);
            this.ClearBT.Name = "ClearBT";
            this.ClearBT.Size = new System.Drawing.Size(129, 26);
            this.ClearBT.TabIndex = 11;
            this.ClearBT.Text = "Очистить";
            this.ClearBT.UseVisualStyleBackColor = true;
            this.ClearBT.Click += new System.EventHandler(this.ClearBT_Click);
            // 
            // Picture_2
            // 
            this.Picture_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture_2.Enabled = false;
            this.Picture_2.Location = new System.Drawing.Point(446, 12);
            this.Picture_2.Name = "Picture_2";
            this.Picture_2.Size = new System.Drawing.Size(563, 555);
            this.Picture_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Picture_2.TabIndex = 12;
            this.Picture_2.TabStop = false;
            // 
            // CalculateBT
            // 
            this.CalculateBT.Location = new System.Drawing.Point(311, 595);
            this.CalculateBT.Name = "CalculateBT";
            this.CalculateBT.Size = new System.Drawing.Size(129, 26);
            this.CalculateBT.TabIndex = 13;
            this.CalculateBT.Text = "Расчет";
            this.CalculateBT.UseVisualStyleBackColor = true;
            this.CalculateBT.Click += new System.EventHandler(this.CalculateBT_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DrawAccuracyTB);
            this.groupBox3.Controls.Add(this.textBox4);
            this.groupBox3.Controls.Add(this.SizeR_TB);
            this.groupBox3.Controls.Add(this.textBox5);
            this.groupBox3.Location = new System.Drawing.Point(12, 530);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(211, 79);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры расчета ДН";
            // 
            // DrawAccuracyTB
            // 
            this.DrawAccuracyTB.Location = new System.Drawing.Point(160, 48);
            this.DrawAccuracyTB.Name = "DrawAccuracyTB";
            this.DrawAccuracyTB.Size = new System.Drawing.Size(45, 22);
            this.DrawAccuracyTB.TabIndex = 6;
            this.DrawAccuracyTB.Text = "50";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(6, 51);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(148, 15);
            this.textBox4.TabIndex = 5;
            this.textBox4.Text = "Точность отрисовки";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // AngleZ_Track
            // 
            this.AngleZ_Track.Location = new System.Drawing.Point(446, 578);
            this.AngleZ_Track.Maximum = 36;
            this.AngleZ_Track.Name = "AngleZ_Track";
            this.AngleZ_Track.Size = new System.Drawing.Size(563, 56);
            this.AngleZ_Track.TabIndex = 15;
            this.AngleZ_Track.Scroll += new System.EventHandler(this.AngleZ_Track_Scroll);
            // 
            // AngleY_Track
            // 
            this.AngleY_Track.Location = new System.Drawing.Point(1015, 12);
            this.AngleY_Track.Maximum = 36;
            this.AngleY_Track.Name = "AngleY_Track";
            this.AngleY_Track.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.AngleY_Track.Size = new System.Drawing.Size(56, 555);
            this.AngleY_Track.TabIndex = 16;
            this.AngleY_Track.Scroll += new System.EventHandler(this.AngleY_Track_Scroll);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 636);
            this.Controls.Add(this.AngleY_Track);
            this.Controls.Add(this.AngleZ_Track);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.CalculateBT);
            this.Controls.Add(this.Picture_2);
            this.Controls.Add(this.ClearBT);
            this.Controls.Add(this.ChooseColorBT);
            this.Controls.Add(this.GroupBox_1);
            this.Controls.Add(this.GroupBox_2);
            this.Controls.Add(this.InitBT);
            this.Controls.Add(this.Picture_1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.Picture_1)).EndInit();
            this.GroupBox_2.ResumeLayout(false);
            this.GroupBox_2.PerformLayout();
            this.GroupBox_1.ResumeLayout(false);
            this.GroupBox_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AngleZ_Track)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AngleY_Track)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture_1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox SizeY_TB;
        private System.Windows.Forms.TextBox K_TB;
        private System.Windows.Forms.TextBox d_TB;
        private System.Windows.Forms.Button InitBT;
        private System.Windows.Forms.TextBox Lambda_TB;
        private System.Windows.Forms.TextBox textbox11;
        private System.Windows.Forms.GroupBox GroupBox_2;
        private System.Windows.Forms.GroupBox GroupBox_1;
        private System.Windows.Forms.TextBox SizeR_TB;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.ColorDialog ChooseColor;
        private System.Windows.Forms.Button ChooseColorBT;
        private System.Windows.Forms.Button ClearBT;
        private System.Windows.Forms.PictureBox Picture_2;
        private System.Windows.Forms.Button CalculateBT;
        private System.Windows.Forms.TextBox SizeX_TB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox DrawAccuracyTB;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TrackBar AngleZ_Track;
        private System.Windows.Forms.TrackBar AngleY_Track;
    }
}

