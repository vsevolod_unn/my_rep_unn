﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;


namespace IT_Task_2
{
    public partial class Form1 : Form
    {
        int size, sizeW, sizeH, RADIUS = 6, scene_sizeW, scene_sizeH;
        int size_x, size_y;
        double lambda, k, d, w, r;
        int DrawAccuracy = 256;
        double[,] intenseM;

        Color clr;
        Bitmap bmp;
        Bitmap bmp2;

        Graphics g;
        Graphics g2;

        Drawer3D drw;
        double angleZ, angleY, mult;

        List<Antenna> antennas = new List<Antenna>();

        public Form1()
        {
            InitializeComponent();
            sizeW = Picture_1.Width;
            sizeH = Picture_1.Height;

            bmp = new Bitmap(sizeW + 0, sizeH + 0);
            g = Graphics.FromImage(bmp);
            g.Clear(Color.White);

            bmp2 = new Bitmap(sizeW + 0, sizeH + 0);
            g2 = Graphics.FromImage(bmp2);
            g2.Clear(Color.White);

            angleZ = 0;
            angleY = 0;

            Picture_1.Image = bmp;
            Picture_2.Image = bmp;

            //this.Controls.Add(this);
            this.KeyPress += new KeyPressEventHandler(Form1_KeyPress);
        }

        private void InitBT_Click(object sender, EventArgs e)
        {
            reInit();
            antennas.Clear();
            clr = Color.Black;

            bmp = new Bitmap(sizeW, sizeH);
            g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            Pen pen = new Pen(Color.Black, 2);
            for (int i = 0; i < size_x + 1; i++)
                g.DrawLine(new Pen(Color.Black, 2), i * (sizeW - 0) / (size_x), 0, i * (sizeW - 0) / (size_x), sizeH);
            for (int j = 0; j < size_y + 1; j++)
                g.DrawLine(new Pen(Color.Black, 2), 0, j * (sizeH - 0) / (size_y), sizeW, j * (sizeH - 0) / (size_y));
            Picture_1.Image = bmp;

            Picture_1.Enabled = true;
        }

        private void CalculateBT_Click(object sender, EventArgs e)
        {
            reInit();
            calculate_intense_picture();
        }

        void calculate_intense_picture()
        {
            // Получение диаграммы направленности 
            double cx = (size_x) * lambda * k / 2;
            double cy = (size_y) * lambda * k / 2;
            double teta;
            double fi;
            Vector[,] intense = new Vector[DrawAccuracy, DrawAccuracy];
            for (int i = 0; i < DrawAccuracy; i++)
                for (int j = 0; j < DrawAccuracy; j++)
                    intense[i, j] = new Vector(0, 0, 0);

            // расчет интенсивности 
            Vector re, im;
            Vector total_re = new Vector(0, 0, 0);
            Vector total_im = new Vector(0, 0, 0);
            intenseM = new double[DrawAccuracy, DrawAccuracy];
            for (int i = 0; i < DrawAccuracy; i++)
            {
                for (int j = 0; j < DrawAccuracy; j++)
                {
                    teta = Math.PI * (double)i / (double)(DrawAccuracy * 2.0);
                    fi = Math.PI * (double)j / (double)(DrawAccuracy / 2.0);
                    double x = cx + r * Math.Sin(teta) * Math.Cos(fi);
                    double y = cy + r * Math.Sin(teta) * Math.Sin(fi);
                    double z = r * Math.Cos(teta);
                    for (int l = 0; l < antennas.Count; l++)
                    {
                        antennas[l].get_intense(x, y, z, out re, out im);
                        intense[i, j] += antennas[l].get_intense(x, y, z);
                        total_re += re;
                        total_im += im;
                    }
                    double Re = total_re.Magnitude() * total_re.Magnitude(), Im = total_im.Magnitude() * total_im.Magnitude();
                    intenseM[i, j] = Math.Sqrt(Re - Im);
                }
            }

            for (int i = 0; i < DrawAccuracy; i++)
                for (int j = 0; j < DrawAccuracy; j++)
                    intenseM[i, j] = intense[i, j].Magnitude();

            // фокусы с отрисовкой
            double minX = cx - r;
            double maxX = cx + r;
            double minY = cy - r;
            double maxY = cy + r;
            intenseM = flat(DrawAccuracy, DrawAccuracy, intenseM);

            mult = 1;
            drw = new Drawer3D(Picture_2);
            double[,] arr = new double[DrawAccuracy, DrawAccuracy];
            drw.resize(new double[] { -2, 2, -2, 2, 0, 1 });
            mult = 1;
            drw.Draw(intenseM, angleZ, angleY, mult);
        }

        private void AngleZ_Track_Scroll(object sender, EventArgs e)
        {
            angleZ = AngleZ_Track.Value * 5;
            drw.Draw(intenseM, angleZ, angleY, mult);
        }

        private void AngleY_Track_Scroll(object sender, EventArgs e)
        {
            angleY = AngleY_Track.Value * 5;
            drw.Draw(intenseM, angleZ, angleY, mult);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.NumPad4:
                    angleZ += 1;
                    break;
                case Keys.NumPad6:
                    angleZ -= 1;
                    break;
                case Keys.NumPad8:
                    angleY += 1;
                    break;
                case Keys.NumPad2:
                    angleY -= 1;
                    break;
                default:
                    break;
            }
            drw.Draw(intenseM, angleZ, angleY, mult);
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '4') angleZ -= 10;
            drw.Draw(intenseM, angleZ, angleY, mult);
        }

        void reInit()
        {
            size = Convert.ToInt32(SizeY_TB.Text);
            size_x = Convert.ToInt32(SizeX_TB.Text);
            size_y = Convert.ToInt32(SizeY_TB.Text);
            DrawAccuracy = Convert.ToInt32(DrawAccuracyTB.Text);
            r = Convert.ToInt32(SizeR_TB.Text);
            lambda = Convert.ToDouble(Lambda_TB.Text);
            k = Convert.ToDouble(K_TB.Text);

            if (antennas.Count != 0)
                for (int i = 0; i < antennas.Count; i++)
                {
                    antennas[i].k = k;
                    antennas[i].lambda = lambda;
                    antennas[i].w = w;
                }
        }

        private void CoordinateZ_Track_Scroll(object sender, EventArgs e)
        {
            //reInit();
            AngleY_Track.Maximum = 10 * Convert.ToInt32(SizeR_TB.Text);
            r = AngleY_Track.Value / 10.0;

            bmp2 = new Bitmap(DrawAccuracy, DrawAccuracy);
            g2 = Graphics.FromImage(bmp2);
            g2.Clear(Color.Black);
            g2.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            calculate_intense_picture();
        }

        private void ChooseColorBT_Click(object sender, EventArgs e)
        {
            if (ChooseColor.ShowDialog() == DialogResult.OK)
            {
                clr = ChooseColor.Color;
            }
        }

        private void ClearBT_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < antennas.Count; i++)
            {
                g.FillEllipse(new SolidBrush(Color.White),
                    (int)antennas[i].pos.X - RADIUS - 1,
                    (int)antennas[i].pos.Y - RADIUS - 1,
                    2 * RADIUS + 2,
                    2 * RADIUS + 2);
                Picture_1.Image = bmp;
            }

            antennas.Clear();
            Picture_1.Image = bmp;
        }

        private void Picture_1_MouseClick(object sender, MouseEventArgs e)
        {
            // Общая инициализация
            int x = 0, y = 0, idx_x = 0, idx_y = 0;
            Point location = e.Location;
            for (int i = 0; i < size_x; i++)
                if (location.X > i * sizeW / size_x && location.X < (i + 1) * sizeW / size_x)
                {
                    idx_x = i;
                    x = (int)((double)(2 * i + 1) / 2 * sizeW / size_x);
                    break;
                }
            for (int j = 0; j < size_y; j++)
                if (location.Y > j * sizeH / size_y && location.Y < (j + 1) * sizeH / size_y)
                {
                    idx_y = j;
                    y = (int)((double)(2 * j + 1) / 2 * sizeH / size_y);
                    break;
                }

            // Добавление антенны
            if (e.Button == MouseButtons.Left)
            {
                lambda = Convert.ToDouble(Lambda_TB.Text);
                bool add = true;
                if (x == 0 || y == 0)
                    add = false;
                else
                    foreach (Antenna p in antennas)
                    {
                        if (p.pos == new Point(x, y)) { add = false; break; }
                    }
                if (add)
                {
                    Point p = new Point(x, y);
                    antennas.Add(new Antenna(p, lambda, 1, k, size_x, size_y, sizeW, sizeH));
                    g.FillEllipse(new SolidBrush(clr), x - RADIUS, y - RADIUS, 2 * RADIUS, 2 * RADIUS);
                    Picture_1.Image = bmp;
                }
            }

            // Удаление антенны
            if (e.Button == MouseButtons.Right)
            {
                if (!(x == 0 || y == 0))
                    foreach (Antenna p in antennas)
                    {
                        if (p.pos == new Point(x, y))
                        {
                            antennas.Remove(p);
                            g.FillEllipse(new SolidBrush(Color.White), x - RADIUS - 1, y - RADIUS - 1, 2 * RADIUS + 2, 2 * RADIUS + 2);
                            Picture_1.Image = bmp;
                            break;
                        }
                    }
            }
        }

        public void draw(PictureBox pic_box, double[,] to_draw, int N, int M)
        {
            Color clr = new Color();
            Bitmap bmp = new Bitmap(N, M);
            to_draw = flat(N, M, to_draw);
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    clr = Color.FromArgb(
                        (int)(to_draw[i, j] * 255),
                        (int)(to_draw[i, j] * 255),
                        (int)(to_draw[i, j] * 255));
                    bmp.SetPixel(i, j, clr);
                }
            pic_box.Image = bmp;
        }

        double[,] flat(int N, int M, double[,] to_flat)
        {
            double[,] flatten = new double[N, M];
            double max = 0;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    if (to_flat[i, j] > max) max = to_flat[i, j];
                }
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    flatten[i, j] = to_flat[i, j] / max;
                }

            return flatten;
        }

        double[,] flat_255(int N, int M, double[,] to_flat)
        {
            double[,] flatten = new double[N, M];
            double max = 0;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    if (to_flat[i, j] > max) max = to_flat[i, j];
                }
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    flatten[i, j] = 255 * to_flat[i, j] / max;
                }

            return flatten;
        }
    }
}
