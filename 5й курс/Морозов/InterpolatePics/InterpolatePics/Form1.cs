﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InterpolatePics
{
    public partial class Form1 : Form
    {
        OpenFileDialog OD;

        public Form1()
        {
            InitializeComponent();
            DIM = 1;
            enable = false;
        }

        private void OpenImageBT_Click(object sender, EventArgs e)
        {
            OD = new OpenFileDialog();
            OD.Filter = "Файлы изображений|*.bmp;*.jpg;*.png";

            if (OD.ShowDialog() != DialogResult.OK) return;

            try
            {
                loaded_pic = Image.FromFile(OD.FileName);
            }
            catch
            {
                MessageBox.Show("Error");
                return;
            }

            //loaded_pic = Image.FromFile("C:\\Users\\vsevo\\OneDrive\\Изображения\\ИТ\\Pic2.png");
            InterpolateBT.Enabled = true;
            ResearchBT.Enabled = true;
            Picture1.Image = loaded_pic;
            oldW = loaded_pic.Width;
            oldH = loaded_pic.Height;
        }

        private void InterpolateBT_Click(object sender, EventArgs e)
        {
            reqW = Convert.ToInt32(reqW_Num.Text);
            reqH = Convert.ToInt32(reqH_Num.Text);

            Bitmap pic1bmp = new Bitmap(loaded_pic);
            Interpolate obj;
            obj = new Interpolate(pic1bmp, DIM, reqW, reqH);
            obj.InterpolateImage();
            Picture2.Image = obj.new_pic;
        }

        private void ResearchBT_Click(object sender, EventArgs e)
        {
            mult_arr = new double[exp_count];
            Random rand = new Random();
            mult = 1;
            for (int i = 1; i < exp_count; i++)
            {
                mult_arr[i] = (double)((int)(10 * (0.4 + 1.2 * rand.NextDouble()))) / 10.0;
                mult /= mult_arr[i];
            }
            if (mult > 1)
                mult_arr[0] = mult * 1.1;
            else
                mult_arr[0] = 1.5;

            /*for (int i = 0; i < mult_arr.Length; i++)
            {
                mult /= mult_arr[i];
            }*/

            Progress.Minimum = 0;
            Progress.Maximum = 2 * exp_count - 1;
            Progress.Value = 0;

            DIM = 1;
            counter = 0;
            LinearRB.Checked = true;
            ProgressTB.Text = 0.ToString() + "%";
            MainThread.Start();
        }

        private void MainThread_Tick(object sender, EventArgs e)
        {
            int dim;
            int exp_number;
            Interpolate obj;

            if (counter < exp_count * 2)
            {
                dim = (int)(counter / exp_count) + 1;
                exp_number = counter % exp_count;

                if (counter % exp_count == 0)
                    pic1bmp = new Bitmap(loaded_pic);

                obj = new Interpolate(pic1bmp, dim, (int)(oldW * mult_arr[exp_number]), (int)(oldH * mult_arr[exp_number]));
                obj.InterpolateImage();
                pic1bmp = obj.new_pic;
                oldW = pic1bmp.Width;
                oldH = pic1bmp.Height;

                if (exp_number == exp_count - 1)
                {
                    obj = new Interpolate(pic1bmp, dim, loaded_pic.Width, loaded_pic.Height);
                    obj.InterpolateImage();
                    pic1bmp = obj.new_pic;
                    oldW = pic1bmp.Width;
                    oldH = pic1bmp.Height;
                    Bitmap pic0bmp = new Bitmap(loaded_pic);

                    double sko = 0, energy = 0;
                    for (int i = 0; i < oldW; i++)
                    {
                        for (int j = 0; j < oldH; j++)
                        {
                            energy += Math.Pow(pic0bmp.GetPixel(i, j).R, 2) +
                                      Math.Pow(pic0bmp.GetPixel(i, j).G, 2) +
                                      Math.Pow(pic0bmp.GetPixel(i, j).B, 2);
                        }
                    }
                    for (int i = 0; i < oldW; i++)
                    {
                        for (int j = 0; j < oldH; j++)
                        {
                            sko += 1.0 / 3.0 / energy / (double)(oldW * oldH) * (
                                        Math.Pow(pic0bmp.GetPixel(i, j).R - pic1bmp.GetPixel(i, j).R, 2) +
                                        Math.Pow(pic0bmp.GetPixel(i, j).G - pic1bmp.GetPixel(i, j).G, 2) +
                                        Math.Pow(pic0bmp.GetPixel(i, j).B - pic1bmp.GetPixel(i, j).B, 2));
                        }
                    }
                    if (dim == 1)
                    {
                        SKO1TB.Text = sko.ToString();
                        ParabolicRB.Checked = true;
                        DIM++;
                        ResearchPic1 = pic1bmp;
                    }
                    if (dim == 2)
                    {
                        SKO2TB.Text = sko.ToString();
                        ResearchPic2 = pic1bmp;
                    }

                    Picture2.Image = pic1bmp;
                }
                counter++;
                if (counter < exp_count * 2)
                {
                    Progress.Value = counter;
                    ProgressTB.Text = ((int)(100.0 * (double)counter / (double)(exp_count * 2))).ToString() + "%";
                }
            }
            else
            {
                ProgressTB.Text = 100.ToString() + "%";
                enable = true;
                MainThread.Stop();
            }
        }

        private void LinearRB_CheckedChanged(object sender, EventArgs e)
        {
            if (LinearRB.Checked) DIM = 1;
            else DIM = 2;

            if (enable)
            {
                if (DIM == 1)
                {
                    Picture2.Image = ResearchPic1;
                }
                if (DIM == 2)
                {
                    Picture2.Image = ResearchPic2;
                }
            }
        }

        private void ParabolicRB_CheckedChanged(object sender, EventArgs e)
        {
            if (LinearRB.Checked) DIM = 1;
            else DIM = 2;

            if (enable)
            {
                if (DIM == 1)
                {
                    Picture2.Image = ResearchPic1;
                }
                if (DIM == 2)
                {
                    Picture2.Image = ResearchPic2;
                }
            }
        }

        double mult;
        double[] mult_arr;
        int reqW, reqH;
        int oldW, oldH;
        int exp_count = 15;
        int counter;
        bool enable;

        Image loaded_pic;
        Image ResearchPic1, ResearchPic2;

        Bitmap pic1bmp;
        int DIM;
    }
}
