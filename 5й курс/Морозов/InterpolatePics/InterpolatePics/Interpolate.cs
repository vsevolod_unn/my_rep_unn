﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace InterpolatePics
{
    class Interpolate
    {
        int old_w, old_h, new_w, new_h;

        public Bitmap old_pic, new_pic;
        public double[][,] old_pic_arr;
        public double[][,] new_pic_arr;
        int DIM;
        public Interpolate(Bitmap pic, int dim, int w, int h)
        {
            this.old_pic = pic;
            this.old_w = pic.Width;
            this.old_h = pic.Height;
            this.DIM = dim;

            this.new_w = w;
            this.new_h = h;
            this.new_pic = new Bitmap(new_w, new_h);

            this.old_pic_arr = new double[3][,];
            this.new_pic_arr = new double[3][,];
            for (int color = 0; color < 3; color++)
            {
                old_pic_arr[color] = new double[old_w, old_h];
                new_pic_arr[color] = new double[new_w, new_h];
            }

            for (int i = 0; i < old_w; i++)
                for (int j = 0; j < old_h; j++)
                {
                    this.old_pic_arr[0][i, j] = pic.GetPixel(i, j).R;
                    this.old_pic_arr[1][i, j] = pic.GetPixel(i, j).G;
                    this.old_pic_arr[2][i, j] = pic.GetPixel(i, j).B;
                }
        }

        public void InterpolateImage()
        {
            for (int color = 0; color < 3; color++)
            {
                new_pic_arr[color] = ExpandPic(old_pic_arr[color], new_h, new_w, DIM);
            }

            for (int i = 0; i < new_w; i++)
                for (int j = 0; j < new_h; j++)
                {
                    new_pic.SetPixel(i, j, Color.FromArgb((int)new_pic_arr[0][i, j], (int)new_pic_arr[1][i, j], (int)new_pic_arr[2][i, j]));
                }
        }

        /// <summary>
        /// Растягивает матрицу до указанных значений ширины/высоты.
        /// </summary>
        /// <param name="src"> Исходная матрица </param>
        /// <param name="reqH"> Необходимая ширина </param>
        /// <param name="reqW"> Необходимая высота </param>
        /// <returns> Растянутая до указанных значений матрица </returns>
        private double[,] ExpandPic(double[,] src, int reqH, int reqW, int mode)
        {
            double[,] result = new double[reqW, reqH];
            double[,] temp_res = new double[src.GetLength(0), reqH];

            // Идем по строкам
            double[] bufferBefore = new double[src.GetLength(1)];
            double[] bufferAfter = new double[reqH];

            for (int i = 0; i < src.GetLength(0); i++)
            {
                for (int j = 0; j < src.GetLength(1); j++)
                {
                    bufferBefore[j] = src[i, j];
                }

                bufferAfter = ExpandArr(bufferBefore, reqH, mode);

                for (int j = 0; j < reqH; j++)
                {
                    temp_res[i, j] = bufferAfter[j];
                }
            }

            // Идем по столбцам
            bufferBefore = new double[src.GetLength(0)];
            bufferAfter = new double[reqW];

            for (int i = 0; i < reqH; i++)
            {
                for (int j = 0; j < src.GetLength(0); j++)
                {
                    bufferBefore[j] = temp_res[j, i];
                }

                bufferAfter = ExpandArr(bufferBefore, reqW, mode);

                for (int j = 0; j < reqW; j++)
                {
                    result[j, i] = bufferAfter[j];
                }
            }
            return result;
        }

        /// <summary>
        /// Растягивает массив данных
        /// </summary>
        /// <param name="src"> Исходный массив </param>
        /// <param name="requiredLength"> Требуемая длина нового массива </param>
        /// <returns> Интерполированный массив </returns>
        private double[] ExpandArr(double[] src, int requiredLength, int mode)
        {
            double[] destination = new double[requiredLength];

            if (mode == 1)
            {
                destination[0] = src[0];
                destination[requiredLength - 1] = src[src.Length - 1];

                for (int i = 1; i < requiredLength - 1; i++)
                {
                    double jd = ((double)i * (double)(src.Length - 1) / (double)(requiredLength - 1));
                    int j = (int)jd;
                    destination[i] = src[j] + (src[j + 1] - src[j]) * (jd - (double)j);
                }
            }
            if (mode == 2)
            {
                //destination[0] = src[0];
                //destination[1] = src[1];
                destination[requiredLength - 2] = src[src.Length - 2];
                destination[requiredLength - 1] = src[src.Length - 1];

                for (int i = 0; i < requiredLength - 1; i++)
                {
                    double jd = ((double)i * (double)(src.Length - 1) / (double)(requiredLength - 1));
                    int j = (int)jd;
                    if (j > 0 && j < src.Length - 1)
                        destination[i] = src[j] + 0.5 * (src[j + 1] - src[j]) * (jd - (double)j) +
                            0.25 * (src[j + 1] - 2 * src[j] + src[j - 1]) * (jd - (double)j) * (jd - (double)j);
                    if (j == 0)
                        destination[i] = src[j] + 0.5 * (src[j + 1] - src[j]) * (jd - (double)j) +
                            0.25 * (src[j + 1] - 2 * src[j]) * (jd - (double)j) * (jd - (double)j);
                    if (j == src.Length - 1)
                        destination[i] = src[j] + 0.5 * (src[j + 1] - src[j]) * (jd - (double)j) +
                            0.25 * (0 - 2 * src[j] + src[j - 1]) * (jd - (double)j) * (jd - (double)j);

                    if (destination[i] < 0) destination[i] = 0;
                    if (destination[i] > 255) destination[i] = 255;
                }
            }
            return destination;
        }
    }
}
