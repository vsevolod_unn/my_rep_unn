﻿namespace InterpolatePics
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Picture1 = new System.Windows.Forms.PictureBox();
            this.Picture2 = new System.Windows.Forms.PictureBox();
            this.InterpolateBT = new System.Windows.Forms.Button();
            this.OpenImageBT = new System.Windows.Forms.Button();
            this.LinearRB = new System.Windows.Forms.RadioButton();
            this.ParabolicRB = new System.Windows.Forms.RadioButton();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.reqW_Num = new System.Windows.Forms.NumericUpDown();
            this.reqH_Num = new System.Windows.Forms.NumericUpDown();
            this.ResearchBT = new System.Windows.Forms.Button();
            this.SKO1TB = new System.Windows.Forms.TextBox();
            this.SKO2TB = new System.Windows.Forms.TextBox();
            this.MainThread = new System.Windows.Forms.Timer(this.components);
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.ProgressTB = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reqW_Num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reqH_Num)).BeginInit();
            this.SuspendLayout();
            // 
            // Picture1
            // 
            this.Picture1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture1.Location = new System.Drawing.Point(12, 12);
            this.Picture1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new System.Drawing.Size(666, 615);
            this.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Picture1.TabIndex = 0;
            this.Picture1.TabStop = false;
            // 
            // Picture2
            // 
            this.Picture2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture2.Location = new System.Drawing.Point(684, 12);
            this.Picture2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Picture2.Name = "Picture2";
            this.Picture2.Size = new System.Drawing.Size(666, 615);
            this.Picture2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Picture2.TabIndex = 1;
            this.Picture2.TabStop = false;
            // 
            // InterpolateBT
            // 
            this.InterpolateBT.Enabled = false;
            this.InterpolateBT.Location = new System.Drawing.Point(938, 634);
            this.InterpolateBT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.InterpolateBT.Name = "InterpolateBT";
            this.InterpolateBT.Size = new System.Drawing.Size(203, 27);
            this.InterpolateBT.TabIndex = 2;
            this.InterpolateBT.Text = "Интерполировать";
            this.InterpolateBT.UseVisualStyleBackColor = true;
            this.InterpolateBT.Click += new System.EventHandler(this.InterpolateBT_Click);
            // 
            // OpenImageBT
            // 
            this.OpenImageBT.Location = new System.Drawing.Point(729, 634);
            this.OpenImageBT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OpenImageBT.Name = "OpenImageBT";
            this.OpenImageBT.Size = new System.Drawing.Size(203, 27);
            this.OpenImageBT.TabIndex = 3;
            this.OpenImageBT.Text = "Открыть изображение";
            this.OpenImageBT.UseVisualStyleBackColor = true;
            this.OpenImageBT.Click += new System.EventHandler(this.OpenImageBT_Click);
            // 
            // LinearRB
            // 
            this.LinearRB.AutoSize = true;
            this.LinearRB.Checked = true;
            this.LinearRB.Location = new System.Drawing.Point(227, 640);
            this.LinearRB.Name = "LinearRB";
            this.LinearRB.Size = new System.Drawing.Size(124, 21);
            this.LinearRB.TabIndex = 4;
            this.LinearRB.TabStop = true;
            this.LinearRB.Text = "Квадратичная";
            this.LinearRB.UseVisualStyleBackColor = true;
            this.LinearRB.CheckedChanged += new System.EventHandler(this.LinearRB_CheckedChanged);
            // 
            // ParabolicRB
            // 
            this.ParabolicRB.AutoSize = true;
            this.ParabolicRB.Location = new System.Drawing.Point(227, 666);
            this.ParabolicRB.Name = "ParabolicRB";
            this.ParabolicRB.Size = new System.Drawing.Size(95, 21);
            this.ParabolicRB.TabIndex = 5;
            this.ParabolicRB.Text = "Линейная";
            this.ParabolicRB.UseVisualStyleBackColor = true;
            this.ParabolicRB.CheckedChanged += new System.EventHandler(this.ParabolicRB_CheckedChanged);
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(11, 642);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(136, 15);
            this.textBox3.TabIndex = 8;
            this.textBox3.Text = "Требуемая ширина:";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Location = new System.Drawing.Point(11, 668);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(136, 15);
            this.textBox4.TabIndex = 9;
            this.textBox4.Text = "Требуемая высота:";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // reqW_Num
            // 
            this.reqW_Num.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.reqW_Num.Location = new System.Drawing.Point(154, 640);
            this.reqW_Num.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.reqW_Num.Name = "reqW_Num";
            this.reqW_Num.Size = new System.Drawing.Size(67, 22);
            this.reqW_Num.TabIndex = 10;
            this.reqW_Num.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // reqH_Num
            // 
            this.reqH_Num.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.reqH_Num.Location = new System.Drawing.Point(154, 666);
            this.reqH_Num.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.reqH_Num.Name = "reqH_Num";
            this.reqH_Num.Size = new System.Drawing.Size(67, 22);
            this.reqH_Num.TabIndex = 11;
            this.reqH_Num.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // ResearchBT
            // 
            this.ResearchBT.Enabled = false;
            this.ResearchBT.Location = new System.Drawing.Point(1147, 634);
            this.ResearchBT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ResearchBT.Name = "ResearchBT";
            this.ResearchBT.Size = new System.Drawing.Size(203, 27);
            this.ResearchBT.TabIndex = 12;
            this.ResearchBT.Text = "Исследование";
            this.ResearchBT.UseVisualStyleBackColor = true;
            this.ResearchBT.Click += new System.EventHandler(this.ResearchBT_Click);
            // 
            // SKO1TB
            // 
            this.SKO1TB.Location = new System.Drawing.Point(1210, 666);
            this.SKO1TB.Name = "SKO1TB";
            this.SKO1TB.ReadOnly = true;
            this.SKO1TB.Size = new System.Drawing.Size(67, 22);
            this.SKO1TB.TabIndex = 13;
            // 
            // SKO2TB
            // 
            this.SKO2TB.Location = new System.Drawing.Point(1283, 666);
            this.SKO2TB.Name = "SKO2TB";
            this.SKO2TB.ReadOnly = true;
            this.SKO2TB.Size = new System.Drawing.Size(67, 22);
            this.SKO2TB.TabIndex = 14;
            // 
            // MainThread
            // 
            this.MainThread.Tick += new System.EventHandler(this.MainThread_Tick);
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(729, 666);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(412, 22);
            this.Progress.TabIndex = 15;
            // 
            // ProgressTB
            // 
            this.ProgressTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ProgressTB.Location = new System.Drawing.Point(907, 668);
            this.ProgressTB.Name = "ProgressTB";
            this.ProgressTB.ReadOnly = true;
            this.ProgressTB.Size = new System.Drawing.Size(67, 15);
            this.ProgressTB.TabIndex = 16;
            this.ProgressTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(1147, 669);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(61, 15);
            this.textBox1.TabIndex = 17;
            this.textBox1.Text = "СКО:";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1365, 695);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.ProgressTB);
            this.Controls.Add(this.Progress);
            this.Controls.Add(this.SKO2TB);
            this.Controls.Add(this.SKO1TB);
            this.Controls.Add(this.ResearchBT);
            this.Controls.Add(this.reqH_Num);
            this.Controls.Add(this.reqW_Num);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.ParabolicRB);
            this.Controls.Add(this.LinearRB);
            this.Controls.Add(this.OpenImageBT);
            this.Controls.Add(this.InterpolateBT);
            this.Controls.Add(this.Picture2);
            this.Controls.Add(this.Picture1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Исследование эффективности линейной и квадратичной интерполяции";
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reqW_Num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reqH_Num)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture1;
        private System.Windows.Forms.PictureBox Picture2;
        private System.Windows.Forms.Button InterpolateBT;
        private System.Windows.Forms.Button OpenImageBT;
        private System.Windows.Forms.RadioButton LinearRB;
        private System.Windows.Forms.RadioButton ParabolicRB;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.NumericUpDown reqW_Num;
        private System.Windows.Forms.NumericUpDown reqH_Num;
        private System.Windows.Forms.Button ResearchBT;
        private System.Windows.Forms.TextBox SKO1TB;
        private System.Windows.Forms.TextBox SKO2TB;
        private System.Windows.Forms.Timer MainThread;
        private System.Windows.Forms.ProgressBar Progress;
        private System.Windows.Forms.TextBox ProgressTB;
        private System.Windows.Forms.TextBox textBox1;
    }
}

