﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IT_practice
{
    public partial class Form1 : Form
    {
        OpenFileDialog OD;

        public Form1()
        {
            InitializeComponent();
            DIM = 1;
        }

        private void InterpolateBT_Click(object sender, EventArgs e)
        {
            Bitmap pic1bmp = new Bitmap(loaded_pic);
            //Interpolate obj = new Interpolate(pic1bmp, DIM, 600, 600);
            //obj.InterpolateImage();
            //Picture2.Image = obj.new_pic;
        }

        Image loaded_pic;
        int DIM;

        private void button1_Click(object sender, EventArgs e)
        {
            OD = new OpenFileDialog();
            //OD.Filter = "Файлы изображений|*.bmp;*.jpg;*.png";

            if (OD.ShowDialog() != DialogResult.OK) return;

            try
            {
                loaded_pic = Image.FromFile(OD.FileName);
            }
            catch
            {
                MessageBox.Show("Error");
                return;
            }

            //loaded_pic = Image.FromFile("C:\\Users\\vsevo\\OneDrive\\Изображения\\ИТ\\Лена.png");
            InterpolateBT.Enabled = true;
            Picture1.Image = loaded_pic;
        }
    }
}
