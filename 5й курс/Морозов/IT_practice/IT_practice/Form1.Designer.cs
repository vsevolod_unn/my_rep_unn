﻿namespace IT_practice
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Picture1 = new System.Windows.Forms.PictureBox();
            this.Picture2 = new System.Windows.Forms.PictureBox();
            this.InterpolateBT = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).BeginInit();
            this.SuspendLayout();
            // 
            // Picture1
            // 
            this.Picture1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture1.Location = new System.Drawing.Point(12, 12);
            this.Picture1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new System.Drawing.Size(666, 615);
            this.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Picture1.TabIndex = 0;
            this.Picture1.TabStop = false;
            // 
            // Picture2
            // 
            this.Picture2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Picture2.Location = new System.Drawing.Point(684, 12);
            this.Picture2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Picture2.Name = "Picture2";
            this.Picture2.Size = new System.Drawing.Size(666, 615);
            this.Picture2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Picture2.TabIndex = 1;
            this.Picture2.TabStop = false;
            // 
            // InterpolateBT
            // 
            this.InterpolateBT.Enabled = false;
            this.InterpolateBT.Location = new System.Drawing.Point(1148, 665);
            this.InterpolateBT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.InterpolateBT.Name = "InterpolateBT";
            this.InterpolateBT.Size = new System.Drawing.Size(203, 27);
            this.InterpolateBT.TabIndex = 2;
            this.InterpolateBT.Text = "Интерполировать";
            this.InterpolateBT.UseVisualStyleBackColor = true;
            this.InterpolateBT.Click += new System.EventHandler(this.InterpolateBT_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(653, 649);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(191, 27);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1365, 695);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.InterpolateBT);
            this.Controls.Add(this.Picture2);
            this.Controls.Add(this.Picture1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Исследование эффективности линейной и квадратичной интерполяции";
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture1;
        private System.Windows.Forms.PictureBox Picture2;
        private System.Windows.Forms.Button InterpolateBT;
        private System.Windows.Forms.Button button1;
    }
}

