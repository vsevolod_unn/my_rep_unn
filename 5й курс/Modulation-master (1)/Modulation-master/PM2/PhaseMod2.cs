﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Modulator
{
    /// <summary>
    /// Дифференцированный фазовый модулятор.
    ///
    /// Смена фазы происходит при смене бита в битовой последовательности, т.е. контроль фазы не требуется.
    /// </summary>
    public class PhaseMod2
    {
        /// <summary>
        /// Несущая частота сигнала (Гц).
        /// </summary>
        private double _freq;

        /// <summary>
        /// Частота дискретизации (Гц).
        /// </summary>
        private double _sR;

        /// <summary>
        /// Амплитуда сигнала.
        /// </summary>
        private double _amp;

        /// <summary>
        /// Длительность одного слота (сек.).
        /// </summary>
        private const double SlotDur = 0.1;

        /// <summary>
        /// Количество нулевых бит (конфигурационная последовательность).
        /// </summary>
        private readonly int _startVoid;

        /// <summary>
        /// Количество бит, отведенных под меандр (конфигурационная последовательность).
        /// </summary>
        private readonly int _startMeander;

        /// <summary>
        /// Флаг, отвечающий за инвертирование битовой последовательности меандра.
        /// </summary>
        private readonly bool _invertMeander;

        /// <summary>
        /// Количество отсчётов модулированного сигнала.
        /// </summary>
        private int _allCounts;

        /// <summary>
        /// Массив отсчётов модулированного сигнала.
        /// </summary>
        private double[] _signal;

        /// <summary>
        /// Счётчик для автоматизированного геттера.
        /// </summary>
        private int _countsForGet;

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="sR">Частота дискретизации (Гц).</param>
        /// <param name="freq">Несущая частота сигнала (Гц).</param>
        /// <param name="amp">Амплитуда сигнала.</param>
        /// <param name="startMeander">Количество бит, отведенных под меандр. По умолчанию - 90 бит.</param>
        /// <param name="startVoid">Количество нулевых бит. По умолчанию - 10 бит.</param>
        /// <param name="invertMeander">Последовательность бит меандра: [false] (по умолчанию) - "101010...",
        /// [true] - "010101..."</param>
        public PhaseMod2(double sR, double freq, double amp, int startMeander = 90, int startVoid = 10,
            bool invertMeander = false)
        {
            _sR = sR;
            _freq = freq;
            _amp = amp;
            _startMeander = startMeander;
            _startVoid = startVoid;
            _invertMeander = invertMeander;
        }

        /// <summary>
        /// Генерирование конфигурационной последовательности.
        /// </summary>
        /// <returns>Строка, содержащая конфигурационную последовательность.</returns>
        private string InitSequence()
        {
            var strVoid = "";
            for (var i = 0; i < _startVoid; i++)
            {
                strVoid += '0';
            }

            var firstChar = _invertMeander ? '0' : '1';
            var secondChar = _invertMeander ? '1' : '0';

            var strMeander = "";
            for (var i = 0; i < _startMeander; i++)
            {
                strMeander += i % 2 == 0 ? firstChar : secondChar;
            }

            return strVoid + strMeander;
        }

        /// <summary>
        /// Формирование (модулирование) синусоидального сигнала.
        /// </summary>
        /// <param name="bitData">Битовая строка данных.</param>
        /// <param name="numSlots">Количество слотов (? из 10), отведенных под модулированный сигнал.</param>
        /// <remarks>
        /// Для корректной работы количество всех отсчётов (_allCounts) должно делиться на длину битовой строки
        /// с данными нацело.
        /// </remarks>
        public void ModulateSignal(string bitData, int numSlots)
        {
            var strMod = InitSequence() + bitData;
            var listMod = strMod.Select((_, i) => strMod.Substring(i, 1)).ToList();

            _countsForGet = 0;

            if (numSlots < 1) throw new Exception("Количество слотов меньше единицы");
            _allCounts = (int) (numSlots * SlotDur * _sR);
            _signal = new double[_allCounts];
            var countsFor1Bit = _allCounts / listMod.Count;

            var phaseMod = 1;
            var listCount = 0;

            for (var i = 0; i < _allCounts; i++)
            {
                if (i != 0 && i % countsFor1Bit == 0)
                {
                    try
                    {
                        if (listMod[listCount] != listMod[++listCount]) phaseMod *= -1;
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Не выполнено правило деления нацело");
                    }
                }

                _signal[i] = _amp * Math.Sin(2 * Math.PI * phaseMod * _freq / _sR * i);
            }
        }
        
        public double[] GetSignal() => _signal;

        public double GetSignal(int i) => i < _allCounts ? _signal[i] : 0;

        /// <summary>
        /// Полезно при использовании во внешних циклах, где индекс цикла i не начинается с нуля.
        /// </summary>
        public double GetSignalAuto() => _countsForGet < _allCounts ? _signal[_countsForGet++] : 0;
    }
}