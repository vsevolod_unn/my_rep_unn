﻿using OxyPlot;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Interface
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        private int _invalidate;

        public int Invalidate
        {
            get => _invalidate;
            set
            {
                _invalidate = value;
                OnPropertyChanged(nameof(Invalidate));
            }
        }

        public List<DataPoint> Waveform { get; set; }

        public MainViewModel()
        {
            Invalidate = 0;

            Waveform = new List<DataPoint>();
        }
    }
}
