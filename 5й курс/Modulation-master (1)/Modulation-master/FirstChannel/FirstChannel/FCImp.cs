﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstChannelNamespace
{
    public class FirstChannel
    {
        private double T, DFreq, MainFreq, DevianFreq;
        private int ChannelsCount;
        private double[] output;
        private int size;

        FirstChannel() { }

        /// <summary>
        /// Конструктор, который принимает все данные о структуре канала и пакета в целом
        /// </summary>
        /// <param name="_DFreq"> Частота дискретизации, по умолчанию 1 МГц (все частоты передаются в ГЦ) </param>
        /// <param name="_MainFreq"> Несущая частота синусоиды с А = 1, по умолчанию 10 КГц </param>
        /// <param name="_DevianFreq"> Частота на которую меняется частота синусоиды в ходе модуляции, по умолчанию 3 КГц</param>
        /// <param name="_T"> Длительность временного канала, по умолчанию 0,1 с (измеряется в секундах) </param>        
        /// <param name="_ChannelsCount"> Количество каналов в одном пакете, по умолчанию 10 </param>
        public FirstChannel(double _DFreq = 1e6, double _MainFreq = 1e4, double _DevianFreq = 3 * 1e3, double _T = 0.1, int _ChannelsCount = 10)
        {
            DFreq = _DFreq;
            MainFreq = _MainFreq;
            DevianFreq = _DevianFreq;
            T = _T;
            ChannelsCount = _ChannelsCount;
            size = (int)(_DFreq * T);
            output = new double[size];
        }

        /// <summary>
        /// Формирование отчетов синусоиды. Можно вызвать один раз, а потом вызывать только GetTime для каждого следующего пакета (если надо будет отрисовывать)
        /// </summary>
        /// <returns> Возвращает найденные отсчеты output[T * DFreq] синусоиды с выбраной модуляцией </returns>
        public double[] GetSignal()
        {
            var max_min = 0.5 * (1 - 1 / Math.E);
            var avg = 0.5 * (1 + 1 / Math.E);
            double phase = 0;
            double CurrentFreq;
            for (int i = 0; i < size; i++)
            {
                if (phase > Math.PI * 2)
                    phase -= Math.PI * 2;
                output[i] = Math.Sin(phase);
                CurrentFreq = MainFreq + (Gaussian(i) - avg) / max_min * DevianFreq;
                phase += CurrentFreq / DFreq;
            }
            return output;
        }

        /// <summary>
        /// Формирование временной оси для отрисовки очетов (если надо будет)
        /// </summary>
        /// <param name="_iteration"> Номер пакета, по умолчанию пакет 0й </param>
        /// <returns> Возвращает time[T * DFreq] отчетов времени, каждый из которых соответствует своему отчету сигнала </returns>
        public double[] GetTime(int _iteration = 0)
        {
            double[] time = new double[size];
            for (int i = 0; i < size; i++)
            {
                time[i] = _iteration * T * 10 + 1 / DFreq;
            }
            return time;
        }

        private double Gaussian(int i)
        {
            return Math.Exp(-(i / (double)size - 1) * (i / (double)size - 1));
        }
    }
}
