﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Interface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double DFreq = 1920 * 10*5;
            double MainFreq = DFreq / 40;

            double DevianFreq = DFreq / 80;
            double T = 0.1;
            int ChannelsCount = 9;

            double[] signal1 = new FirstChannel(DFreq, MainFreq, DevianFreq, T, ChannelsCount).GetSignal();

            string text = "Привет, Земляне!";
            int[] convertstring = Coder.ConvertStringToBinary(text, false);
            Coder.Encoder(convertstring, out int[] exit1);
            exit1 = Coder.Interleaving(exit1);// после перемежения
            string exit1str = "";
            for (int i = 0; i < exit1.Length; i++)
            {
                exit1str += exit1[i].ToString();
            }
            PhaseMod2 pm = new PhaseMod2(DFreq, MainFreq, 1, 112, 16);
            pm.ModulateSignal(exit1str, 4);
            var signal2 = pm.GetSignal();

            int[] exit2 = BitGenerator.GenSequenceArr(convertstring.Length);
            string exit2str = "";
            for (int i = 0; i < exit2.Length; i++)
            {
                exit2str += exit2[i].ToString();
            }
            pm.ModulateSignal(exit2str, 4);
            var signal3 = pm.GetSignal();

            List<double> Signal = new List<double>();
            Signal.AddRange(signal1);
            Signal.AddRange(signal2);
            Signal.AddRange(signal3);

            using (StreamWriter sw = new StreamWriter("out.wav"))
            {
                for (int i = 0; i < Signal.Count; i++)
                {
                    sw.WriteLine(Signal[i].ToString());
                }
            }

            using (StreamWriter sw_bits = new StreamWriter("bits.txt"))
            {
                sw_bits.WriteLine("Передаваемые биты в 1й, 2й, 3й каналы\n");
                sw_bits.WriteLine("Первый канал:\n\nНичего");

                sw_bits.WriteLine("\n----------------------------------------------------------\n");
                sw_bits.WriteLine("Второй канал:\n");
                for (int k = 0; k < exit1str.Length/16; k++)
                {
                    string str = "";
                    for (int i = 0; i < 16; i++)
                    {
                        str += exit1str[k * 16 + i];
                    }
                    sw_bits.WriteLine(str);
                }

                sw_bits.WriteLine("\n----------------------------------------------------------\n");
                sw_bits.WriteLine("Третий канал:\n");

                for (int k = 0; k < exit2str.Length/16; k++)
                {
                    string str = "";
                    for (int i = 0; i < 16; i++)
                    {
                        str += exit2str[k * 16 + i];
                    }
                    sw_bits.WriteLine(str);
                }

                sw_bits.WriteLine("\n----------------------------------------------------------\n");
                sw_bits.WriteLine("Конец\n");
            }

            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisX.ScrollBar.IsPositionedInside = true;

            chart1.ChartAreas[0].CursorY.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisY.ScrollBar.IsPositionedInside = true;

            chart1.Series[0].Points.Clear();
            for (int i = 0; i < Signal.Count; i++)
            {
                chart1.Series[0].Points.AddXY(i, Signal[i]);
            }
        }
    }
}
