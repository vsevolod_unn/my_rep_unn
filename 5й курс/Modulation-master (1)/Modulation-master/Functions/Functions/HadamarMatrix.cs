﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
    public class HadamarMatrix
    {
        readonly public int[][] M;
        readonly public int size;

        HadamarMatrix() { }
        /// <summary>
        /// </summary>
        /// <param name="size"> Всегда должен быть степенью двойки, больше 1 </param>
        public HadamarMatrix(int _size = 2)
        {
            size = 1;
            M = new int[1][];
            M[0] = new int[1];
            M[0][0] = 1;

            for (int i = 0; i < (int)Math.Log(_size, 2); i++)
            {
                this.M = this.Expand();
                this.size *= 2;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="number"> Номер функции Уолша = номер строки матрицы Адамара</param>
        /// <returns> Возвращает M[number] строку матрицы Адамара </returns>
        public int[] GetWolsh(int number)
        {
            if (number < size)
                return M[number];
            else
                return new int[] { 1 };
        }

        private int[][] Expand()
        {
            int n = size * 2;
            int[][] matr = new int[n][];
            for (int i = 0; i < n; i++)
            {
                matr[i] = new int[n];
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i < size && j < size)
                        matr[i][j] = M[i][j];
                    if (i < size && j >= size)
                        matr[i][j] = M[i][j - size];
                    if (i >= size && j < size)
                        matr[i][j] = M[i - size][j];
                    if (i >= size && j >= size)
                        matr[i][j] = -1 * M[i - size][j - size];
                }
            }
            return matr;
        }
    }
}
