﻿using System;
using System.Collections.Generic;

namespace InterleavingCoding
{
    public static class Coder
    {
        /// <summary>
        /// Сдвиг регистра вправо, с добавлением значения слева.
        /// </summary>
        /// <param name="register">Массив регистра</param>
        /// <param name="value">Добавляемое слева значение</param>
        private static void ShiftRegister(int[] register, int value)
        {
            for (int i = register.Length - 1; i > 0; i--)
            {
                register[i] = register[i - 1];
            }
            register[0] = value;
        }

        #region Функции для конвертации строк в байты

        /// <summary>
        /// Конвертировать строку в массив 0 и 1.
        /// </summary>
        /// <param name="text">Исходная строка</param>
        /// <param name="ascii">Выбранная кодировка, если true, то будет использоваться ASCII, если false - Unicode</param>
        /// <returns></returns>
        public static int[] ConvertStringToBinary(string text, bool ascii = true)
        {
            byte[] strBytes;
            if (!ascii) strBytes = System.Text.Encoding.Unicode.GetBytes(text);
            else strBytes = System.Text.Encoding.ASCII.GetBytes(text);

            string[] binaryCode = new string[strBytes.Length];
            for (int b = 0; b < strBytes.Length; b++)
            {
                binaryCode[b] = Convert.ToString(strBytes[b], 2).PadLeft(8, '0');
            }

            List<int> listBytes = new List<int>();
            for (int i = 0; i < binaryCode.Length; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    listBytes.Add(Convert.ToInt32(binaryCode[i][j]) - 48);
                }
            }

            return listBytes.ToArray();
        }

        /// <summary>
        /// Конвертировать строку в массив 0 и 1, где каждый символ записывается в отдельный массив.
        /// </summary>
        /// <param name="text">Исходная строка</param>
        /// <param name="ascii">Выбранная кодировка, если true, то будет использоваться ASCII, если false - Unicode</param>
        /// <returns></returns>
        public static int[][] ConvertStringToBinArray(string text, bool ascii = true)
        {
            byte[] strBytes;
            if (!ascii) strBytes = System.Text.Encoding.Unicode.GetBytes(text);
            else strBytes = System.Text.Encoding.ASCII.GetBytes(text);

            string[] binaryCode = new string[strBytes.Length];
            for (int b = 0; b < strBytes.Length; b++)
            {
                binaryCode[b] = Convert.ToString(strBytes[b], 2).PadLeft(8, '0');
            }

            int[][] masBinArray = new int[binaryCode.Length][];
            for (int i = 0; i < binaryCode.Length; i++)
            {
                masBinArray[i] = new int[8];
                for (int j = 0; j < 8; j++)
                {
                    masBinArray[i][j] = Convert.ToInt32(binaryCode[i][j]) - 48;
                }
            }

            return masBinArray;
        }

        #endregion

        #region Фукнции кодера и перемежения

        /// <summary>
        /// Кодер.
        /// </summary>
        /// <param name="enter">Входная последовательность 0 и 1</param>
        /// <param name="exit1">Первая выходная последовательность</param>
        /// <param name="exit2">Вторая выходная последовательность</param>
        public static void Encoder(int[] enter, out int[] exit1, out int[] exit2)
        {
            int[] register = new int[7] { 0, 0, 0, 0, 0, 0, 0 };
            int[] sum1 = new int[5] { 0, 1, 2, 3, 6 };
            int[] sum2 = new int[5] { 0, 2, 3, 5, 6 };

            // Length = m*n/k; 7*2/1;
            int lengthExit = register.Length * 2;
            exit1 = new int[lengthExit];
            exit2 = new int[lengthExit];

            for (int i = 0; i < lengthExit; i++)
            {
                int tempSum = 0;
                for (int j = 0; j < sum1.Length; j++)
                {
                    tempSum += register[sum1[j]];
                }
                exit1[i] = (tempSum % 2 == 0) ? 0 : 1;

                tempSum = 0;
                for (int j = 0; j < sum2.Length; j++)
                {
                    tempSum += register[sum2[j]];
                }
                exit2[i] = (tempSum % 2 == 0) ? 0 : 1;

                int value = 0;
                if (i < enter.Length) value = enter[i];
                ShiftRegister(register, value);
            }
        }

        /// <summary>
        /// Кодер.
        /// </summary>
        /// <param name="enter">Входная последовательность 0 и 1</param>
        /// <param name="exit">Выходная последовательность 0 и 1</param>
        public static void Encoder(int[] enter, out int[] exit)
        {
            int[] exit1, exit2;
            Encoder(enter, out exit1, out exit2);
            exit = new int[exit1.Length + exit2.Length];

            for (int i = 0, j = 0; i < exit.Length / 2; i++, j += 2)
            {
                exit[j] = exit1[i];
                exit[j + 1] = exit2[i];
            }
        }

        /// <summary>
        /// Перемежение входной последовательности с матрицей размером (<paramref name="matrixSize"/> x <paramref name="matrixSize"/>); запись по строкам, вычитание по столбцам.
        /// </summary>
        /// <param name="enter">Входная последовательность 0 и 1</param>
        /// <param name="matrixSize">Размер матрицы</param>
        /// <returns></returns>
        public static int[] Interleaving(int[] enter, int matrixSize = 8)
        {
            int[] returnMas = new int[matrixSize * matrixSize];

            int k = 0;
            for (int i = 0; i < matrixSize; i++)
                for (int j = 0; j < matrixSize; j++)
                {
                    int value = 0;
                    if (k < enter.Length) value = enter[k];

                    returnMas[j * matrixSize + i] = value;
                    k++;
                }

            return returnMas;
        }

        #endregion
    }
}
