﻿using System;
using System.Linq;

namespace Generators
{
    /// <summary>
    /// Класс, обеспечивающий генерацию битовых последовательностей.
    /// </summary>
    public static class BitGenerator
    {
        private static readonly Random Rnd = new();
        
        /// <summary>
        /// Сгенерировать бит. последовательность указанной длины.
        /// </summary>
        /// <param name="length">Длина бит. последовательности.</param>
        /// <returns>Строка с бит. последовательностью указанной длины.</returns>
        public static string GenSequenceStr(int length)
        {
            var result = "";
            for (var i = 0; i < length; i++) result += Rnd.Next(2).ToString();
            return result;
        }

        /// <summary>
        /// Сгенерировать бит. последовательность указанной длины.
        /// </summary>
        /// <param name="length">Длина бит. последовательности.</param>
        /// <returns>Целочисленный массив с бит. последовательностью указанной длины.</returns>
        public static int[] GenSequenceArr(int length)
        {
            var arr = new int[length];
            for (var i = 0; i < length; i++) arr[i] = Rnd.Next(2);
            return arr;
        }
        
        /// <summary>
        /// Преобразовать целочисленный массив с битовой последовательностью в строку.
        /// </summary>
        /// <param name="arr">Целочисленный массив с битовой последовательностью.</param>
        /// <returns>Строка с битовой последовательностью.</returns>
        public static string ArrToStr(int[] arr)
        {
            return arr.Aggregate("", (current, d) => current + d);
        }
    }
}